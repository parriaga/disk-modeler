import pyklip.klip
import glob
import pyklip.instruments.GPI as gpi
import pyklip.klip as klip
import scipy.stats as stats
import matplotlib.pyplot as plot
import pyfits
import numpy

def align(filt):
    if filt == 'J':
        filepath = '/home/parriaga/hr4796a_new/april_j/*podc*.fits'
        alignedcube = '/home/parriaga/hr4796a_new/april_j/S20140422J-aligned.fits'
    if filt == 'K':
        filepath = '/home/parriaga/hr4796a_new/april_k/*podc*.fits'
        alignedcube = '/home/parriaga/hr4796a_new/april_k/S20140422K-aligned.fits'
    globlist = glob.glob(filepath)
    globlist = sorted(globlist)
    bigdata = []
    for fil in globlist:
        dataset = gpi.GPIData(fil)
        # shift images
        cent = dataset.centers
        print cent
        dat = klip.rotate(dataset.input[0], 0,dataset.centers[0], new_center = [140, 140], flipx = False)
        dat[0:40] = numpy.nan
        dat[-40:] = numpy.nan
        dat[:, 0:40] = numpy.nan
        dat[:,-40:] = numpy.nan
        sample = dat[44:58,226:236] 
        hatchval = numpy.nanmax(sample)
        dat[numpy.where(dat == hatchval)] = numpy.nan

        dataset.input[0] = dat
        #update centers
        dataset.centers = [[140, 140]]

        newfilename = fil.replace('podc.fits', 'podc_aligned.fits')
        dataset.savedata(newfilename, dataset.input)
        bigdata.append(dataset.input[0])
    bigdata = numpy.array(bigdata)
    pyfits.writeto(alignedcube, bigdata)
    

def makeoned(filt):
    if filt == 'J':
        alignedcube = '/home/parriaga/hr4796a_new/S20140422J-forfm-KLmodes-all.fits'
        newfile = '/home/parriaga/hr4796a_new/S20140422J-forfm-KLmodes-KL2.fits'
    if filt == 'K':
        alignedcube = '/home/parriaga/hr4796a_new/S20140422K-forfm-KLmodes-all.fits'
        newfile = '/home/parriaga/hr4796a_new/S20140422K-forfm-KLmodes-KL2.fits'
    dataset = pyfits.open(alignedcube)
    dataset[1].data = dataset[1].data[0]
    dataset.writeto(newfile)


def maketwod(filt):
    if filt == 'J':
        alignedcube = 'HR4796A-J-Qr.fits'
        newfile = 'HR4796A-J-Qr-two.fits'
    if filt == 'K':
        alignedcube = 'HR4796A-K1-Qr.fits'
        newfile = 'HR4796A-K1-Qr-two.fits'
    dataset = pyfits.open(alignedcube)
    dataset[0].data = numpy.array([dataset[0].data])
    dataset.writeto(newfile)
    
