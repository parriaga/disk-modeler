from diskmodeler import ModelComparator
import diskmodeler
import numpy as n
import matplotlib.pyplot as plot
import matplotlib as mpl
from matplotlib import gridspec



def show_model(model, data, xlims_pix = 63, ylims_pix = 70, vmin_data = -150, vmax_data = 200, vmin_resids = -2, vmax_resids = 2, convolve_resids = True,**plot_kwargs):
    '''
    takes a modelcomparator's diskobject
    '''
    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix
    
    ylims = xlims_pix * pix_au
    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    calibk = 5.7e-8             # Jy/ADU/coadd                                                    
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 
    

    cmap_dict = {'red':   ((0.0, 0.0, 0.0), 
                           (0.756, 0.0, 0.0),
                           (1.0, 1.0, 1.0)),
                 'green': ((0.0, 0.0, 0.0),
                           (0.38, 0.0, 0.0),
                           (1.00, 1.0, 1.0)),
                 'blue':  ((0.00, 0.0, 0.0),
                           (0.737, 1.0, 1.0),
                           (1.00, 1.0, 1.0))
                }
    colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',cmap_dict)
    colormap_idl_bluewhite.set_bad('black')

    label_size = 10.5
    mpl.rcParams['xtick.labelsize'] = label_size
    mpl.rcParams['ytick.labelsize'] = label_size
    fig = plot.figure()
    fig.set_size_inches(9,4.5)

    mim = diskobj.get_model()
    
    gs1 = gridspec.GridSpec(2,2, height_ratios = [11, 1])
    gs1.update(left=.05, right=.60)
    
    mim_tfim_ax = plot.subplot(gs1[0,1])
    mim_tfim_ax.set_xlim([-xlims, xlims])
    mim_tfim_ax.set_ylim([-90,90])
    
    tfim_ax = plot.subplot(gs1[0,0])
    tfim_ax.set_xlim([-xlims, xlims])
    tfim_ax.set_ylim([-90,90])
    
    cbar_mim_tfim_ax = plot.subplot(gs1[1,:]) 
    mim_fig = mim_tfim_ax.imshow(model, vmin = vmin_data, vmax = vmax_data, extent = extent, **plot_kwargs)
    
    tfim_ax.imshow(data, vmin = vmin_data, vmax = vmax_data, extent = extent, **plot_kwargs)
    fig.colorbar(mim_fig, cax = cbar_mim_tfim_ax, orientation = 'horizontal', label = 'Jy/arcsec$^2$')

    gs2 = gridspec.GridSpec(2,1, height_ratios = [11, 1])
    gs2.update(left=.65, right = .98)
    resid_ax = plot.subplot(gs2[0,0])
    resid_ax.set_xlim([-xlims, xlims])
    resid_ax.set_ylim([-90,90])
    cbar_resid_ax = plot.subplot(gs2[1,0])
    
    mim_fig = mim_tfim_ax.imshow(mim, vmin = vmin_data, vmax = vmax_data, extent = extent, **plot_kwargs)
#    tfim_ax.imshow(diskobj.tfim, vmin = vmin_data, vmax = vmax_data, **plot_kwargs)
    fig.colorbar(mim_fig, cax = cbar_mim_tfim_ax, orientation = 'horizontal', label = 'Jy/arcsec$^2$')

    if convolve_resids == False or diskobj.psf_method != 'inputpsf':
        resids = (diskobj.tfim - mim) / diskobj.stdim
    else:
        resids = (diskobj.tfim - mim) / diskobj.stdim
        psf = diskobj.psf_im
        from scipy.signal import fftconvolve
        resids[n.isnan(resids)] = 0
        resids = fftconvolve(resids, psf, mode = 'same')
        
    resids_fig = resid_ax.imshow(resids, vmin = vmin_resids, vmax = vmax_resids, extent = extent,  **plot_kwargs)
    fig.colorbar(resids_fig, cax = cbar_resid_ax, orientation = 'horizontal', label = 'SNR')    
    plot.show()
