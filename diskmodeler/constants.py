# $Id$
#
# Useful constants.
#
# Michael Fitzgerald  (mpfitz@llnl.gov)  2007-10-26
#

import numpy as n

# mathematical constants
pi = n.pi
e = n.e


# physical constants
c = 2.99792458e10     # [cm/s]           speed of light
h = 6.6260755e-27     # [erg/s]          Planck's constant
hbar = 1.05457266e-27 # [erg/s]          Planck's constant
k = 1.380658e-16      # [erg/K]          Boltzmann's constant
G = 6.67259e-8        # [cm^3/g/s^2]     Gravitational constant
sigma = 5.67051e-5    # [erg/cm^2/K^4/s] Stefan-Boltzmann constant
a = 7.5646e-15        # [erg/cm^3/K^4]   radiation density constant
alpha = 7.29735308e-3 #                  fine structure constant
Ryd = 2.1798741e-11   # [erg]            Rydberg constant

mp = 1.6726231e-24    # [g]              mass of proton
mn = 1.6749286e-24    # [g]              mass of neutron
me = 9.1093897e-28    # [g]              mass of electron
mH = 1.6733e-24       # [g]              mass of Hydrogen
amu = 1.6605402e-24   # [g]              atomic mass unit

qe = 4.8032068e-10    # [esu]            charge of electron

N_A = 6.0221367e23    #                  Avagadro's number



# conversion factors
as_per_rad = 648000. / pi # [arcsec/radian] arcseconds per radian
dtor = pi / 180.          # [rad/deg]       degrees to radians
rtod = 180. / pi          # [deg/rad]       radians to degrees
yr = 60.*60.*24.*365.25   # [s/yr]          year  FIXME check val
Jy = 1e-23                # [erg/s/cm^2/Hz] Jansky
eV = 1.602e-12            # [erg]           electron volts
N = 1e5                   # [dyn]           dynes per Newton
J = 1e7                   # [erg]           ergs per Joule


# astronomical constants
AU = 1.496e13      # [cm]    astronomical unit
pc = 3.086e18      # [cm]    parsec
ly = 9.463e17      # [cm]    light-year
Msol = 1.989e33    # [g]     Solar mass
Rsol = 6.96e10     # [cm]    Solar radius
Lsol = 3.827e33    # [erg/s] Solar luminosity

Mjup = 1.899e30    # [g]     Jupiter mass
Mearth = 5.9742e27 # [g]     Earth mass
