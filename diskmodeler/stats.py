import numpy
import numpy.linalg as lin
import matplotlib.pyplot as plot


from string import letters
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plot

sns.set(data,style="white")

# Generate a large random dataset
rs = np.random.RandomState(33)
d = pd.DataFrame(data=rs.normal(size=(100, 26)),
                 columns=list(letters[:26]))

# Compute the correlation matrix
corr = d.corr()

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3,
            square=True, xticklabels=5, yticklabels=5,
            linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)



def covplot(meanarray, covarray, limits = None, nsample = [100., 100.]):
    '''
    Plots a covariance matrix. Limits are the x and y limits and the sampling is the sampling of the grid. Covarray is the array from leastsq times the reduced chi square. 
    '''
    if limits is None:
        limits = numpy.zeros(4)
        limits[0] = meanarray[0] - 3. * covarray[0, 0]
        limits[1] = meanarray[0] = 3. * covarray[0, 0]
        limits[2] = meanarray[1] - 3. * covarray[1, 1]
        limits[3] = meanarray[1] = 3. * covarray[1, 1]
    minicovinv = lin.inv(covarray)
    det = lin.det(covarray)
    prefix = 1. / ((2. * numpy.pi) * numpy.sqrt(det))
    xvec,yvec = numpy.meshgrid(imsize[0], imsize[1])
    pvec = numpy.zeros((nsample[0], nsample[1]))
    for i in numpy.arange(nsample[0]):
        for j in numpy.arange(nsample[1]):
            xpos = i / sampling[0] - limits[0]
            ypos = j / sampling[1] - limits[2]
            xvec = numpy.array([xpos - meanarray[0], ypos - meanarray[1]])
            pvec[i,j] = prefix * numpy.exp( -.5 * numpy.dot(numpy.dot(numpy.transpose(xvec), minicovinv), xvec))
    plot.imshow(pvel, extent = limits)
    plot.show()
    return pvec
