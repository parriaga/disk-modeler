from diskmodeler import ModelComparator
from diskmodeler_plotutils import *


conf = 'hr4796_config_allpol.ini'
mc_save = 'mcmc_all_pol_7.p'
mc = ModelComparator(conf, mc_save = mc_save)
for param in mc.parms:
    if 'int' in param:
        mc.is_fixed[param] = False
    else:
        mc.is_fixed[param] = True
for diskmodel in mc.diskobjs:
    diskmodel.is_fixed = mc.is_fixed

mc.run_mcmc(fname = 'mcmc_allpol_intparms_testx.p')
#loaded_conf = 'hr4796_config_kpol.ini' 
#mc = ModelComparator(conf)
#loaded_mc_fname = 'mcmc_kpol_3.p'
#loaded_mc = ModelComparator(loaded_conf, mc_save = loaded_mc_fname)
#loaded_mc.mcmc_get_best_parms()
#bestparms = loaded_mc.parms
#mc.update_all_parms(bestparms)
#show_model(mc.diskobjs[0])
#show_model(mc.diskobjs[1])#
#show_model(mc.diskobjs[2])

#mc.do_leastsq()
#mc.save_parms('leastsq_allpol.p')
