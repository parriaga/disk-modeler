import matplotlib.pyplot as plot
import pyfits
import pyklip.parallelized as par
import pyklip.fakes as fakes
import pyklip.instruments.GPI as gpi
import pyklip.klip as klip
import numpy
import glob
import matplotlib.pyplot as plot
import astropy.wcs as wcs

def calibrate_throughput(filepattern = 'april_j/*podc*', psf = 'psf_j_short.fits', contrastname = 'contrastk-KLmodes-all.fits'):
    x = glob.glob(filepattern)
    a = gpi.GPIData(x)
    frames = a.input
    centers = a.centers
    psf_im = pyfits.getdata(psf)
    #Mask disk
    inputflux = numpy.zeros(len(centers)) + 1000
#    inputflux = [psf_im * 1000000. for i in range(len(centers))] 
    astr_hdrs = a.wcs
    radius = numpy.array([30., 50., 70.])
    pa = -60.
    for rad in radius:
        fakes.inject_planet(frames, centers, inputflux, astr_hdrs, rad, pa)
        fakes.inject_planet(frames, centers, inputflux, astr_hdrs, -rad, pa)

    a.input = frames
    par.klip_dataset(a, fileprefix='testinj', annuli = 1, subsections = 1, numbasis = [2], mode = 'ADI')
    fits = pyfits.open('testinj-KLmodes-all.fits')
    head = wcs.WCS(fits[1].header)
    im = fits[1].data[0]
    corr = []
    for sep in radius:
        pos = fakes.retrieve_planet_flux([im], [[140,140]], [head], sep, pa, thetas = [30]) 
        neg = fakes.retrieve_planet_flux([im], [[140,140]], [head], -sep, pa, thetas = [30])
        corr.append((pos + neg) / 2.)
    print corr
    sep, contrast = cont(contrastname)
    contrast[numpy.where(sep < 40)] *= corr[0] / 1000.
    contrast[numpy.where((sep > 40) & (sep < 60))] *=  corr[1] / 1000.
    contrast[numpy.where(sep > 60)] *= corr[2] / 1000.
    return sep, contrast


def mask_disk(diskim):
    mask = pyfits.getdata('testring.fits')
    wh = numpy.where(mask > 1)
    im[wh] = numpy.nan
    return im




def cont(contrastname):
    im = pyfits.getdata(contrastname)[0]
    seps, contrast = klip.meas_contrast(im, 25, 90 , 4.)
    return seps,contrast



calibk = 5.7e-8
calibj = 7.3e-8
sepj,contj = calibrate_throughput(filepattern='april_j/*podc*', psf = 'psf_j_short.fits', contrastname = 'contrastj-KLmodes-all.fits')
sepk,contk = calibrate_throughput(filepattern='april_k/*podc*', psf = 'psf_k_short.fits', contrastname = 'contrastk-KLmodes-all.fits')
plot.plot(sepj, contj*1.e6 * calibj, label = 'J band')
plot.plot(sepk, contk*1.e6 * calibk, label = 'K band')
plot.title('5 $\sigma$ detection limit')
plot.ylabel('Upper limit ($\mu$Jy)')
plot.xlabel('Radius (AU)')
plot.legend()
plot.show()


