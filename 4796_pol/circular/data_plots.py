import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
import copy
from pyklip.fmlib.diskfm import DiskFM
_log = logging.getLogger('hr4796a')
import matplotlib.pyplot as plot 
import configparser
from diskmodeler_circular import ModelComparator
from matplotlib import gridspec
import numpy.ma as ma
import plotutils as gpipu


def make_plot_j(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale
    mimj = pyfits.getdata('jpol_model_im.fits') 
    dataimj = pyfits.getdata('jpol_data_im.fits') 
    stdimj = pyfits.getdata('jpol_stdim.fits') 
#    chisq = (dataimj - mimj)**2. / stdimj**2.
#    chisq = chisq.flatten()
#    chisq = n.nansum(chisq)
#    wh = n.where(~n.isnan(dataimk))
#    numpix = len(wh[0]) + 22
    

    allims = [mimj,dataimj, stdimj]

    for im in allims:
        im = ma.masked_invalid(im)

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    cmap_idl = get_colormap_idl()


    plot_kwargs_1 = {'extent': extent,
                     'vmin': -30,
                     'vmax': 80,
                     'cmap': cmap_idl,
                     'origin': 'lower'}
    plot_kwargs_2 = {'extent': extent,
                     'vmin': -3,
                     'vmax': 3,
                     'cmap': 'PRGn',
    'origin': 'lower'}
#                     'cmap': 'seismic'}

 
    gs1 = gridspec.GridSpec(1,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8.5,5)

    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = 0.15, right = .9)
    jband_data_ax = plot.subplot(gs1[0,0])
    gpipu.compass([50, 40], length=15, ax = jband_data_ax, head_width = 4, head_length = 4, labeleast = True)

    jband_model_ax = plot.subplot(gs1[0,1])

    jband_resid_ax = plot.subplot(gs1[0,2])    


    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .15, hspace = 0, left =  .065 )
    resid_cbar = plot.subplot(gs2[0,2])

    jband_data_ax.imshow(dataimj, **plot_kwargs_1)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    jband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    
    jband_model = jband_model_ax.imshow(mimj, **plot_kwargs_1)

    resids = jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_2)


    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Resids', ticks = [-3, -2, -1, 0 , 1, 2, 3])  

    all_ax = (jband_data_ax,jband_model_ax,jband_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (jband_model_ax,jband_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    text_kwargs = {'fontsize': 14,
                   'ha': 'center'}

    jband_data_ax.text(0, 100, 'Data', **text_kwargs)
    jband_model_ax.text(0, 100, 'Model', **text_kwargs)
    jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    
    jband_data_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_model_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
    jband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
    jband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])

    text_kwargs = {'fontsize': 17, 
                   'ha': 'center',
                   'va': 'center'}
    
    jband_model_ax.text(0., 125., 'J-band Model and Data', **text_kwargs)

    plot.show()



def make_plot(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    mimj_1 = pyfits.getdata('jpol_model_im.fits')
    plot.imshow(mimj_1)
    plot.savefig('jpol_model_im1.png')
    mimk = pyfits.getdata('kpol_model_im.fits') 
    mimj = pyfits.getdata('jpol_model_im_5.fits') 
    mimh = pyfits.getdata('hpol_model_im.fits') 
    
    mimj = mimj * n.max(mimj_1) / n.max(mimj)

    plot.clf()
    plot.imshow(mimj)
    plot.savefig('jpol_model_im2.png')
    return


    pyfits.writeto('jpol_model_im_6.fits', mimj)

#    dataimk = pyfits.getdata('kpol_data_im.fits') 
    dataimk = pyfits.getdata('hr4796_k1_qr_moremasked.fits')
    dataimj = pyfits.getdata('jpol_data_im.fits') 
    dataimh = pyfits.getdata('hpol_data_im.fits') 

    stdimk = pyfits.getdata('kpol_stdim.fits') 
    stdimj = pyfits.getdata('jpol_stdim.fits') 
    stdimh = pyfits.getdata('hpol_stdim.fits') 

#    dataimk[140:, :] = n.nan

    chisq = (dataimk - mimk)**2. / stdimk**2.
    chisq = chisq.flatten()
    chisq = n.nansum(chisq)
    wh = n.where(~n.isnan(dataimk))
    numpix = len(wh[0]) + 22
    

    allims = [mimk, mimj, mimh,
              dataimk, dataimj, dataimh,
              stdimk, stdimj, stdimh]

    for im in allims:
        im = ma.masked_invalid(im)


    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 
    cmap_idl = get_colormap_idl()


    plot_kwargs_1 = {'extent': extent,
                     'vmin': -30,
                     'vmax': 80,
                     'cmap': cmap_idl}
    plot_kwargs_2 = {'extent': extent,
                     'vmin': -3,
                     'vmax': 3,
                     'cmap': 'PRGn'}
#                     'cmap': 'seismic'}

 
    gs1 = gridspec.GridSpec(3,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8.5,11)

    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = 0.15, right = .9)
    jband_data_ax = plot.subplot(gs1[0,0])
    gpipu.compass([50, 40], length=15, ax = jband_data_ax, head_width = 4, head_length = 4, labeleast = True)

    hband_data_ax = plot.subplot(gs1[1,0])
    kband_data_ax = plot.subplot(gs1[2,0])

    jband_model_ax = plot.subplot(gs1[0,1])
    hband_model_ax = plot.subplot(gs1[1,1])
    kband_model_ax = plot.subplot(gs1[2,1])

    jband_resid_ax = plot.subplot(gs1[0,2])    
    hband_resid_ax = plot.subplot(gs1[1,2])
    kband_resid_ax = plot.subplot(gs1[2,2])


    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .13, hspace = 0, left =  .065 )
    resid_cbar = plot.subplot(gs2[0,2])

    jband_data_ax.imshow(dataimj, **plot_kwargs_1)
    hband_data_ax.imshow(dataimh, **plot_kwargs_1)
    kband_data_ax.imshow(dataimk, **plot_kwargs_1)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    jband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    hband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    kband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    
    jband_model = jband_model_ax.imshow(mimj, **plot_kwargs_1)
    hband_model_ax.imshow(mimh, **plot_kwargs_1)
    kband_model = kband_model_ax.imshow(mimk, **plot_kwargs_1)

    jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_2)
    hband_resid_ax.imshow((mimh - dataimh) / stdimh, **plot_kwargs_2)
    resids = kband_resid_ax.imshow((mimk - dataimk) / stdimk, **plot_kwargs_2)

    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Resids', ticks = [-3, -2, -1, 0 , 1, 2, 3])  

    all_ax = (kband_data_ax,hband_data_ax,jband_data_ax,
              kband_model_ax,hband_model_ax,jband_model_ax,
              kband_resid_ax,hband_resid_ax,jband_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kband_model_ax,hband_model_ax,jband_model_ax,
              kband_resid_ax,hband_resid_ax,jband_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    hide_x_ax = (jband_model_ax,jband_resid_ax,jband_data_ax,
                 hband_model_ax,hband_resid_ax,hband_data_ax)
    for ax in hide_x_ax:
        ax.xaxis.set_visible(False)


    text_kwargs = {'fontsize': 15,
                   'ha': 'center'}

    jband_data_ax.text(0, 100, 'Data', **text_kwargs)
    jband_model_ax.text(0, 100, 'Model', **text_kwargs)
    jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    
    kband_data_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_model_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])

    text_kwargs = {'fontsize': 17, 
                   'va': 'center',
                   'ha': 'right'}
    
    jband_data_ax.text(-110., 0., 'J', **text_kwargs)
    hband_data_ax.text(-110., 0., 'H', **text_kwargs)
    kband_data_ax.text(-110., 0., 'K1', **text_kwargs)


    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/data_plot_2.png')

    plot.show()
    
    

    
def get_colormap_idl():
    # colors 
    colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',
                                                                {'red':   ((0.0, 0.0, 0.0),
                                                                           (0.756, 0.0, 0.0),
                                                                           (1.0, 1.0, 1.0)),
                                                                 
                                                                 
                                                                 'green': ((0.0, 0.0, 0.0),
                                                                           (0.38, 0.0, 0.0),
                                                                           (1.00, 1.0, 1.0)),
                                                                 
                                                                 'blue':  ((0.00, 0.0, 0.0),
                                                                           (0.737, 1.0, 1.0),
                                                                           (1.00, 1.0, 1.0))
                                                            })
    colormap_idl_bluewhite.set_bad('black')

    return colormap_idl_bluewhite




def make_plot_k(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    mimtot = pyfits.getdata('ktot_model_im.fits')
    mimpol = pyfits.getdata('jtot_model_im.fits')
    
    dataimtot = pyfits.getdata('ktot_data_im.fits')
    dataimpol = pyfits.getdata('kpol_data_im.fits')
    
    stdimtot = pyfits.getdata('ktot_stdim.fits')
    stdimpol = pyfits.getdata('kpol_stdim.fits')

    allims = [mimtot, mimpol, dataimtot, dataimpol, stdimtot, stdimpol]
    for im in allims:
        im = ma.masked_invalid(im)

    cmap_idl = get_colormap_idl()
    
    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix
#    ylims = xlims_pix * pix_au
#    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 

    plot_kwargs_1 = {'extent': extent,
                     'vmin': -10,
                     'vmax': 120,
                     'cmap': cmap_idl}
    plot_kwargs_2 = {'extent': extent,
                     'vmin': -3,
                     'vmax': 3,
                     'cmap': 'seismic'}
 
    gs1 = gridspec.GridSpec(2,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8.5, 7)

#    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = -.9, right = .9)
    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = 0.15, right = .9)
    ktot_data_ax = plot.subplot(gs1[0,0])
    kpol_data_ax = plot.subplot(gs1[1,0])

    ktot_model_ax = plot.subplot(gs1[0,1])
    kpol_model_ax = plot.subplot(gs1[1,1])

    ktot_resid_ax = plot.subplot(gs1[0,2])    
    kpol_resid_ax = plot.subplot(gs1[1,2])


    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .13, hspace = 0)
    resid_cbar = plot.subplot(gs2[0,2])

    ktot_data_ax.imshow(dataimtot, **plot_kwargs_1)
    kpol_data_ax.imshow(dataimpol, **plot_kwargs_1)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    ktot_data_ax.text(-95, 0, 'AU', **text_kwargs)
    kpol_data_ax.text(-95, 0, 'AU', **text_kwargs)

    ktot_model = ktot_model_ax.imshow(mimtot, **plot_kwargs_1)
    kpol_model = kband_model_ax.imshow(mimpol, **plot_kwargs_1)

    jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_2)
    resids = kband_resid_ax.imshow((mimk - dataimk) / stdimk, **plot_kwargs_2)

    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Resids', ticks = [-2, -1, 0, 1, 2])  

    all_ax = (kband_data_ax,jband_data_ax,
              kband_model_ax,jband_model_ax,
              kband_resid_ax,jband_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kband_model_ax,jband_model_ax,
                 kband_resid_ax,jband_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    hide_x_ax = (jband_model_ax,jband_resid_ax,jband_data_ax)
    for ax in hide_x_ax:
        ax.xaxis.set_visible(False)


    text_kwargs = {'fontsize': 15,
                   'ha': 'center'}
    #-25
    jband_data_ax.text(0, 100, 'Data', **text_kwargs)
    jband_model_ax.text(0, 100, 'Model', **text_kwargs)
    jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    

    kband_data_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_model_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
#    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])


    text_kwargs = {'fontsize': 17, 
                   'va': 'center',
                   'ha': 'right'}
    
    jband_data_ax.text(-110., 0., 'J', **text_kwargs)
    hband_data_ax.text(-110., 0., 'H', **text_kwargs)

    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/tot_data_plot.png')
    plot.show()
