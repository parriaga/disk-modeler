
def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.

    return y

