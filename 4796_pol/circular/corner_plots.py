import numpy as n
import constants as c
import corner
from diskmodeler_circular import ModelComparator
import matplotlib.pyplot as plot
from astropy.stats import sigma_clip




def plot_walker(chain, ind, param, nwalkers, is_1d = False):
    if is_1d is False:
        param_set = chain[:, :, ind]
    else:
        param_set = chain
    for walker in range(nwalkers):
        line = param_set[walker, :]
        plot.plot(line, c = 'blue')
    plot.xlabel('Walker')
    plot.ylabel(param)
    plot.show()




def get_med_and_std(patch, return_frame = False):
    patch = patch.flatten()

    std = n.std(patch)
    med = n.median(patch)
    wh = n.where(n.abs(patch - med) > 3. * std)
    patch = n.delete(patch, wh)
    if return_frame is False:
        return n.median(patch), n.std(patch)
    else:
        return n.median(patch), n.std(patch), patch


def plot_k():
    mc = ModelComparator('hr4796_config_hpol.ini',mc_save = 'mcmc_hpol_method3_2.p')
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
    mc.mcmc_get_best_parms(set_best = True, fit_parmlist = fit_parmlist)
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)

    nburn = 200
    chain = mc.chain
    lnprobability = mc.lnprobability
    lnprobability = lnprobability[:, :]

    #kband masked has some weird shit
#    wh = n.where(lnprobability[:, -1:] < -11000)
#    lnprobability = n.delete(lnprobability, 31, 0)
#    lnprobability = n.delete(lnprobability, 72, 0)

#    chain = n.delete(chain, 31, 0)
#    chain = n.delete(chain, 72, 0)


    sh = n.shape(chain)
    nwalkers = sh[0]
    fit_parmlist = mc.fit_parmlist
    int_sets = []
    sample_sets = []
    chain = chain[:, nburn:, :]
    labels = []
    osino = []
    ocoso = []
        
    plate_scale = 0.01414 #arcseconds / pixel
    plate_scale_err = 1.e-5

    north_angle = -1.
    north_angle_err = 0.03
    
    star_distance = 72.8 # parsec 
    star_distance_err = 1.7 #parsec
    au_per_pix = plate_scale * star_distance

    param_counter = 0
    print('\n')


    for param in fit_parmlist:
        if param == 'osino' or param == 'ocoso':
            parm_len = 1
        else:
            parm_len = parm_lens[param]
        if parm_len == 1:
            if param == 'osino':
                osino = n.array(chain[:, :, param_counter].flatten())
            elif param == 'ocoso':
                ocoso = n.array(chain[:, :, param_counter].flatten())
            else:
                # parameters that need a transformation
                if param == 'I':
                    chain[:, :, param_counter] *= 180 / n.pi 
                if param == 'omega1': 
                    chain[:, :, param_counter] *= 180 / n.pi - north_angle
                if param == 'r':
                    # Let's fix all ths later
#                     r_au = med
#                     std_au = std
                    
#                     r_pix = med / au_per_pix
#                     std_pix = std_au / au_per_pix

#                     std_au_real = r_au * n.sqrt((std_pix / r_pix)**2. + (plate_scale_err / plate_scale)**2 + (star_distance_err / star_distance)**2.)
# #                    print('Radius in AU: ' + str(r_au)[:5] + ' pm ' + str(std_au_real)[:5])
                    
#                     r_as = r_pix * plate_scale * 1000
#                     std_as = r_as * n.sqrt((std_pix / r_pix)**2. + (plate_scale_err / plate_scale)**2.) 


                    # au to milliarcseconds
                    chain[:, :, param_counter] /= au_per_pix
                    chain[:, :, param_counter] *= plate_scale 
                    chain[:, :, param_counter] *= 1000 
                # elif param == 'omega1':
                #     best_omega = med
                    # std_omega = n.sqrt(std**2. + north_angle_err**2.)
                    # print('Omega: ' + str(best_omega)[:5] + ' pm ' + str(std_omega)) 

                line = chain[:, :, param_counter]
                for lin in line:
                    plot.plot(lin)
                plot.show()
                paramchain = chain[:, :, param_counter].flatten()
                med = n.median(paramchain)
                std = n.std(paramchain)

#                med, std, patch = get_med_and_std(paramchain, return_frame = True)
#                med, std, patch = get_med_and_std(patch, return_frame = True)
                sample_sets.append(chain[:, :, param_counter].flatten())

                std = std * 3.

                if param == 'omega1':
                    error = n.sqrt(std**2. + north_angle_err**2.)
                elif param == 'r':
                    error = med * n.sqrt((std / med)**2. + (plate_scale_err / plate_scale)**2.)
                else:
                    error = std
                print(param + ': ' + str(med)[:7] + ' stddev ' +str(std)[:7] + ' error ' + str(error)[:7])


                line = chain[:, :, param_counter]

            param_counter += 1
            if param == 'omega1':
                labels.append('PA')
            elif param == 'gamma_in':
                labels.append('$\gamma_{in}$')
            elif param == 'gamma':
                labels.append('$\gamma$')
            elif param == 'osino' or param == 'ocoso':
                pass
            else:
                labels.append(param)

        else:
            int_sets.append(chain[:, :, param_counter:param_counter+parm_len])
            param_counter += parm_len

    off_au = n.sqrt(osino**2. + ocoso**2.)
    off_pix = au_per_pix * off_au
    off_mas = off_pix *plate_scale * 1000
    sample_sets.append(off_mas)

    med = n.median(off_mas)
    std = n.std(off_mas) * 3
    error = med * n.sqrt((std / med)**2. + (plate_scale_err / plate_scale)**2.)

    print('Offset ' + ': ' + str(med)[:7] + ' stddev ' +str(std)[:7] + ' error ' + str(error)[:7])
    
    labels.append('offset')
    # off_au_med = n.median(n.sqrt(osino**2. + ocoso**2.))
    # std_off_au = n.std(n.sqrt(osino**2. + ocoso**2.))
    
    # off_pix = off_au / au_per_pix
    # std_off_pix = std_off_au / au_per_pix

    # std_off_au_real = off_au  * n.sqrt((std_off_pix / off_pix)**2. + (plate_scale_err / plate_scale)**2. + (star_distance_err / star_distance)**2.)

    # off_as = off_pix * plate_scale * 1000
    # std_off_as = off_as * n.sqrt((std_off_pix / off_pix)**2. + (plate_scale_err / plate_scale)**2.)
    # print('Offset in milliarcsecs: ' + str(off_as)[:5] + ' pm ' + str(std_off_as)[:5])

    sample_sets.append(n.arctan2(osino, ocoso) * 180 / n.pi + 1.)
    omega2 =  n.median(n.arctan2(osino, ocoso) * 180 / n.pi) + 1.
    std_omega2 = n.std(n.arctan2(osino, ocoso) * 180 / n.pi) * 3
    std_omega2_real = n.sqrt((std_omega2)**2. + (north_angle_err)**2.)
    
    print('omega: ' + str(omega2)[:7] + ' pm ' + str(std_omega2_real)[:7] + ' error ' + str(error)[:7])
    labels.append('$\omega$')

    sample_sets = n.array(sample_sets).T
    corner.corner(sample_sets, plot_contours = True, labels = labels)
    plot.savefig('corner_kband.png')
    plot.show()


#####################################################################################
#####################################################################################
#####################################################################################
def plot_h():
    mc = ModelComparator('hr4796_config_hpol.ini',mc_save = 'mcmc_hpol_even2.p')
    nburn = 50
    chain = mc.chain
    sh = n.shape(chain)
    nwalkers = sh[0]
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
#    fit_parmlist = ['I', 'omega1', 'J_Polint_parms', 'gamma_in', 'gamma']
    print fit_parmlist
    routind = n.where(fit_parmlist == 'r_out')
    rinind = n.where(fit_parmlist == 'r_in')
    counter = 0
    int_sets = []

    sample_sets = []
    chain = chain[:, nburn:, :]
    labels = ['I']
    for param in fit_parmlist:
        print param
        parm_len = parm_lens[param]
        print parm_len
        if parm_len == 1:
            if param != 'r_in' and param != 'r_out' and param != 'gamma_in' and param != 'gamma':
                if param == 'I' or param == 'omega1':
                    chain[:, :, counter] *= 180 / n.pi 
                else:
                    plot_walker(chain, counter, param, nwalkers)
                sample_sets.append(chain[:, :, counter].flatten())
                print param
                if param == 'omega1':
                    labels.append('$\Omega$')
                elif param == 'gamma_in':
                    labels.append('$gamma_{in}$')
                elif param == 'r':
                    labels.append('r')

                print labels
            counter += 1
        else:
            int_sets.append(chain[:, :, counter:counter+parm_len])
            counter += parm_len
    

    sample_sets = n.array(sample_sets).T
    print sample_sets
#   fit_parmlist = ['I', 'omega1', 'gamma_in', 'gamma']
    triangle.corner(sample_sets, plot_contours = False, labels = labels)
    plot.show()


def plot_j():
    mc = ModelComparator('hr4796_config_jpol.ini',mc_save = 'mcmc_jpol_even.p')
    mc.print_parms()
    nburn = 100
    chain = mc.chain
    sh = n.shape(chain)
    nwalkers = sh[0]
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
    print fit_parmlist
#    fit_parmlist = ['I', 'omega1', 'J_Polint_parms', 'gamma_in', 'gamma']
    counter = 0
    int_sets = []

    sample_sets = []

    chain = chain[:, nburn:, :]
    for param in fit_parmlist:
        parm_len = parm_lens[param]
        if parm_len == 1:
            if param == 'I' or param == 'omega1':
                chain[:, :, counter] *= 180 / n.pi 
                #        plot_walker(chain, counter, param, nwalkers)
            sample_sets.append(chain[:, :, counter].flatten())
            counter += 1
        else:
            int_sets.append(chain[:, :, counter:counter+parm_len])
            counter += parm_len


    sample_sets = n.array(sample_sets).T
#    fit_parmlist = ['I', 'omega1', 'gamma_in', 'gamma']
    triangle.corner(sample_sets, plot_contours = False)
    plot.show()

