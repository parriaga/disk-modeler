# $Id$
#
# routines for image processing
#
# Michael Fitzgerald (mpfitz@llnl.gov)  2008-10-08
#


## #---------------------------------------------------------------------------
## # This code loads IPython but modifies a few things if it detects it's running
## # embedded in another IPython session (helps avoid confusion)
## try:
##     get_ipython
## except NameError:
##     banner=exit_msg=''
## else:
##     banner = '*** Nested interpreter ***'
##     exit_msg = '*** Back in main IPython ***'
## from IPython.terminal.embed import InteractiveShellEmbed
## # Now create the IPython shell instance. Put ipshell() anywhere in your code
## # where you want it to open.
## ipshell = InteractiveShellEmbed(banner1=banner, exit_msg=exit_msg)
## #------------------------------------------------------------------------------


import numpy as n
import scipy
#import matplotlib as mpl
from numpy import ma

import constants as const

import logging
_log = logging.getLogger('image')



def mask_cr(im, skyfilt=15, sigfilt=10, medfilt=5, thresh=7.,
            show=False, fignum=0, silent=False, **kwargs):
    "mask out likely cosmic rays"

    from scipy.ndimage import median_filter, percentile_filter

    if not silent:
        _log.debug('masking cosmic rays')

    # get 'sky-filtered' image
    sfim = im - median_filter(im.filled(n.nan), size=skyfilt,
                              #mode='constant',
                              cval=0.)
    fsfim = sfim.filled(n.nan)

    # get the sky-filtered local image sigma
    pf1 = percentile_filter(fsfim, 50-34.1, size=sigfilt,
                            #mode='constant',
                            cval=0.)
    pf2 = percentile_filter(fsfim, 50+34.1, size=sigfilt,
                            #mode='constant',
                            cval=0.)
    sig = (pf2-pf1)/2. # half dist. between locations of 1-sig confidence

    # get median-filtered image
    mfim = median_filter(fsfim, size=medfilt,
                         #mode='constant',
                         cval=0.)

    # get the image filtered for cosmic rays
    crim = sfim - mfim

    # locations where pixel is many sigma times the median value
    fcrim = crim.filled(0.)
    #w = n.where((fcrim > thresh*sig) & (fcrim > 0.))
    w = n.where(n.abs(fcrim) > thresh*sig)
    
    if not silent:
        _log.info("masking %d pixels as likely cosmic rays" % len(w[0]))

    # mask locations
    mim = im.copy()
    mim[w] = ma.masked

    # show results
    if show:
        fig = pylab.figure(fignum)
        fig.clear()
        fig.hold(True)
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        # show image
        ax1.imshow(sfim.filled(0.), interpolation='bilinear')
        ax1.set_title('unsharp')
        ax2.imshow((crim/sig).filled(0.), interpolation='bilinear',
                   cmap=mpl.cm.gray, vmin=0., vmax=thresh)
        ax2.set_title('filt, noisenorm')
        # mark locations
        if len(w[0]) > 0:
            for ax in (ax1, ax2):
                ax.set_autoscale_on(False)
                ax.scatter(w[1], w[0], marker='o', s=5.**2,
                           edgecolor='g', facecolor='none')
        # finalize
        pylab.draw()
        pylab.show()

    return mim

    
def median_replace(im, boxsize=5, fast=False):
    """
    Replace masked pixels by median in local box (size boxsize).

    If fast is True, then median is computed with masked values set to
    zero.

    im is required to be a masked array.  Returns a masked array.
    """

    # don't do anything if not needed
    if not n.any(im.mask): return im

    # get median-filtered image
    if fast:
        from scipy.ndimage import median_filter
        mim = median_filter(im.filled(0.), size=boxsize,
                            mode='constant', cval=0.)
    else:
        from scipy.ndimage import generic_filter
        nanmed = lambda x: n.nanmedian(x, axis=None)
        mim = generic_filter(im.filled(n.nan), nanmed, size=boxsize,
                             mode='constant', cval=n.nan)
        mim = ma.masked_where(n.isnan(mim), mim)

    # replace masked values with median-filtered values
    rim = im.copy()
    wm = n.where(rim.mask)
    ma.putmask(rim, im.mask, mim)

    return rim


def rotate(im, theta, cen=None, nearest=False, scale=1., shift=None):
    """
    rotate an image by theta [rad] about the center (cen [pix])

    rotation is counter-clockwise for positive theta

    scale > 1 corresponds to magnification.  The data values will be
    scaled to preserve flux.

    set nearest keyword to use nearest-neighbor interpolation

    shift is applied after the rotation and scaling, so the new center is cen+shift
    """

    # get rotation center if not defined
    if cen is None:
        cen = n.array(im.shape)/2.
    else:
        cen = n.array(cen)

    # get shift if not defined
    if shift is None:
        shift = n.zeros(2, dtype=n.float)
    else:
        shift = n.array(shift)

    masked_input = hasattr(im, 'mask')
    im = ma.array(im)

    # mask out bad values
    im[n.isnan(im)] = ma.masked
    im[n.isinf(im)] = ma.masked

    mask = im.mask
    masked = n.any(mask)
    
    # replace bad values with median-filtered versions
    if masked:
        im = median_replace(im, boxsize=3, fast=True)

    # fill remaining bad values with zeros
    im = im.filled(0.)
        
    # current coordinates
    y, x = n.mgrid[0:im.shape[0],0:im.shape[1]]
    c = n.array((y,x))

    # rotation matrix
    R = n.array(((n.cos(theta), -n.sin(theta)),
                 (n.sin(theta), n.cos(theta))))

    # rotate coordinates
    dc = (c-cen[:,n.newaxis,n.newaxis]).reshape(2,n.product(im.shape)) - shift[:,n.newaxis]
    rc = n.dot(R, dc).reshape((2,)+im.shape)/scale + \
         cen[:,n.newaxis,n.newaxis]

    # interpolate
    from scipy.ndimage import map_coordinates
    if nearest:
        rim = map_coordinates(im, n.round(rc), output=n.float64)
    else:
        rim = map_coordinates(im, rc, output=n.float64)
    
    # flux perservation
    if scale != 1.:
        rim /= scale**2

    # re-fill masking
    if masked:
        thresh = 0.1
        # rotate the mask
        rmask = (map_coordinates(mask.astype(n.float), rc, cval=1.) > thresh)
        # apply mask as NaN
        rim[n.where(rmask)] = n.nan

    if masked_input:
        # return masked array as output
        rim = ma.masked_where(n.isnan(rim), rim)

    return rim


def get_extent(shape, scale=1., cen=None, flip_x=False):
    "Given the shape of an image and the pixel scale, return the extent"

    shape = n.array(shape)
    
    if cen is None:
        #cen = (shape-1.)/2.
        cen = n.zeros_like(shape)

    extent = (-cen[1],
              shape[1]-cen[1],
              -cen[0],
              shape[0]-cen[0],
              )
    extent = (n.array(extent) - 0.5) * scale

    if flip_x:
        extent[0:2] *= -1.

    return extent

def test_extent():
    nx, ny = 5, 3
    im = n.arange(nx*ny)
    im.shape = (ny,nx)

    scale = 3.

    extent = get_extent(im.shape,
                        #cen=(n.array(im.shape)-1.)/2.,
                        cen=(ny-2, nx-2),
                        scale=scale,
                        flip_x=True,
                        )
    
    import matplotlib as mpl
    import pylab
    fig = pylab.figure(0.)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)
    ax.imshow(im, extent=extent,
              interpolation='nearest',
              cmap=mpl.cm.jet,
              )
    pylab.draw()
    pylab.show()
    

def stack(ims, cens, return_full=False):
    """
    Given an array of images and their centroids, place them in a
    common frame (translation only).
    """

    masked_input = hasattr(ims, 'mask')
    ims = ma.array(ims)

    # mask out bad values
    ims[n.isnan(ims)] = ma.masked
    ims[n.isinf(ims)] = ma.masked

    masks = ims.mask
    masked = n.any(masks)


    # dimensions of bounding array
    n_im = ims.shape[0]     # number of images
    im_s = ims.shape[1:3]   # shape of each image
    cen = cens.mean(axis=0) # average position
    dcen = n.ceil(cens.max(axis=0)).astype(n.int) - \
           n.floor(cens.min(axis=0)).astype(n.int) # range of deviations
    dcen += (dcen % 2)      # rounded up to an even integer
    #r_s = im_s + dcen       # size of registered image
    r_s = im_s + dcen + 2   # size of registered image  FIXME  need to for some reason?
    pdcen = n.ceil((cens-cen[n.newaxis,:]).max(axis=0)).astype(n.int) # greatest positive deviation from center
    r_cen = n.round(cen).astype(n.int) + pdcen # center of image in registered stack

    # shifts and beginning coordinates of images in bounding array frame
    dev = cens - cen[n.newaxis,:]     # deviation of each image from center
    r_c = dev + r_cen[n.newaxis,:]    # current position in bounding frame
    rdev = n.round(dev).astype(n.int) # integer portion of deviation
    fdev = dev - rdev                 # fractional portion of deviation
    b = pdcen[n.newaxis,:] - rdev     # beginning coordinate

    # place images in bounding array
    from scipy.ndimage.interpolation import shift
    rims = ma.masked_all((n_im, r_s[0], r_s[1]), dtype=n.float)
    for i, im in enumerate(ims):
        # replace bad values with median-filtered versions
        if masked:
            im = median_replace(im, boxsize=3, fast=True)
        # fill remainig bad values with zeros
        im = im.filled(0.)

        # shift the image by the fractional amount
        sim = shift(im, -fdev[i,:], output=n.float64)
        # place in large array at appropriate position for integral shift
        rims[i,b[i,0]:b[i,0]+im_s[0],b[i,1]:b[i,1]+im_s[1]] = sim

        # re-fill masking
        if masked:
            thresh = 0.1
            # shift the mask
            smask = (shift(masks[i,:,:].astype(n.float), -fdev[i,:], output=n.float64) > thresh)
            rims.mask[i,b[i,0]:b[i,0]+im_s[0],b[i,1]:b[i,1]+im_s[1]] = smask

    if not masked_input:
        # return array with NaNs in output bad pixels
        rims = rims.filled(n.nan)

    if return_full:
        return rims, r_cen, fdev, b
    else:
        return rims, r_cen


def _combine_images(cols, method, chi2, signal):
    "helper routine to do combination on a column"

    from misc import biweight

    if chi2 is None: chi2=1.

    # mask nans
    mcols = ma.masked_where(n.isnan(cols),cols)

    # skip if all bad
    if n.all(mcols.mask):
        col = ma.masked_all(cols.shape[1:3], dtype=n.float)
        return col

    # compute (normalized) weights
    if method in ('wtave', 'rwtave'):
        # load chi2 values
        # weights (S/N)^2
        #wt = (signal**2)[:,n.newaxis,n.newaxis] / chi2
        wt = signal[:,n.newaxis,n.newaxis] / chi2
        if n.any(mcols.mask):
            if not wt.shape == mcols.shape:
                wt = wt * n.ones((1,)+mcols.shape[1:],dtype=n.float)
            wt[mcols.mask] = 0. # no weight for NaN pixels
        wt /= wt.sum(axis=0)[n.newaxis,...] # normalize

    # combine
    if method=='ave': # average
        col = mcols.mean(axis=0)

    elif method=='med': # median
        col = ma.median(mcols, axis=0)

    elif method=='rave': # robust average
        # compute robust weights
        col = mcols.sum(axis=0) # simple average
        d  = mcols-col[n.newaxis,:,:] # deviation
        s = ma.median(ma.abs(d), axis=0) # MAD at each pixel
        wt = biweight(d/1.483/s[n.newaxis,:,:])
        if n.any(s==0.):
            #  if MAD=0, then only pixels with d=0 get any weight
            sz = (s==0.)[n.newaxis,:,:]
            wt[(d == 0) & sz] = 1.
            wt[(d != 0) & sz] = 1.
        wt /= wt.sum(axis=0)[n.newaxis,...] # normalize
        # combine
        col = (mcols*wt).sum(axis=0)

    elif method=='wtave': # weighted average
        # combine
        col = (mcols*wt).sum(axis=0)

    elif method=='rwtave': # robust weighted average
        # compute robust weights
        col = (mcols*wt).sum(axis=0) # simple weighted average
        d = wt*(mcols-col[n.newaxis,:,:]) # deviation
        s = ma.median(ma.abs(d), axis=0) # MAD at each pixel
        bwt = biweight(d/1.483/s[n.newaxis,:,:])
        if n.any(s==0.):
            #  if MAD=0, then only pixels with d=0 get any weight
            sz = (s==0.)[n.newaxis,:,:]
            bwt[(d == 0) & sz] = 1.
            bwt[(d != 0) & sz] = 1.
        wt2 = wt * bwt
        wt2 /= wt2.sum(axis=0)[n.newaxis,...] # normalize
        # combine
        col = (mcols*wt2).sum(axis=0)

    else: raise NotImplementedError

    return col


def combine_images(ims, method='ave', chi2=None, signal=None, ncol=16,
                   n_process=None, w_ims=n.s_[:], n_process_max=None,
                   **kwargs):
    "ncol is number of columns in a chunk (should match HDF5 chunk)"


    n_im, ny, nx = ims.shape
    if signal is None: signal = n.ones(n_im, dtype=n.float)

    n_wim = len(n.arange(n_im)[w_ims])

    # single image case
    if n_wim==1:
        _log.debug('single image')
        cim = ims[w_ims,:,:].reshape(ims.shape[1:])
        return cim

    # array chunking data
    fx = nx/ncol # number of whole chunks
    nc = fx + (fx*ncol < nx) # number of chunks
    bx = n.arange(nc,dtype=n.int)*ncol # chunk beginning indices
    ex = n.clip((n.arange(nc,dtype=n.int)+1)*ncol, 0, nx) # chunk ending indices


    def worker(input, output):
        for i, args in iter(input.get, 'STOP'):
            col = _combine_images(*args)
            output.put((i,col))

    def feeder(input):
        for i in range(nc):
            # read data
            cols = ims[w_ims,:,bx[i]:ex[i]]

            if chi2 is not None:
                if len(chi2.shape) == 3:
                    c2 = chi2[w_ims,:,bx[i]:ex[i]]
                else:
                    c2 = chi2[w_ims,n.newaxis,n.newaxis]
            else: c2 = None

            args = (cols, method, c2, signal[w_ims])
            # place on queue (block if queue full)
            input.put((i, args), True)
        _log.debug('feeder finished')
            
    _log.debug("combining images with %s method" % method)

    # set up queues
    import multiprocessing as mp
    if n_process is None:
        n_process = mp.cpu_count()
    if n_process_max is not None:
        n_process = n.min((n_process, n_process_max))
    n_max = n_process*2
    inqueue = mp.Queue(n_max)
    outqueue = mp.Queue()

    # set up process data
    #
    # worker processes
    for i in range(n_process):
        mp.Process(target=worker, args=(inqueue, outqueue)).start()
    # feeder process
    feedp = mp.Process(target=feeder, args=(inqueue,))
    feedp.start()

    # process output as it arrives
    cim = n.empty((ny,nx),dtype=n.float)
    for j in range(nc):
        i, col = outqueue.get()
        _log.debug("column %d/%d" % (i, nc))
        # place in output array
        cim[:,bx[i]:ex[i]] = col.filled(n.nan)

    # kill worker processes
    _log.debug('received all columns; killing workers ...')
    for i in range(n_process):
        inqueue.put('STOP')

    # block until feeder finished
    _log.debug('waiting for feeder to finish ...')
    feedp.join(1.)
 
    return cim



def radial_profile(im, cen, do_max=False, return_im=False):
    "extract the radial profile of an image"

    # ensure image is masked
    im = ma.masked_where(n.isnan(im), im)

    # construct radial distance array
    ny, nx = im.shape
    y, x = n.ogrid[0:ny,0:nx]
    y -= cen[0]
    x -= cen[1]
    R = n.sqrt(y**2+x**2)

    # maximum radius
    if n.any(im.mask):
        w = n.nonzero(~im.mask)
        rmax = R[w].max()
    else:
        rmax = R.max()

    # radius array
    n_r = int(rmax)
    r = n.arange(n_r, dtype=n.float) + 0.5

    from misc import biweight
    prof = n.empty(n_r, dtype=n.float)
    for i, rr in enumerate(r):
        w = n.nonzero((R >= rr-0.5) & (R < rr+0.5))

        vals = im[w].compressed() # good values
        if len(vals) == 0: m = 0. # no data
        elif len(vals) == 1: m = vals[0] # single value
        else:
            if do_max:
                m = vals.max()
            else:
                # compute robust mean
                m = n.median(vals)       # median
                d = vals - m             # deviation from median
                s = n.median(n.abs(d))   # median absolute deviation
                if s != 0:
                    wt = biweight(d/1.483/s) # weights
                    wt /= wt.sum()           # normalized weights
                    m = (wt*vals).sum()      # weighted mean
        # store
        prof[i] = m

    if not return_im:
        return r, prof
    else:
        from scipy.interpolate import interp1d
        pim = interp1d(r, prof, bounds_error=False)(R)
        pim = ma.masked_where(n.isnan(pim), pim)
        return r, prof, pim


def radial_stdev_profile(im, cen, ap_dia=None, get_mprof=False,
                         rmin=0., rmax=None,
                         PAmin=0., PAmax=2.*n.pi,
                         robust=True):
    """
    Get the robust radial stdev profile.  If ap_dia=None, do so on a
    pixel-by-pixel basis.  Otherwise use circular apertures.

    rmin, rmax [pix] specify minimum and maximum radius of inner aperture edge

    PAmin, PAmax [rad] specify minimum and maximum PA
    """

    # ensure image is masked
    im = ma.masked_where(n.isnan(im), im)
    assert ~n.all(im.mask)

    # construct radial distance, angle arrays
    ny, nx = im.shape
    y, x = n.ogrid[0:ny,0:nx]
    y -= cen[0]
    x -= cen[1]
    R = n.sqrt(y**2+x**2)
    PA = (-n.arctan2(x,y)) % 2*n.pi # [rad] [0., 2pi)

    # phase wrap
    PAmin = PAmin % 2.*n.pi
    PAmax = PAmax % 2.*n.pi

    # maximum radius
    if rmax is None:
        if n.any(im.mask):
            w = n.nonzero(~im.mask)
            rmax = R[w].max()
        else:
            rmax = R.max()

    # get radial locations
    if ap_dia is None:
        n_r = int(rmax-rmin)
        r = n.arange(n_r, dtype=n.float) + 0.5 + rmin
    else:
        n_r = int((rmax-rmin) * 2 / ap_dia)
        r = (n.arange(n_r, dtype=n.float) + 0.5) * ap_dia / 2 + rmin

        dd = int(n.ceil(ap_dia/2.))*2 # aperture box size (even)
        timy, timx = n.mgrid[0:dd,0:dd]

        from misc import pixwt
    #_log.debug("%d radial zones" % n_r)
    
    # get azimuthal locations
    if ap_dia is None:
        if PAmax >= PAmin:
            PAg = (PA >= PAmin) & (PA <= PAmax)
        else:
            PAg = ((PA >= 0.) & (PA <= PAmax)) | \
                  ((PA >= PAmin) & (PA <= 360.))
            
    from misc import biweight
    def get_std_worker(input, output):
        for i in iter(input.get, 'STOP'):
            rr = r[i]
            if ap_dia is None: # pixel-by-pixel
                rg = (R >= rr-0.5) & (R < rr+0.5)
                w = n.nonzero(rg & PAg)
                vals = im[w].compressed()
            else: # apertures
                # get aperture locations
                n_ap = int(2.*n.pi*rr / (ap_dia/2.))
                dth = 2.*n.pi / n_ap # [rad]
                th = n.arange(n_ap, dtype=n.float) * dth # [rad]
                if PAmax >= PAmin:
                    w = n.nonzero((th >= PAmin) & (th <= PAmax))
                else:
                    w = n.nonzero(((th >= 0.) & (th <= PAmax)) |
                                  ((th >= PAmin) & (th <= 360.)))
                n_ap = len(w[0])
                cens = n.array((rr*n.sin(th[w])+cen[0], rr*n.cos(th[w])+cen[1])).T
                # get values within apertures
                vals = ma.masked_all(n_ap, dtype=n.float)
                for j, apc in enumerate(cens):
                    # extract thumbnail around aperture
                    iyb = int(n.floor(apc[0]-dd/2))
                    ixb = int(n.floor(apc[1]-dd/2))
                    if (ixb < 0) or (iyb < 0) or \
                           (ixb+dd >= nx) or (iyb+dd >= ny):
                        continue
                    tim = im[iyb:iyb+dd, ixb:ixb+dd]
                    assert tim.shape == (dd,dd)
                    tc = apc - n.array((iyb,ixb))
                    # get pixel weight array for aperture
                    p = pixwt(tc[1], tc[0], ap_dia/2., timx, timy)
                    vals[j] = (tim*p).sum() # image sum within aperture
                vals = vals.compressed() # remove missing entries
                _log.debug("(zone %d/%d) - %d/%d apertures used" % (i, n_r, len(vals), n_ap))

            if robust:
                # get robust stdev
                m = n.median(vals) # median value
                d = vals - m       # deviation
                ad = n.abs(d)      # absolute deviation
                mad = n.median(ad) # median absolute deviation
                if mad == 0: std = 0. # no deviation -> zero stdev
                else:
                    wt = biweight(d/1.483/mad) # weights
                    sum_wt = wt.sum()
                    sum_wt2 = (wt**2).sum()
                    m = (vals*wt).sum() / sum_wt # weighted mean
                    d = vals-m # deviation from weighted mean
                    var = (d**2 * wt).sum() / (sum_wt-sum_wt2/sum_wt) # weighted var
                    std = n.sqrt(var) # weighted stdev
            else:
                m = n.mean(vals)
                std = n.std(vals)

            output.put((i, m, std))

    # set up queues
    import multiprocessing as mp
    inqueue = mp.Queue()
    outqueue = mp.Queue()
    
    # get robust stdev profile
    for i in range(n_r): inqueue.put(i)

    # set up processes
    n_process = mp.cpu_count()
    for i in range(n_process):
        mp.Process(target=get_std_worker, args=(inqueue, outqueue)).start()

    # process output
    mprof = n.empty(n_r, dtype=n.float)
    sprof = n.empty(n_r, dtype=n.float)
    for j in range(n_r):
        # get output from queue
        i, mn, std = outqueue.get()
        mprof[i] = mn
        sprof[i] = std
        if ap_dia is None:
            _log.debug("zone %d/%d finished" % (i, n_r))

    # kill all processes
    for i in range(n_process): inqueue.put('STOP')
    _log.debug('all radial stdev tasks finished')

    if get_mprof:
        return r, mprof, sprof
    else:
        return r, sprof


if __name__=='__main__':
    test_extent()
    
