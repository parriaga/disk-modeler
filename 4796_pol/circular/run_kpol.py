from diskmodeler_circular import ModelComparator
from diskmodeler_plotutils import *


conf = 'hr4796_config_hpol.ini'
#mc_save = 'mcmc_jpol_intparms_method3.p'                                                                                                                                                                     
mc_save = 'mcmc_hpol_intparms_method3.p'
mc = ModelComparator(conf, mc_save = mc_save)


for label in mc.labels:
    print mc.parms[label + 'int_parms']
    wh = n.where(mc.parms[label + 'int_parms'] == 0.)
    print wh
    mc.parms[label + 'int_parms'][wh] = 100.

mc.is_fixed['omega1'] = False
mc.is_fixed['gamma'] = False
mc.is_fixed['gamma_in'] = False
mc.is_fixed['r'] = False
mc.is_fixed['r_in'] = True
mc.is_fixed['r_out'] = True
mc.is_fixed['I'] = False
mc.is_fixed['omega2'] = False
mc.is_fixed['offset'] = False
mc.n_walker = 80
mc.n_sample = 250
mc.n_burn = 10

mc.run_mcmc(fname='mc_kpol_masked_setgamma.p')
