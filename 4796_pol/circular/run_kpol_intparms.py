from diskmodeler_circular import ModelComparator
from diskmodeler_plotutils import *


conf = 'hr4796_config_kpol.ini'
#mc_save = 'mcmc_jpol_intparms_method3.p'                                                                                                                                                                     
mc_save = 'mcmc_kpol_method3_long.p'
mc = ModelComparator(conf, mc_save = mc_save)


for label in mc.labels:
    print mc.parms[label + 'int_parms']
    wh = n.where(mc.parms[label + 'int_parms'] == 0.)
    print wh
    mc.parms[label + 'int_parms'][wh] = 100.

mc.update_all_int_parms([25])
mc.is_fixed['omega1'] = True
mc.is_fixed['gamma'] = True
mc.is_fixed['gamma_in'] = True
mc.is_fixed['r'] = True
mc.is_fixed['r_in'] = True
mc.is_fixed['r_out'] = True
mc.is_fixed['I'] = True
mc.is_fixed['omega2'] = True
mc.is_fixed['offset'] = True
mc.n_walker = 100
mc.n_sample = 40
mc.n_burn = 10

mc.run_mcmc(fname='mc_kpol_intparms_moreknots.p')
