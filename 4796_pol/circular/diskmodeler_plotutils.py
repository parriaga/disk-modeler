from diskmodeler_circular import ModelComparator
import diskmodeler_circular
import numpy as n
import matplotlib.pyplot as plot
import matplotlib as mpl
from matplotlib import gridspec

def show_model(diskobj, xlims_pix = 63, ylims_pix = 70, vmin_data = -150, vmax_data = 200, vmin_resids = -2, vmax_resids = 2, convolve_resids = True,**plot_kwargs):
    '''
    takes a modelcomparator's diskobject
    '''
    pixelscale = .014            # arcseconds / pix
    if diskobj.scale == None:
        dist = 72.8                   # parsecs,cn  
        pix_au = dist * pixelscale   # AU / pix
    else: 
        pix_au = diskobj.scale
#    pix_au = 73 *.014 #pixelscale
    ylims = xlims_pix * pix_au
    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    calibk = 5.7e-8             # Jy/ADU/coadd                                                    
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 
    

    cmap_dict = {'red':   ((0.0, 0.0, 0.0), 
                           (0.756, 0.0, 0.0),
                           (1.0, 1.0, 1.0)),
                 'green': ((0.0, 0.0, 0.0),
                           (0.38, 0.0, 0.0),
                           (1.00, 1.0, 1.0)),
                 'blue':  ((0.00, 0.0, 0.0),
                           (0.737, 1.0, 1.0),
                           (1.00, 1.0, 1.0))
                }
    colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',cmap_dict)
    colormap_idl_bluewhite.set_bad('black')

    label_size = 10.5
    mpl.rcParams['xtick.labelsize'] = label_size
    mpl.rcParams['ytick.labelsize'] = label_size
    fig = plot.figure()
    fig.set_size_inches(9,4.5)

    mim = diskobj.get_model()
    
    gs1 = gridspec.GridSpec(2,2, height_ratios = [11, 1])
    gs1.update(left=.05, right=.60)
    
    mim_tfim_ax = plot.subplot(gs1[0,1])
    mim_tfim_ax.set_xlim([-xlims, xlims])
    mim_tfim_ax.set_ylim([-90,90])
    
    tfim_ax = plot.subplot(gs1[0,0])
    tfim_ax.set_xlim([-xlims, xlims])
    tfim_ax.set_ylim([-90,90])
    
    cbar_mim_tfim_ax = plot.subplot(gs1[1,:]) 
    mim_fig = mim_tfim_ax.imshow(mim, vmin = vmin_data, vmax = vmax_data, extent = extent, **plot_kwargs)
    
    tfim_ax.imshow(diskobj.tfim, vmin = vmin_data, vmax = vmax_data, extent = extent, **plot_kwargs)
    fig.colorbar(mim_fig, cax = cbar_mim_tfim_ax, orientation = 'horizontal', label = 'ADU')

    gs2 = gridspec.GridSpec(2,1, height_ratios = [11, 1])
    gs2.update(left=.65, right = .98)
    resid_ax = plot.subplot(gs2[0,0])
    resid_ax.set_xlim([-xlims, xlims])
    resid_ax.set_ylim([-90,90])
    cbar_resid_ax = plot.subplot(gs2[1,0])
    
    mim_fig = mim_tfim_ax.imshow(mim, vmin = vmin_data, vmax = vmax_data, extent = extent, **plot_kwargs)
#    tfim_ax.imshow(diskobj.tfim, vmin = vmin_data, vmax = vmax_data, **plot_kwargs)
    fig.colorbar(mim_fig, cax = cbar_mim_tfim_ax, orientation = 'horizontal', label = 'Jy/arcsec$^2$')

    if convolve_resids == False or diskobj.psf_method != 'inputpsf':
        resids = (diskobj.tfim - mim) / diskobj.stdim
    else:
        resids = (diskobj.tfim - mim) / diskobj.stdim
        psf = diskobj.psf_im
        from scipy.signal import fftconvolve
        resids[n.isnan(resids)] = 0
        resids = fftconvolve(resids, psf, mode = 'same')
        
    resids_fig = resid_ax.imshow(resids, vmin = vmin_resids, vmax = vmax_resids, extent = extent,  **plot_kwargs)
    fig.colorbar(resids_fig, cax = cbar_resid_ax, orientation = 'horizontal', label = 'SNR')    

    chisq = n.nanmean((diskobj.tfim - mim)**2. / diskobj.stdim**2.)
    print 'Chi square = ' + str(chisq)

    plot.show()


def plot_walker(chain, ind, param, nwalkers, is_1d = False):
    if is_1d is False:
        param_set = chain[:, :, ind]
    else:
        param_set = chain
    if param == 'I' or param == 'omega1' or param == 'omega2':
        param_set = param_set * 180. / n.pi
    for walker in range(nwalkers):
        line = param_set[walker, :]
        plot.plot(line, c = 'blue')
    plot.xlabel('Walker')
    plot.ylabel(param)
    plot.show()
    

def show_chains(mc):
    chain = mc.chain
    sh = n.shape(chain)
    nwalkers = sh[0]
    
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
    counter = 0
    int_sets = []
    osino = None
    ocoso = None
    for param in fit_parmlist:
        parm_len = parm_lens[param]
        if parm_len == 1:
            if param == 'osino':
                osino = chain[:, :, counter]
                osino_ind = counter
            elif param == 'ocoso':
                ocoso = chain[:, :, counter]
                ocoso_ind = counter
            else:
                plot_walker(chain, counter, param, nwalkers)
            counter += 1
        else:
            int_sets.append(chain[:, :, counter:counter+parm_len])
            counter += parm_len
    if osino is not None and ocoso is not None:
        offset = n.sqrt(osino**2. + ocoso**2.)
        omega2 = n.arctan2(osino, ocoso) 
        plot_walker(offset, None, 'offset', nwalkers, is_1d = True)
        plot_walker(omega2, None, 'omega2', nwalkers, is_1d = True)
    return



def plotall():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    curves(mc, 0, color='blue')
    curves(mc, 1, color='red')
    curves(mc, 2, color='green')


def curves(mc, diskobj_ind, color = 'blue', lnprob_cutoff = 100):
    chain = mc.chain
    sh = n.shape(chain)
    n_burn = sh[1] - 10
    n_walkers = sh[0]
    best_lnprob = n.max(mc.lnprobability)
    print best_lnprob
    for step in range(n_burn, sh[1]):
        for walker in range(n_walkers):
            param_set = chain[walker, step,:]
            if mc.lnprobability[walker, step] > best_lnprob - lnprob_cutoff:
                mc.set_fit_parms(param_set, fit_parmlist_wtf = mc.fit_parmlist)
                nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=500)
                plot.plot(nu * 180. / n.pi, intensity / n.max(intensity), color = color)
#    plot.show()






def plot_unconvolved(diskobj):
    # colors 
    colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',
                                                                {'red':   ((0.0, 0.0, 0.0),
                                                                           (0.756, 0.0, 0.0),
                                                                           (1.0, 1.0, 1.0)),
                                                                 
                                                                 
                                                                 'green': ((0.0, 0.0, 0.0),
                                                                           (0.38, 0.0, 0.0),
                                                                           (1.00, 1.0, 1.0)),
                                                                 
                                                                 'blue':  ((0.00, 0.0, 0.0),
                                                                           (0.737, 1.0, 1.0),
                                                                           (1.00, 1.0, 1.0))
                                                             })
    colormap_idl_bluewhite.set_bad('black')
        
    fig,(ax1, ax2, ax3) = plot.subplots(1, 3)
    fig.set_size_inches(9,4.5)
    plot_kwargs =  {'extent': extent,
                    'cmap': colormap_idl_bluewhite}

    
    
    fig.suptitle(diskobj.label + ' Unconvolved',fontsize = 15)
    unc_model = diskobj.get_model(convolve = False, filt = False)
    im = ax1.imshow(unc_model,  vmin = vmin, vmax = vmax, **plot_kwargs)
    ax1.set_xlim([-xlims, xlims])
    ax1.set_ylim([-90, 90])
    ax1.set_title('Data')
        
    unklip_model = diskobj.get_model(convolve = True, filt = False)
    ax2.imshow(unklip_model, vmin = vmin, vmax = vmax, **plot_kwargs)
    ax2.set_ylim([-90, 90])
    ax2.set_xlim([-xlims, xlims])
    ax2.set_title('Model')
    
        
    ax3.imshow(mim, vmin = vmin, vmax = vmax, **plot_kwargs) #make this divided by stdim and show color bar
    ax3.set_ylim([-90, 90])
    ax3.set_xlim([-xlims, xlims])
    ax3.set_title('Residuals')
    fig.subplots_adjust(wspace = .2)        
    fig.subplots_adjust(bottom = .23)
    
    cbar_ax = fig.add_axes([0.15,0.12,0.7,0.03])
    cb = fig.colorbar(im, cax = cbar_ax, orientation = 'horizontal', label = 'mJy / arcsec$^2$')
    plot.show()

