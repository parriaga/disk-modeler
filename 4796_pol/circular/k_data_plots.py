import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
import copy
from pyklip.fmlib.diskfm import DiskFM
_log = logging.getLogger('hr4796a')
import matplotlib.pyplot as plot 
import configparser
from diskmodeler_circular import ModelComparator
from matplotlib import gridspec



def make_plot_k(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    mimtot = pyfits.getdata('ktot_model_im.fits')
    mimpol = pyfits.getdata('kpol_model_im.fits')
    
    dataimtot = pyfits.getdata('ktot_data_im.fits')
    dataimpol = pyfits.getdata('kpol_data_im.fits')
    
    stdimtot = pyfits.getdata('ktot_stdim.fits')
    stdimpol = pyfits.getdata('kpol_stdim.fits')
    
    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix
#    ylims = xlims_pix * pix_au
#    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 

    plot_kwargs_1 = {'extent': extent,
                     'vmin': -10,
                     'vmax': 120}
    plot_kwargs_2 = {'extent': extent,
                     'vmin': -3,
                     'vmax': 3}
 
    gs1 = gridspec.GridSpec(2,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8.5, 7)

#    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = -.9, right = .9)
    gs1.update(bottom = .16,wspace = 0, hspace = 0, left = 0.19, right = .9)
    ktot_data_ax = plot.subplot(gs1[0,0])
    kpol_data_ax = plot.subplot(gs1[1,0])

    ktot_model_ax = plot.subplot(gs1[0,1])
    kpol_model_ax = plot.subplot(gs1[1,1])

    ktot_resid_ax = plot.subplot(gs1[0,2])    
    kpol_resid_ax = plot.subplot(gs1[1,2])


    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .13, hspace = 0)
    resid_cbar = plot.subplot(gs2[0,2])

    ktot_data_ax.imshow(dataimtot, **plot_kwargs_1)
    kpol_data_ax.imshow(dataimpol, **plot_kwargs_1)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    ktot_data_ax.text(-95, 0, 'AU', **text_kwargs)
    kpol_data_ax.text(-95, 0, 'AU', **text_kwargs)

    ktot_model = ktot_model_ax.imshow(mimtot, **plot_kwargs_1)
    kpol_model = kpol_model_ax.imshow(mimpol, **plot_kwargs_1)

    ktot_resid_ax.imshow((mimtot - dataimtot) /stdimtot, **plot_kwargs_2)
    resids = kpol_resid_ax.imshow((mimpol - dataimpol) / stdimpol, **plot_kwargs_2)

    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Resids', ticks = [-2, -1, 0, 1, 2])  

    all_ax = (kpol_data_ax,ktot_data_ax,
              kpol_model_ax,ktot_model_ax,
              kpol_resid_ax,ktot_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kpol_model_ax,ktot_model_ax,
                 kpol_resid_ax,ktot_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    hide_x_ax = (ktot_model_ax,ktot_resid_ax,ktot_data_ax)
    for ax in hide_x_ax:
        ax.xaxis.set_visible(False)


    text_kwargs = {'fontsize': 15,
                   'ha': 'center'}
    #-25
    ktot_data_ax.text(0, 100, 'Data', **text_kwargs)
    ktot_model_ax.text(0, 100, 'Model', **text_kwargs)
    ktot_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    

    kpol_data_ax.set_xticks([-50, -25, 0, 25, 50])
    kpol_model_ax.set_xticks([-50, -25, 0, 25, 50])
    kpol_resid_ax.set_xticks([-50, -25, 0, 25, 50])
#    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])


    text_kwargs = {'fontsize': 15, 
                   'va': 'center',
                   'ha': 'right',
                   'rotation': 90}
    
    ktot_data_ax.text(-110., 0., 'Total Intensity', **text_kwargs)
    kpol_data_ax.text(-110., 0., 'Polarized Intensity', **text_kwargs)

    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/k_data_plot.png')
    plot.show()



def make_plot_k_noresids(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    mimtot = pyfits.getdata('ktot_model_im.fits')
    mimpol = pyfits.getdata('kpol_model_im.fits')
    
    dataimtot = pyfits.getdata('ktot_data_im.fits')
    dataimpol = pyfits.getdata('kpol_data_im.fits')
    
    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix
#    ylims = xlims_pix * pix_au
#    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 

    plot_kwargs_1 = {'extent': extent,
                     'vmin': -10,
                     'vmax': 120}
    plot_kwargs_2 = {'extent': extent,
                     'vmin': -3,
                     'vmax': 3}
 
    gs1 = gridspec.GridSpec(2,2)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(6, 7)

    gs1.update(wspace = 0, hspace = 0, left = 0.176, right = 0.9)
#    gs1.update(bottom = .16,wspace = 0, hspace = 0, left = 0.19, right = .9)
    ktot_data_ax = plot.subplot(gs1[0,0])
    kpol_data_ax = plot.subplot(gs1[1,0])

    ktot_model_ax = plot.subplot(gs1[0,1])
    kpol_model_ax = plot.subplot(gs1[1,1])

    ktot_data_ax.imshow(dataimtot, extent = extent, vmin = -20, vmax = 200)
    kpol_data_ax.imshow(dataimpol, **plot_kwargs_1)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    ktot_data_ax.text(-95, 0, 'AU', **text_kwargs)
    kpol_data_ax.text(-95, 0, 'AU', **text_kwargs)

    ktot_model = ktot_model_ax.imshow(mimtot, extent = extent, vmin = -20, vmax = 200)
    kpol_model = kpol_model_ax.imshow(mimpol, **plot_kwargs_1)


    all_ax = (kpol_data_ax,ktot_data_ax,
              kpol_model_ax,ktot_model_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kpol_model_ax,ktot_model_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    hide_x_ax = (ktot_model_ax,ktot_data_ax)
    for ax in hide_x_ax:
        ax.xaxis.set_visible(False)


    text_kwargs = {'fontsize': 15,
                   'ha': 'center'}
    #-25
    ktot_data_ax.text(0, 100, 'Data', **text_kwargs)
    ktot_model_ax.text(0, 100, 'Model', **text_kwargs)
    

    kpol_data_ax.set_xticks([-50, -25, 0, 25, 50])
    kpol_model_ax.set_xticks([-50, -25, 0, 25, 50])
#    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])


    text_kwargs = {'fontsize': 15, 
                   'va': 'center',
                   'ha': 'right',
                   'rotation': 90}
    
    ktot_data_ax.text(-110., 0., 'Total Intensity', **text_kwargs)
    kpol_data_ax.text(-110., 0., 'Polarized Intensity', **text_kwargs)

    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/k_data_plot_noresid.png')
    plot.show()
