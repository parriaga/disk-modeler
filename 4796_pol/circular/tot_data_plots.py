import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
import copy
from pyklip.fmlib.diskfm import DiskFM
_log = logging.getLogger('hr4796a')
import matplotlib.pyplot as plot 
import configparser
from diskmodeler_circular import ModelComparator
from matplotlib import gridspec
import plotutils as gpipu
import matplotlib.patheffects as path_effects

def make_plot(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    mimk = pyfits.getdata('ktot_model_im.fits')
    mimj = pyfits.getdata('jtot_model_im.fits')
    
    dataimk = pyfits.getdata('ktot_data_im.fits')
    dataimj = pyfits.getdata('jtot_data_im.fits')

    stdimk = pyfits.getdata('ktot_stdim.fits')
    stdimj = pyfits.getdata('jtot_stdim.fits')

    allims = [mimk, mimj, dataimk, dataimj, stdimk, stdimj]
    for im in allims:
        im = ma.masked_invalid(im)
    cmap_idl = get_colormap_idl()


    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix
#    ylims = xlims_pix * pix_au
#    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.

    plot_kwargs_j = {'extent': extent,
                     'vmin': -500,
                     'vmax': 600,
                     'cmap': cmap_idl}
    plot_kwargs_k = {'extent': extent,
                     'vmin': -150,
                     'vmax': 200,
                     'cmap': cmap_idl}
    plot_kwargs_resids = {'extent': extent,
                     'vmin': -4,
                     'vmax': 4,
                          'cmap': 'seismic'}
 
    gs1 = gridspec.GridSpec(2,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8.5, 7)

#    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = -.9, right = .9)
    gs1.update(bottom = .17,wspace = 0, hspace = 0, left = 0.15, right = .85)
    jband_data_ax = plot.subplot(gs1[0,0])
    kband_data_ax = plot.subplot(gs1[1,0])

    jband_model_ax = plot.subplot(gs1[0,1])
    kband_model_ax = plot.subplot(gs1[1,1])

    jband_resid_ax = plot.subplot(gs1[0,2])    
    kband_resid_ax = plot.subplot(gs1[1,2])


    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .13, right = .84, hspace = 0)
    resid_cbar = plot.subplot(gs2[0,2])

    jband_data_ax.imshow(dataimj, **plot_kwargs_j)
    kband_data_ax.imshow(dataimk, **plot_kwargs_k)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    jband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    kband_data_ax.text(-95, 0, 'AU', **text_kwargs)

    jband_model = jband_model_ax.imshow(mimj, **plot_kwargs_j)
    kband_model = kband_model_ax.imshow(mimk, **plot_kwargs_k)

    jband_data_ax = plot.subplot(gs1[0,0])
    gpipu.compass([50, 40], length=15, ax = jband_data_ax, head_width = 4, head_length = 4, labeleast = True)


    jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_resids)
    resids = kband_resid_ax.imshow((mimk - dataimk) / stdimk, **plot_kwargs_resids)

    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Residuals', ticks = [-3, -2, -1, 0, 1, 2, 3])  

    all_ax = (kband_data_ax,jband_data_ax,
              kband_model_ax,jband_model_ax,
              kband_resid_ax,jband_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kband_model_ax,jband_model_ax,
                 kband_resid_ax,jband_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    hide_x_ax = (jband_model_ax,jband_resid_ax,jband_data_ax)
    for ax in hide_x_ax:
        ax.xaxis.set_visible(False)


    text_kwargs = {'fontsize': 15,
                   'ha': 'center'}
    #-25
    jband_data_ax.text(0, 100, 'Data', **text_kwargs)
    jband_model_ax.text(0, 100, 'Model', **text_kwargs)
    jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    

    kband_data_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_model_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
#    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])


    text_kwargs = {'fontsize': 17, 
                   'va': 'center',
                   'ha': 'right'}
    
    jband_data_ax.text(-110., 0., 'J', **text_kwargs)
    kband_data_ax.text(-110., 0., 'K1', **text_kwargs)

    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/tot_data_plot.png')
    plot.show()
    


def make_plot_unc(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    uncimk = pyfits.getdata('ktot_unconvolved_model.fits')
    uncimj = pyfits.getdata('jtot_unconvolved_model.fits')

    allims = [uncimk, uncimj]
    for im in allims:
        im = ma.masked_invalid(im)
    cmap_idl = get_colormap_idl()


    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.

 
    gs1 = gridspec.GridSpec(1,2)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8,6)

    gs1.update(wspace = 0, hspace = 0)
    jband_unc_ax = plot.subplot(gs1[0,0])
    kband_unc_ax = plot.subplot(gs1[0,1])


    plot_kwargs_j = {'extent': extent,
                     'vmin': 0,
                     'vmax': 500,
                     'cmap': cmap_idl}
    plot_kwargs_k = {'extent': extent,
                     'vmin': 0,
                     'vmax': 500,
                     'cmap': cmap_idl}

    jband_unc_ax.imshow(uncimj, **plot_kwargs_j)
    kband_unc_ax.imshow(uncimk, **plot_kwargs_k)
    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center'}

    jband_unc_ax.set_title('J')
    kband_unc_ax.set_title('K1')

    jband_unc_ax.text(-95, 0, 'AU', rotation = 90, **text_kwargs)
    jband_unc_ax.text(0, -110, 'AU', **text_kwargs)
    kband_unc_ax.text(0, -110, 'AU', **text_kwargs)



    gpipu.compass([50, 40], length=15, ax = jband_unc_ax, head_width = 4, head_length = 4, labeleast = True)


    all_ax = (kband_unc_ax,jband_unc_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    kband_unc_ax.yaxis.set_visible(False)

    text_kwargs = {'fontsize': 15,
                   'ha': 'center'}
    #-25
    
    kband_unc_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_unc_ax.set_xticks([-50, -25, 0, 25, 50])


    text_kwargs = {'fontsize': 17, 
                   'va': 'center',
                   'ha': 'right'}
    
#    jband_unc_ax.text(-110., 0., 'J', **text_kwargs)
#    kband_unc_ax.text(-110., 0., 'K1', **text_kwargs)

    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/tot_data_unc_plot.png')
    plot.show()
    

    

    
def get_colormap_idl():
    # colors 
    colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',
                                                                {'red':   ((0.0, 0.0, 0.0),
                                                                           (0.756, 0.0, 0.0),
                                                                           (1.0, 1.0, 1.0)),
                                                                 
                                                                 
                                                                 'green': ((0.0, 0.0, 0.0),
                                                                           (0.38, 0.0, 0.0),
                                                                           (1.00, 1.0, 1.0)),
                                                                 
                                                                 'blue':  ((0.00, 0.0, 0.0),
                                                                           (0.737, 1.0, 1.0),
                                                                           (1.00, 1.0, 1.0))
                                                            })
    colormap_idl_bluewhite.set_bad('black')

    return colormap_idl_bluewhite



# def all_combined(xlims_pix = 60, ylims_pix = 90):
#     print 'test'
#     #GPI parameters
#     pixelscale = .014
#     dist = 72.8
#     pix_au = dist * pixelscale

#     # reading in model, data, noise and unconvolved images
#     mimk = pyfits.getdata('ktot_model_im.fits')
#     mimj = pyfits.getdata('jtot_model_im.fits')
    
#     dataimk = pyfits.getdata('ktot_data_im.fits')
#     dataimj = pyfits.getdata('jtot_data_im.fits')

#     stdimk = pyfits.getdata('ktot_stdim.fits')
#     stdimj = pyfits.getdata('jtot_stdim.fits')

#     uncimk = pyfits.getdata('ktot_unconvolved_model.fits')
#     uncimj = pyfits.getdata('jtot_unconvolved_model.fits')

#     # Making bad pixels black
#     allims = [mimk, mimj, dataimk, dataimj, stdimk, stdimj, uncimk, uncimj]
#     for im in allims:
#         im = ma.masked_invalid(im)
#     cmap_idl = get_colormap_idl()

#     pixelscale = .014            # arcseconds / pix
#     dist = 72.8                   # parsecs,cn  
#     pix_au = dist * pixelscale   # AU / pix

#     cenx = 140
#     ceny = 140

#     extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
#     ylims = 70
#     xlims = 90

#     calibk = 5.7e-8             # Jy/ADU/coadd                                      
#     calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
#     calibj = 1.03e-7 * .73
#     calibj_as = calibj * 1000. / pixelscale**2.

#     plot_kwargs_j = {'extent': extent,
#                      'vmin': -500,
#                      'vmax': 600,
#                      'cmap': cmap_idl}
#     plot_kwargs_k = {'extent': extent,
#                      'vmin': -150,
#                      'vmax': 200,
#                      'cmap': cmap_idl}
#     plot_kwargs_resids = {'extent': extent,
#                      'vmin': -4,
#                      'vmax': 4,
#                           'cmap': 'seismic'}

#     plot_kwargs_j_unc = {'extent': extent,
#                      'vmin': 0,
#                      'vmax': 500,
#                          'cmap': cmap_idl}
#     plot_kwargs_k_unc = {'extent': extent,
#                      'vmin': 0,
#                      'vmax': 400,
#                          'cmap': cmap_idl}


 
#     gs1 = gridspec.GridSpec(2,4)  # y axis: 3 data plots + cbar, x: data,model,resids
#     fig = plot.figure()
#     fig.set_size_inches(9.3, 7)

# #    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = -.9, right = .9)
# #    gs1.update(bottom = .17,wspace = 0, hspace = 0, left = 0.15, right = .85)
#     gs1.update(bottom = .17,wspace = 0, hspace = 0, left = 0.1, right = .95)
#     jband_data_ax = plot.subplot(gs1[0,0])
#     kband_data_ax = plot.subplot(gs1[1,0])

#     jband_model_ax = plot.subplot(gs1[0,1])
#     kband_model_ax = plot.subplot(gs1[1,1])

#     jband_resid_ax = plot.subplot(gs1[0,2])    
#     kband_resid_ax = plot.subplot(gs1[1,2])

#     jband_unc_ax = plot.subplot(gs1[0,3])
#     kband_unc_ax = plot.subplot(gs1[1,3])



#     gs2 = gridspec.GridSpec(1, 4)    #cbar gridspec
#     gs2.update(top = .13, right = .93, hspace = 0)
#     resid_cbar = plot.subplot(gs2[0,2])

#     jband_data_ax.imshow(dataimj, **plot_kwargs_j)
#     kband_data_ax.imshow(dataimk, **plot_kwargs_k)

#     text_kwargs = {'fontsize': 10,
#                    'va': 'center',
#                    'ha': 'center',
#                    'rotation': 90}

#     jband_data_ax.text(-95, 0, 'AU', **text_kwargs)
#     kband_data_ax.text(-95, 0, 'AU', **text_kwargs)

#     jband_model = jband_model_ax.imshow(mimj, **plot_kwargs_j)
#     kband_model = kband_model_ax.imshow(mimk, **plot_kwargs_k)

#     jband_data_ax = plot.subplot(gs1[0,0])
#     gpipu.compass([50, 40], length=15, ax = jband_data_ax, head_width = 4, head_length = 4, labeleast = True)

#     jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_resids)
#     resids = kband_resid_ax.imshow((mimk - dataimk) / stdimk, **plot_kwargs_resids)

#     print plot_kwargs_j_unc
#     jband_unc_ax.imshow(uncimj, **plot_kwargs_j_unc)
#     kband_unc_ax.imshow(uncimk, **plot_kwargs_k_unc)


#     resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Residuals', ticks = [-3, -2, -1, 0, 1, 2, 3])  

#     all_ax = (kband_data_ax,jband_data_ax,
#               kband_model_ax,jband_model_ax,
#               kband_resid_ax,jband_resid_ax,
#               kband_unc_ax, jband_unc_ax)
#     for ax in all_ax:        
#         ax.set_xlim(xmin = -70, xmax = 70)
#         ax.set_ylim(ymin = -90, ymax = 90)

#     hide_y_ax = (kband_model_ax,jband_model_ax,
#                  kband_resid_ax,jband_resid_ax,
#                  kband_unc_ax, jband_unc_ax)
#     for ax in hide_y_ax:
#         ax.yaxis.set_visible(False)

#     hide_x_ax = (jband_model_ax,jband_resid_ax,jband_data_ax, jband_unc_ax)
#     for ax in hide_x_ax:
#         ax.xaxis.set_visible(False)


#     text_kwargs = {'fontsize': 12,
#                    'ha': 'center'}
#     #-25
#     jband_data_ax.text(0, 100, 'Data', **text_kwargs)
#     jband_model_ax.text(0, 100, 'Model', **text_kwargs)
#     jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
#     jband_unc_ax.text(0, 100, 'No-FM Model',wrap = True, **text_kwargs)
    

#     kband_data_ax.set_xticks([-50, -25, 0, 25, 50])
#     kband_model_ax.set_xticks([-50, -25, 0, 25, 50])
#     kband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
#     kband_unc_ax.set_xticks([-50, -25, 0, 25, 50])
# #    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
# #    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
# #    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])


#     text_kwargs = {'fontsize': 17, 
#                    'va': 'center',
#                    'ha': 'right'}
    
#     jband_data_ax.text(-110., 0., 'J', **text_kwargs)
#     kband_data_ax.text(-110., 0., 'K1', **text_kwargs)

#     plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/tot_data_unc_plot.png')
#     plot.show()
    


def all_combined(xlims_pix = 60, ylims_pix = 90):
    print 'test'
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    mimk = pyfits.getdata('ktot_model_im.fits')
    mimj = pyfits.getdata('jtot_model_im.fits')
    
    dataimk = pyfits.getdata('ktot_data_im.fits')
    dataimj = pyfits.getdata('jtot_data_im.fits')

    stdimk = pyfits.getdata('ktot_stdim.fits')
    stdimj = pyfits.getdata('jtot_stdim.fits')

    uncimk = pyfits.getdata('ktot_unconvolved_model.fits')
    uncimj = pyfits.getdata('jtot_unconvolved_model.fits')


    allims = [mimk, mimj, dataimk, dataimj, stdimk, stdimj, uncimk, uncimj]
    for im in allims:
        im = ma.masked_invalid(im)
    cmap_idl = get_colormap_idl()

    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.

    plot_kwargs_j = {'extent': extent,
                     'vmin': -500,
                     'vmax': 600,
                     'cmap': cmap_idl}
    plot_kwargs_k = {'extent': extent,
                     'vmin': -150,
                     'vmax': 200,
                     'cmap': cmap_idl}
    plot_kwargs_resids = {'extent': extent,
                     'vmin': -4,
                     'vmax': 4,
                          'cmap': 'PRGn'}

    plot_kwargs_j_unc = {'extent': extent,
                     'vmin': 0,
                     'vmax': 500,
                         'cmap': cmap_idl}
    plot_kwargs_k_unc = {'extent': extent,
                     'vmin': 0,
                     'vmax': 250,
                         'cmap': cmap_idl}


 
    gs1 = gridspec.GridSpec(2,4)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(9.3, 7)

#    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = -.9, right = .9)
#    gs1.update(bottom = .17,wspace = 0, hspace = 0, left = 0.15, right = .85)
    gs1.update(bottom = .17,wspace = 0, hspace = 0, left = 0.1, right = .95)
    jband_data_ax = plot.subplot(gs1[0,0])
    kband_data_ax = plot.subplot(gs1[1,0])

    jband_model_ax = plot.subplot(gs1[0,1])
    kband_model_ax = plot.subplot(gs1[1,1])

    jband_resid_ax = plot.subplot(gs1[0,2])    
    kband_resid_ax = plot.subplot(gs1[1,2])

    jband_unc_ax = plot.subplot(gs1[0,3])
    kband_unc_ax = plot.subplot(gs1[1,3])



    gs2 = gridspec.GridSpec(1, 4)    #cbar gridspec
    gs2.update(top = .13, right = .93, hspace = 0)
    resid_cbar = plot.subplot(gs2[0,2])

    jband_data_ax.imshow(dataimj, **plot_kwargs_j)
    kband_data_ax.imshow(dataimk, **plot_kwargs_k)

    text_kwargs = {'fontsize': 10,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    jband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    kband_data_ax.text(-95, 0, 'AU', **text_kwargs)

    jband_model = jband_model_ax.imshow(mimj, **plot_kwargs_j)
    kband_model = kband_model_ax.imshow(mimk, **plot_kwargs_k)

    jband_data_ax = plot.subplot(gs1[0,0])
    gpipu.compass([50, 40], length=15, ax = jband_data_ax, head_width = 4, head_length = 4, labeleast = True)

    jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_resids)
    resids = kband_resid_ax.imshow((mimk - dataimk) / stdimk, **plot_kwargs_resids)

    jband_unc_ax.imshow(uncimj, **plot_kwargs_j_unc)
    kband_unc_ax.imshow(uncimk, **plot_kwargs_k_unc)
    ellipse_arc(jband_unc_ax)
    ellipse_arc(kband_unc_ax)

    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Residuals', ticks = [-3, -2, -1, 0, 1, 2, 3])  

    all_ax = (kband_data_ax,jband_data_ax,
              kband_model_ax,jband_model_ax,
              kband_resid_ax,jband_resid_ax,
              kband_unc_ax, jband_unc_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kband_model_ax,jband_model_ax,
                 kband_resid_ax,jband_resid_ax,
                 kband_unc_ax, jband_unc_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    hide_x_ax = (jband_model_ax,jband_resid_ax,jband_data_ax, jband_unc_ax)
    for ax in hide_x_ax:
        ax.xaxis.set_visible(False)


    text_kwargs = {'fontsize': 12,
                   'ha': 'center'}
    #-25
    jband_data_ax.text(0, 100, 'Data', **text_kwargs)
    jband_model_ax.text(0, 100, 'Model', **text_kwargs)
    jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    jband_unc_ax.text(0, 100, 'No-FM Model',wrap = True, **text_kwargs)
    

    kband_data_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_model_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
    kband_unc_ax.set_xticks([-50, -25, 0, 25, 50])
#    kband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
#    kband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])


    text_kwargs = {'fontsize': 17, 
                   'va': 'center',
                   'ha': 'right'}
    
    jband_data_ax.text(-110., 0., 'J', **text_kwargs)
    kband_data_ax.text(-110., 0., 'K1', **text_kwargs)

    plot.savefig('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/circular/tot_data_unc_plot.png')
    plot.show()
    


def ellipse_arc(ax):
    import constants as c

    omega = (90 - 25.4) * n.pi /180
    Omega = 26.2* n.pi /180
    I = 76.56* n.pi /180
    r = 77.
    offset = 0.68

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    def get_NE(nu):
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)

        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        return pN, pE, phi_sca

    ## # draw line of nodes
    ## nu = n.array((-omega, n.pi-omega))
    ## pN, pE, phi_sca = get_NE(nu)
    ## ax.plot(pE, pN, ls='--', c='k', lw=1., zorder=1)

    # calculate ellipse
    nu = n.linspace(0, 0.908, 100, endpoint = True)
    pN, pE, phi_sca = get_NE(nu)
    ax.plot(-pE, pN, ls='-', c='red', lw=2., zorder=2, linewidth = 15, alpha = 0.6) #path_effects = [path_effects                                                                                         .PathPatchEffect(hatch='x', facecolor = 'gray', linewidth = 15, alpha = 0.4)])

#    nu = n.linspace(6.25, 2. * n.pi, 100, endpoint=True)
#    pN, pE, phi_sca = get_NE(nu)
#    ax.plot(-pE, pN, ls='-', c='k', lw=2., zorder=2, linewidth = 10, alpha = 0.2)
 
    nu = n.linspace(3.32, 3.85, 100, endpoint=True)
    pN, pE, phi_sca = get_NE(nu)
    ax.plot(-pE, pN, ls='-', c='red', lw=2., zorder=2, linewidth = 15, alpha = 0.6) #, path_effects = [path_effects                                                                                         .PathPatchEffect(hatch='x', facecolor = 'gray', linewidth = 15, alpha = 0.4, edgecolor = None)])

