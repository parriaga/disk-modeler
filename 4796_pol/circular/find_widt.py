import numpy as n
import matplotlib.pyplot as plot
from scipy.interpolate import interp1d
from scipy.optimize import fmin


def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y

x = n.linspace(0, 3., 1600)
x_b = 1.
x_0 = 1.
y_0 = 1
gam2 = -15.87
gam1 = 45.5
beta = 0
x_b_as = 1062.
x_max = 1377.
x_min = 964.1
ax = plot.subplot('111')
y = smooth_broken_power_law(x, 1., 1., 1., gam1, gam2, beta) 


plot.plot(x, y, label = 'x0 = 1')
y = smooth_broken_power_law(x, 1., 1., 2., gam1, gam2, beta) 
plot.plot(x, y, label = 'x0 = 2')
plot.legend()
plot.show()

x = x * x_b_as
y /= x
ax.plot(x, y, label = 'K1')


f = interp1d(x, y, kind = 'nearest')


y_fit = n.nanmax(y) / 2.
def minimize(x_fit):
    return n.abs(f(x_fit) - y_fit)

popt_1 = fmin(minimize, 940)
popt_2 = fmin(minimize, 1075)

print 'FWHM width ' + str(popt_1 - popt_2)

#plot.vlines(popt_1[0], 0, .001, label = 'half maximum 1045')
#plot.vlines(popt_2[0], 0, .001, label = 'half maximum 1105')

plot.vlines(x_min, 0, .001, color = 'red', label = 'limit')
plot.vlines(x_max, 0, .001, color = 'red', label = 'limit')
plot.legend()

ax.set_xlim([800, 1600])



x = n.linspace(0, 3., 1600)
x_b = 1.
x_0 = 1.
y_0 = 1
gam2 = -13
gam1 = 18
beta = 0
x_b_as = 1053.
x_max = 1377.
x_min = 964.1
ax = plot.subplot('111')
y = smooth_broken_power_law(x, 1., 1., 1., gam1, gam2, beta) 
x = x * x_b_as
y /= x
ax.plot(x, y, color = 'orange', label = 'H')


f = interp1d(x, y, kind = 'nearest')


y_fit = n.nanmax(y) / 2.
def minimize(x_fit):
    return n.abs(f(x_fit) - y_fit)

popt_1 = fmin(minimize, 940)
popt_2 = fmin(minimize, 1075)

print 'FWHM width ' + str(popt_1 - popt_2)

plot.vlines(popt_1[0], 0, .001, label = 'half maximum 1045')
plot.vlines(popt_2[0], 0, .001, label = 'half maximum 1105')

plot.legend()

ax.set_xlim([800, 1600])



plot.show()
