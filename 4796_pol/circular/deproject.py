import pyfits
import matplotlib.pyplot as plot
import numpy
import scipy.interpolate as interpol
from scipy.ndimage.filters import gaussian_filter
import pylab
#from hr4796a import OffsetRingModel
#import hr4796a
import scipy.ndimage.filters as filt
from diskmodeler_circular import ModelComparator
import numpy as n
import image
from matplotlib import gridspec
from diskmodeler_plotutils import *

    
def plot_profiles(mc, **kwargs):
    diskobj = mc.diskobjs[0] 

    scale = mc.scale
    gamma_in = mc.parms['gamma_in']
    gamma = mc.parms['gamma']
    beta = mc.parms['beta']
    offs = n.array((mc.parms[diskobj.label + 'soffy'], mc.parms[diskobj.label + 'soffx']))
    offset = mc.parms['offset']
    I = mc.parms['I']
    omega = mc.parms['omega2']
    Omega = mc.parms['omega1']

    if diskobj.psf_method == 'inputpsf':
        psf_im = diskobj.psf_im

    # get position/intensity profile                                       
    n_step = 100
    nu, r, phi_sca, r_proj, PA_proj, intensity = diskobj.parametric_model_ring(n_step=n_step)
    dnu = nu[1]-nu[0]
    assert nu[2]-nu[1] == dnu

    r_in, r_mid, r_out = diskobj.parms['r_in'], diskobj.parms['r'], diskobj.parms['r_out']
    
    if r_in is None:
        r_in = r_mid
    if r_out is None:
        r_out = r_mid
    drad0 = .125
    if drad0 > int(r_out - r_in):
        r_out = r_in + drad0
    n_step_rad = int((r_out-r_in)/drad0)
    drad = (r_out-r_in)/n_step_rad

    if n_step_rad > 1:
        r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid
    else:
        n_step_rad = 1
        r_factors = n.array((1.,))

    i_factors = smooth_broken_power_law(r_factors, 1., 1., 1., -gamma_in, -gamma, beta)/r_factors**2/n_step_rad
    r_axis = r_factors * r_mod

    print("gamma_in = " + str(gamma_in)[:5])
    print("gamma = " + str(gamma)[:5])
    print("r_c = " + str(diskobj.parms['r'])[:5])

    return r_axis, i_factors


    

def plot_series_with_im():
#    angs = n.linspace(0,360,80)
    
    # Spline point angles
#    angs = n.array([0.0, 0.21335875125753462, 0.4787841660388815, 1.2310771065577726, 
#                    2.3990117289342026, 2.8190995582316294, 3.02108408163149, 
#                    3.262101225548096, 3.464085748947957, 3.884173578245382, 
#                    5.052108200621809, 5.804401141140698, 6.0698265559220435,
#                    6.283185307179577]) * 180 / n.pi + 26.2 
    
    plot.clf()


    xcoords = n.array([-34.12364549911034, -10.299563576218867, 7.589740440816535, 
                       23.375365796223456, 34.19271285888921, 33.86609227336834]) + 140
    ycoords = n.array([69.3484046344364, 51.30464676158523, 23.711112371072, 
                       -8.438948763950604, -39.26691526031952, -68.82498736608787]) + 140

    im = pyfits.getdata('hpol_model_im.fits') 
    for i  in range(len(xcoords)):
        ax = plot.subplot('211')
        xcoord = xcoords[i]
        ycoord = ycoords[i]
        ang = 180. - n.arctan2(ycoord - 140., xcoord - 140.) * 180. / n.pi
        sample_rad(ang, ax, band = 'k', mode = 'pol', data = True, normalize = False)
        sample_rad(ang, ax, band = 'k', mode = 'pol', data = False, normalize = False)
#        plot.vlines(77, 0, 800)
#        plot.ylim([0,1500])
        ax2 = plot.subplot('212')
        plot.imshow(im)
        plot.scatter([xcoords[i]],[ycoords[i]], s = 50, c='white')
        plot.text(40, 40, 'Angle: ' + str(ang)[:5], color= 'white')
        sting = str(i) #str(int(ang))
        if len(sting) == 1:
            sting = '00' + sting
        elif len(sting) == 2:
            sting = '0' + sting
        print sting
        plot.savefig('kpol_series/' + sting + '.png')
        plot.clf()


def plot_ansae_sep(band = 'k'):
    xcoords_SW = n.array([20.921654867865833, 33.86609227336834, 36.76271084427712, 
                           32.984513051076895, 27.060143804403005, 19.76081295154047]) + 140
    ycoords_SW = n.array([-63.37405158121793, -68.82498736608787, -51.58534813102004, 
                           -34.9242620802104, -17.553666668626445, -0.238981459739678]) + 140
    xcoords_NE = n.array([-37.68948720740681, -34.12364549911034, -18.554607825485324, 
                           -7.579238561714792, 2.6101941371167596, 11.87815233025544]) + 140
    ycoords_NE = n.array([55.60354670173765, 69.3484046344364, 60.9780890138007, 
                           47.65877244888864, 32.23827010038681, 15.816122388208893]) + 140


    figure = plot.figure(figsize = [9, 6])
    gs = gridspec.GridSpec(2,1)

#    gs.update(hspace = 0, right = 0.88, left = 0.12)
    ax1 = plot.subplot(gs[0, 0])
    ax2 = plot.subplot(gs[1, 0])

    if band == 'k':
        ax1.set_title('K1 Polarized Intensity Radial Profiles')
    if band == 'j':
        ax1.set_title('J Polarized Intensity Radial Profiles')
    if band == 'h':
        ax1.set_title('H Polarized Intensity Radial Profiles')


    cs = ['b', 'r', 'g', 'darkviolet', 'darkred', 'orange']
    for i in n.arange(len(xcoords_NE) - 1):
        xcoord = xcoords_NE[i + 1]
        ycoord = ycoords_NE[i + 1]
        ang = 180. - n.arctan2(ycoord - 140., xcoord - 140.) * 180. / n.pi
        plot_kwargs_data = {'c': cs[i],
 #                           'label': 'Location ' + str(i + 1) + ' Data',
                            'linewidth': 2.0,
                            'alpha': 0.6,
                            'linestyle': '--'}
        xaxis, dataprof = sample_rad(ang, ax1,band = band, mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
        plot_kwargs_model = {'c': cs[i],
#                             'label': 'Location ' + str(i + 1) + ' Model',
                             'linewidth': 2.0}
        xaxis, prof = sample_rad(ang, ax1, band = band, mode = 'pol', data = False, normalize = True, **plot_kwargs_model)
        wh = n.where(prof == n.max(prof))[0][0]
        xtextloc = xaxis[wh]
        textheight = n.max(dataprof + .05)
        ax1.text(xtextloc, textheight, str(i + 1), horizontalalignment='center', weight = 'bold')

    ax1.plot([0, 0.01], [0,0.01], linewidth = 2.0, color = 'black', label = 'Model')
    ax1.plot([0, 0.01], [0,0.01], linewidth = 2.0, alpha = 0.6, linestyle = '--', color = 'black', label = 'Data')

    ax1.set_ylim([0,1.2])
    ax1.xaxis.set_visible(False)
    ax1.set_yticks([0.2, 0.4, 0.6, 0.8, 1.0])
    ax1.legend()#prop = {'size':10}, bbox_to_anchor = (1.15, .5))
    ax1.text(-12, 0.5, 'NE', ha = 'center', va = 'center')
    ax1.set_zorder(3)

    for i in n.arange(len(xcoords_SW) - 1):
        xcoord = xcoords_SW[i + 1]
        ycoord = ycoords_SW[i + 1]
        ang = 180. - n.arctan2(ycoord - 140., xcoord - 140.) * 180. / n.pi
        plot_kwargs_data = {'c': cs[i],
#                            'label': 'Location ' + str(i + 1) + ' Data',
                            'linewidth': 2.0,
                            'linestyle': '--',
                            'alpha': 0.6}
        xaxis, dataprof = sample_rad(ang, ax2, band = band, mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
        plot_kwargs_model = {'c': cs[i],
 #                            'label': 'Location ' + str(i + 1) + ' Model',
                             'linewidth': 2.0}
        xaxis, prof = sample_rad(ang, ax2, band = band, mode = 'pol', data = False, normalize = True, **plot_kwargs_model)
        wh = n.where(prof == n.max(prof))[0][0]
        xtextloc = xaxis[wh]
        textheight = n.max(dataprof) + .05
        ax2.text(xtextloc, textheight, str(i + 1), horizontalalignment='center', weight = 'bold')


    ax2.set_ylim([0, 1.2])

    ax2.set_xlabel('Projected Separation (Pixels)')
    ax2.text(-12, 0.5, 'SW', ha = 'center', va = 'center')
#    ax2.legend(prop = {'size':10})
    ax1.set_ylabel('Intensity (arbitrary)')
    ax2.set_ylabel('Intensity (arbitrary)')
    
    ax1.set_xlim([0, 100])
    ax2.set_xlim([0, 100])

#    handles, labels = ax1.get_legend_handles_labels()
    

    plot.show()
    if band == 'k':
        figure.savefig('kpol_radprofs.png')
    if band == 'h':
        figure.savefig('hpol_radprofs.png')
    if band == 'j':
        figure.savefig('jpol_radprofs.png')


def plot_onlyone_ansa(band = 'k'):
    xcoord_SW = 33.86609227336834 + 140
    ycoord_SW = -68.82498736608787 + 140
    xcoord_NE = -34.12364549911034 + 140
    ycoord_NE = 69.3484046344364 + 140


    figure = plot.figure(figsize = [6, 6])
    gs = gridspec.GridSpec(2,1)

    gs.update(hspace = 0, right = 0.88, left = 0.18)
    ax1 = plot.subplot(gs[0, 0])
    ax2 = plot.subplot(gs[1, 0])

    if band == 'k':
        ax1.set_title('K1 Polarized Intensity Radial Profiles')
    if band == 'j':
        ax1.set_title('J Polarized Intensity Radial Profiles')
    if band == 'h':
        ax1.set_title('H Polarized Intensity Radial Profiles')


    cs = ['b', 'r', 'g', 'darkviolet', 'darkred', 'orange']
    ang = 180. - n.arctan2(ycoord_NE - 140., xcoord_NE - 140.) * 180. / n.pi
    plot_kwargs_data = {'c': '#3B73FF',
                        'label': 'Data',
                        'linewidth': 2.0,
                        'linestyle': '--'}
    xaxis, dataprof = sample_rad(ang, ax1,band = band, mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
    plot_kwargs_model = {'c': '#ED0052',
                             'label': 'Model',
                         'linewidth': 2.0}
    xaxis, prof = sample_rad(ang, ax1, band = band, mode = 'pol', data = False, normalize = True, **plot_kwargs_model)

    ax1.set_ylim([0,0.9])
    ax1.xaxis.set_visible(False)
    ax1.set_yticks([0.2, 0.4, 0.6, 0.8])
    ax1.legend()#prop = {'size':10}, bbox_to_anchor = (1.15, .5))
    ax1.text(-12, 0.5, 'NE', ha = 'center', va = 'center')
    ax1.set_zorder(3)
    ax1.legend()

    ang = 180. - n.arctan2(ycoord_SW - 140., xcoord_SW - 140.) * 180. / n.pi

    plot_kwargs_model = {'c': '#ED0052', 
 #                            'label': 'Location ' + str(i + 1) + ' Model',
                         'linewidth': 2.0}
    xaxis, prof = sample_rad(ang, ax2, band = band, mode = 'pol', data = False, normalize = True, **plot_kwargs_model)


    plot_kwargs_data = {'c': '#3B73FF', 
                     
                        'linewidth': 2.0,
                        'linestyle': '--'}
    xaxis, dataprof = sample_rad(ang, ax2, band = band, mode = 'pol', data = True, normalize = True, **plot_kwargs_data)


    ax2.set_xlabel('Projected Separation (Pixels)')
    ax2.text(41, 0.45, 'SW', ha = 'center', va = 'center')
    ax1.text(41, 0.45, 'NE', ha = 'center', va = 'center')

    ax1.set_ylabel('Intensity (arbitrary)')
    ax2.set_ylabel('Intensity (arbitrary)')
    ax2.set_yticks([0.2, 0.4, 0.6, 0.8])
    ax2.set_ylim([0,.9])
    ax1.set_xlim([50, 100])
    ax2.set_xlim([50, 100])

    

    plot.show()


        

def plot_ansae_sep_all():
    # UNFINISHED

    xcoords_SW = n.array([33.86609227336834, 36.76271084427712, 
                           32.984513051076895, 27.060143804403005, 19.76081295154047]) + 140
    ycoords_SW = n.array([-68.82498736608787, -51.58534813102004, 
                           -34.9242620802104, -17.553666668626445, -0.238981459739678]) + 140
    xcoords_NE = n.array([-34.12364549911034, -18.554607825485324, 
                           -7.579238561714792, 2.6101941371167596, 11.87815233025544]) + 140
    ycoords_NE = n.array([69.3484046344364, 60.9780890138007, 
                           47.65877244888864, 32.23827010038681, 15.816122388208893]) + 140

    figure = plot.figure(figsize = [9, 11])
    gs = gridspec.GridSpec(6,1)

    gs.update(hspace = 0, right = 0.85, left = 0.15)
    ax_kne = plot.subplot(gs[0, 0])
    ax_ksw = plot.subplot(gs[1, 0])
    ax_jne = plot.subplot(gs[2, 0])
    ax_jsw = plot.subplot(gs[3, 0])
    ax_hne = plot.subplot(gs[4, 0])
    ax_hsw = plot.subplot(gs[5, 0])

    axes = [ax_kne, ax_ksw, ax_hne, ax_hsw, ax_jne, ax_jsw]
    bands = ['K1', 'H', 'J']

    ax1.set_title('Polarized Intensity Radial Profiles')
    cs = ['b', 'r', 'g', 'darkblue', 'darkred']
    for bandind in range(len(bands)):
        ne_ax = axes[bandind * 2]
        sw_ax = axes[bandind * 2]

        # Plot the lines
        for i in n.arange(len(xcoords_SW)):
            # Plot NE lines
            xcoord_NE = xcoords_NE[i]
            ycoord_NE = ycoords_NE[i]
            ang_NE = 180. - n.arctan2(ycoord_NE - 140., xcoord_NE - 140.) * 180. / n.pi

            # Plot data
            plot_kwargs_data = {'c': cs[i],
                                'label': 'Spline Point ' + str(i + 1) + ' Data',
                                'linewidth': 2.0,
                                'alpha': 0.6,
                                'linestyle': '--'}
            sample_rad(ang_NE, ne_ax, band = 'k', mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
            # Plot model
            plot_kwargs_model = {'c': cs[i],
                                 'label': 'Spline Point ' + str(i + 1) + ' Model',
                                 'linewidth': 2.0}
            sample_rad(ang_NE, ne_ax, band = 'k', mode = 'pol', data = False, normalize = True, **plot_kwargs_model)
        
            # Plot SW
            xcoord_SW = xcoords_SW[i]
            ycoord_SW = ycoords_SW[i]
            ang_SW = 180. - n.arctan2(ycoord_SW - 140., xcoord_SW - 140.) * 180. / n.pi

            #Plot data
            plot_kwargs_data = {'c': cs[i],
                                'label': 'Spline Point ' + str(i + 1) + ' Data',
                                'linewidth': 2.0,
                                'alpha': 0.6,
                                'linestyle': '--'}
            sample_rad(ang_SW, swax,band = 'k', mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
            #Plot model
            plot_kwargs_model = {'c': cs[i],
                                 'label': 'Spline Point ' + str(i + 1) + ' Model',
                                 'linewidth': 2.0}
            sample_rad(ang_SW, sw_ax, band = 'k', mode = 'pol', data = False, normalize = True, **plot_kwargs_model)

        # Label 
        axes[bandind * 2].text(-14, 0.5, bands[bandind] + ' NE', ha = 'center', va = 'center')
        axes[bandind * 2 + 1].text(-14, 0.5, bands[bandind] + ' SW', ha = 'center', va = 'center')

        if bandind != 2:
            axes[bandind * 2].xaxis.set_visible(False)
            axes[bandind * 2 + 1].xaxis.set_visible(False)
        if bandind != 2:
            axes.xaxis.set_visible(False)

        
    ax1.set_ylim([0,1])
    ax1.set_yticks([0.2, 0.4, 0.6, 0.8, 1.0])
    ax1.legend(prop = {'size':10}, bbox_to_anchor = (1.2, .5))
    ax1.text(-14, 0.5, 'NE', ha = 'center', va = 'center')
    ax1.set_zorder(3)

    for i in n.arange(len(xcoords_SW)):
        xcoord = xcoords_SW[i]
        ycoord = ycoords_SW[i]
        ang = 180. - n.arctan2(ycoord - 140., xcoord - 140.) * 180. / n.pi
        plot_kwargs_data = {'c': cs[i],
                            'label': 'Spline Point ' + str(i + 1) + ' Data',
                            'linewidth': 2.0,
                            'linestyle': '--'}
        sample_rad(ang, ax2, band = 'k', mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
        plot_kwargs_model = {'c': cs[i],
                             'label': 'Spline Point ' + str(i + 1) + ' Model',
                             'linewidth': 2.0}
        sample_rad(ang, ax2, band = 'k', mode = 'pol', data = False, normalize = True, **plot_kwargs_model)
    ax2.set_ylim([0, 1])

    ax2.set_xlabel('Project Separation (Pixels)')
    ax2.text(-14, 0.5, 'SW', ha = 'center', va = 'center')
#    ax2.legend(prop = {'size':10})
    ax1.set_ylabel('Intensity (arbitrary)')
    ax2.set_ylabel('Intensity (arbitrary)')
    
    ax1.set_xlim([0, 100])
    ax2.set_xlim([0, 100])

    handles, labels = ax1.get_legend_handles_labels()
    

    plot.show()
    figure.savefig('kpol_radprofs.png')
    




def plot_pol_data_model_profiles():
    plot.figure(figsize=(15, 10))
    gs1 = gridspec.GridSpec(3, 2)
    gs1.update(wspace = 0, hspace = 0)
    ax1 = plot.subplot(gs1[0])

    kpsf_imname = '/home/pauline/gpi_disks/hr4796a_new/psfs/kband_psf.txt'
    kpsf_line = n.loadtxt(kpsf_imname) 
    kpsf_line /= n.max(kpsf_line)

    plot.plot(n.arange(len(kpsf_line)) + 43.6, kpsf_line, color = 'darkgrey')
    sample_rad(0, band='k', mode = 'pol', data = True, sw = True)
    sample_rad(0, band='k', mode = 'pol', data = False, sw = True)
    ax1.get_xaxis().set_ticks([])

    plot.ylabel('Normalized Intensity')

    ax2 = plot.subplot(gs1[1])
    sample_rad(0, band='k', mode = 'pol', data = True, sw = False)
    sample_rad(0, band='k', mode = 'pol', data = False, sw = False)
    ax2.get_xaxis().set_ticks([])
    ax2.get_yaxis().set_ticks([])

    ax3 = plot.subplot(gs1[2])
    plot.ylabel('Normalized Intensity')
    sample_rad(0, band='j', mode = 'pol', data = True, sw = True,normalize = True)
    sample_rad(0, band='j', mode = 'pol', data = False, sw = True,normalize = True)
    ax3.get_xaxis().set_ticks([])

    ax4 = plot.subplot(gs1[3])
    sample_rad(0, band='j', mode = 'pol', data = True, sw = False,normalize = True)
    sample_rad(0, band='j', mode = 'pol', data = False, sw = False,normalize = True)
    ax4.get_xaxis().set_ticks([])
    ax4.get_yaxis().set_ticks([])

    ax5 = plot.subplot(gs1[4])
    plot.ylabel('Normalized Intensity')
    sample_rad(0, band='h', mode = 'pol', data = True, sw = True)
    sample_rad(0, band='h', mode = 'pol', data = False, sw = True)
    plot.xlabel('Distance from star (AU)')

    ax6 = plot.subplot(gs1[5])
    sample_rad(0, band='h', mode = 'pol', data = True, sw = False)
    sample_rad(0, band='h', mode = 'pol', data = False, sw = False)
    plot.xlabel('Distance from star (AU)')
    ax6.get_yaxis().set_ticks([])

    plot.savefig('radial_profs.png')




def sample_rad(angle, ax, band = 'k', mode = 'pol', data = True, sw = True, normalize = True, plot_errs = False, **plot_kwargs):
    plot_ktot_data = False
    plot_kpol_data = False
    plot_kpol_model = False
    plot_hpol_data = False
    plot_hpol_model = False
    plot_jpol_data = False
    plot_jpol_model = False
    if band == 'k':
        if mode == 'tot':
            plot_ktot_data = True
        if mode == 'pol':
            if data is True:
                plot_kpol_data = True
            else:
                plot_kpol_model = True

    if band == 'h':
        if mode == 'pol':
            if data is True:
                plot_hpol_data = True
            if data is False:
                plot_hpol_model = True

    if band == 'j':
        if mode == 'pol':
            if data is True:
                plot_jpol_data = True
            else:
                plot_jpol_model = True 

    if plot_ktot_data is True:
        im = pyfits.getdata('/home/pauline/gpi_disks/hr4796a_new/reduced_data/april_k/kl1-minrot10.fits')
        noise = pyfits.getdata('/home/pauline/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/ktot_noisemap.fits')
        
        totbins_ne, totbins_sw = get_imbins(im, noise, angle = angle) 
        tot_imbins_ne, tot_noisebins_ne = totbins_ne
        tot_imbins_sw, tot_noisebins_sw = totbins_sw
        if sw is True:
            ax.errorbar(range(len(tot_imbins_sw)), tot_imbins_sw, yerr = tot_noisebins_sw, label = 'K1 Tot Data SW', **plot_kwargs)
            return range(len(tot_imbins_sw)), tot_imbins_sw
        else:
            ax.errorbar(range(len(tot_imbins_ne)), tot_imbins_ne, yerr = tot_noisebins_ne, label = 'K1 Tot Data NE', **plot_kwargs)
            return range(len(tot_imbins_ne)), tot_imbins_ne
    
        
    if plot_kpol_data is True:
        im = pyfits.getdata('/home/pauline/gpi_disks/hr4796a_new/files_for_fit/HR4796A-K1-Qr-masked.fits')
        noise = pyfits.getdata('/home/pauline/gpi_disks/hr4796a_new/code/4796_pol/circular/kpol_stdim.fits')

#        im = pyfits.getdata('deinc_kpol_data.fits')
        polbins_ne, polbins_sw = get_imbins(im, noise, angle = angle) 
        pol_imbins_ne, pol_noisebins_ne = polbins_ne
        pol_imbins_sw, pol_noisebins_sw = polbins_sw    
        if normalize is True:
            nem = n.max(pol_imbins_ne)
            swm = n.max(pol_imbins_sw)
            pol_imbins_ne /= 1400. #n.max(pol_imbins_ne)
            pol_imbins_sw /= 1400. #n.max(pol_imbins_sw)
#            pol_noisebins_ne /= nem
#            pol_noisebins_sw /= swm
        if sw is True:
            if plot_errs == True:
                ax.errorbar(n.arange(len(pol_imbins_sw)), pol_imbins_sw, yerr = pol_noisebins_sw, **plot_kwargs)
            else: 
                ax.plot(n.arange(len(pol_imbins_sw)), pol_imbins_sw,  **plot_kwargs)
            return range(len(pol_imbins_sw)), pol_imbins_sw

        else:
            if plot_errs == True:
                ax.errorbar(range(len(pol_imbins_ne)), pol_imbins_ne, yerr = pol_noisebins_ne, **plot_kwargs)
            else:
                ax.plot(n.arange(len(pol_imbins_ne)), pol_imbins_ne, **plot_kwargs)
            return range(len(tot_imbins_ne)), tot_imbins_ne



    if plot_kpol_model is True:
        im = pyfits.getdata('kpol_model_im.fits')
        noise = pyfits.getdata('/home/pauline/gpi_disks/hr4796a_new/code/4796_pol/circular/kpol_stdim.fits')
        modelbins_ne, modelbins_sw = get_imbins(im, noise, angle = angle) 
        model_imbins_ne, model_noisebins_ne = modelbins_ne
        model_imbins_sw, model_noisebins_sw = modelbins_sw

        if normalize is True:
            model_imbins_ne /= 1400. #n.max(model_imbins_ne)
            model_imbins_sw /= 1400. #n.max(model_imbins_sw)

        if sw is True:
            ax.plot(range(len(model_imbins_sw)), model_imbins_sw, **plot_kwargs)
            return range(len(model_imbins_sw)), model_imbins_sw
        else:
            ax.plot(range(len(model_imbins_ne)), model_imbins_ne, **plot_kwargs)
            return range(len(model_imbins_ne)), model_imbins_ne
                    
    if plot_hpol_model is True:
        im = pyfits.getdata('hpol_model_im.fits')
        noise = pyfits.getdata('hpol_stdim.fits')
        modelbins_ne, modelbins_sw = get_imbins(im, noise, angle = angle) 
        model_imbins_ne, model_noisebins_ne = modelbins_ne
        model_imbins_sw, model_noisebins_sw = modelbins_sw
        
        if normalize is True:
            nem = n.max(model_imbins_ne)
            swm = n.max(model_imbins_sw)
            model_imbins_ne /= 1700. #n.max(model_imbins_ne)
            model_imbins_sw /= 1700. #n.max(model_imbins_sw)

        if sw is True:
            ax.plot(range(len(model_imbins_sw)), model_imbins_sw, **plot_kwargs) 
            return range(len(model_imbins_sw)), model_imbins_sw
        else:
            ax.plot(n.arange(len(model_imbins_ne)), model_imbins_ne, **plot_kwargs) 
            return range(len(model_imbins_ne)), model_imbins_ne
    if plot_hpol_data is True:
        im = pyfits.getdata('hpol_data_im.fits')
        noise = pyfits.getdata('hpol_stdim.fits')
        polbins_ne, polbins_sw = get_imbins(im, noise, angle = angle) 
        pol_imbins_ne, pol_noisebins_ne = polbins_ne
        pol_imbins_sw, pol_noisebins_sw = polbins_sw    

        if normalize is True:
            nem = n.max(pol_imbins_ne)
            swm = n.max(pol_imbins_sw)
            pol_imbins_ne /= 1700. #n.max(pol_imbins_ne)
            pol_imbins_sw /= 1700. #n.max(pol_imbins_sw)
            pol_noisebins_ne /= 1700 #nem

            pol_noisebins_sw /= 1700 #swm
        if sw is True:
            if plot_errs == True:
                ax.errorbar(n.arange(len(pol_imbins_sw)), pol_imbins_sw, yerr = pol_noisebins_sw, **plot_kwargs)
            else:
                ax.plot(n.arange(len(pol_imbins_sw)), pol_imbins_sw, **plot_kwargs)
            return range(len(pol_imbins_sw)), pol_imbins_sw
        else:
            if plot_errs == True:
                ax.errorbar(range(len(pol_imbins_ne)), pol_imbins_ne, yerr = pol_noisebins_ne, **plot_kwargs)
            else:
                ax.plot(n.arange(len(pol_imbins_ne)), pol_imbins_ne, **plot_kwargs)
            return range(len(model_imbins_ne)), model_imbins_ne
    if plot_jpol_model is True:
        im = pyfits.getdata('jpol_model_im.fits')
        noise = pyfits.getdata('jpol_stdim.fits')

        modelbins_ne, modelbins_sw = get_imbins(im, noise, angle = angle) 
        model_imbins_ne, model_noisebins_ne = modelbins_ne
        model_imbins_sw, model_noisebins_sw = modelbins_sw
        
        if normalize is True:
            nem = n.max(model_imbins_ne)
            swm = n.max(model_imbins_sw)
            model_imbins_ne /= 1500. # n.max(model_imbins_ne)
            model_imbins_sw /= 1500. #n.max(model_imbins_sw)
            model_noisebins_ne /= 1500. #nem
            model_noisebins_sw /= 1500. #swm
            
        if sw is True:
            ax.plot(range(len(model_imbins_sw)), model_imbins_sw, **plot_kwargs)
            return range(len(model_imbins_sw)), model_imbins_sw
        else:
            ax.plot(n.arange(len(model_imbins_ne)), model_imbins_ne, **plot_kwargs)
            return range(len(model_imbins_ne)), model_imbins_ne

    if plot_jpol_data is True:
        im = pyfits.getdata('jpol_data_im.fits')
        noise = pyfits.getdata('deinc_jpol_data.fits')
        polbins_ne, polbins_sw = get_imbins(im, noise, angle = angle) 
        pol_imbins_ne, pol_noisebins_ne = polbins_ne
        pol_imbins_sw, pol_noisebins_sw = polbins_sw    

        if normalize is True:
            nem = n.max(pol_imbins_ne)
            swm = n.max(pol_imbins_sw)
            pol_imbins_ne /= 1500. #n.max(pol_imbins_ne)
            pol_imbins_sw /= 1500. #n.max(pol_imbins_sw)
            pol_noisebins_ne /= 1500. #nem
            pol_noisebins_sw /= 1500. #swm

        if sw is True:
            if plot_errs == True:
                ax.errorbar(n.arange(len(pol_imbins_sw)), pol_imbins_sw, yerr = pol_noisebins_sw, **plot_kwargs)
            else:
                ax.plot(n.arange(len(pol_imbins_sw)), pol_imbins_sw, **plot_kwargs)
            return range(len(pol_imbins_sw)), pol_imbins_sw
        else:
            if plot_errs ==True:
                ax.errorbar(range(len(pol_imbins_ne)), pol_imbins_ne, yerr = pol_noisebins_ne, **plot_kwargs)
            else:
                ax.plot(n.arange(len(pol_imbins_ne)), pol_imbins_ne, **plot_kwargs)
            return range(len(pol_imbins_sw)), pol_imbins_sw

    if normalize is True:
        plot.ylim([-0.1, 1.2])
#    plot.xlim([50,100])

def get_imbins(im, noise, angle = 0):
    ''' 
    input:
    image - 140, 140
    noise - noisemap
    angle in degrees 
    '''
    rotim = image.rotate(im, (angle) * n.pi / 180)

    rotim = rotim[138:142]
    noise = noise[138:142]

    binsize = 1
    bins = n.arange(281)
    imbins = []
    noisebins = []
    nelems = 10 * 3
    for b in bins:
        lind = b * binsize
        uind = b * binsize + 3
        sl = rotim[:, lind:uind]
        sl = sl[~n.isnan(sl)]
        imbins.append(n.sum(sl))
        noisebins.append(n.median(noise[:, lind:uind]))
    imbins = n.array(imbins)
    noisebins = n.array(noisebins) * nelems
    
    imbins1 = imbins[140:]
    noisebins1 = noisebins[140:]

    imbins2 = n.flip(imbins[:140],0)
    noisebins2 = n.flip(noisebins[:140],0)

    return [imbins1, noisebins1], [imbins2, noisebins2]
    



    




def deproject(im, Omega = -27.8 * numpy.pi / 180., I = 77 * numpy.pi / 180., omega = -2.3 * numpy.pi / 180., offset = 0):
    ''' 
    Deproject - Given a disk's orbital parameters, gives the azimuthal 
    profile of the disk

    Omega, I, omega = Orbital parameters. Angles in radians
    rmin, rmax      = Bounds of the image deprojection, in pixels
    
    '''
    rmin = 50
    rmax = 130

    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]





    def rnu_to_NEzr(r, nu):
        "geometric transformation"
        sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
        
        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si
        
        pN -= oN 
        pE -= oE 
        pz -= oz
        
        r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
        
        return pN, pE, pz, r_star



    #Must set nans to zero otherwise interpolation errors
    im[numpy.isnan(im)] = 0
        


    #interpolation grid
    rimin, rimax = rmin-5., rmax+5. # [pix], [pix]
    #Evaluated numbers of rs and nus. Increase for finer sampling
    nri, nnui = 150, 100
#    nri, nnui = 20, 40
    dri = (rimax-rimin)/nri
    rri, nuis = numpy.mgrid[0:nri, 0:nnui]
    rri = rri*dri+rimin # [pix]
    nuis = nuis*2. *numpy.pi/nnui



    # coordinates in observed image
    pN, pE, pz, r_star = rnu_to_NEzr(rri, nuis)

    # This is assuming an image centered around (140,140)
    Nmin = -140. 
    Emin = -140. 
    dN = 1.
    dE = 1.

    # perform interpolation
    from scipy.ndimage import map_coordinates
    iy = (pN-Nmin)/dN
    ix = (pE-Emin)/dE

    #Displays interpolation points
    plot.imshow(im)
    plot.scatter(ix, iy)
    plot.show()
    im = map_coordinates(im, (iy, ix))

    # multiply by jacobian
#    detJ = rri*ci
#    im *= detJ
    
    # multiply by r_star^2
#    im *= r_star**2
    im = numpy.append(im[:, 50:], im, axis = 1)
#    print im
    # show recovered map
    fig = pylab.figure(1)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)
    cax = ax.imshow(im,
                    extent=(nuis.min()*180./numpy.pi,
                            nuis.max()*180./numpy.pi,
                            rri.min(), rri.max()),
                    interpolation='nearest')
    ax.set_aspect('auto')
    ax.set_xlabel(r"$\nu$ [deg]")
    ax.set_ylabel(r"$r$ [pix]")
    fig.colorbar(cax)
    pylab.draw()
    pylab.show()
#    im = numpy.sum(im, axis = 0)
#    plot.plot(im)
#    plot.show()
    return im






def fit_sbpl(params, x, y):
    x_b, x_0, y_0, gam1, gam2, beta = params
    beta = n.exp(beta)
    ymodel = smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta)
    return (y - ymodel)**2.

def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y





def get_de_im():
    #Only deinclines the disk,
    maxrad = 90
    maxphi = 360
    inc = 77. * numpy.pi / 180.
    center = [140,140]

    conf = 'hr4796_config_tester.ini'
    mc = ModelComparator(conf)
    
#    mc.update_all_int_parms([1,1,1])
    Omega = mc.parms['omega1'] * 180. / numpy.pi
    

#    mc.parms['omega2'] = 0
#    mc.parms['K1_Polsoffx'] = 0
#    mc.parms['K1_Polsoffy'] = 0
#    mc.update_all_parms(mc.parms)
#    
#   im = mc.diskobjs[0].get_model_ring_image()
    
    im = pyfits.getdata('k1_mcfost_unconvolved.fits')

    sh = im.shape
    x, y = numpy.meshgrid(numpy.arange(sh[0]), numpy.arange(sh[1]))
    x_u = []
    y_u = []
    vals = []

    r, phi = numpy.meshgrid(numpy.arange(maxrad), numpy.arange(maxphi)- 180)
    r_u = []
    phi_u = []
    vals = []

    for i in numpy.arange(sh[0]):
        for j in numpy.arange(sh[1]):
            if not numpy.isnan(im[i,j]) and im[i,j] < 300:
                phi = Omega * numpy.pi / 180.
                x_c = (i-center[0]) * numpy.cos(phi) - (j-center[1]) * numpy.sin(phi) + center[0]
                y_c = (i-center[0]) * numpy.sin(phi) + (j-center[1]) * numpy.cos(phi) 
                vals.append(im[i,j] * numpy.cos(77.3 * numpy.pi / 180.))

                y_c = y_c / numpy.cos(77 * numpy.pi / 180.) + center[1]
#                y_c += offset * numpy.cos(omega)
#                x_c += offset * numpy.sin(omega)
                x_u.append(x_c)
                y_u.append(y_c)

#    plot.show()
    grid = interpol.griddata(numpy.array([x_u, y_u]).transpose(), vals, (x, y), method = 'linear')
#    pyfits.writeto('grid.fits', grid)
#    grid = numpy.zeros(x.shape)
#    for x_c, y_c in x, y:
    pyfits.writeto('deproj_mcfost.fits', grid)

    plot.imshow(grid)
    plot.show()

    return grid
    









def deincline():
    #Only deinclines the disk,
    maxrad = 200
    maxphi = 360
    inc = 1.3382898773327323
    center = [140,140]
    Omega = 0.46604799253833273
#    im = pyfits.getdata('flatring.fits')
    im = pyfits.getdata('jpol_data_im.fits')
    sh = im.shape
    x, y = numpy.meshgrid(numpy.arange(sh[0] ), numpy.arange(sh[1]))
    x_u = []
    y_u = []
    vals = []

    r, phi = numpy.meshgrid(numpy.arange(maxrad), numpy.arange(maxphi)- 180)
    r_u = []
    phi_u = []
    vals = []

    for i in numpy.arange(sh[0]):
        for j in numpy.arange(sh[1]):
            if not numpy.isnan(im[i,j]) and im[i,j] < 300:
                phi = Omega 
                x_c = (i-140.) * numpy.cos(phi) - (j-140.) * numpy.sin(phi) + 140
                y_c = (i-140.) * numpy.sin(phi) + (j-140) * numpy.cos(phi) 
                vals.append(im[i,j] * numpy.cos(77.3 * numpy.pi / 180.))

                y_c = y_c / numpy.cos(77 * numpy.pi / 180.) + 140.
#                y_c += offset * numpy.cos(omega)
#                x_c += offset * numpy.sin(omega)
                x_u.append(x_c)
                y_u.append(y_c)

#    plot.show()
    grid = interpol.griddata(numpy.array([x_u, y_u]).transpose(), vals, (x, y), method = 'linear')
#    pyfits.writeto('grid.fits', grid)
#    grid = numpy.zeros(x.shape)
#    for x_c, y_c in x, y:
        
#    grid = gaussian_filter(grid, 3)
    plot.imshow(grid, vmin = 0)
    plot.plot([140,140],[1,280], lw = 3, color = 'w')
    plot.plot([1,280],[140,140], lw = 3, color = 'w')
    plot.plot([140 - 76,140-76],[1,280], lw = 1, color = 'w')
    plot.plot([1,280],[140-76,140-76], lw = 2, color = 'w')
    plot.plot([140+76,140+76],[1,280], lw = 2, color = 'w')
    plot.plot([1,280],[140+76,140+76], lw = 2, color = 'w')
    plot.colorbar()
    plot.show()

    return grid
    







#####################################################################################
#####################################################################################
def plot_psf():
    sample_rad(0, band='k', mode = 'pol', data = False, sw = True)
    psf_imname = '/home/pauline/gpi_disks/hr4796a_new/psfs/kband_psf.txt'
    psf_line = n.loadtxt(psf_imname) 
    psf_line /= n.max(psf_line)
    plot.plot(n.arange(len(psf_line)) + 43.6, psf_line)
    plot.show()



def oplot_rads():
    sample_rad(0, band='k', mode = 'pol', data = False, sw = True,normalize = True)
    sample_rad(0, band='j', mode = 'pol', data = False, sw = True,normalize = True)
    sample_rad(0, band='h', mode = 'pol', data = False, sw = True,normalize = True)
    plot.legend()
    plot.show()

def test_flat_ring_model():
    psf = pyfits.getdata('psf_k.fits')
    im = OffsetRingModel(psf_method = 'inputim', psf_ims = [psf])
    im.parms['r_in'] = im.parms['r'] - 5
    im.parms['r_out'] = im.parms['r'] + 5
    im.parms['offset'] = 1.5
    im.parms['gamma_in'] = -18
    im.parms['gamma'] = 4.
    im.parms['beta'] = 0.
    im.parms['I'] = 70. * numpy.pi / 180.
    im.parms['Omega'] = 25. * numpy.pi / 180.
    scale = 1.
    model = im.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = 2., n_step = 200, fast = True)
    return model

def test_h():
    conf = 'hr4796_config_hpsf.ini'
    leastsq_save = 'leastsq_hpol.p'
    mc = ModelComparator(conf, leastsq_save = leastsq_save)
    diskobj = mc.diskobjs[0]
    p = mc.parms
    model = diskobj.get_model()
    image = deproject(model, Omega = -p['omega1'], I = p['I'], omega = p['omega2'], offset = p['offset'])
    return image



def polcoords(im = None):
    ''' 
    Only transforms from Cartesion to polar
    '''
    maxrad = 140
    maxphi = 360
    center = [140,140]
    if im is None:
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    shape = im.shape
    r, phi = numpy.meshgrid(numpy.arange(maxrad *4. )/4. + 1, numpy.arange(maxphi*4.)/4. - 180.)
    r_u = []
    phi_u = []
    vals = []
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
            if not numpy.isnan(im[i,j]):
                phi_c = numpy.arctan2((j - center[0]),(i - center[1])) * 180. / numpy.pi
                r_c = numpy.sqrt((center[0] - i)**2. + (center[1] - j)**2.)
                if r_c * numpy.sin(phi_c) != 0:
                    r_u.append(r_c)
                    phi_u.append(phi_c)
                    vals.append(im[i, j] )
    grid = interpol.griddata(numpy.array([r_u, phi_u]).transpose(), vals, (r, phi), method = 'cubic')
    plot.clf()
    plot.imshow(grid.transpose(), vmin = 0, vmax = 100, extent=[0,360,0,140])
    plot.plot([1,360],[76.4,76.4],color = 'white', lw = 3)
    cm = plot.get_cmap('gist_rainbow')
    for i in numpy.arange(24):
        sli = i * 10. + 60
        plot.plot([sli,sli],[1,140],color = cm(i * 10), lw = 2)

    plot.ylabel('Radius (AU)')
    plot.xlabel('Angle (degrees)')
    plot.colorbar()
    plot.show()

    ch = []

    for i in numpy.arange(24):
        sli = (i * 10. + 60) * 4
        chunk = numpy.median(grid[sli:sli+40], axis = 0) 
        chunk = chunk / numpy.nanmax(chunk)#+ .1* i
        plot.plot(numpy.arange(maxrad *4.)/4., chunk, color = cm(i * 10))
    plot.xlabel('Radius (AU')
    plot.ylabel('Scaled intensity')
    plot.plot([76.4,76.4],[1,1], lw = 3)
    plot.show()






def test1(imname):
    import numpy as n
    im = pyfits.getdata(imname)
    axis = n.arange(n.shape(im)[0]) - 140.
    axis *= 72.8 * .014
    import image

    x_b_init = 77.
    x_0_init = 60.
    y_0_init = 10.
    gam1_init = 8.
    gam2_init = -8.
    beta_init = -1.6
    y0s = []
    xbs = []
    angs = []
    from scipy.optimize import leastsq
    for i in n.arange(280 ):
        angle = i / 8.
        rotim = image.rotate(im, (angle + 75) * n.pi / 180)
        y = n.nansum(rotim[135:145], axis = 0)
        y = y[50:95]
        xaxis = n.abs(axis[50:95])
        init_params = [x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init]
#        model = smooth_broken_power_law(xaxis, x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init)
        p_opt, cov, info, mesg, ier = leastsq(fit_sbpl, init_params, args=(xaxis, y), full_output = True)
        x_b, x_0, y_0, gam1, gam2,beta = p_opt
        xaxis_smooth = n.linspace(50, 100, 200)
        mod = smooth_broken_power_law(xaxis_smooth, x_b, x_0, y_0, gam1, gam2, n.exp(beta)) 
        y0s.append(n.max(mod))
        xbs.append(x_b)
        angs.append(angle)
#        plot.plot(axis, y)
#        angles.append(angle)
#        wh = n.where(y == n.nanmax(y[50:90]))
#        maxval.append(axis[wh])
#    plot.show()
    plot.plot(angs, y0s)
    plot.xlabel('Angle from NE ansa (degrees)')
    plot.ylabel('SBPL parameter')
    plot.show()
    return y0s

def plot_all_profiles():
    mc = ModelComparator('hr4796_config_hpol.ini', mc_save = 'mcmc_hpol_even2.p')

    mc.update_all_parms(mc.parms)
    show_model(mc.diskobjs[0],convolve_resids = False, vmin_data = -20, vmax_data = 120, vmin_resids = -2, vmax_resids = 2)
    plot_profiles(mc, label = 'H')
    mc2 = ModelComparator('hr4796_config_kpol.ini', mc_save = 'mcmc_kpol_even.p')
    plot_profiles(mc2, label = 'K')
  
    mc.print_parms()
    mc2.print_parms()
    
    mc3 = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol_even.p')
    plot_profiles(mc3, label = 'J')
    plot.legend()
    plot.show()



