from phase_curves import *

conf = 'hr4796_config_jpol.ini'
mc_save = 'mcmc_jpol_intparms_method3.p'
write_out(conf, mc_save, True, 'jpol_NE.txt')
write_out(conf, mc_save, False, 'jpol_SW.txt')

conf = 'hr4796_config_kpol.ini'
mc_save = 'mcmc_kpol_intparms_method3.p'
write_out(conf, mc_save, True, 'kpol_NE.txt')
write_out(conf, mc_save, False, 'kpol_SW.txt')

conf = 'hr4796_config_hpol.ini'
mc_save = 'mcmc_hpol_intparms_method3.p'
write_out(conf, mc_save, True, 'hpol_NE.txt')
write_out(conf, mc_save, False, 'hpol_SW.txt')
