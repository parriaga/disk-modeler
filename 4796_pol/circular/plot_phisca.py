def parametric_model_ring(mc, n_step = 1601):
             
    """
    Parametric representation of an eccentric ring.
    
    Inputs:
    r           [AU]  radius from ring center
    r_out       [AU]  outer radius
    offset      [AU]  offset size in disk plane
    I           [rad] inclination
    omega       [rad] argument of pericenter (for offset direction)
    Omega       [rad] longitude of ascending node
    """

    r = mc.parms['r']
    offset = mc.parms['offset']
    I = mc.parms['I']
    omega = mc.parms['omega2']
    Omega = mc.parms['omega1']

    # get parameteric construction of ring position
    nu = n.linspace(0., 2.*n.pi, n_step, endpoint=False) # [rad]

    so, co = n.sin(omega), n.cos(omega)
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
        
    #Euler angle transformation
    pN_noff = r * (cO*conu-sO*sonu*ci) # [AU]
    pE_noff = r * (sO*conu+cO*sonu*ci) # [AU]
    pz_noff = r * sonu*si

    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    
    #Subtract offset angles
    pN = pN_noff - oN
    pE = pE_noff - oE
    pz = pz_noff - oz


    r_star = n.sqrt(pN**2 + pE**2 + pz**2)

    # sczattering angle
    phi_sca = n.pi - n.arccos(pz/r_star) # [rad]

    # projected radius
    r_proj = n.sqrt(pN**2+pE**2) # [AU]
    
    # projected PA        
    PA_proj = n.arctan2(pE, pN)
