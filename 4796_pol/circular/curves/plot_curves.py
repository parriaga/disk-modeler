import numpy as n
import matplotlib.pyplot as plot
import matplotlib.gridspec as gridspec

gs = gridspec.GridSpec(2,1)
gs.update(wspace=0.025, hspace=0.0)
ax_SW = plot.subplot(gs[0, 0])

jarr = n.loadtxt('jpol_SW.txt')
harr = n.loadtxt('hpol_SW.txt')
karr = n.loadtxt('kpol_SW.txt')

angs = jarr[:120, 0]

jflux = jarr[:120, 1]
jerr = jarr[:120, 2]
jerr /= n.max(jflux)
jflux /= n.max(jflux)

hflux = harr[:120, 1]
herr = harr[:120, 2]
herr /= n.max(hflux)
hflux /= n.max(hflux)

kflux = karr[:120, 1]
kerr = karr[:120, 2]
kerr /= n.max(kflux)
kflux /= n.max(kflux)

ax_SW.errorbar(angs, jflux, yerr = jerr, color = 'red', label = 'J band')
ax_SW.errorbar(angs, hflux, yerr = herr, color = 'green', label = 'H band')
ax_SW.errorbar(angs, kflux, yerr = kerr, color = 'blue', label = 'K band')
ax_SW.legend()
plot.ylabel('Normalized intensity')

ax_NE = plot.subplot(gs[1, 0])

jarr = n.loadtxt('jpol_NE.txt')
harr = n.loadtxt('hpol_NE.txt')
karr = n.loadtxt('kpol_NE.txt')

angs = jarr[:120, 0]

jflux = jarr[:120, 1]
jerr = jarr[:120, 2]
jerr /= n.max(jflux)
jflux /= n.max(jflux)

hflux = harr[:120, 1]
herr = harr[:120, 2]
herr /= n.max(hflux)
hflux /= n.max(hflux)

kflux = karr[:120, 1]
kerr = karr[:120, 2]
kerr /= n.max(kflux)
kflux /= n.max(kflux)

ax_NE.errorbar(angs, jflux, yerr = jerr, color = 'red', label = 'J band')
ax_NE.errorbar(angs, hflux, yerr = herr, color = 'green', label = 'H band')
ax_NE.errorbar(angs, kflux, yerr = kerr, color = 'blue', label = 'K band')
ax_NE.legend()
plot.xlabel('Phase angle')
plot.ylabel('Normalized intensity')

plot.show()


