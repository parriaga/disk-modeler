def plot_onlyone_ansa(band = 'k'):
    xcoord_SW = 33.86609227336834 + 140
    ycoord_SW = -68.82498736608787 + 140
    xcoord_NE = -34.12364549911034 + 140
    ycoord_NE = 69.3484046344364 + 140


    figure = plot.figure(figsize = [6, 6])
    gs = gridspec.GridSpec(2,1)

    gs.update(hspace = 0, right = 0.88, left = 0.18)
    ax1 = plot.subplot(gs[0, 0])
    ax2 = plot.subplot(gs[1, 0])

    if band == 'k':
        ax1.set_title('K1 Polarized Intensity Radial Profiles')
    if band == 'j':
        ax1.set_title('J Polarized Intensity Radial Profiles')
    if band == 'h':
        ax1.set_title('H Polarized Intensity Radial Profiles')


    cs = ['b', 'r', 'g', 'darkviolet', 'darkred', 'orange']
    ang = 180. - n.arctan2(ycoord_NE - 140., xcoord_NE - 140.) * 180. / n.pi
    plot_kwargs_data = {'c': '#3B73FF',
                        'label': 'Data',
                        'linewidth': 2.0,
                        'linestyle': '--'}
    xaxis, dataprof = sample_rad(ang, ax1,band = band, mode = 'pol', data = True, normalize = True, **plot_kwargs_data)
    plot_kwargs_model = {'c': '#ED0052',
                             'label': 'Model',
                         'linewidth': 2.0}
    xaxis, prof = sample_rad(ang, ax1, band = band, mode = 'pol', data = False, normalize = True, **plot_kwargs_model)

    ax1.set_ylim([0,0.9])
    ax1.xaxis.set_visible(False)
    ax1.set_yticks([0.2, 0.4, 0.6, 0.8])
    ax1.legend()#prop = {'size':10}, bbox_to_anchor = (1.15, .5))
    ax1.text(-12, 0.5, 'NE', ha = 'center', va = 'center')
    ax1.set_zorder(3)
    ax1.legend()

    ang = 180. - n.arctan2(ycoord_SW - 140., xcoord_SW - 140.) * 180. / n.pi

    plot_kwargs_model = {'c': '#ED0052', 
 #                            'label': 'Location ' + str(i + 1) + ' Model',
                         'linewidth': 2.0}
    xaxis, prof = sample_rad(ang, ax2, band = band, mode = 'pol', data = False, normalize = True, **plot_kwargs_model)


    plot_kwargs_data = {'c': '#3B73FF', 
                     
                        'linewidth': 2.0,
                        'linestyle': '--'}
    xaxis, dataprof = sample_rad(ang, ax2, band = band, mode = 'pol', data = True, normalize = True, **plot_kwargs_data)


    ax2.set_xlabel('Projected Separation (Pixels)')
    ax2.text(41, 0.45, 'SW', ha = 'center', va = 'center')
    ax1.text(41, 0.45, 'NE', ha = 'center', va = 'center')

    ax1.set_ylabel('Intensity (arbitrary)')
    ax2.set_ylabel('Intensity (arbitrary)')
    ax2.set_yticks([0.2, 0.4, 0.6, 0.8])
    ax2.set_ylim([0,.9])
    ax1.set_xlim([50, 100])
    ax2.set_xlim([50, 100])

    

    plot.show()

def make_plot_j(xlims_pix = 60, ylims_pix = 90):
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale
    mimj = pyfits.getdata('jpol_model_im.fits') 
    dataimj = pyfits.getdata('jpol_data_im.fits') 
    stdimj = pyfits.getdata('jpol_stdim.fits') 
#    chisq = (dataimj - mimj)**2. / stdimj**2.
#    chisq = chisq.flatten()
#    chisq = n.nansum(chisq)
#    wh = n.where(~n.isnan(dataimk))
#    numpix = len(wh[0]) + 22
    

    allims = [mimj,dataimj, stdimj]

    for im in allims:
        im = ma.masked_invalid(im)

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    cmap_idl = get_colormap_idl()


    plot_kwargs_1 = {'extent': extent,
                     'vmin': -30,
                     'vmax': 80,
                     'cmap': cmap_idl,
                     'origin': 'lower'}
    plot_kwargs_2 = {'extent': extent,
                     'vmin': -3,
                     'vmax': 3,
                     'cmap': 'PRGn',
    'origin': 'lower'}
#                     'cmap': 'seismic'}

 
    gs1 = gridspec.GridSpec(1,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8.5,5)

    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = 0.15, right = .9)
    jband_data_ax = plot.subplot(gs1[0,0])
    gpipu.compass([50, 40], length=15, ax = jband_data_ax, head_width = 4, head_length = 4, labeleast = True)

    jband_model_ax = plot.subplot(gs1[0,1])

    jband_resid_ax = plot.subplot(gs1[0,2])    


    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .15, hspace = 0, left =  .065 )
    resid_cbar = plot.subplot(gs2[0,2])

    jband_data_ax.imshow(dataimj, **plot_kwargs_1)

    text_kwargs = {'fontsize': 12,
                   'va': 'center',
                   'ha': 'center',
                   'rotation': 90}

    jband_data_ax.text(-95, 0, 'AU', **text_kwargs)
    
    jband_model = jband_model_ax.imshow(mimj, **plot_kwargs_1)

    resids = jband_resid_ax.imshow((mimj - dataimj) /stdimj, **plot_kwargs_2)


    resids_cbar_ax = fig.colorbar(resids, cax = resid_cbar, orientation = 'horizontal', label = 'Normalized Resids', ticks = [-3, -2, -1, 0 , 1, 2, 3])  

    all_ax = (jband_data_ax,jband_model_ax,jband_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (jband_model_ax,jband_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)

    text_kwargs = {'fontsize': 14,
                   'ha': 'center'}

    jband_data_ax.text(0, 100, 'Data', **text_kwargs)
    jband_model_ax.text(0, 100, 'Model', **text_kwargs)
    jband_resid_ax.text(0, 100, 'Normalized Residuals',wrap = True, **text_kwargs)
    
    jband_data_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_model_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_resid_ax.set_xticks([-50, -25, 0, 25, 50])
    jband_data_ax.set_xticklabels([-50, -25, 0, 25, 50])
    jband_model_ax.set_xticklabels([-50, -25, 0, 25, 50])
    jband_resid_ax.set_xticklabels([-50, -25, 0, 25, 50])

    text_kwargs = {'fontsize': 17, 
                   'ha': 'center',
                   'va': 'center'}
    
    jband_model_ax.text(0., 125., 'J-band Model and Data', **text_kwargs)

    plot.show()


