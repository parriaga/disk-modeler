import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
import copy
from pyklip.fmlib.diskfm import DiskFM
_log = logging.getLogger('hr4796a')
import matplotlib.pyplot as plot 
import configparser
from diskmodeler import ModelComparator
from matplotlib import gridspec

def make_plot(xlims_pix = 60, ylims_pix = 90):
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    pixelscale = .014
    dist = 72.8
    pix_au = dist * pixelscale

    diskobjk = mc.diskobjs[0]
    diskobjj = mc.diskobjs[1]
    diskobjh = mc.diskobjs[2]

    mimk = diskobjk.get_model()
    mimj = diskobjj.get_model()
    mimh = diskobjh.get_model()
    
    dataimk = diskobjk.tfim
    dataimj = diskobjj.tfim
    dataimh = diskobjh.tfim    

    pixelscale = .014            # arcseconds / pix
    dist = 72.8                   # parsecs,cn  
    pix_au = dist * pixelscale   # AU / pix
#    ylims = xlims_pix * pix_au
#    xlims = ylims_pix * pix_au

    cenx = 140
    ceny = 140

    extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
    ylims = 70
    xlims = 90

    calibk = 5.7e-8             # Jy/ADU/coadd                                      
    calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2  
    calibj = 1.03e-7 * .73
    calibj_as = calibj * 1000. / pixelscale**2.
    calibh = 3.17e-8
    calibh_as = calibh * 1000. / pixelscale**2. 

    plot_kwargs = {'extent': extent}

 


    gs1 = gridspec.GridSpec(3,3)  # y axis: 3 data plots + cbar, x: data,model,resids
    fig = plot.figure()
    fig.set_size_inches(8,11.2)
    gs1.update(bottom = .15,wspace = 0, hspace = 0, left = .1, right = .9)
    kband_data_ax = plot.subplot(gs1[0,0])
    hband_data_ax = plot.subplot(gs1[1,0])
    jband_data_ax = plot.subplot(gs1[2,0])


    kband_model_ax = plot.subplot(gs1[0,1])
    hband_model_ax = plot.subplot(gs1[1,1])
    jband_model_ax = plot.subplot(gs1[2,1])
    
    kband_resid_ax = plot.subplot(gs1[0,2])
    hband_resid_ax = plot.subplot(gs1[1,2])
    jband_resid_ax = plot.subplot(gs1[2,2])

    gs2 = gridspec.GridSpec(1, 3)    #cbar gridspec
    gs2.update(top = .13, hspace = 0)
    data_cbar = plot.subplot(gs2[0,0:2])
    resid_cbar = plot.subplot(gs2[0,2])
    data_cbar.yaxis.set_visible(False)
    resid_cbar.yaxis.set_visible(False)

    kband_data_ax.imshow(dataimk * calibk_as, **plot_kwargs)
    hband_data_ax.imshow(dataimh * calibh_as, **plot_kwargs)
    jband_data_ax.imshow(dataimj * calibj_as, **plot_kwargs)

    kband_model_ax.imshow(mimk * calibk_as, **plot_kwargs)
    hband_model_ax.imshow(mimh * calibh_as, **plot_kwargs)
    jband_model_ax.imshow(mimj * calibj_as, **plot_kwargs)
    

    kband_resid_ax.imshow(mimk - dataimk, **plot_kwargs)
    hband_resid_ax.imshow(mimh - dataimh, **plot_kwargs)
    jband_resid_ax.imshow(mimj - dataimj, **plot_kwargs)
    



    all_ax = (kband_data_ax,hband_data_ax,jband_data_ax,
              kband_model_ax,hband_model_ax,jband_model_ax,
              kband_resid_ax,hband_resid_ax,jband_resid_ax)
    for ax in all_ax:        
        ax.set_xlim(xmin = -70, xmax = 70)
        ax.set_ylim(ymin = -90, ymax = 90)

    hide_y_ax = (kband_model_ax,hband_model_ax,jband_model_ax,
              kband_resid_ax,hband_resid_ax,jband_resid_ax)
    for ax in hide_y_ax:
        ax.yaxis.set_visible(False)



    fig.savefig('/home/parriaga/gpi_disks/hr4796a_new/figures/fig.pdf')
    
    

    
def get_colormap_idl():
    # colors 
    colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',
                                                                {'red':   ((0.0, 0.0, 0.0),
                                                                           (0.756, 0.0, 0.0),
                                                                           (1.0, 1.0, 1.0)),
                                                                 
                                                                 
                                                                 'green': ((0.0, 0.0, 0.0),
                                                                           (0.38, 0.0, 0.0),
                                                                           (1.00, 1.0, 1.0)),
                                                                 
                                                                 'blue':  ((0.00, 0.0, 0.0),
                                                                           (0.737, 1.0, 1.0),
                                                                           (1.00, 1.0, 1.0))
                                                            })
    colormap_idl_bluewhite.set_bad('black')

    return colormap_idl_bluewhite

