from diskmodeler import ModelComparator
import matplotlib.pyplot as plot
import numpy as n
mc = ModelComparator('test.ini')
diskobj = mc.diskobjs[0]
im = diskobj.get_model(convolve = False)
plot.imshow(im)
xaxis = n.arange(281)
plot.plot(xaxis, xaxis, linewidth = 2, color = 'white')
plot.plot(xaxis, n.zeros(281) + 140, linewidth = 2, color = 'white')
plot.plot(n.zeros(281) + 140, xaxis, linewidth = 2, color = 'white')
plot.plot(xaxis, -xaxis + 281, linewidth = 2, color = 'white')
plot.show()
