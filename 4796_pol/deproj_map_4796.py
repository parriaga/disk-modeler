import numpy as n
import matplotlib.pyplot as plot
import pyfits
import numpy
import scipy.interpolate as interpol
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter
import pylab
import deproject
from diskmodeler import ModelComparator
import matplotlib.gridspec as gridspec




def get_deproj_grid(diskobj,samp = 4, model = True):
    I = diskobj.parms['I']
    Omega = diskobj.parms['omega1']
    offset = diskobj.parms['offset']
    omega = diskobj.parms['omega2']
    if model == True:
        im = diskobj.get_model()
    else:
        im = diskobj.fim
    grid = deprojmap(im, I, -Omega, offset, omega, samp = samp, show = False)
    gr = n.fliplr(grid.T)
    return gr #shifted_grid.T


def plot_integrated_curve():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_7.p'
    mc = ModelComparator(conf, mc_save = mc_save)

    imk = get_deproj_grid(mc.diskobjs[0], model = True)
    imj = get_deproj_grid(mc.diskobjs[1], model = True)
    imh = get_deproj_grid(mc.diskobjs[2], model = True)
    imk_data = get_deproj_grid(mc.diskobjs[0], model = False)
    imj_data = get_deproj_grid(mc.diskxsobjs[1], model = False)
    imh_data = get_deproj_grid(mc.diskobjs[2], model = False)


#    xaxis = n.linspace(0., 360., n.shape(imk)[1])

    def sumsection(im):
        subim = im[260:400, 600:1000]
        return n.nansum(subim, axis=0)
    xaxis = n.linspace(0., 360., len(sumsection(imk_data)))
 
    plot.plot(xaxis, sumsection(imk_data), color='red', alpha = .3)
    plot.plot(xaxis, sumsection(imh_data), color='green', alpha = .3)
    plot.plot(xaxis, sumsection(imj_data), color='blue', alpha = .3)

    plot.plot(xaxis, sumsection(imk), label='K1 band', color='red')
    plot.plot(xaxis, sumsection(imh), label = 'H band', color = 'green')
    plot.plot(xaxis, sumsection(imj), label='J band', color = 'blue')

    plot.legend()
    plot.show()









def offset_ims():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'

    mc = ModelComparator(conf, mc_save = mc_save)

#    mcj = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol.p')
#    imj = get_deproj_grid(mcj.diskobjs[0], model=True)

    imk = get_deproj_grid(mc.diskobjs[0], model = True)
    imj = get_deproj_grid(mc.diskobjs[1], model = True)
    imh = get_deproj_grid(mc.diskobjs[2], model = True)
    
    
    
    mc = ModelComparator(conf, mc_save = mc_save)


    gs1 = gridspec.GridSpec(3, 2)
    gs1.update(wspace = 0, hspace = 0)


    ax1 = plot.subplot(gs1[0,0])
    ax2 = plot.subplot(gs1[1,0])
    ax3 = plot.subplot(gs1[2,0])
    ax4 = plot.subplot(gs1[0,1])
    ax5 = plot.subplot(gs1[1,1])
    ax6 = plot.subplot(gs1[2,1])

    
    ax1.imshow(imk, vmin = 40, vmax = 130, extent = (0,360, 0, 140))
    ax1ylabel = ax1.set_ylabel('r (AU)')
    offset = n.array([-.25, 0])
    ax1.text(500, 325, 'K1')
    ax1.set_title('Model')
    ax1.xaxis.set_visible(False)
    

    
    ax2.imshow(imh, vmin = 40, vmax = 150, extent = (0,360, 0, 140))
    ax2.xaxis.set_visible(False)
    ax2.text(490, 325, 'H')
    ax2.set_ylabel('r (AU)')

    ax3.imshow(imj, vmin = 40, vmax = 130, extent = (0,360, 0, 140))
    ax3.text(500, 325, 'J')
    ax3.set_ylabel('r (AU)')

    imk = get_deproj_grid(mc.diskobjs[0], model = False)
    imj = get_deproj_grid(mc.diskobjs[1], model = False)
    imh = get_deproj_grid(mc.diskobjs[2], model = False)

    ax4.imshow(imk, vmin = 40, vmax = 130, extent = (0,360, 0, 140))
    ax4.set_title('Data')
    ax4.yaxis.set_visible(False)
    ax4.xaxis.set_visible(False)

    ax5.imshow(imh, vmin = 40, vmax = 150, extent = (0,360, 0, 140))
    ax5.yaxis.set_visible(False)
    ax5.xaxis.set_visible(False)


    ax6.imshow(imj, vmin = 40, vmax = 130, extent = (0,360, 0, 140))
    ax6.yaxis.set_visible(False)


    axes = (ax1, ax2, ax3, ax4, ax5, ax6)
    for ax in axes:
        ax.set_ylim([60, 100])
        ax.set_xlim([260, 335])
#        ax.set_xlim([160, 230])


    vert_axes = (ax1, ax2, ax3)
    for ax in vert_axes:
        ax.yaxis.set_ticks(n.arange(60, 100, 10))

    import pickle
    f = open('phi_tick_values.p')
    phinus = n.array(pickle.load(f))
    phis = phinus[0:6, 0]
    nus = phinus[0:6, 1]
    phis = [int(phi) for phi in phis]
    horiz_axes = (ax3, ax6)
    for ax in horiz_axes:
        ax.set_xticks(nus)
        ax.set_xticklabels(phis)
        plot.xlabel('Scattering Angle (degrees)')
    plot.show()
    
def offset_ims_withresids():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'
#    mc_save = 'mcmc_allpol_intparms_3.p'

    mc = ModelComparator(conf, mc_save = mc_save)

#    mcj = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol.p')
#    imj = get_deproj_grid(mcj.diskobjs[0], model=True)

    imk = get_deproj_grid(mc.diskobjs[0], model = True)
    imj = get_deproj_grid(mc.diskobjs[1], model = True)
    imh = get_deproj_grid(mc.diskobjs[2], model = True)
    
    
    
    mc = ModelComparator(conf, mc_save = mc_save)


    gs1 = gridspec.GridSpec(3, 3)
    gs1.update(wspace = 0, hspace = 0)


    ax1 = plot.subplot(gs1[0,0])
    ax2 = plot.subplot(gs1[1,0])
    ax3 = plot.subplot(gs1[2,0])
    ax4 = plot.subplot(gs1[0,1])
    ax5 = plot.subplot(gs1[1,1])
    ax6 = plot.subplot(gs1[2,1])
    ax7 = plot.subplot(gs1[0,2])
    ax8 = plot.subplot(gs1[1,2])
    ax9 = plot.subplot(gs1[2,2])

    plot_kwargs = {'extent': (0, 360, 0, 140)}
    
    ax1.imshow(imk, vmin = 40, vmax = 130, **plot_kwargs)
    ax1ylabel = ax1.set_ylabel('r (AU)')
    offset = n.array([-.25, 0])
    ax1.text(500, 325, 'K1')
    ax1.set_title('Model')
    ax1.xaxis.set_visible(False)
    
    ax2.imshow(imh, vmin = 40, vmax = 150, **plot_kwargs)
    ax2.xaxis.set_visible(False)
    ax2.text(490, 325, 'H')
    ax2.set_ylabel('r (AU)')

    ax3.imshow(imj, vmin = 40, vmax = 130, **plot_kwargs)
    ax3.text(500, 325, 'J')
    ax3.set_ylabel('r (AU)')

    imk_data = get_deproj_grid(mc.diskobjs[0], model = False)
    imj_data = get_deproj_grid(mc.diskobjs[1], model = False)
    imh_data = get_deproj_grid(mc.diskobjs[2], model = False)

    ax4.imshow(imk_data, vmin = 40, vmax = 130, **plot_kwargs)
    ax4.set_title('Data')
    ax4.yaxis.set_visible(False)
    ax4.xaxis.set_visible(False)

    ax5.imshow(imh_data, vmin = 40, vmax = 150, **plot_kwargs)
    ax5.yaxis.set_visible(False)
    ax5.xaxis.set_visible(False)


    ax6.imshow(imj_data, vmin = 40, vmax = 130, **plot_kwargs)
    ax6.yaxis.set_visible(False)


    ax7.imshow(imk_data - imk, vmin = -30, vmax = 30, **plot_kwargs)
    ax8.imshow(imh_data - imh, vmin = -30, vmax = 30, **plot_kwargs)
    ax9.imshow(imj_data - imj, vmin = -30, vmax = 30, **plot_kwargs)



    axes = (ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9)
    for ax in axes:
        ax.set_ylim([60, 100])
        ax.set_xlim([260, 335])
#        ax.set_xlim([160, 230])


    vert_axes = (ax1, ax2, ax3)
    for ax in vert_axes:
        ax.yaxis.set_ticks(n.arange(60, 100, 10))

    import pickle
    f = open('phi_tick_values.p')
    phinus = n.array(pickle.load(f))
    phis = phinus[0:6, 0]
    nus = phinus[0:6, 1]
    phis = [int(phi) for phi in phis]
    horiz_axes = (ax3, ax6)
    for ax in horiz_axes:
        ax.set_xticks(nus)
        ax.set_xticklabels(phis)
        plot.xlabel('Scattering Angle (degrees)')
    plot.show()
    



def rnu_to_NEzr(r, nu, omega, Omega, I, offset):
    ''' 
    Transforms from disk plane to sky plane
        Inputs: (in radians) r, nu, omega Omega I, offset
        Outputs: pN, pE, omega, Omega, I, offset
    '''
    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]

    "geometric transformation"
    sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
    
    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si
    
    pN -= oN 
    pE -= oE 
    pz -= oz
    
    r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
    
    return pN, pE, pz, r_star

def NEzr_to_rnu(pN, pE, omega, Omega, I, offset):
    '''
    Transforms from sky plane to disk plane
        Inputs: pN, pE, omega, Omega, I, offset (in radians)
        Outputs: r, nu
    '''
    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    alpha = (pN + oN) / (pE + oE)
    #Geometric 
    nu = numpy.arctan2((cO - alpha * sO), ci * ( sO + alpha * cO))
    sinu, conu = numpy.sin(nu), numpy.cos(nu)
    r = (pN + oN) / (cO * conu - sO * sinu * ci)
    nu = nu - numpy.pi / 8
    return r, nu


def shift_map(im, diskobj, retphiscas = False):
    n_step = n.shape(im)[0]
    nu, r_star, phi_sca, r_proj, PA_proj, intensities = diskobj.parametric_model_ring(n_step = n_step)
    phimin = n.where(phi_sca == min(phi_sca))[0]
    phibegin = phimin[0] + n_step / 4
    im = n.vstack((im[phibegin:,:], im[5:phibegin , :]))

    phibegin2 = n_step / 8
    im = n.vstack((im[:phibegin2,:], im[phibegin2+1:, :]))

    return im
    


def deprojmap(im, I, Omega, offset, omega, maxrad = 140, maxphi = 360, samp = 4, show = True):
    center = [140,140]
    shape = im.shape

    # R and phi of each i and j
    r_u = []
    phi_u = []
    # Flattened image
    vals = []
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
            if not numpy.isnan(im[i,j]):
                # Phi and r in the disk plane
                r_c, phi_c = NEzr_to_rnu(i - center[0], j - center[1], omega, Omega, I, offset)
                if ~numpy.isnan(r_c) and ~numpy.isnan(phi_c):
                    if r_c < 0:
                        r_c = numpy.abs(r_c)
                        if phi_c > 0:
                            phi_c = - numpy.pi + numpy.abs(phi_c)
                        else: 
                            phi_c = numpy.pi - numpy.abs(phi_c)
                    phi_c *= 180. / numpy.pi
                    if phi_c < 0:
                        phi_c +=360.
                    if r_c * numpy.sin(phi_c) != 0:
                        r_u.append(r_c)
                        phi_u.append(phi_c)
                        if im[i, j] < 1.e-2:
                            vals.append(0.)
                        else:
                            vals.append(im[i, j])

    # Make grid to interpolate onto 
    r, phi = numpy.meshgrid(numpy.arange(maxrad * samp) / samp + 1, numpy.arange(maxphi * samp) / samp )

    # Interpolate vals which are at r_u and phi_u onto new gris of r and phi
    grid = interpol.griddata(numpy.array([r_u, phi_u]).transpose(), vals, (r, phi), method = 'cubic')
    if show == True:
        plot.clf()
        plot.imshow(grid.transpose(), vmin = 0, vmax = 110, extent=[0,360,0,maxrad])
        plot.show()
    return grid




###################################################################
###################################################################
###################################################################
###################################################################
###################################################################


def test1_offset_ims(model = True):
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_7.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    imk = get_deproj_grid(mc.diskobjs[0], model = model)
    imj = get_deproj_grid(mc.diskobjs[1], model = model)
    imh = get_deproj_grid(mc.diskobjs[2], model = model)
    
    plot.subplot('311')
    plot.imshow(imk, vmin = 40, vmax = 150)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])
    plot.subplot('312')
    plot.imshow(imh, vmin = 40, vmax = 150)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])

    plot.subplot('313')
    plot.imshow(imj, vmin = 40, vmax = 150)

    plot.ylim([200, 400])
    plo.txlim([600, 1000])

    plot.show()


def test2_offset_ims():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_7.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    imk = get_deproj_grid(mc.diskobjs[0], model = True)
    imj = get_deproj_grid(mc.diskobjs[1], model = True)
    imh = get_deproj_grid(mc.diskobjs[2], model = True)
    plot.subplot('321')
    plot.imshow(imk, vmin = 40, vmax = 130)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])
    plot.subplot('323')
    plot.imshow(imh, vmin = 40, vmax = 150)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])
    plot.subplot('325')
    plot.imshow(imj, vmin = 40, vmax = 130)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])


    imk = get_deproj_grid(mc.diskobjs[0], model = False)
    imj = get_deproj_grid(mc.diskobjs[1], model = False)
    imh = get_deproj_grid(mc.diskobjs[2], model = False)
    plot.subplot('322')
    plot.imshow(imk, vmin = 40, vmax = 130)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])
    plot.subplot('324')
    plot.imshow(imh, vmin = 40, vmax = 150)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])
    plot.subplot('326')
    plot.imshow(imj, vmin = 40, vmax = 130)
    plot.ylim([200, 400])
    plot.xlim([600, 1000])
    plot.show()
    
