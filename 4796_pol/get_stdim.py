def get_stdims(self):
    from image import radial_stdev_profile


    # Can use Ur map as noise map
    if self.noise_map is not None:
        # assumes all images are same size and are aligned.                                       
        rprof, sprof = radial_stdev_profile(self.noise_map, [int(self.center[0]), int(self.center\
[1])])

    else:
        # This just uses the data image to generate the noise map
        # Note that this doesn't subtract out the model disk first
        rprof, sprof = radial_stdev_profile(self.fim, [int(self.center[0]), int(self.center[1])])
    y, x = n.mgrid[0:self.fim.shape[0],
                   0:self.fim.shape[1]]
    y -= int(self.center[0])
    x -= int(self.center[1])
    R = n.sqrt(x**2+y**2)
    stdim = n.zeros_like(self.fim)

    # Turn profiles into image of standard dev as a function of radius.                           
    for r, s in zip(rprof, sprof):
        wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
        stdim[wr] = s

    if hasattr(self, 'psf_im'):
        psf = self.psf_im
        psf = psf[141:-140, 141:-140]
    else:
        psf_sig = self.parms[self.label + 'psf_sig']
        import gauss
        psf = gauss.tdgauss(1, self.center[0], self.center[1], psf_sig, 0, self.fim.shape)
        psf = psf / n.sum(psf)

    from scipy.signal import fftconvolve

    stdim[n.isnan(stdim)] = 0
    # Uncomment if convolving 
    #        stdim = fftconvolve(n.sqrt(stdim), psf, mode = 'same')                                      #        stdim = stdim**2.
    stdim[n.where(stdim < .5)] = n.nan
    return stdim, R
