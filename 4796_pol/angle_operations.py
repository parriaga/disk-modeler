import matplotlib.pyplot as plot
import matplotlib.pylab as pylab
import numpy as n
from scipy.special import ellipeinc
import pyfits
from scipy.optimize import fmin, brentq




#####################################################
##################Geometric functions################
#####################################################
def get_NE(nu):
    '''
    input: nu, argument from periastron
    output: 
           pN: North coordinate in AU
           pE: East coordinate in AU
           phi_sca in radians
    '''
    omega = (90 - 25.4) * n.pi /180
    Omega = 26.2* n.pi /180
    I = 76.56* n.pi /180
    r = 77.
    offset = 0.68

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si
    
    pN -= oN
    pE -= oE
    pz -= oz
    
    r_star = n.sqrt(pN**2 + pE**2 + pz**2)
    
    # scattering angle
    phi_sca = n.arccos(pz/r_star) # [rad]
    
    return pN, pE, phi_sca


def xp_yp_to_nu(xp, yp):
    '''
    Inverse of get_NE
    Input: xp, yp, coordinates in au in projected space
    Output: nu, the argument from periastron in radians
    '''
    opts = []
    print 'e'
    for xpf, ypf in zip(xp, yp):

        def minimize_func(nu_fit):
            pNf, pEf, throwaway = get_NE(nu_fit)
            minf = n.sqrt((pNf - ypf)**2. + (pEf - xpf)**2.)
            return minf
        popt = fmin(minimize_func, 5., disp = False)
        opts.append(popt[0])
        print 'f'
    opts = n.array(opts)#n.array(xp_yp_to_nu(xp, yp))
    return opts




def xp_yp_to_phisca(xp, yp):
    '''
    Input: An array of projected x and y coordinates
    Output: The scattering angles
    '''
    nus = xp_yp_to_nu(xp, yp)
    
    phi_scas = []
    for nu in nus:
        pN, pE, phi_sca = get_nu(nu)
        phi_scas.append(phi_sca)
    return phi_sca


def phisca_to_nus(phi_sca):
    nus = n.array([fmin(lambda nu: get_NE(nu)[2], 0.)[0],
                   fmin(lambda nu: -get_NE(nu)[2], 0.)[0]]) % (2.*n.pi)
    nu_min = nus.min()
    nu_max = nus.max()

    def comp(nu):
        d = (get_NE(nu)[2]-phi_sca*n.pi/180.) % (2.*n.pi)
        if d > n.pi: d -= 2.*n.pi
        return d
        
    nu1 = brentq(comp, nu_min, nu_max) % (2.*n.pi)
    nu2 = brentq(comp, nu_max, nu_min+2.*n.pi) % (2.*n.pi)
    return nu1, nu2


def get_arc(phi, a, b):
    '''                                                                                               
    Input: phi, the angle around the ellipse starting from the major axis                             
    k, the eccentricity^2 of the ellipse                                                              
    Output: The arclength starting from the major axis                                                
    '''
    m = 1. - a**2. / b**2.

    x, y = phi_to_xy_on_ellipse(phi, a, b)
    t = n.arctan2(a * y, b * x)
    if t < 0:
        t = 2 * n.pi + t
    s = b * ellipeinc(t, m)
    return s


def phi_to_xy_on_ellipse(phi, a, b):
    '''
    Input: phi, the angle from the point to the center tar
    Output: x and y location of where a line in the direction of 
    phi intercepts an ellipse with parameters a and b
    '''

    if phi > n.pi / 2 and phi <= 3 * n.pi / 2:
        sign = -1
    elif phi >= 0 and phi <= n.pi / 2:
        sign = 1
    elif phi > 3. * n.pi / 2 <= 2. * n.pi:
        sign = 1
    elif phi < 0 and phi >= -n.pi / 2:
        sign = 1
    elif phi < -n.pi / 2 and phi >= -3 * n.pi / 2:
        sign = -1
    else:
        sign = 1
    x = sign * a * b / n.sqrt(b**2. + a**2. * n.tan(phi)**2.)
    y = x * n.tan(phi)
    return x, y


def get_sky_phi(s, a, b):
    '''
    Inverse of get_arc
    Input: s, arclength along an ellipse with semimajor axis a
           semiminor axis b
    Output: phi, angle from semimajor axis in the direction of spot where
            arclength s intercepts with the ellipse
    '''
    from scipy.optimize import fmin
    def fun(phi):
        sfun = get_arc(phi, a, b)
        return n.abs(s - sfun)
    x0 = n.pi
    xopt = fmin(fun, [x0],disp=False)
    return xopt[0]




###################################################################
#############################Plots#################################
###################################################################




def make_figure_cartoon(save_fn='HR4796A_cartoon.pdf'):
    "make figure for geometry cartoon"
    import constants as c

    kpol = pyfits.getdata('ktot_data_im.fits')

    omega = (90 - 25.4) * n.pi /180
    Omega = 26.2* n.pi /180
    I = 76.56* n.pi /180
    r = 77.
    offset = 0.68

    
    fig = pylab.figure(3, figsize=(4,5))
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)


    # calculate ellipse
    nu = n.linspace(0., 2.*n.pi, 100, endpoint=True)
    pN, pE, phi_sca = get_NE(nu)

    # draw ellipse
    ax.plot(pE, pN, ls='-', c='k', lw=2., zorder=2)


    ft = 1.7
    facts = n.array((0.95, 1.3))

    # nus for which theta_sca = min, max
    nus = n.array([fmin(lambda nu: get_NE(nu)[2], 0.)[0],
                   fmin(lambda nu: -get_NE(nu)[2], 0.)[0]]) % (2.*n.pi)
    nu_min = nus.min()
    nu_max = nus.max()
    #print "min", nu_min*180./n.pi, get_NE(nu_min)[2]*180./n.pi
    #print "max", nu_max*180./n.pi, get_NE(nu_max)[2]*180./n.pi

    # plot these phi_scas 
    phi_scas = 180 - n.array([14, 20, 60, 90, 120, 150])

    ftx = [1.6, 1.4, 1.4, 1.4, 1.6, 1.7]
    fty = [1.5, 1.4, 1.4, 1.4, 1.6, 1.7]
    for i, phi in enumerate(phi_scas):
        phi_calc = phi
        def comp(nu):
            d = (get_NE(nu)[2]-phi*n.pi/180.) % (2.*n.pi)
            if d > n.pi: d -= 2.*n.pi
            return d
        # which nus does this correspond to?
        nu1 = brentq(comp, nu_min, nu_max) % (2.*n.pi)
        nu2 = brentq(comp, nu_max, nu_min+2.*n.pi) % (2.*n.pi)
        for nu in (nu1, nu2):
            pN, pE, phi_sca = get_NE(nu)
#            print phi, nu
            ax.plot(facts*pE, facts*pN, c='k', zorder=3)
            tstr =  str(int(180 - phi)) + "$^\circ$"
            ax.text(ftx[i]*pE, fty[i]*pN,
                    tstr,
                    #ha='left' if nu*c.rtod < 180. else 'right',
                    #va='top' if pN < 0. else 'bottom',
                    ha='center', va='center',
                    )

    a = r
    I = 76.56
    b = a * n.sin((90 - I) * n.pi /180)
    smax = get_arc(360 * n.pi / 180, a, b)

    ses = n.linspace(0, smax, 14)
    phis = [get_sky_phi(s, a, b) for s in ses]
    coords = [phi_to_xy_on_ellipse(phi, a, b) for phi in phis]

    x = n.array([coord[0] for coord in coords])
    y = n.array([coord[1] for coord in coords])

    t = (-26.2 + 90)* n.pi / 180
    co = n.cos(t)
    si = n.sin(t)
    xp = x * co - y * si
    yp = x * si + y * co

    opts = xp_yp_to_nu(xp, yp)

    testpn, testpe, phis = get_NE(opts)

    print zip(phis, yp)
    
    plot.scatter(xp, yp, s = 90, c='black')
    plot.scatter(xp, yp, s = 70, c='white')


    plot.xlim([-90,90])
    plot.ylim([-90,90])

    ax.scatter((0.,), (0.,), c='k', marker='*', s=80.)

    ax.set_aspect('equal')
    ax.set_xlim(60, -60)
    ax.set_axis_off()
    pylab.draw()

    pylab.show()







def cartoon_with_numbers():
#    phi_scas = [90, 43, 19]
    phi_scas = [90, 56, 39, 25, 15]
    nus_NE = []
    nus_SW = []
    for phi_sca in phi_scas:
        nu1, nu2 = phisca_to_nus(phi_sca)
        nus_NE.append(nu1)
        nus_SW.append(nu2)
    nus_SW.reverse()
    nus = nus_NE + nus_SW
    xcoords = []
    ycoords = []
    for nu in nus:
        pN, pE, throwaway = get_NE(nu)
        xcoords.append(pE)
        ycoords.append(-pN)


    fig = plot.figure()
    ax = plot.subplot('111')
    plot_ellipse(ax)
    ax.set_xlim([-60,60])
    ax.set_ylim([-80,80])
    
    ax.set_aspect('equal')
    ax.set_axis_off()

    plot.scatter(xcoords,ycoords, marker = 'o', s = 260, c = 'white', edgecolors = 'none', zorder = 10)
    
#    labels = ['1', '2', '3', '4', '5', '6']
    labels = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
    
    cs = ['b', 'r', 'g', 'darkblue', 'darkred', 'darkolivegreen','b', 'r', 'g', 'darkblue', 'darkred', 'darkolivegreen']    

    for i in range(len(labels)):
        print cs[i]
        plot.scatter(xcoords[i],ycoords[i], marker = 'o', s = 600, c = cs[i], edgecolors = 'none', zorder = 8)

        plot.text(xcoords[i], ycoords[i], labels[i], color = 'black', zorder = 15, va = 'center', ha = 'center', weight = 'bold')
    print xcoords
    print ycoords
    plot.show()
    fig.savefig('radial_profs_spline.png')


def cartoon_with_numbers_mirrored():
#    phi_scas = [90, 43, 19]
    phi_scas = [90, 56, 39, 25, 15]
#    phi_scas = [120, 90, 56, 39, 25, 15]
    nus_NE = []
    nus_SW = []
    for phi_sca in phi_scas:
        nu1, nu2 = phisca_to_nus(phi_sca)
        nus_NE.append(nu1)
        nus_SW.append(nu2)

    xcoords_SW = []
    ycoords_SW = []
    for nu in nus_SW:
        pN, pE, throwaway = get_NE(nu)
        xcoords_SW.append(pE)
        ycoords_SW.append(-pN)
    print 'xcoords SW'
    print xcoords_SW
    print 'ycoords_SW'
    print ycoords_SW

    xcoords_NE = []
    ycoords_NE = []
    for nu in nus_NE:
        pN, pE, throwaway = get_NE(nu)
        xcoords_NE.append(pE)
        ycoords_NE.append(-pN)

    print 'xcoords NE'
    print xcoords_NE
    print 'ycoords_NE'
    print ycoords_NE

    fig = plot.figure()
    ax = plot.subplot('111')
    plot_ellipse(ax)
    ax.set_xlim([-60,60])
    ax.set_ylim([-80,80])
    
    ax.set_aspect('equal')
    ax.set_axis_off()

    plot.scatter(xcoords_NE, ycoords_NE, marker = 'o', s = 260, c = 'white', edgecolors = 'none', zorder = 10)
    plot.scatter(xcoords_SW, ycoords_SW, marker = 'o', s = 260, c = 'white', edgecolors = 'none', zorder = 10)
    
#    labels = ['1', '2', '3', '4', '5', '6']
    labels = ['1', '2', '3', '4', '5']
    
    cs = ['b', 'r', 'g', 'darkblue', 'darkred']
#    cs = ['b', 'r', 'g', 'darkblue', 'darkred', 'orange']

    for i in range(len(labels)):
        plot.scatter(xcoords_NE[i],ycoords_NE[i], marker = 'o', s = 600, c = cs[i], edgecolors = 'none', zorder = 8)
        plot.scatter(xcoords_SW[i],ycoords_SW[i], marker = 'o', s = 600, c = cs[i], edgecolors = 'none', zorder = 8)
        plot.text(xcoords_NE[i], ycoords_NE[i], labels[i], color = 'black', zorder = 15, va = 'center', ha = 'center', weight = 'bold')
        plot.text(xcoords_SW[i], ycoords_SW[i], labels[i], color = 'black', zorder = 15, va = 'center', ha = 'center', weight = 'bold')

    offset_angle = 6.85 * n.pi / 180
    star_center_dist = 15.
    xpos = n.sin(offset_angle) * star_center_dist
    ypos = n.cos(offset_angle) * star_center_dist
    
    plot.scatter([xpos], [ypos], marker = '*', s = 140)
    plot.scatter([0],[0], s = 40)

    plot.text(xpos - 37, ypos, 'Star Center', va = 'center', zorder = 15)
    plot.plot([xpos - 37, xpos - 8], [ypos, ypos], color = 'white', linewidth = 20)

    plot.text(xpos - 39, 0, 'Disk Center', va = 'center', zorder = 15)
    plot.plot([-37, -8], [0, 0], color = 'white', linewidth = 20)


    arrow_length = 50.
    


#    xend = -n.sin(offset_angle) * arrow_length
#    yend = -n.cos(offset_angle) * arrow_length

    xpos2 = n.sin(offset_angle) * 7.
    ypos2 = n.cos(offset_angle) * 7.

    xpos3 = n.sin(offset_angle) * 2
    ypos3 = n.cos(offset_angle) * 2


    plot.arrow(xpos - xpos3, ypos-ypos3, -xpos + xpos2, -ypos + ypos2,  width = 0.1)
#    plot.arrow(0, 0, xend, yend, width = 0.1)
#    plot.text(xend - 20, yend, 'Offset')
#    plot.show()
    fig.savefig('radial_profs_spline_test.png')



def plot_ellipse(ax):
    import constants as c

    omega = (90 - 25.4) * n.pi /180
    Omega = 26.2* n.pi /180
    I = 76.56* n.pi /180
    r = 77.
    offset = 0.68

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    def get_NE(nu):
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)

        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        return pN, pE, phi_sca

    # calculate ellipse
    nu = n.linspace(0., 2.*n.pi, 100, endpoint=True)
    pN, pE, phi_sca = get_NE(nu)

    # draw ellipse
    ax.plot(-pE, pN, ls='-', c='k', lw=2.)



###############################################################################
#############################Old code##########################################
###############################################################################



def ellipse_arc():
    import constants as c

    fig = pylab.figure(3, figsize=(4,5))
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)


    omega = (90 - 25.4) * n.pi /180
    Omega = 26.2* n.pi /180
    I = 76.56* n.pi /180
    r = 77.
    offset = 0.68

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    def get_NE(nu):
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)

        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        return pN, pE, phi_sca

    ## # draw line of nodes
    ## nu = n.array((-omega, n.pi-omega))
    ## pN, pE, phi_sca = get_NE(nu)
    ## ax.plot(pE, pN, ls='--', c='k', lw=1., zorder=1)

    # calculate ellipse
    nu = n.linspace(0, 0.908, 100, endpoint = True)
    pN, pE, phi_sca = get_NE(nu)
#    ax.plot(-pE, pN, ls='-', c='k', lw=2., zorder=2, linewidth = 10, alpha = 0.2)

    

#    nu = n.linspace(6.25, 2. * n.pi, 100, endpoint=True)
#    pN2, pE2, phi_sca = get_NE(nu)
#    pN = n.append(pN2, pN)
#    pE = n.append(pE2, pE)
    ax.plot(-pE, pN, ls='-', c='k', lw=2., zorder=2, linewidth = 10, alpha = 0.2)

    

 
    nu = n.linspace(3.32, 3.85, 100, endpoint=True) 
    pN, pE, phi_sca = get_NE(nu)
    ax.plot(-pE, pN, ls='-', c='k', lw=2., zorder=2, linewidth = 10, alpha = 0.2)

    ax.set_xlim([-70,70])
    ax.set_ylim([-100, 100])
    plot.show()


#         so, co = n.sin(omega), n.cos(omega)
#         sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
#         sO, cO = n.sin(0), n.cos(0)
#         si, ci = n.sin(I), n.cos(I)
        
#         pN_derot = r * (cO*conu-sO*sonu*ci) # [AU]                                                   
#         pE_derot = r * (sO*conu+cO*sonu*ci) # [AU]                                                   
#         pz_derot = r * sonu*si

#         # Ellipse parameters                                                                           
#         a = r
#         b = r * n.cos(I)
#         m = 1. - a**2. / b**2.
#         t = n.arctan2(a * pE_derot, b * pN_derot)
#         if t < 0:
#             t = 2 * n.pi + t
#         arc = b * ellipeinc(t, m)
#         return arc

# #    plot.clf()
#     # #test for arcs
# #    arcs = []
# #    nus = n.linspace(0, 2. * n.pi, 100)
# #    for nup in nus:
# #        arcs.append(get_arc_from_nu(nup, a, b))
# #        
# #    plot.scatter(nus, arcs)
# #    plot.show()

    
#     nus_for_ses = []
# #    ses = n.linspace(0, 320, 100)
#     for arc in ses:
#         def opt_arc_from_nu(nu):
#             return n.abs(arc - get_arc_from_nu(nu, a, b))
#         if s < 50:
#             initguess = 6.
#         else:
#             initguess = 2.
#         nu = fmin(opt_arc_from_nu, initguess)
#         if nu < 0:
#             nu = nu + 2. * n.pi 
#         nus_for_ses.append(nu)
#     plot.clf()

#     print nus_for_ses
#     print ses
#     plot.scatter(nus_for_ses, ses)
#     plot.show()
        
#     phi_scas_ses = []
#     for nup in nus_for_ses:
#         throwaway, throwaway2, phi_sca_ses = get_NE(nup)
#         phi_scas_ses.append(phi_sca_ses)
