import numpy as n
import matplotlib.pyplot as plot


def make_psf(int_parms,size = [581,581]):
    l = len(int_parms)
    small_im_size = 4 * l + 1
    small_im = n.zeros((small_im_size, small_im_size))
    rs = n.arange(l)
    y, x = n.mgrid[0:l * 4, 0:l * 4]
    x = x - l * 2 
    y = y - l * 2 
    R = n.sqrt(x**2. + y**2.)

    small_im[l * 2, l * 2] = int_parms[0]
    for i in range(1,l):
        r = rs[i]
        val = int_parms[i]
        wh = n.nonzero((R >= r - 0.5) & (R < r + 0.5))
        small_im[wh] = val
    plot.imshow(small_im)
    plot.show()
    

    
