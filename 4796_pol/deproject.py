import pyfits
import matplotlib.pyplot as plot
import numpy
import scipy.interpolate as interpol
from scipy.ndimage.filters import gaussian_filter
import pylab
#from hr4796a import OffsetRingModel
#import hr4796a
import scipy.ndimage.filters as filt
from diskmodeler import ModelComparator
import numpy as n
import image
from matplotlib import gridspec
from diskmodeler_plotutils import *

def plot_all_profiles():
    mc = ModelComparator('hr4796_config_hpol.ini', mc_save = 'mcmc_hpol_even2.p')

    mc.update_all_parms(mc.parms)
    show_model(mc.diskobjs[0],convolve_resids = False, vmin_data = -20, vmax_data = 120, vmin_resids = -2, vmax_resids = 2)
    plot_profiles(mc, label = 'H')
    mc2 = ModelComparator('hr4796_config_kpol.ini', mc_save = 'mcmc_kpol_even.p')
    plot_profiles(mc2, label = 'K')
  
    mc.print_parms()
    mc2.print_parms()
    
    mc3 = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol_even.p')
    plot_profiles(mc3, label = 'J')
    plot.legend()
    plot.show()
    
def plot_profiles(mc, **kwargs):

    diskobj = mc.diskobjs[0] 
    scale = mc.scale
    gamma_in = mc.parms['gamma_in']
    gamma = mc.parms['gamma']
    beta = mc.parms['beta']
    offs = n.array((mc.parms[diskobj.label + 'soffy'], mc.parms[diskobj.label + 'soffx']))
    offset = mc.parms['offset']
    I = mc.parms['I']
    omega = mc.parms['omega2']
    Omega = mc.parms['omega1']

    if diskobj.psf_method == 'inputpsf':
        psf_im = diskobj.psf_im

    # get position/intensity profile                                       
    n_step = 100
    nu, r, phi_sca, r_proj, PA_proj, intensity = diskobj.parametric_model_ring(n_step=n_step)
    dnu = nu[1]-nu[0]
    assert nu[2]-nu[1] == dnu

    r_in, r_mid, r_out = diskobj.parms['r_in'], diskobj.parms['r'], diskobj.parms['r_out']
    
    # FIXME                                                                                       
    if r_in is None:
        r_in = r_mid
    if r_out is None:
        r_out = r_mid
    drad0 = .125
    if drad0 > int(r_out - r_in):
        r_out = r_in + drad0
    n_step_rad = int((r_out-r_in)/drad0)
    drad = (r_out-r_in)/n_step_rad

    if n_step_rad > 1:
        r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid
    else:
        n_step_rad = 1
        r_factors = n.array((1.,))

    i_factors = smooth_broken_power_law(r_factors, 1., 1., 1., -gamma_in, -gamma, beta)/r_factors**2/n_step_rad
    plot.plot(r_factors * r_mid, i_factors, **kwargs)

    

def plot_series():
    angs = n.linspace(0,180,41)

    plot.clf()
    for ang in angs:
        sample_rad(ang, band = 'k', mode = 'pol', data = True, normalize = True)
        sample_rad(ang, band = 'k', mode = 'pol', data = False, normalize = True)
        sting = str(int(ang))
        if len(sting) == 1:
            sting = '00' + sting
        elif len(sting) == 2:
            sting = '0' + sting
        print sting
        plot.savefig(sting + '.png')
        plot.clf()

def plotsome():
    plot.figure(figsize=(15, 10))
    gs1 = gridspec.GridSpec(3, 2)
    gs1.update(wspace = 0, hspace = 0)
    ax1 = plot.subplot(gs1[0])

    kpsf_imname = '/home/parriaga/gpi_disks/hr4796a_new/psfs/kband_psf.txt'
    kpsf_line = n.loadtxt(kpsf_imname) 
    kpsf_line /= n.max(kpsf_line)

    plot.plot(n.arange(len(kpsf_line)) + 43.6, kpsf_line, color = 'darkgrey')
    sample_rad(0, band='k', mode = 'pol', data = True, sw = True)
    sample_rad(0, band='k', mode = 'pol', data = False, sw = True)
    ax1.get_xaxis().set_ticks([])

    plot.ylabel('Normalized Intensity')

    ax2 = plot.subplot(gs1[1])
    sample_rad(0, band='k', mode = 'pol', data = True, sw = False)
    sample_rad(0, band='k', mode = 'pol', data = False, sw = False)
    ax2.get_xaxis().set_ticks([])
    ax2.get_yaxis().set_ticks([])

    ax3 = plot.subplot(gs1[2])
    plot.ylabel('Normalized Intensity')
    sample_rad(0, band='j', mode = 'pol', data = True, sw = True,normalize = True)
    sample_rad(0, band='j', mode = 'pol', data = False, sw = True,normalize = True)
    ax3.get_xaxis().set_ticks([])

    ax4 = plot.subplot(gs1[3])
    sample_rad(0, band='j', mode = 'pol', data = True, sw = False,normalize = True)
    sample_rad(0, band='j', mode = 'pol', data = False, sw = False,normalize = True)
    ax4.get_xaxis().set_ticks([])
    ax4.get_yaxis().set_ticks([])

    ax5 = plot.subplot(gs1[4])
    plot.ylabel('Normalized Intensity')
    sample_rad(0, band='h', mode = 'pol', data = True, sw = True)
    sample_rad(0, band='h', mode = 'pol', data = False, sw = True)
    plot.xlabel('Distance from star (AU)')

    ax6 = plot.subplot(gs1[5])
    sample_rad(0, band='h', mode = 'pol', data = True, sw = False)
    sample_rad(0, band='h', mode = 'pol', data = False, sw = False)
    plot.xlabel('Distance from star (AU)')
    ax6.get_yaxis().set_ticks([])

    plot.savefig('radial_profs.png')


def plot_psf():
    sample_rad(0, band='k', mode = 'pol', data = False, sw = True)
    psf_imname = '/home/parriaga/gpi_disks/hr4796a_new/psfs/kband_psf.txt'
    psf_line = n.loadtxt(psf_imname) 
    psf_line /= n.max(psf_line)
    plot.plot(n.arange(len(psf_line)) + 43.6, psf_line)
    plot.show()


def oplot_rads():
    sample_rad(0, band='k', mode = 'pol', data = False, sw = True,normalize = True)
    sample_rad(0, band='j', mode = 'pol', data = False, sw = True,normalize = True)
    sample_rad(0, band='h', mode = 'pol', data = False, sw = True,normalize = True)
    plot.legend()
    plot.show()


def sample_rad(angle, band = 'k', mode = 'pol', data = True, sw = True, normalize = True):
    plot_ktot_data = False
    plot_kpol_data = False
    plot_kpol_model = False
    plot_hpol_data = False
    plot_hpol_model = False
    plot_jpol_data = False
    plot_jpol_model = False
    if band == 'k':
        if mode == 'tot':
            plot_ktot_data = True
        if mode == 'pol':
            if data is True:
                plot_kpol_data = True
            else:
                plot_kpol_model = True

    if band == 'h':
        if mode == 'pol':
            if data is True:
                plot_hpol_data = True
            if data is False:
                plot_hpol_model = True

    if band == 'j':
        if mode == 'pol':
            if data is True:
                plot_jpol_data = True
            else:
                plot_jpol_model = True

 

    if plot_ktot_data is True:
        im = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/reduced_data/april_k/kl1-minrot10.fits')
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/ktot_noisemap.fits')
        
        totbins_ne, totbins_sw = get_imbins(im, noise, angle = angle) 
        tot_imbins_ne, tot_noisebins_ne = totbins_ne
        tot_imbins_sw, tot_noisebins_sw = totbins_sw
        if sw is True:
            plot.errorbar(range(len(tot_imbins_sw)), tot_imbins_sw, yerr = tot_noisebins_sw, label = 'K1 Tot Data SW')
        else:
            plot.errorbar(range(len(tot_imbins_ne)), tot_imbins_ne, yerr = tot_noisebins_ne, label = 'K1 Tot Data NE')
    
        
    if plot_kpol_data is True:
        im = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/files_for_fit/HR4796A-K1-Qr-masked.fits')
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/kpol_noisemap.fits')

        polbins_ne, polbins_sw = get_imbins(im, noise, angle = angle) 
        pol_imbins_ne, pol_noisebins_ne = polbins_ne
        pol_imbins_sw, pol_noisebins_sw = polbins_sw    
        if normalize is True:
            nem = n.max(pol_imbins_ne)
            swm = n.max(pol_imbins_sw)
            pol_imbins_ne /= n.max(pol_imbins_ne)
            pol_imbins_sw /= n.max(pol_imbins_sw)
            pol_noisebins_ne /= nem
            pol_noisebins_sw /= swm
        if sw is True:
#            plot.errorbar(n.arange(len(pol_imbins_sw)), pol_imbins_sw, yerr = pol_noisebins_sw, label = 'K1 Pol Data SW')
            plot.plot(n.arange(len(pol_imbins_sw)), pol_imbins_sw, label = 'K1 Pol Data SW')
        else:
            plot.errorbar(range(len(pol_imbins_ne)), pol_imbins_ne, yerr = pol_noisebins_ne, label = 'K1 Pol Data NE')


    if plot_kpol_model is True:
#        mc = ModelComparator('hr4796_config_kpol.ini', mc_save = 'mcmc_kpol_intparms_even.p')
#        mc = ModelComparator('hr4796_config_kpol.ini', mc_save = 'mcmc_kpol_even.p')
        mc = ModelComparator('hr4796_config_kpol.ini', mc_save = 'mcmc_kpol_even.p')
        mc.diskobjs[0].parms['beta'] = 0
        im = mc.diskobjs[0].get_model_ring_image(convolve = True)
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/kpol_noisemap.fits')
        modelbins_ne, modelbins_sw = get_imbins(im, noise, angle = angle) 
        model_imbins_ne, model_noisebins_ne = modelbins_ne
        model_imbins_sw, model_noisebins_sw = modelbins_sw

        if normalize is True:
            model_imbins_ne /= n.max(model_imbins_ne)
            model_imbins_sw /= n.max(model_imbins_sw)

        if sw is True:
            plot.plot(range(len(model_imbins_sw)), model_imbins_sw, label = 'K1 Pol Model SW', linewidth = 2, color = 'red')

        else:
            plot.plot(range(len(model_imbins_ne)), model_imbins_ne, label = 'K1 Pol Model NE', linewidth = 2, color = 'red')

    if plot_hpol_model is True:
#        mc = ModelComparator('hr4796_config_hpol.ini', mc_save = 'mcmc_hpol_intparms_even.p')
        mc = ModelComparator('hr4796_config_hpol.ini', mc_save = 'mcmc_hpol_even.p')
        im = mc.diskobjs[0].get_model_ring_image(convolve = True)
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/kpol_noisemap.fits')
        modelbins_ne, modelbins_sw = get_imbins(im, noise, angle = angle) 
        model_imbins_ne, model_noisebins_ne = modelbins_ne
        model_imbins_sw, model_noisebins_sw = modelbins_sw
        
        if normalize is True:
            nem = n.max(model_imbins_ne)
            swm = n.max(model_imbins_sw)
            model_imbins_ne /= n.max(model_imbins_ne)
            model_imbins_sw /= n.max(model_imbins_sw)

        if sw is True:
            plot.plot(range(len(model_imbins_sw)), model_imbins_sw, label = 'H Pol Model SW', linewidth = 2, color = 'green')
        else:
            plot.plot(n.arange(len(model_imbins_ne)), model_imbins_ne, label = 'H Pol Model NE', linewidth = 2, color = 'red')

    if plot_hpol_data is True:
        im = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/files_for_fit/hr4796_h_qr_masked.fits')
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/hpol_noisemap.fits')
        polbins_ne, polbins_sw = get_imbins(im, noise, angle = angle) 
        pol_imbins_ne, pol_noisebins_ne = polbins_ne
        pol_imbins_sw, pol_noisebins_sw = polbins_sw    

        if normalize is True:
            nem = n.max(pol_imbins_ne)
            swm = n.max(pol_imbins_sw)
            pol_imbins_ne /= n.max(pol_imbins_ne)
            pol_imbins_sw /= n.max(pol_imbins_sw)
            pol_noisebins_ne /= nem
            pol_noisebins_sw /= swm



        if sw is True:
#            plot.errorbar(n.arange(len(pol_imbins_sw)), pol_imbins_sw, yerr = pol_noisebins_sw, label = 'H Pol Data SW')
            plot.plot(n.arange(len(pol_imbins_sw)), pol_imbins_sw, label = 'H Pol Data SW')
        else:
            plot.errorbar(range(len(pol_imbins_ne)), pol_imbins_ne, yerr = pol_noisebins_ne, label = 'H Pol Data NE')


    if plot_jpol_model is True:
        mc = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol_even.p')
        im = mc.diskobjs[0].get_model_ring_image(convolve = True)
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/jpol_noisemap.fits')

        modelbins_ne, modelbins_sw = get_imbins(im, noise, angle = angle) 
        model_imbins_ne, model_noisebins_ne = modelbins_ne
        model_imbins_sw, model_noisebins_sw = modelbins_sw
        
        if normalize is True:
            nem = n.max(model_imbins_ne)
            swm = n.max(model_imbins_sw)
            model_imbins_ne /= n.max(model_imbins_ne)
            model_imbins_sw /= n.max(model_imbins_sw)
            model_noisebins_ne /= nem
            model_noisebins_sw /= swm
            
        if sw is True:
            plot.plot(range(len(model_imbins_sw)), model_imbins_sw, label = 'J Pol Model SW', linewidth = 2, color = 'blue')
        else:
            plot.plot(n.arange(len(model_imbins_ne)), model_imbins_ne, label = 'J Pol Model NE', linewidth = 2, color = 'red')




    if plot_jpol_data is True:
        print 'passed'

        im = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/files_for_fit/HR4796A-J-Qr-masked.fits')
        noise = pyfits.getdata('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/noisemaps/jpol_noisemap.fits')
        polbins_ne, polbins_sw = get_imbins(im, noise, angle = angle) 
        pol_imbins_ne, pol_noisebins_ne = polbins_ne
        pol_imbins_sw, pol_noisebins_sw = polbins_sw    

        if normalize is True:
            nem = n.max(pol_imbins_ne)
            swm = n.max(pol_imbins_sw)
            pol_imbins_ne /= n.max(pol_imbins_ne)
            pol_imbins_sw /= n.max(pol_imbins_sw)
            pol_noisebins_ne /= nem
            pol_noisebins_sw /= swm



        if sw is True:
            plot.errorbar(n.arange(len(pol_imbins_sw)), pol_imbins_sw, yerr = pol_noisebins_sw, label = 'J Pol Data SW')
        else:
            plot.errorbar(range(len(pol_imbins_ne)), pol_imbins_ne, yerr = pol_noisebins_ne, label = 'J Pol Data NE')


    plot.legend()
    if normalize is True:
        plot.ylim([-0.1, 1.2])
#    plot.xlim([50,100])

def get_imbins(im, noise, angle = 0):
    rotim = image.rotate(im, (63.3 + angle) * n.pi / 180)
#    rotim = image.rotate(im, 50 * n.pi / 180)
    rotim = rotim[135:145]
    noise = noise[135:145]
    binsize = 1
    bins = n.arange(281)# / binsize)
    imbins = []
    noisebins = []
    nelems = 10 * 3
    for b in bins:
        lind = b * binsize
        uind = b * binsize + 3
        sl = rotim[:, lind:uind]
        sl = sl[~n.isnan(sl)]
        imbins.append(n.sum(sl))
#        noisebins.append(n.std(sl))
        noisebins.append(n.median(noise[:, lind:uind]))
    imbins = n.array(imbins)
    noisebins = n.array(noisebins) * nelems
    
    imbins1 = imbins[140:]
    noisebins1 = noisebins[140:]
 #   imbins1_max = n.max(imbins1)
 #   imbins1 /= imbins1_max
 #   noisebins1 /= imbins1_max

    imbins2 = n.flip(imbins[:140],0)
    noisebins2 = n.flip(noisebins[:140],0)

#    imbins2_max = n.max(imbins2)
#    imbins2 /= imbins2_max
#    noisebins2 /= imbins2_max


    return [imbins1, noisebins1], [imbins2, noisebins2]
    

def test_flat_ring_model():
    psf = pyfits.getdata('psf_k.fits')
    im = OffsetRingModel(psf_method = 'inputim', psf_ims = [psf])
    im.parms['r_in'] = im.parms['r'] - 5
    im.parms['r_out'] = im.parms['r'] + 5
    im.parms['offset'] = 1.5
    im.parms['gamma_in'] = -18
    im.parms['gamma'] = 4.
    im.parms['beta'] = 0.
    im.parms['I'] = 70. * numpy.pi / 180.
    im.parms['Omega'] = 25. * numpy.pi / 180.
    scale = 1.
    model = im.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = 2., n_step = 200, fast = True)
    return model



    



def test_h():
    conf = 'hr4796_config_hpsf.ini'
    leastsq_save = 'leastsq_hpol.p'
    mc = ModelComparator(conf, leastsq_save = leastsq_save)
    diskobj = mc.diskobjs[0]
    p = mc.parms
    model = diskobj.get_model()
    image = deproject(model, Omega = -p['omega1'], I = p['I'], omega = p['omega2'], offset = p['offset'])
    return image


def deproject(im, Omega = -27.8 * numpy.pi / 180., I = 77 * numpy.pi / 180., omega = -2.3 * numpy.pi / 180., offset = 0):
    ''' 
    Deproject - Given a disk's orbital parameters, gives the azimuthal 
    profile of the disk

    Omega, I, omega = Orbital parameters. Angles in radians
    rmin, rmax      = Bounds of the image deprojection, in pixels
    
    '''
    rmin = 50
    rmax = 130

    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]





    def rnu_to_NEzr(r, nu):
        "geometric transformation"
        sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
        
        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si
        
        pN -= oN 
        pE -= oE 
        pz -= oz
        
        r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
        
        return pN, pE, pz, r_star



    #Must set nans to zero otherwise interpolation errors
    im[numpy.isnan(im)] = 0
        


    #interpolation grid
    rimin, rimax = rmin-5., rmax+5. # [pix], [pix]
    #Evaluated numbers of rs and nus. Increase for finer sampling
    nri, nnui = 150, 100
#    nri, nnui = 20, 40
    dri = (rimax-rimin)/nri
    rri, nuis = numpy.mgrid[0:nri, 0:nnui]
    rri = rri*dri+rimin # [pix]
    nuis = nuis*2. *numpy.pi/nnui



    # coordinates in observed image
    pN, pE, pz, r_star = rnu_to_NEzr(rri, nuis)

    # This is assuming an image centered around (140,140)
    Nmin = -140. 
    Emin = -140. 
    dN = 1.
    dE = 1.

    # perform interpolation
    from scipy.ndimage import map_coordinates
    iy = (pN-Nmin)/dN
    ix = (pE-Emin)/dE

    #Displays interpolation points
    plot.imshow(im)
    plot.scatter(ix, iy)
    plot.show()
    im = map_coordinates(im, (iy, ix))

    # multiply by jacobian
#    detJ = rri*ci
#    im *= detJ
    
    # multiply by r_star^2
#    im *= r_star**2
    im = numpy.append(im[:, 50:], im, axis = 1)
#    print im
    # show recovered map
    fig = pylab.figure(1)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)
    cax = ax.imshow(im,
                    extent=(nuis.min()*180./numpy.pi,
                            nuis.max()*180./numpy.pi,
                            rri.min(), rri.max()),
                    interpolation='nearest')
    ax.set_aspect('auto')
    ax.set_xlabel(r"$\nu$ [deg]")
    ax.set_ylabel(r"$r$ [pix]")
    fig.colorbar(cax)
    pylab.draw()
    pylab.show()
#    im = numpy.sum(im, axis = 0)
#    plot.plot(im)
#    plot.show()
    return im



def polcoords(im = None):
    ''' 
    Only transforms from Cartesion to polar
    '''
    maxrad = 140
    maxphi = 360
    center = [140,140]
    if im is None:
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    shape = im.shape
    r, phi = numpy.meshgrid(numpy.arange(maxrad *4. )/4. + 1, numpy.arange(maxphi*4.)/4. - 180.)
    r_u = []
    phi_u = []
    vals = []
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
            if not numpy.isnan(im[i,j]):
                phi_c = numpy.arctan2((j - center[0]),(i - center[1])) * 180. / numpy.pi
                r_c = numpy.sqrt((center[0] - i)**2. + (center[1] - j)**2.)
                if r_c * numpy.sin(phi_c) != 0:
                    r_u.append(r_c)
                    phi_u.append(phi_c)
                    vals.append(im[i, j] )
    grid = interpol.griddata(numpy.array([r_u, phi_u]).transpose(), vals, (r, phi), method = 'cubic')
    plot.clf()
    plot.imshow(grid.transpose(), vmin = 0, vmax = 100, extent=[0,360,0,140])
    plot.plot([1,360],[76.4,76.4],color = 'white', lw = 3)
    cm = plot.get_cmap('gist_rainbow')
    for i in numpy.arange(24):
        sli = i * 10. + 60
        plot.plot([sli,sli],[1,140],color = cm(i * 10), lw = 2)

    plot.ylabel('Radius (AU)')
    plot.xlabel('Angle (degrees)')
    plot.colorbar()
    plot.show()

    ch = []

    for i in numpy.arange(24):
        sli = (i * 10. + 60) * 4
        chunk = numpy.median(grid[sli:sli+40], axis = 0) 
        chunk = chunk / numpy.nanmax(chunk)#+ .1* i
        plot.plot(numpy.arange(maxrad *4.)/4., chunk, color = cm(i * 10))
    plot.xlabel('Radius (AU')
    plot.ylabel('Scaled intensity')
    plot.plot([76.4,76.4],[1,1], lw = 3)
    plot.show()






def test1(imname):
    import numpy as n
    im = pyfits.getdata(imname)
    axis = n.arange(n.shape(im)[0]) - 140.
    axis *= 72.8 * .014
    import image

    x_b_init = 77.
    x_0_init = 60.
    y_0_init = 10.
    gam1_init = 8.
    gam2_init = -8.
    beta_init = -1.6
    y0s = []
    xbs = []
    angs = []
    from scipy.optimize import leastsq
    for i in n.arange(280 ):
        angle = i / 8.
        rotim = image.rotate(im, (angle + 75) * n.pi / 180)
        y = n.nansum(rotim[135:145], axis = 0)
        y = y[50:95]
        xaxis = n.abs(axis[50:95])
        init_params = [x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init]
#        model = smooth_broken_power_law(xaxis, x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init)
        p_opt, cov, info, mesg, ier = leastsq(fit_sbpl, init_params, args=(xaxis, y), full_output = True)
        x_b, x_0, y_0, gam1, gam2,beta = p_opt
        xaxis_smooth = n.linspace(50, 100, 200)
        mod = smooth_broken_power_law(xaxis_smooth, x_b, x_0, y_0, gam1, gam2, n.exp(beta)) 
        y0s.append(n.max(mod))
        xbs.append(x_b)
        angs.append(angle)
#        plot.plot(axis, y)
#        angles.append(angle)
#        wh = n.where(y == n.nanmax(y[50:90]))
#        maxval.append(axis[wh])
#    plot.show()
    plot.plot(angs, y0s)
    plot.xlabel('Angle from NE ansa (degrees)')
    plot.ylabel('SBPL parameter')
    plot.show()
    return y0s




def fit_sbpl(params, x, y):
    x_b, x_0, y_0, gam1, gam2, beta = params
    beta = n.exp(beta)
    ymodel = smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta)
    return (y - ymodel)**2.

def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y





def get_de_im():
    #Only deinclines the disk,
    maxrad = 90
    maxphi = 360
    inc = 77. * numpy.pi / 180.
    center = [140,140]

    conf = 'hr4796_config_tester.ini'
    mc = ModelComparator(conf)
    
#    mc.update_all_int_parms([1,1,1])
    Omega = mc.parms['omega1'] * 180. / numpy.pi
    

#    mc.parms['omega2'] = 0
#    mc.parms['K1_Polsoffx'] = 0
#    mc.parms['K1_Polsoffy'] = 0
#    mc.update_all_parms(mc.parms)
#    
#   im = mc.diskobjs[0].get_model_ring_image()
    
    im = pyfits.getdata('k1_mcfost_unconvolved.fits')

    sh = im.shape
    x, y = numpy.meshgrid(numpy.arange(sh[0]), numpy.arange(sh[1]))
    x_u = []
    y_u = []
    vals = []

    r, phi = numpy.meshgrid(numpy.arange(maxrad), numpy.arange(maxphi)- 180)
    r_u = []
    phi_u = []
    vals = []

    for i in numpy.arange(sh[0]):
        for j in numpy.arange(sh[1]):
            if not numpy.isnan(im[i,j]) and im[i,j] < 300:
                phi = Omega * numpy.pi / 180.
                x_c = (i-center[0]) * numpy.cos(phi) - (j-center[1]) * numpy.sin(phi) + center[0]
                y_c = (i-center[0]) * numpy.sin(phi) + (j-center[1]) * numpy.cos(phi) 
                vals.append(im[i,j] * numpy.cos(77.3 * numpy.pi / 180.))

                y_c = y_c / numpy.cos(77 * numpy.pi / 180.) + center[1]
#                y_c += offset * numpy.cos(omega)
#                x_c += offset * numpy.sin(omega)
                x_u.append(x_c)
                y_u.append(y_c)

#    plot.show()
    grid = interpol.griddata(numpy.array([x_u, y_u]).transpose(), vals, (x, y), method = 'linear')
#    pyfits.writeto('grid.fits', grid)
#    grid = numpy.zeros(x.shape)
#    for x_c, y_c in x, y:
    pyfits.writeto('deproj_mcfost.fits', grid)

    plot.imshow(grid)
    plot.show()

    return grid
    









def deincline():
    #Only deinclines the disk,
    maxrad = 90
    maxphi = 360
    inc = 77. * numpy.pi / 180.
    center = [140,140]
    im = pyfits.getdata('flatring.fits')
    sh = im.shape
    x, y = numpy.meshgrid(numpy.arange(sh[0] ), numpy.arange(sh[1]))
    x_u = []
    y_u = []
    vals = []

    r, phi = numpy.meshgrid(numpy.arange(maxrad), numpy.arange(maxphi)- 180)
    r_u = []
    phi_u = []
    vals = []

    for i in numpy.arange(sh[0]):
        for j in numpy.arange(sh[1]):
            if not numpy.isnan(im[i,j]) and im[i,j] < 300:
                phi = Omega * numpy.pi / 180.
                x_c = (i-140.) * numpy.cos(phi) - (j-140.) * numpy.sin(phi) + 140
                y_c = (i-140.) * numpy.sin(phi) + (j-140) * numpy.cos(phi) 
                vals.append(im[i,j] * numpy.cos(77.3 * numpy.pi / 180.))

                y_c = y_c / numpy.cos(77 * numpy.pi / 180.) + 140.
#                y_c += offset * numpy.cos(omega)
#                x_c += offset * numpy.sin(omega)
                x_u.append(x_c)
                y_u.append(y_c)

#    plot.show()
    grid = interpol.griddata(numpy.array([x_u, y_u]).transpose(), vals, (x, y), method = 'linear')
#    pyfits.writeto('grid.fits', grid)
#    grid = numpy.zeros(x.shape)
#    for x_c, y_c in x, y:
        
    grid = gaussian_filter(grid, 3)
    plot.imshow(grid, vmin = 0)
    plot.plot([140,140],[1,280], lw = 3, color = 'w')
    plot.plot([1,280],[140,140], lw = 3, color = 'w')
    plot.plot([140 - 76,140-76],[1,280], lw = 1, color = 'w')
    plot.plot([1,280],[140-76,140-76], lw = 2, color = 'w')
    plot.plot([140+76,140+76],[1,280], lw = 2, color = 'w')
    plot.plot([1,280],[140+76,140+76], lw = 2, color = 'w')
    plot.colorbar()
    plot.show()

    return grid
    




