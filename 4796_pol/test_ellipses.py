from kepler import *
from matplotlib.patches import Ellipse
import matplotlib.pyplot as plot
import numpy as n




def plot_ellipses(e, inc, omega, Omega):
    smas = [1., 3, 5., 7., 10.]
    fig, ax = plot.subplots()
    for a in smas:
        ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a, e, inc, omega, Omega)
        ax.add_artist(Ellipse((E0, N0), width = 2. * ap, height = 2. * bp, angle = phi * 180. / n.pi,fill = False))

    plot.xlim([-20, 20])
    plot.ylim([-20, 20])

    plot.show()




def ellipse_parameterizations(e, inc, omega, Omega, a):

    fig, ax = plot.subplots()
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a, e, inc, omega, Omega)
    print E0
    print N0
    ax.add_artist(Ellipse((E0, N0), width = 2. * ap, height = 2. * bp, angle = phi * 180. / n.pi,fill = False))
                                  
    n_step = 20
    nu = n.linspace(0., n.pi, n_step, endpoint=False)
    r = a * (1. - e**2.) / (1. + e * n.cos(nu))
    
    so, co = n.sin(omega), n.cos(omega)
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(inc), n.cos(inc)
    
    #Euler angle transformation                                                                   
    pN = r * (cO*conu-sO*sonu*ci) # [AU]                                                          
    pE = r * (sO*conu+cO*sonu*ci) # [AU]  

    plot.scatter(pE, pN)


    plot.xlim([-20, 20])
    plot.ylim([-20, 20])

    plot.show()


def offset_circle(I, omega, Omega, offset, r):
    n_step = 20
    nu = n.linspace(0., n.pi, n_step, endpoint=False) # [rad]                             

    so, co = n.sin(omega), n.cos(omega)
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)

    #Euler angle transformation                                                              
    pN_noff = r * (cO*conu-sO*sonu*ci) # [AU]                                                
    pE_noff = r * (sO*conu+cO*sonu*ci) # [AU]                                                
    pz_noff = r * sonu*si

    oN = offset * (cO*co-sO*so*ci) # [AU]                                                    
    oE = offset * (sO*co+cO*so*ci) # [AU]                                                    
    oz = offset * so*si # [AU]                                                               
    
    #Subtract offset angles                                                                  
    pN = pN_noff - oN
    pE = pE_noff - oE
    pz = pz_noff - oz
    plot.scatter(pE, pN)
#    plot.show()
    



def face_on():
    e = .4
    inc = 77. * n.pi / 180
    omega = 0. * n.pi / 180
    Omega = -22.6 *  n.pi / 180.
    a = 10
    ellipse_parameterizations(e, inc, omega, Omega, a)

    # e = 0.
    # inc = 77. * n.pi / 180
    # omega = 0. * n.pi / 180
    # Omega = -22.6 *  n.pi / 180.
    # a = 10
    # ellipse_parameterizations(e, inc, omega, Omega, a)


    offset = 10
    offset_circle(inc, omega, Omega, offset, a)
#    plot_ellipses(e, inc, omega, Omega)

    
