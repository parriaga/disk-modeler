import numpy as n


def au_to_pix(r_au):
    au_per_pix = 72.8 * 0.01414
    r_pix = r_au / au_per_pix
    return r_pix


def kband():
    rau = 77.28
    drau = 0.109
    rpix = au_to_pix(rau)
    drpix = au_to_pix(drau)
    
    ps = 0.0141 #plate scale Konopacky 2014, arcseconds / pix
    dps = 1e-5
    
    drau_real = n.sqrt((drpix / rpix)**2. + (dps / ps)**2.) * rau
    print drau_real

def radii():
    kau = 77.32 
    hau = 76.63
    jau = 77.13
    
    print au_to_arcsecs(kau)
    print au_to_arcsecs(hau)
    print au_to_arcsecs(jau)




def au_to_arcsecs(au):
    pix = au_to_pix(au)
    ps = 0.0141 #plate scale Konopacky 2014, arcseconds / pix
    arcsecs = ps * pix

    return arcsecs

def offs(off_au, off_dau):
    off_pix = au_to_pix(off_au) - 1
    off_dpix = au_to_pix(off_dau) - 1
    
    ps = 0.0141 #plate scale Konopacky 2014, arcseconds / pix
    dps = 1e-5
    
    off_as = off_pix * ps
    off_das = off_dpix * ps

    off_das = off_as * n.sqrt((off_das / off_as)**2 + (dps / ps)**2.)
    
    print 'offset = ' + str(off_as * 1000)[:6] + ' pm ' + str(off_das * 1000)[:5]

def cal_offs():
    offk = 3.82
    offh = 5.57
    offj = 2.27
    doffk = .16
    doffh = .138
    doffj = .06

    offs(offk, doffk)
    offs(offh, doffh)
    offs(offj, doffj)
    

def dx_dy_to_off(a, da, b, db):
    err_denom = n.sqrt(a**2. + b**2)
    err_nom = n.sqrt(a*2 * da**2. + b**2. * db**2.)
    off = n.sqrt(a**2. + b**2.)
    print 'offset = ' + str(off)[:6] + ' pm ' + str(err_nom / err_denom)[:5]

def milli():
    a = -4.
    da = 4.
    b = 28.
    db = 5.
    dx_dy_to_off(a, da, b, db)
    
