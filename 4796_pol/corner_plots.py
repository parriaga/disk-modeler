import numpy as n
import constants as c
import triangle
from diskmodeler import ModelComparator
import matplotlib.pyplot as plot





def plot_walker(chain, ind, param, nwalkers, is_1d = False):
    if is_1d is False:
        param_set = chain[:, :, ind]
    else:
        param_set = chain
    for walker in range(nwalkers):
        line = param_set[walker, :]
        plot.plot(line, c = 'blue')
    plot.xlabel('Walker')
    plot.ylabel(param)
    plot.show()




def get_med_and_std(patch, return_frame = False):
    patch = patch.flatten()

    std = n.std(patch)
    med = n.median(patch)
    wh = n.where(n.abs(patch - med) > 3. * std)
    patch = n.delete(patch, wh)
    if return_frame is False:
        return n.median(patch), n.std(patch)
    else:
        return n.median(patch), n.std(patch), patch


def plot_k():
    mc = ModelComparator('hr4796_config_kpol2.ini',mc_save = 'mcmc_kpol_intfactor.p')
    mc.print_parms()
    nburn = 90
    chain = mc.chain
    sh = n.shape(chain)
    nwalkers = sh[0]
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
    print mc.is_fixed
    fit_parmlist = mc.fit_parmlist
#    fit_parmlist = ['I', 'omega1', 'J_Polint_parms', 'gamma_in', 'gamma']
    print fit_parmlist
    counter = 0
    int_sets = []

    sample_sets = []

    chain = chain[:, nburn:, :]
    labels = []
    osino = []
    ocoso = []
    for param in fit_parmlist:

        if param == 'osino' or param == 'ocoso':
            parm_len = 1
        else:
            parm_len = parm_lens[param]
        if parm_len == 1:
            if param == 'I' or param == 'omega1':
                chain[:, :, counter] *= 180 / n.pi 
                #        plot_walker(chain, counter, param, nwalkers)
            if param == 'osino':
                osino = chain[:, :, counter].flatten()
            elif param == 'ocoso':
                ocoso = chain[:, :, counter].flatten()
            else:
                sample_sets.append(chain[:, :, counter].flatten())
                print param
                paramchain = chain[:, :, counter].flatten()
                med, std, patch = get_med_and_std(paramchain, return_frame = True)
                med, std, patch = get_med_and_std(patch, return_frame = True)

                print n.std(chain[:, :, counter].flatten())
            counter += 1
            if param == 'omega1':
                labels.append('$\Omega$')
            elif param == 'gamma_in':
                labels.append('$gamma_{in}$')
            elif param == 'osino' or param == 'ocoso':
                pass
            else:
                labels.append(param)

        else:
            int_sets.append(chain[:, :, counter:counter+parm_len])
            counter += parm_len

    sample_sets.append(n.sqrt(osino**2. + ocoso**2.))
    print 'offset'
    print n.median(n.sqrt(osino**2. + ocoso**2.))
    print n.std(n.sqrt(osino**2. + ocoso**2.))
    sample_sets.append(n.arctan2(osino, ocoso) * 180 / n.pi)
    print 'direction'
    print n.median(n.arctan2(osino, ocoso) * 180 / n.pi)
    print n.std(n.arctan2(osino, ocoso) * 180 / n.pi)

    labels.append('offset')
    labels.append('$\omega$')
    sample_sets = n.array(sample_sets).T
    triangle.corner(sample_sets, plot_contours = False, labels = labels)
    plot.show()


#####################################################################################
#####################################################################################
#####################################################################################
def plot_h():
    mc = ModelComparator('hr4796_config_hpol.ini',mc_save = 'mcmc_hpol_even2.p')
    nburn = 50
    chain = mc.chain
    sh = n.shape(chain)
    nwalkers = sh[0]
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
#    fit_parmlist = ['I', 'omega1', 'J_Polint_parms', 'gamma_in', 'gamma']
    print fit_parmlist
    routind = n.where(fit_parmlist == 'r_out')
    rinind = n.where(fit_parmlist == 'r_in')
    counter = 0
    int_sets = []

    sample_sets = []
    chain = chain[:, nburn:, :]
    labels = ['I']
    for param in fit_parmlist:
        print param
        parm_len = parm_lens[param]
        print parm_len
        if parm_len == 1:
            if param != 'r_in' and param != 'r_out' and param != 'gamma_in' and param != 'gamma':
                if param == 'I' or param == 'omega1':
                    chain[:, :, counter] *= 180 / n.pi 
                else:
                    plot_walker(chain, counter, param, nwalkers)
                sample_sets.append(chain[:, :, counter].flatten())
                print param
                if param == 'omega1':
                    labels.append('$\Omega$')
                elif param == 'gamma_in':
                    labels.append('$gamma_{in}$')
                elif param == 'r':
                    labels.append('r')

                print labels
            counter += 1
        else:
            int_sets.append(chain[:, :, counter:counter+parm_len])
            counter += parm_len
    

    sample_sets = n.array(sample_sets).T
    print sample_sets
#   fit_parmlist = ['I', 'omega1', 'gamma_in', 'gamma']
    triangle.corner(sample_sets, plot_contours = False, labels = labels)
    plot.show()


def plot_j():
    mc = ModelComparator('hr4796_config_jpol.ini',mc_save = 'mcmc_jpol_even.p')
    mc.print_parms()
    nburn = 100
    chain = mc.chain
    sh = n.shape(chain)
    nwalkers = sh[0]
    parms_throwaway, fit_parmlist, parm_lens = mc.get_fit_parms(get_parmlist = True)
    print fit_parmlist
#    fit_parmlist = ['I', 'omega1', 'J_Polint_parms', 'gamma_in', 'gamma']
    counter = 0
    int_sets = []

    sample_sets = []

    chain = chain[:, nburn:, :]
    for param in fit_parmlist:
        parm_len = parm_lens[param]
        if parm_len == 1:
            if param == 'I' or param == 'omega1':
                chain[:, :, counter] *= 180 / n.pi 
                #        plot_walker(chain, counter, param, nwalkers)
            sample_sets.append(chain[:, :, counter].flatten())
            counter += 1
        else:
            int_sets.append(chain[:, :, counter:counter+parm_len])
            counter += parm_len


    sample_sets = n.array(sample_sets).T
#    fit_parmlist = ['I', 'omega1', 'gamma_in', 'gamma']
    triangle.corner(sample_sets, plot_contours = False)
    plot.show()

