import numpy as n
import matplotlib.pyplot as plot
import pyfits
import numpy
import scipy.interpolate as interpol
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter
import pylab
import deproject
from diskmodeler import ModelComparator
import matplotlib.gridspec as gridspec
import image
from lazy_functions import *


def test1():
    '''
    Testing rotation of images 
    '''

    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_7.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    
    omega = mc.parms['omega1']
    kim = mc.diskobjs[0].tfim
    jim = mc.diskobjs[1].tfim
    him = mc.diskobjs[2].tfim
    rotimk = image.rotate(kim, -omega + n.pi / 2)[150:165,120:160]
    rotimj = image.rotate(jim, -omega + n.pi / 2)[150:165,120:160]
    rotimh = image.rotate(him, -omega + n.pi / 2)[150:165,120:160]

    fig, (ax1, ax2, ax3) = plot.subplots(3,1)
    
    ax1.imshow(rotimk,vmin = 10, vmax = 150)
    ax2.imshow(rotimh,vmin = 10, vmax = 160)
    ax3.imshow(rotimj,vmin = 10, vmax = 150)

    plot.show()
            
def getlines(tfim, omega):
    rotim = image.rotate(tfim, -omega + n.pi / 2.)
    line_NE = n.sum(rotim[138:142,51:90],axis=0)
    line_SW = n.sum(rotim[138:142,195:230], axis=0)
    return line_NE, line_SW

def test2():
    '''
    Testing fitting a sbpl to the profile. Won't work because of PSF
    '''
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    
    omega = mc.parms['omega1']



    linek_NE, linek_SW = getlines(mc.diskobjs[0].tfim, omega)
    linej_NE, linej_SW = getlines(mc.diskobjs[1].tfim, omega)
    lineh_NE, lineh_SW = getlines(mc.diskobjs[2].tfim, omega)
    
    ## Inits for NE
    x_b_init = 11.
    x_0_init = 3.
    y_0_init = 50.
    gam1_init = 2
    gam2_init = -3
    beta_init = .2
    
    inits_NE = [x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init]

    def get_sbpl_func(line, inits):
        # Initial parameters
        xaxis = n.arange(len(line))
        x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init = inits
    
        
        y = smooth_broken_power_law(xaxis, x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init)
        # Fitting for sbpl parameters
        from scipy.optimize import leastsq
        init_params = [x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init]
        p_opt, cov, info, mesg, ier = leastsq(fit_sbpl, init_params, args=(xaxis, linej_NE), full_output = True)
        x_b, x_0, y_0, gam1, gam2, beta = p_opt
        sbpl = lambda x: smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta) 
        return sbpl, p_opt
        
    sbpl_h_NE, p_opt_h_NE = get_sbpl_func(linek_NE, inits_NE)
            
    
    def find_fwhm(func, params):
        # Find HM value
        x_b, x_0, y_0, gam1, gam2, beta = params
        xaxis_new = n.linspace(x_0,x_b * 10, 1000)

        
        y = func(xaxis_new)
        hm = n.max(y) / 2.

        def zero_func(x, f, hm):
            return n.abs(f(x) - hm)
        from scipy.optimize import fmin 
    
        xopt1 = fmin(zero_func, x_b / 2, args=(func, hm))
        xopt2 = fmin(zero_func, x_b * 3 / 2, args=(func, hm))
        
        return xopt2 - xopt1 


    print find_fwhm(sbpl_h_NE, p_opt_h_NE) * .01421

    return
    xaxis = range(len(linek_NE))
    print linek_NE
    a, x0, sig, c = gauss_fit(xaxis, lineh_SW)
    
    fwhm = 2. * n.sqrt(2. * n.log(2)) * sig
    print 'FWHM' + str(fwhm)
    print 'FWHM in arcsec' + str(fwhm * .0142)
    
    kfunc = lambda x: a * n.exp(-(x - x0)**2. / (2 * sig**2)) + c
    

    plot.plot(linek_NE, color = 'blue')
    plot.plot(xaxis, kfunc(xaxis))
#    plot.plot(lineh_NE, color = 'green')
#    plot.plot(linej_NE, color = 'red')
    plot.show()


def test3():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    mc.update_all_int_parms([1,1,1])
    omega = mc.parms['omega1']
    tfim = mc.diskobjs[0].tfim
    mod = mc.diskobjs[0].get_model_ring_image()
    line_ne, line_sw = getlines(tfim, omega)
    line_nem, line_swm = getlines(mod, omega)
    mc.print_parms()
    mc.parms['beta'] = 0.
    mc.parms['r_in'] = 72.
    mc.parms['r'] = 77.
    mc.parms['r_out'] = 92.
    mc.parms['K1_Polsoffy'] = 0
    mc.parms['K1_Polsoffx'] = 0
    mc.parms['offset'] = 1.5
    mc.parms['omega2'] = -23. * n.pi / 180.
    def fit_fn_fm(p, mc, return_im = False):
        gamma_in, gamma, r_in, r, r_out, intparm = p
        mc.parms['r'] = r
        mc.parms['r_in'] = r_in
        mc.parms['r_out'] = r_out
        mc.parms['gamma_in'] = gamma_in
        mc.parms['gamma'] = gamma
        mc.parms['K1_Polint_parms'][:] = intparm
        mc.update_all_parms(mc.parms)
        mc.print_parms()
        mod = mc.diskobjs[0].get_model_ring_image()
        line_ne, line_sw = getlines(tfim, omega)
        line_nem, line_swm = getlines(mod, omega)
        if return_im:
            return line_ne, line_nem, line_sw, line_swm
        else:
            return (line_ne - line_nem)**2. #n.append((line_ne - line_nem)**2.,(line_sw - line_swm)**2.)
    init_parms = [-13., 14.,72., 76, 92, 1.e8] 
    p_opt, foo = leastsq(fit_fn_fm, init_parms, args = (mc), epsfcn = 1.e-7)
    line_ne, line_nem, line_sw, line_swm = fit_fn_fm(p_opt, mc, return_im = True)
#    plot.plot(line_sw)
#    plot.plot(line_swm)
    plot.plot(line_ne)
    plot.plot(line_nem)
    plot.show()
    return mc
    

def test5(fit = True):
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'
    mc = ModelComparator(conf, mc_save = mc_save)
#    mc.update_all_int_parms([1,1,1])
    omega = mc.parms['omega1']

#    line_sw, line_swm = fit_fn_fm(p_opt, mc, return_im = True)#    mc.parms['gamma_in'] = -10
#    mc.parms['r_in'] = 60
#    mc.update_all_parms(mc.parms)

    
    tfim = mc.diskobjs[0].tfim
    mod = mc.diskobjs[0].get_model_ring_image()#convolve=False, drad0=.5, n_step = 400)


    # rotim = image.rotate(tfim, -omega)
    # plot.imshow(rotim)
    # plot.show()
    # rotimm = image.rotate(mod, -omega)
    # line_NE = n.nansum(rotim[137:142],axis=0)
    # line_NEm = n.nansum(rotimm[137:142],axis=0)
    # xaxis = n.arange(len(line_NE)) - 140

    # plot.title('Minor axis profile')
    # plot.ylabel('Counts (ADU)')
    # plot.xlabel('Distance from center (pix)')
    # plot.plot(line_NE, label='data')
    # plot.plot(line_NEm, label='model')
    # plot.show()
    # return
    
    

    def getlines(tfim, omega):
        rotim = image.rotate(tfim, -omega)
        line_NE = n.sum(rotim[137:142,153:170],axis=0)
        return line_NE


    line_ne = getlines(tfim, omega)
    mc.print_parms()
#    mc.parms['K1_Polsoffy'] = 0.5
#    mc.parms['K1_Polsoffx'] = 0.5
#    mc.parms['offset'] = 0
    mc.parms['beta'] = 0.
    mc.parms['r_in'] = 72.
    mc.parms['r'] = 76.
    mc.parms['r_out'] = 92.
    def fit_fn_fm(p, mc, return_im = False):
        gamma_in, gamma, r_in, r, r_out, intparm = p
        mc.parms['r'] = r
        mc.parms['r_in'] = r_in
        mc.parms['r_out'] = r_out
        mc.parms['gamma_in'] = gamma_in
        mc.parms['gamma'] = gamma
        mc.parms['K1_Polint_parms'][:] = intparm
        mc.update_all_parms(mc.parms)
        mc.print_parms()
        mod = mc.diskobjs[0].get_model_ring_image()
        line_ne = getlines(tfim, omega)
        line_nem = getlines(mod, omega)
        if return_im:
            return line_ne, line_nem
        else:
            return (line_ne - line_nem)**2.
    
#        plot.plot(line_ne)
#        plot.plot(line_nem)
#        plot.show()
    init_parms = [-3., 1.4,72.,77,130, 5e8] 
    if fit == True:
        p_opt, foo = leastsq(fit_fn_fm, init_parms, args = (mc), epsfcn = 1.e-3)
        line_sw, line_swm = fit_fn_fm(p_opt, mc, return_im = True)
        print p_opt
    else:
        line_sw, line_swm = fit_fn_fm(init_parms, mc, return_im = True)
    plot.plot(line_sw)
    plot.plot(line_swm)
    plot.show()
    return mc
    


def test6(fit = True):
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    omega = mc.parms['omega1']

    tfim = mc.diskobjs[0].tfim
    mod = mc.diskobjs[0].get_model_ring_image()#convolve=False, drad0=.5, n_step = 400)


    rotim = image.rotate(tfim, -omega)
    return rotim
    plot.imshow(rotim[120:160,140:170])
    plot.show()
    return
    
    

    def getlines(tfim, omega):
        rotim = image.rotate(tfim, -omega)
        line_NE = n.sum(rotim[137:142,153:170],axis=0)
        return line_NE


    line_ne = getlines(tfim, omega)
    mc.print_parms()
#    mc.parms['K1_Polsoffy'] = 0.5
#    mc.parms['K1_Polsoffx'] = 0.5
#    mc.parms['offset'] = 0
    mc.parms['beta'] = 0.
    mc.parms['r_in'] = 72.
    mc.parms['r'] = 76.
    mc.parms['r_out'] = 92.
    def fit_fn_fm(p, mc, return_im = False):
        gamma_in, gamma, r_in, r, r_out, intparm = p
        mc.parms['r'] = r
        mc.parms['r_in'] = r_in
        mc.parms['r_out'] = r_out
        mc.parms['gamma_in'] = gamma_in
        mc.parms['gamma'] = gamma
        mc.parms['K1_Polint_parms'][:] = intparm
        mc.update_all_parms(mc.parms)
        mc.print_parms()
        mod = mc.diskobjs[0].get_model_ring_image()
        line_ne = getlines(tfim, omega)
        line_nem = getlines(mod, omega)
        if return_im:
            return line_ne, line_nem
        else:
            return (line_ne - line_nem)**2.
    
#        plot.plot(line_ne)
#        plot.plot(line_nem)
#        plot.show()
    init_parms = [-5., 10,72.,77,100, 5e8] 
    if fit == True:
        p_opt, foo = leastsq(fit_fn_fm, init_parms, args = (mc), epsfcn = 1.e-3)
        line_sw, line_swm = fit_fn_fm(p_opt, mc, return_im = True)
        print p_opt
    else:
        line_sw, line_swm = fit_fn_fm(init_parms, mc, return_im = True)
    plot.plot(line_sw)
    plot.plot(line_swm)
    plot.show()
    return mc
    




def fit_sbpl(params, x, y):
    x_b, x_0, y_0, gam1, gam2, beta = params
    ymodel = smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta)
    return (y - ymodel)**2.

def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y
