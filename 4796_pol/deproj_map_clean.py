import numpy as n
import matplotlib.pyplot as plot
import pyfits
import numpy
import scipy.interpolate as interpol
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter
import pylab
import deproject
from diskmodeler import ModelComparator

import matplotlib.gridspec as gridspec




def get_deproj_grid(diskobj,samp = 4, model = True):
    I = diskobj.parms['I']
    Omega = diskobj.parms['omega1']
    offset = diskobj.parms['offset']
    omega = diskobj.parms['omega2']
    if model == True:
        im = diskobj.get_model()
    else:
        im = diskobj.fim
    grid = deprojmap(im, I, -Omega, offset, omega, samp = samp, show = False)
    gr = n.fliplr(grid.T)
    return gr #shifted_grid.T




def offset_ims():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_8.p'

    mc = ModelComparator(conf, mc_save = mc_save)

    imk = get_deproj_grid(mc.diskobjs[0], model = False)
    imj = get_deproj_grid(mc.diskobjs[1], model = False)
    imh = get_deproj_grid(mc.diskobjs[2], model = False)
    
    mc = ModelComparator(conf, mc_save = mc_save)

    gs1 = gridspec.GridSpec(3, 1)
    gs1.update(wspace = 0, hspace = 0)

    ax1 = plot.subplot(gs1[0,0])
    ax2 = plot.subplot(gs1[1,0])
    ax3 = plot.subplot(gs1[2,0])

    
    ax1.imshow(imk, vmin = 40, vmax = 130, extent = (0,360, 0, 140))
    ax1ylabel = ax1.set_ylabel('r (AU)')
    ax1.text(500, 325, 'K1')
    ax1.set_title('Model')
    ax1.xaxis.set_visible(False)
    
    ax2.imshow(imh, vmin = 40, vmax = 150, extent = (0,360, 0, 140))
    ax2.xaxis.set_visible(False)
    ax2.text(490, 325, 'H')
    ax2.set_ylabel('r (AU)')

    ax3.imshow(imj, vmin = 40, vmax = 130, extent = (0,360, 0, 140))
    ax3.text(500, 325, 'J')
    ax3.set_ylabel('r (AU)')



    axes = (ax1, ax2, ax3)
    for ax in axes:
        ax.set_ylim([60, 100])
        ax.yaxis.set_ticks(n.arange(60, 100, 10))
    plot.show()
    
    



def rnu_to_NEzr(r, nu, omega, Omega, I, offset):
    ''' 
    Transforms from disk plane to sky plane
        Inputs: (in radians) r, nu, omega Omega I, offset
        Outputs: pN, pE, omega, Omega, I, offset
    '''
    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]

    "geometric transformation"
    sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
    
    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si
    
    pN -= oN 
    pE -= oE 
    pz -= oz
    
    r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
    
    return pN, pE, pz, r_star

def NEzr_to_rnu(pN, pE, omega, Omega, I, offset):
    '''
    Transforms from sky plane to disk plane
        Inputs: pN, pE, omega, Omega, I, offset (in radians)
        Outputs: r, nu
    '''
    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    alpha = (pN + oN) / (pE + oE)
    #Geometric 
    nu = numpy.arctan2((cO - alpha * sO), ci * ( sO + alpha * cO))
    sinu, conu = numpy.sin(nu), numpy.cos(nu)
    r = (pN + oN) / (cO * conu - sO * sinu * ci)
    nu = nu - numpy.pi / 8
    return r, nu


def shift_map(im, diskobj, retphiscas = False):
    n_step = n.shape(im)[0]
    nu, r_star, phi_sca, r_proj, PA_proj, intensities = diskobj.parametric_model_ring(n_step = n_step)
    phimin = n.where(phi_sca == min(phi_sca))[0]
    phibegin = phimin[0] + n_step / 4
    im = n.vstack((im[phibegin:,:], im[5:phibegin , :]))

    phibegin2 = n_step / 8
    im = n.vstack((im[:phibegin2,:], im[phibegin2+1:, :]))

    return im
    


def deprojmap(im, I, Omega, offset, omega, maxrad = 140, maxphi = 360, samp = 4, show = True):
    center = [140,140]
    shape = im.shape

    # R and phi of each i and j
    r_u = []
    phi_u = []
    # Flattened image
    vals = []
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
            if not numpy.isnan(im[i,j]):
                # Phi and r in the disk plane
                r_c, phi_c = NEzr_to_rnu(i - center[0], j - center[1], omega, Omega, I, offset)
                if ~numpy.isnan(r_c) and ~numpy.isnan(phi_c):
                    if r_c < 0:
                        r_c = numpy.abs(r_c)
                        if phi_c > 0:
                            phi_c = - numpy.pi + numpy.abs(phi_c)
                        else: 
                            phi_c = numpy.pi - numpy.abs(phi_c)
                    phi_c *= 180. / numpy.pi
                    if phi_c < 0:
                        phi_c +=360.
                    if r_c * numpy.sin(phi_c) != 0:
                        r_u.append(r_c)
                        phi_u.append(phi_c)
                        if im[i, j] < 1.e-2:
                            vals.append(0.)
                        else:
                            vals.append(im[i, j])

    # Make grid to interpolate onto 
    r, phi = numpy.meshgrid(numpy.arange(maxrad * samp) / samp + 1, numpy.arange(maxphi * samp) / samp )

    # Interpolate vals which are at r_u and phi_u onto new gris of r and phi
    grid = interpol.griddata(numpy.array([r_u, phi_u]).transpose(), vals, (r, phi), method = 'cubic')
    if show == True:
        plot.clf()
        plot.imshow(grid.transpose(), vmin = 0, vmax = 110, extent=[0,360,0,maxrad])
        plot.show()
    return grid




