import numpy as n
import matplotlib.pyplot as plot
import pyfits
import numpy
import scipy.interpolate as interpol
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter
import pylab
import deproject
from diskmodeler import ModelComparator
import matplotlib.gridspec as gridspec
import image
from lazy_functions import *


def test1():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_7.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    
    omega = mc.parms['omega1']
    kim = mc.diskobjs[0].tfim
    jim = mc.diskobjs[1].tfim
    him = mc.diskobjs[2].tfim
    rotimk = image.rotate(kim, -omega + n.pi / 2)[150:165,120:160]
    rotimj = image.rotate(jim, -omega + n.pi / 2)[150:165,120:160]
    rotimh = image.rotate(him, -omega + n.pi / 2)[150:165,120:160]




    fig, (ax1, ax2, ax3) = plot.subplots(3,1)
    
    ax1.imshow(rotimk,vmin = 10, vmax = 150)
    ax2.imshow(rotimh,vmin = 10, vmax = 160)
    ax3.imshow(rotimj,vmin = 10, vmax = 150)

    plot.show()
            


def test2():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_all_pol_7.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    
    omega = mc.parms['omega1']

# Show region 
#    fig, ax1 = plot.subplots()
#    ax1.imshow(rotimk)
#    import matplotlib as mpl
#    ax1.add_patch(mpl.patches.Rectangle((55,137), 35, 6))
#    plot.show()
#    return

    def getlines(tfim, omega):
        rotim = image.rotate(tfim, -omega + n.pi / 2.)
        line_NE = n.sum(rotim[137:143,55:90],axis=0)
        line_SW = n.sum(rotim[137:143,195:230], axis=0)
        return line_NE, line_SW

    linek_NE, linek_SW = getlines(mc.diskobjs[0].tfim, omega)
    linej_NE, linej_SW = getlines(mc.diskobjs[1].tfim, omega)
    lineh_NE, lineh_SW = getlines(mc.diskobjs[2].tfim, omega)
    
    
    
    xaxis = n.arange(len(lineh_NE))
    plot.plot(xaxis, lineh_NE)
#    plot.plot(lineh_SW)
 
    x_b_init = 11.
    x_0_init = 3.
    y_0_init = 50.
    gam1_init = 2
    gam2_init = -3
    beta_init = .2
    
    y = smooth_broken_power_law(xaxis, x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init)

    from scipy.optimize import leastsq
    init_params = [x_b_init, x_0_init, y_0_init, gam1_init, gam2_init, beta_init]
    p_opt, cov, info, mesg, ier = leastsq(fit_sbpl, init_params, args=(xaxis, linej_NE), full_output = True)
    print p_opt

    x_b, x_0, y_0, gam1, gam2, beta = p_opt

    y = smooth_broken_power_law(xaxis, x_b, x_0, y_0, gam1, gam2, beta)                                         
    
    plot.plot(xaxis, y)
    plot.show()

    return
    xaxis = range(len(linek_NE))
    print linek_NE
    a, x0, sig, c = gauss_fit(xaxis, lineh_SW)
    
    fwhm = 2. * n.sqrt(2. * n.log(2)) * sig
    print 'FWHM' + str(fwhm)
    print 'FWHM in arcsec' + str(fwhm * .0142)
    
    kfunc = lambda x: a * n.exp(-(x - x0)**2. / (2 * sig**2)) + c
    

    plot.plot(linek_NE, color = 'blue')
    plot.plot(xaxis, kfunc(xaxis))
#    plot.plot(lineh_NE, color = 'green')
#    plot.plot(linej_NE, color = 'red')
    plot.show()


def fit_sbpl(params, x, y):
    x_b, x_0, y_0, gam1, gam2, beta = params
    ymodel = smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta)
    print params
    return (y - ymodel)**2.

def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y
