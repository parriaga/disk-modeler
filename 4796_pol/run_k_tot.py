from diskmodeler import ModelComparator
from diskmodeler_plotutils import *


conf = 'hr4796_config_ktot.ini'
mc = ModelComparator(conf, leastsq_save='leastsq_ktot_radparms.p')

mc.parms['gamma_in'] = -4.3
mc.parms['gamma'] = 13.8
mc.parms['r_in'] = 75.11
mc.parms['r'] = 77.
mc.parms['r_out'] = 113.9
mc.parms['beta'] = 0
mc.update_all_int_parms([20])

#mc.parms['K_Totint_parms'][:] 
mc.is_fixed['r_in'] = True
mc.is_fixed['gamma_in'] = True
mc.is_fixed['gamma_out'] = True
mc.is_fixed['r_out'] = True
mc.is_fixed['omega1'] = False
mc.is_fixed['I'] = False


mc.update_all_parms(mc.parms)
mc.do_leastsq()
mc.save_parms('leastsq_ktot_minrot10.p')
