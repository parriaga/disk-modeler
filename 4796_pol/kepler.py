# $Id$
#
# code for dealing with two-body orbits
#
# Michael Fitzgerald  (mpfitz@llnl.gov)  2008-2-26
#

## #---------------------------------------------------------------------------
## # This code loads IPython but modifies a few things if it detects it's running
## # embedded in another IPython session (helps avoid confusion)
## try:
##     get_ipython
## except NameError:
##     banner=exit_msg=''
## else:
##     banner = '*** Nested interpreter ***'
##     exit_msg = '*** Back in main IPython ***'
## from IPython.terminal.embed import InteractiveShellEmbed
## # Now create the IPython shell instance. Put ipshell() anywhere in your code
## # where you want it to open.
## ipshell = InteractiveShellEmbed(banner1=banner, exit_msg=exit_msg)
## #------------------------------------------------------------------------------


import numpy as n
import scipy


def keplerian_solver(M, e):
    """Solve Kepler's equation, returning E given M, e.

       M = E - e*sin(E)

    Only handles multiple values of mean anomaly (M), not eccentricity (e)
    """
    assert e >= 0.
    assert e < 1.

    MM = n.asarray(M)
    if MM.shape == (): # scalar case
        MM.shape = (1,)

    from scipy.optimize import fmin
    def kep(E, M):
        return n.sum(n.abs(M-E+e*n.sin(E)))
    E0 = MM + n.sign(n.sin(MM)) * 0.85 * e # initial guess
    E = n.empty_like(MM)
    for i, M in enumerate(MM):
        E[i] = fmin(kep, E0[i], args=(M,),
                    disp=False,
                    #maxiter=500,
                    #maxfun=500,
                    )
    return E


def keplerian_solver_hyp(x, t, e):
    """Solve hyperbolic version of Kepler's equation, returning H given x, e.

       e*sinh(H) - H = (mu/a^3)^(1/2) t
       x = (mu/a^3)^(1/2)
       mu = GM_*

    Only handles multiple values of time (t), not eccentricity (e) or x
    """
    assert e > 1.

    tt = n.asarray(t)
    if tt.shape == (): # scalar case
        tt.shape = (1,)

    from scipy.optimize import fmin
    def kep(H, t):
        return n.sum(n.abs(e*n.sinh(H)-H-x*t))
    #disp = True
    disp = False
    H = n.array([fmin(kep, x*ttt, args=(ttt,), disp=disp) for ttt in tt])  # FIXME  make quiet
    H.shape = tt.shape
    return H



def keplerian_initial_value(r, vr, vt, M, beta=0.):
    """
    Given:  radius from the star  r     [cm]
            radial velocity       vr    [cm/s]
            tangential velocity   vt    [cm/s]
            stellar mass          M     [Msol]
            beta (optional)       beta

    Finds the eccentricity, pericenter distance [cm], and true anomaly [rad].

    Assumes point particle in orbit around massive star.
    """
    import constants as const
    
    mu = (1.-beta) * const.G * M*const.Msol # [cm^3/s^2]

    assert mu != 0.

    if mu > 0.:              # attractive potential
        p = (r*vt)**2 / mu   # [cm]
        v0 = n.sqrt(mu / p)  # [cm/s]
        ecosth = vt/v0 - 1.  # e * cos(th)
    else:                    # repulsive potential
        p = -(r*vt)**2 / mu  # [cm]
        v0 = n.sqrt(-mu / p) # [cm/s]
        ecosth = vt/v0 + 1.  # e * cos(th)
    esinth = vr/v0      # e * sin(th)

    # eccentricity
    e = n.sqrt(ecosth**2 + esinth**2 )
    # true anomaly
    nu = n.arctan2(esinth/e, ecosth/e) # [rad]

    # pericenter
    if mu > 0.:
        rp = p / (1.+e) # [cm]
    else:
        rp = p / (e-1.) # [cm]

    return (e, rp, nu)


def get_apparent_orbit(a, e, inc, omega, Omega, M):
    """
    a      semimajor axis [AU]
    e      eccentricity
    inc    inclination [rad]
    omega  argument of periastron [rad]
    Omega  longitude of ascending node [rad]  relative to x axis, counterclockwise
    M      mean anomaly [rad]

    returns x, y, z triplet [AU]
      x is North
      y is East
    """

    # compute eccentric anomaly
    E = keplerian_solver(M, e) # [rad] eccentric anomaly

    # compute true anomaly
    nu = 2.*n.arctan2(n.sqrt(1.+e)*n.sin(E/2.), n.sqrt(1.-e)*n.cos(E/2.)) # [rad]

    # compute radius
    r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU]

    so, co = n.sin(omega), n.cos(omega)
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(inc), n.cos(inc)

    x = r * (cO*conu-sO*sonu*ci) # [AU]
    y = r * (sO*conu+cO*sonu*ci) # [AU]
    z = r * sonu*si

    return x, y, z

def get_apparent_orbit_ellipse(a, e, inc, omega, Omega):
    """
    a      semimajor axis [AU]
    e      eccentricity
    inc    inclination [rad]
    omega  argument of periastron [rad]
    Omega  longitude of ascending node [rad]  relative to x axis, counterclockwise

    returns a, b, phi, E0, N0

    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a, e, inc, omega, Omega)
    ax.add_artist(mpl.patches.Ellipse((E0,N0),
                                      width=2.*ap, height=2.*bp,
                                      angle=phi*180./n.pi,
                                      ))

    Be sure that the x coordinate is East (increases to left)
    """

    # compute Thiele-Innes elements
    A = a*(n.cos(omega)*n.cos(Omega)-n.sin(omega)*n.sin(Omega)*n.cos(inc))
    B = a*(n.cos(omega)*n.sin(Omega)+n.sin(omega)*n.cos(Omega)*n.cos(inc))
    F = a*(-n.sin(omega)*n.cos(Omega)-n.cos(omega)*n.sin(Omega)*n.cos(inc))
    G = a*(-n.sin(omega)*n.sin(Omega)+n.cos(omega)*n.cos(Omega)*n.cos(inc))

    # compute coefficients for general ellipse
    a = (F**2+A**2/(1.-e**2))
    b = -(F*G+A*B/(1.-e**2))
    c = (G**2+B**2/(1.-e**2))
    d = e*F*(B*F-A*G)
    f = -e*G*(B*F-A*G)
    g = -(1.-e**2)*(B*F-A*G)**2

    # solve for ellipse parameters
    E0 = (c*d-b*f) / (b**2-a*c)
    N0 = (a*f-b*d) / (b**2-a*c)
    ap = n.sqrt(2.*(a*f**2+c*d**2+g*b**2-2.*b*d*f-a*c*g) / \
                (b**2-a*c) / (n.sqrt((a-c)**2+4.*b**2)-(a+c)))
    bp = n.sqrt(2.*(a*f**2+c*d**2+g*b**2-2.*b*d*f-a*c*g) / \
                (b**2-a*c) / (-n.sqrt((a-c)**2+4.*b**2)-(a+c)))
    if b==0.:
        phi = 0. if a<c else n.pi/2
    else:
        phi = n.arctan(2.*b/(a-c))/2.
        if a>=c: phi += n.pi/2.

    return ap, bp, phi, E0, N0


def test_apparent_orbit():
    import matplotlib as mpl
    import pylab

    nn = 100
    M = n.linspace(0., 2.*n.pi, nn) # [rad]

    a = 10. # [AU]
    e = 0.6
    inc = 70. * n.pi/180. # [rad]
    omega = 45. * n.pi/180. # [rad]
    Omega = 30. * n.pi/180. # [rad]

    N, E, z = get_apparent_orbit(a, e, inc, omega, Omega, M) # orbit
    Np, Ep, zp = get_apparent_orbit(a, e, inc, omega, Omega, 0.) # peri
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a, e, inc, omega, Omega)
    
    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    # plot orbit
    ax.plot(E[:nn/2],N[:nn/2], lw=2)
    ax.plot(E[nn/2:],N[nn/2:])

    ax.set_autoscale_on(False)

    # mark line of nodes
    ax.plot((2.*a*n.sin(Omega), -2.*a*n.sin(Omega)),
            (2.*a*n.cos(Omega), -2.*a*n.cos(Omega)),
            ls=':', c='k')
    # ascending node
    r = a * (1.-e**2) / (1.+e*n.cos(-omega)) # [AU]
    N = r * n.cos(Omega) # [AU]
    E = r * n.sin(Omega) # [AU]
    ax.scatter((E,),(N,),c='k')

    # mark peri
    ax.scatter(Ep,Np)
    # mark star
    ax.scatter((0.,),(0.,))

    # plot orbit ellipse
    ax.add_artist(mpl.patches.Ellipse((E0,N0),
                                      width=2.*ap, height=2.*bp,
                                      angle=phi*180./n.pi,
                                      ec='k', fc='none',
                                      alpha=0.15,
                                      lw=5,
                                      ))
    
    ax.set_xlim(ax.get_xlim()[::-1])
    ax.set_aspect('equal')
    ax.set_xlabel('E')
    ax.set_ylabel('N')
    pylab.draw()
    pylab.show()

if __name__=='__main__':
    test_apparent_orbit()
    
