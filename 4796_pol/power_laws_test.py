import numpy as n
import matplotlib.pyplot as plot




def power_law(x, gam1):
    y = x**gam1
    return y 
    
    


def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y


r_in = 70.
r_out = 90.
r_mid = 77.
drad0 = .1

n_step_rad = int((r_out-r_in)/drad0)
drad = (r_out-r_in)/n_step_rad
r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid


n_step_a = (r_out - r_mid) / drad0
da = (r_out - r_mid) / n_step_a
a_factors = (r_mid + n.arange(n_step_a) * da) / r_mid


gam = 5.
gam_in = -5

y_sbpl = smooth_broken_power_law(r_factors, 1., 1., 1., -gam_in, -gam, 0) / r_factors**2. / len(r_factors)
y_power_law = power_law(a_factors, -gam) / a_factors**2. / len(a_factors)

plot.plot(r_factors, y_sbpl, color = 'blue', linewidth = 2.0, label = 'sbpl')
plot.plot(a_factors, y_power_law, color = 'red', label = 'power law')
plot.xlabel('rad_factor')
plot.ylabel('Intensity (arbitrary units')
plot.legend()
plot.show()
