from diskmodeler_ellipse_sbpl import ModelComparator
import matplotlib.pyplot as plot
import numpy as n
import pyfits

def get_mask():
    mc = ModelComparator('hr4796_config_kpol.ini', leastsq_save = 'leastsq_kpol_even.p')
    mc.parms['a_in'] = 55.
    mc.parms['a_out'] = 95.
    mc.parms['e'] = 0
    mc.parms['gamma_in'] = -2
    mc.parms['gamma'] = -2
    mc.parms['K1_Polint_parms'][:] = 1.e6
    mc.update_all_parms(mc.parms)
    model = mc.diskobjs[0].get_model_ring_image(convolve = False)

    tfim = mc.diskobjs[0].tfim
    wh = n.where(model > 0)
    return wh


def inverse_mask():
    mc = ModelComparator('hr4796_config_hpol.ini', leastsq_save = 'leastsq_kpol_even.p')
    mc.parms['a_in'] = 30.
    mc.parms['a_out'] = 150.
    mc.parms['e'] = 0
    mc.parms['gamma_in'] = -2
    mc.parms['gamma'] = -2
    mc.parms['K1_Polint_parms'][:] = 1.e6
    mc.update_all_parms(mc.parms)
    model = mc.diskobjs[0].get_model_ring_image(convolve = False)

    tfim = mc.diskobjs[0].tfim
    wh = n.where(model <= 0)
    tfim[wh] = n.nan
    wh = n.where(n.abs(tfim) > 300)
    wh = n.where(tfim < -50)
    tfim[wh] = n.nan
    pyfits.writeto('hr4796_h_qr_moremasked.fits', tfim)
    plot.imshow(tfim)
    plot.show()
    return wh



def to_noisemap(im):
    from image import radial_stdev_profile

    center = [140,140]
    rprof, sprof = radial_stdev_profile(im, [int(center[0]), int(center[1])])
    y, x = n.mgrid[0:im.shape[0],
                   0:im.shape[1]]
    y -= int(center[0])
    x -= int(center[1])
    R = n.sqrt(x**2+y**2)
    stdim = n.zeros_like(im)
        
    # Turn profiles into image of standard dev as a function of radius. 
    for r, s in zip(rprof, sprof):
        wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
        stdim[wr] = s
    return stdim, R


def make_masks():
    mask = get_mask()

    mc = ModelComparator('hr4796_config_kpol.ini')
    kim = mc.diskobjs[0].tfim
    kim[mask] = n.nan
    pyfits.writeto('noisemaps/kpol_masked_disk.fits', kim)

    mc = ModelComparator('hr4796_config_jpol.ini')
    kim = mc.diskobjs[0].tfim
    kim[mask] = n.nan
    pyfits.writeto('noisemaps/jpol_masked_disk.fits', kim)

    mc = ModelComparator('hr4796_config_hpol.ini')
    kim = mc.diskobjs[0].tfim
    kim[mask] = n.nan
    pyfits.writeto('noisemaps/hpol_masked_disk.fits', kim)




def make_masks_stdim():
    mask = get_mask()


    mc = ModelComparator('hr4796_config_kpol.ini')
    kim = mc.diskobjs[0].tfim
    kim[mask] = n.nan
    stdim, R = to_noisemap(kim)
    pyfits.writeto('noisemaps/kpol_stdim.fits', stdim)


    mc = ModelComparator('hr4796_config_jpol.ini')
    kim = mc.diskobjs[0].tfim
    kim[mask] = n.nan
    stdim, R = to_noisemap(kim)
    pyfits.writeto('noisemaps/jpol_stdim.fits', stdim)

    mc = ModelComparator('hr4796_config_hpol.ini')
    kim = mc.diskobjs[0].tfim
    kim[mask] = n.nan
    stdim, R = to_noisemap(kim)
    pyfits.writeto('noisemaps/hpol_stdim.fits', stdim)

