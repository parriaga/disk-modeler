from diskmodeler_method3 import ModelComparator
import diskmodeler_method3
import numpy as n
import matplotlib.pyplot as plot
import matplotlib as mpl
from matplotlib import gridspec
import pyfits






def get_uncertainties():
    # Get scattering angles
    phi_sca = n.arange(13, 120)

    #Read in MC files
    conf = 'hr4796_config_kpol.ini'
#    mc_save = 'mcmc_kpol_intparms_even.p'
    mc_save = 'mcmc_kpol_method3_long.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    
    #ind best model
    nu, r_star, phi_sca, r_proj, PA_proj, intensities = mc.diskobjs[0].parametric_model_ring(n_step = 500)
    x = r_proj * n.cos(PA_proj) 
    y = r_proj * n.sin(PA_proj)
    omega = mc.parms['omega1'] + n.pi * 1.4 / 4
    xr = x * n.cos(omega) - y * n.sin(omega) + 140
    yr = x * n.sin(omega) + y * n.cos(omega) + 140
    std = mc.diskobjs[0].get_model_ring_image()
    stds = []
    for xc, yc in zip(xr,yr):
        stds.append(std[int(xc), int(yc)])
    minphi_sca = n.where(phi_sca == n.min(phi_sca))[0][0]
    maxphi_sca = n.where(phi_sca == n.max(phi_sca))[0][0]

    phi_sca = phi_sca[maxphi_sca:minphi_sca]
    stds = stds[maxphi_sca:minphi_sca]


    def smooth(y, box_pts):
        box = n.ones(box_pts)/box_pts
        y_smooth = n.convolve(y, box, mode='same')
        return y_smooth

#    plot.plot(phi_sca, stds)
    stds = n.array(stds)
    smoothed = smooth(stds, 20)

    plot.plot(phi_sca, smoothed)
    plot.show()



def testers2():
    conf = 'hr4796_config_kpol.ini'
    mc_save = 'mcmc_kpol_intparms_even.p'
    mc = ModelComparator(conf, mc_save = mc_save)



def tester_curves():
    ne = n.loadtxt('phase_mcfost_tot_ne.txt')
    correction = n.loadtxt('phase_flat_ne2.txt')
    angs = ne[:, 0]
    ne_ints = ne[:, 1] 
    nemax = n.max(ne_ints) #/ correction
    ne_ints = ne_ints / nemax 
    ne_errs = ne[:, 2] / nemax

    sw = n.loadtxt('phase_mcfost_tot_sw.txt')
    angs = sw[:, 0]
    sw_ints = sw[:, 1] 
    swmax = n.max(sw_ints)
    sw_ints = sw_ints / swmax
    sw_errs = sw[:, 2] / swmax


    plot.errorbar(angs, ne_ints, yerr = ne_errs, label = 'NE')
    plot.errorbar(angs, sw_ints, yerr = sw_errs, label = 'SW')

    testphase = pyfits.getdata('phase_k1_fitting_tests.fits')
    testphase = testphase[13:]
    testmax = n.max(testphase)
    testphase /= testmax
    angs_test = n.arange(14, 182)
    plot.plot(angs_test,testphase, color = 'red', linewidth = 2, label = 'MCFOST curve')
    plot.legend()
    plot.ylim([0, 1.5])
    plot.xlabel('Scattering angle')
    plot.ylabel('Normalized Intensity')
    plot.title('Flat phase curve')
    plot.show()


def plotk():
    conf = 'hr4796_config_ktot.ini'
    mc_save = 'mcmc_ktot.p'

    mc = ModelComparator(conf, mc_save = mc_save)

    f, ax = plot.subplots(1)

    curves(mc, 0, ax, color='blue', normalize = False, label = 'K1')

    ax.set_title('Phase curve')
    nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[0].parametric_model_ring(n_step=2000)
    phi_max_nu = nu[n.where(phi_sca == n.max(phi_sca))]

    ax.vlines(phi_max_nu * 180. / n.pi, 0., 300000, linewidth = 1.2, label = 'Symmetry')
    ax.legend()
#    ax.set_xlim([290, 315])
#    ax.set_ylim([200000,270000])
    plot.xlabel('True anomaly (degrees)')
    plot.ylabel('Intensity (arbitrary units)')
    plot.show()



def curves_to_stddev(curveset):
    sh = n.shape(curveset) #chain x yval
    upper = []
    lower = []
    curveset = n.array(curveset)
    for i in range(sh[1]):
        m = n.mean(curveset[:, i])
        std = n.std(curveset[:, i])
        upper.append(m + std)
        lower.append(m - std)
    return upper, lower

def curves_to_errs(curveset):
    curveset = n.array(curveset)
    sh = n.shape(curveset) #chain x yval
    print sh
    ms = []
    stds = []
    curveset = n.array(curveset)
    for i in range(sh[1]):
        m = n.mean(curveset[:, i])
        std = n.std(curveset[:, i])
        ms.append(m)
        stds.append(std)
    return ms,stds


def get_best_phi_curve(mc,diskobj_ind, NE = True):
    nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=4321)
    phimin = n.where(phi_sca == min(phi_sca))[0][0]
    phimax = n.where(phi_sca == max(phi_sca))[0][0]
    if NE is True:
        phisca = phi_sca[phimax:phimin]
        phase_curve = intensity[phimax:phimin]
    else:
        phisca = n.concatenate((phi_sca[phimin:], phi_sca[:phimax]))
        phase_curve = n.concatenate((intensity[phimin:], intensity[:phimax]))
    return phisca, phase_curve



def get_phi_curves(mc, diskobj_ind, NE = True, lnprob_cutoff = 100):
    chain = mc.chain
    sh = n.shape(chain)
    n_burn = sh[1] - 1.
    n_walkers = sh[0]
    best_lnprob = n.max(mc.lnprobability)

    phase_curves = []
    phiscas = []
    
    for step in range(n_burn, sh[1]):
        for walker in range(n_walkers):
            param_set = chain[walker, step,:]
#            if mc.lnprobability[walker, step] > best_lnprob - lnprob_cutoff:
            mc.set_fit_parms(param_set, fit_parmlist_wtf = mc.fit_parmlist)
            nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=4321)
            phimin = n.where(phi_sca == min(phi_sca))[0][0]
            phimax = n.where(phi_sca == max(phi_sca))[0][0]
            if NE is True:
                phiscas.append(n.array(phi_sca[phimax:phimin]))
                phase_curves.append(intensity[phimax:phimin])
            else:
                f = n.concatenate((phi_sca[phimin:], phi_sca[:phimax]))
                p = n.concatenate((intensity[phimin:], intensity[:phimax]))
                phiscas.append(f)
                phase_curves.append(p)
            plot.plot(p, alpha = 0.2)
    return phiscas, phase_curves

def plot_phi_curves(mc,diskobj_ind):
    phi_scas, phase_curves = get_phi_curves(mc,diskobj_ind)
    for phi_sca, curve in zip(phi_scas, phase_curves):
        plot.plot(phi_sca, curve)
        print phi_sca
        print curve
    plot.show()


def plotall_2():
    h = n.loadtxt('phase_h.txt')
    j = n.loadtxt('phase_j.txt')
    k = n.loadtxt('phase_k1.txt')
    hm = h[:,1] / n.max(h[:,1])
    jm = j[:,1] / n.max(j[:,1])
    km = k[:,1] / n.max(k[:,1])
    hstd = h[:,2] / n.max(h[:,1])
    jstd = j[:,2] / n.max(j[:,1])
    kstd = k[:,2] / n.max(k[:,1])
    phiscas = h[:, 0]
    plot.errorbar(phiscas, hm, yerr = hstd, label = 'H')
    plot.errorbar(phiscas, jm, yerr = jstd, label = 'J')
    plot.errorbar(phiscas, km, yerr = kstd, label = 'K1')
    plot.xlabel('Phase Angle (degrees)')
    plot.ylabel('Relative Intensity')
    plot.title('Phase Curve SE')
    plot.legend()
    plot.show()

def write_out():
    from scipy.interpolate import interp1d
#    conf = 'hr4796_config_tester.ini'
#    mc_save = 'mcmc_mcfost_tester3.p'

    conf = 'hr4796_config_hpol.ini'
    mc_save = 'mcmc_kpol_intparms_method3.p'


#    conf = 'hr4796_config_jpol.ini'
#    mc_save = 'mcmc_jpol_intparms_even.p'

#    conf = 'hr4796_config_hpol.ini'
#    mc_save = 'mcmc_hpol_intparms_even2_15.p'

#    conf = '/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/MCFOST_tests/HG4_test/hr4796_config_tester_4796_noise.ini'
#    mc_save = '/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/MCFOST_tests/HG4_test/mcmc_ktot_4796_noise.p'

#    conf = 'hr4796_config_ktot.ini'
#    mc_save = 'mcmc_ktot_intparms_even.p'



    mc = ModelComparator(conf, mc_save = mc_save)
    print 'getting phi_curves'
    phi_scas, phase_curves = get_phi_curves(mc, 0, NE=True)
    print 'curves to errorbars'
    ms, stds = curves_to_errs(phase_curves)
    plot.scatter(ms, stds)
    plot.show()
    return
    phi_sca = phi_scas[0] * 180 / n.pi
    fm = interp1d(phi_sca, ms)
    fs = interp1d(phi_sca, stds)
    new_phi_sca = n.arange(14,166)
    
    new_m = fm(new_phi_sca)
    new_s = fs(new_phi_sca)
    arr = n.array([new_phi_sca, new_m, new_s]).T

#    n.savetxt('phase_curves/phase_h_26_sw.txt', arr)
    n.savetxt('/home/parriaga/gpi_disks/hr4796a_new/code/4796_pol/phase_curves/hpol_NE_test.txt', arr)
    plot.errorbar(new_phi_sca, new_m, yerr = new_s)
    
    plot.show()

def test1():
    conf = 'hr4796_config_kpol.ini'
    mc_save = 'mcmc_kpol_intparms_even.p'
    
    mc = ModelComparator(conf, mc_save = mc_save)
    phi_scas, phase_curves = get_phi_curves(mc, 0, NE = False)
    print n.shape(phi_scas)
    print n.shape(phase_curves)
    upper,lower = curves_to_stddev(phase_curves)
    
    phisca = phi_scas[0] * 180. / n.pi
    
    best_curve = get_best_prob_curve(mc, 0)
    m = n.max(best_curve)

    plot.fill_between(phisca, lower / m,upper / m, color = 'blue', alpha = .3)
    phi_scas, phase_curves = get_phi_curves(mc, 0, NE = True)
    upper,lower = curves_to_stddev(phase_curves)
    phisca = phi_scas[0] * 180. / n.pi
    plot.fill_between(phisca, lower / m,upper / m, color = 'red', alpha = .3)
#    plot.plot(phisca, best_curve / m, color = 'blue')


    
    plot.ylim(0, 1)
    plot.xlim(0, 180)
    plot.show()

def test():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    diskobj_ind = 0
    nu, curve_set = get_curves(mc, diskobj_ind)
    nu *= 180. / n.pi
    find_peak_stddev(nu, curve_set)



def plotall():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'

    mcj = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol.p')
    mc = ModelComparator(conf, mc_save = mc_save)

    f, ax = plot.subplots(1)

    curves(mc, 0, ax, color='blue', normalize = False, label = 'K1')
    curves(mcj, 0, ax, color='red', normalize = False, label = 'J')
    curves(mc, 2, ax, color='green', normalize = False, label = 'H')

    ax.set_title('Phase curve')
    nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[0].parametric_model_ring(n_step=2000)
    phi_max_nu = nu[n.where(phi_sca == n.max(phi_sca))]

    ax.vlines(phi_max_nu * 180. / n.pi, 0., 300000, linewidth = 1.2, label = 'Symmetry')
    ax.legend()
    ax.set_xlim([290, 315])
    ax.set_ylim([200000,270000])
    plot.xlabel('True anomaly (degrees)')
    plot.ylabel('Intensity (arbitrary units)')
    plot.show()






def curves(mc, diskobj_ind, ax, color = 'blue', lnprob_cutoff = 100, normalize = True, plotmax = True, label = 'K'):
    nu, best_curve = get_best_prob_curve(mc, diskobj_ind, get_phi_sca = False)
    nu, curve_set = get_curves(mc, diskobj_ind)

    nu *= 180. / n.pi 
    maxval = n.max(best_curve)
    wh = n.where(best_curve == maxval)
    numax = nu[wh]
    
    for curve in curve_set:
        if normalize is False:
            ax.plot(nu, curve, color = color, alpha = .01)
        else:
            ax.plot(nu, curve / n.max(curve), color = color, alpha = .01)
    if normalize is True:
        ax.plot(nu, best_curve / n.max(best_curve), linewidth = 1.5, color = color, label = label)
        vmax = 1.3
    else:
        ax.plot(nu, best_curve, linewidth = 2, color = color, label = label)
        vmax = n.max(best_curve) * 1.3
    if plotmax ==True:
        stdpeaks = find_peak_stddev(nu, curve_set)
        rect = mpl.patches.Rectangle((numax - stdpeaks / 2, 0), stdpeaks, vmax, color = color, alpha = .3)
        ax.vlines([numax], 0, vmax, color = color)
        ax.add_patch(rect)

        

def find_peak_stddev(nu, curve_set):
    peaks = []
    for curve in curve_set:
        peaks.append(nu[n.where(curve == n.max(curve))])
    return n.std(peaks)


def test_curvemaps():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    curve_maps(mc, 0)
    


def curve_maps(mc, diskobj_ind, color = 'blue', lnprob_cutoff = 100, label = 'K'):
    numin = 280
    numax = 330
    imin = 160000
    imax = 300000
    nbins = 300

    nu, curve_set = get_curves(mc, diskobj_ind)
    nu *= 180. / n.pi
    wh = n.where((nu > numin) & (nu < numax))
    nu = nu[wh]
    
    im = n.zeros((nbins, nbins))
    nu_binsize = (numax - numin) / nbins
    i_binsize = (imin - imax) / nbins


    nupoints = []
    curvepoints = []
    for curve in curve_set:
        nupoints.append(nu)
        curvepoints.append(curve[wh])
    
    nupoints = n.array(nupoints).flatten()
    curvepoints = n.array(curvepoints).flatten()

    H, xedges, yedges = n.histogram2d(nupoints, curvepoints, bins = [300,70], range=[[numin,numax], [imin, imax]])
    plot.imshow(H.T,cmap = 'Greys')
    plot.show()
    


def fit_gaussian():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    nu, intensity_k = get_best_prob_curve(mc,0)
    nu, intensity_j = get_best_prob_curve(mc,1)
    nu, intensity_h = get_best_prob_curve(mc,2)

    nu *= 180. / n.pi
    wh = n.where((nu > 290) & (nu < 320))
    nu = nu[wh]

    intensity_k = intensity_k[wh] 
    intensity_j = intensity_j[wh]
    intensity_h = intensity_h[wh] 

    from astropy.modeling import models, fitting
    g_init = models.Gaussian1D(amplitude = 300000, mean = 300, stddev = 30)
    fit_g = fitting.LevMarLSQFitter()
    
    g_k = fit_g(g_init, nu, intensity_k)
    g_j = fit_g(g_init, nu, intensity_j)
    g_h = fit_g(g_init, nu, intensity_h)

    print(g_k)
    print(g_h)
    print(g_j)

    plot.plot(nu, intensity_k)
    plot.plot(nu, g_k(nu))

    plot.plot(nu, intensity_j)
    plot.plot(nu, g_j(nu))

    plot.plot(nu, intensity_h)
    plot.plot(nu, g_h(nu))

    plot.show()
    def slope_func(x, slope):
        y = slope * x / 90  + 1
        return y
        

    g_k.amplitude = 250000
    g_j.amplitude = 250000
    g_h.amplitude = 250000

    g_k.amplitude = 300
    g_j.amplitude = 300
    g_h.amplitude = 300


    plot.plot(nu, g_k(nu) * slope_func(nu, 1), label = 'K')
    plot.plot(nu, g_j(nu) * slope_func(nu, 1), label = 'J')
    plot.plot(nu, g_h(nu) * slope_func(nu, 1), label = 'H')
    plot.vlines([300], 0, 1400)
    plot.legend()
    plot.show()



def get_best_prob_curve(mc, diskobj_ind, get_phi_sca = True):
    chain = mc.chain
    sh = n.shape(chain)
    best_lnprob = n.where(mc.lnprobability == n.max(mc.lnprobability))
    param_set = chain[best_lnprob[0][0], best_lnprob[1][0], :]
    mc.set_fit_parms(param_set, fit_parmlist_wtf = mc.fit_parmlist)
    nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=4321)
    if get_phi_sca is True:
        phimin = n.where(phi_sca == min(phi_sca))[0][0]
        phimax = n.where(phi_sca == max(phi_sca))[0][0]
        return intensity[phimax:phimin]
    return nu, intensity
    


def get_curves(mc, diskobj_ind, lnprob_cutoff = 100):
    chain = mc.chain
    sh = n.shape(chain)
    n_burn = sh[1] - 5
    n_walkers = sh[0]
    best_lnprob = n.max(mc.lnprobability)

    phase_curves = []
    for step in range(n_burn, sh[1]):
        for walker in range(n_walkers):
            param_set = chain[walker, step,:]
            if mc.lnprobability[walker, step] > best_lnprob - lnprob_cutoff:
                mc.set_fit_parms(param_set, fit_parmlist_wtf = mc.fit_parmlist)
                nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=4321)
                
                phase_curves.append(intensity)
    return nu, phase_curves
