from diskmodeler import ModelComparator
from diskmodeler_plotutils import *


conf_save = 'hr4796_config_kpol.ini'
mc_save = 'mcmc_kpol_even.p'
mc = ModelComparator(conf_save,mc_save = mc_save)

for param in mc.parms:
    if 'int' in param:
        pass
    else:
        mc.is_fixed[param] = True
mc.is_fixed['gamma'] = False
mc.is_fixed['gamma_in'] = False
mc.is_fixed['r'] = False
mc.parms['r'] = 77.
mc.parms['beta'] = 0.
mc.parms['r_in'] = 70.
mc.parms['r_out'] = 100.

mc.update_all_int_parms([20])
mc.update_all_parms(mc.parms)
print mc.is_fixed
mc.do_leastsq()
mc.save_parms('leastsq_kpol_newradprof.p')
