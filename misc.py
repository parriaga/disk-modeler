# $Id$
#
# miscellaneous routines
#
# Michael Fitzgerald (mpfitz@llnl.gov)  2009-12-27
#


## #------------------------------------------------------------------------------
## # This code loads IPython but modifies a few things if it detects it's running
## # embedded in another IPython session (helps avoid confusion)
## try:
##     __IPYTHON__
## except NameError:
##     argv = ['']
##     banner = exit_msg = ''
## else:
##     argv = ['-pi1','In <\\#>:','-pi2','   .\\D.:','-po','Out<\\#>:']
##     banner = '*** Nested interpreter ***'
##     exit_msg = '*** Back in main IPython ***'

## from IPython.Shell import IPShellEmbed
## ipshell = IPShellEmbed(argv,banner=banner,exit_msg=exit_msg)
## #------------------------------------------------------------------------------

import numpy as n
import constants as const

import logging
_log = logging.getLogger('misc')

def biweight(x, R=4.685):
    return (n.clip(1.-(x/R)**2, 0., n.inf))**2

def laplace_coeff(alpha, k, s):
    """
    Compute the laplace coefficient b_k^(s) (alpha).
    """

    def func(psi):
        return n.cos(s*psi) / (1. - 2.*alpha*n.cos(psi) + alpha**2)**k

    from scipy.integrate import romberg
    b = 1./n.pi * romberg(func, 0., 2.*n.pi, vec_func=True, tol=1e-10, divmax=20)
    ## from scipy.integrate import quad
    ## b, etc = quad(func, 0., 2.*n.pi,
    ##               epsrel=1e-10,
    ##               )
    ## b *= 1./n.pi

    return b

def add_wcs_to_hdr(hdr, cen, ra, dec, scale, PA=0.):
    """
    hdr is a pyfits header instance

    cen is (y,x) coordinates of center (indexed from zero)

    ra, dec in decimal [h], [deg]

    scale is [arcsec/pix]

    PA is direction of y axis relative to north, measured cw

    returns a pyfits header instance.  you may need
       hdulist[i].header._mod = 1
       hdulist[i].verify('fix')
    """
    import pywcs, pyfits

    # set up WCS transformation info
    wcs = pywcs.WCS(naxis=2)
    wcs.wcs.crpix = (cen[1]+1, cen[0]+1)
    wcs.wcs.crval = [ra*15, dec] # [deg]. [deg]
    wcs.wcs.ctype = ["RA---TAN", "DEC--TAN"]
    th = -PA*const.dtor # th is direction of N relative to image y axis, measured ccw
    sth, cth = n.sin(th), n.cos(th)
    wcs.wcs.cd = n.array(((-cth,-sth),(-sth,cth))) * scale/3600. # [deg]
    # remove duplicate cards before adding new info
    hdr2 = wcs.to_header()
    h1cards = hdr.ascard
    for k in ('LATPOLE', 'LONPOLE', 'RESTFRQ', 'RESTWAV'): del hdr2[k] # ?
    for k in hdr2.ascard.keys():
        while k in h1cards.keys():
            del hdr[k]

    hdr = pyfits.Header(hdr.ascard + hdr2.ascard)

    return hdr


# original code in pixwt.c by Marc Buie
# 
# ported to pixwt.pro (IDL) by Doug Loucks, Lowell Observatory, 1992 Sep
#
# subsequently ported to python by Michael Fitzgerald,
# LLNL, fitzgerald15@llnl.gov, 2007-10-16
#

def _arc(x, y0, y1, r):
    """
    Compute the area within an arc of a circle.  The arc is defined by
    the two points (x,y0) and (x,y1) in the following manner: The
    circle is of radius r and is positioned at the origin.  The origin
    and each individual point define a line which intersects the
    circle at some point.  The angle between these two points on the
    circle measured from y0 to y1 defines the sides of a wedge of the
    circle.  The area returned is the area of this wedge.  If the area
    is traversed clockwise then the area is negative, otherwise it is
    positive.
    """
    return 0.5 * r**2 * (n.arctan(y1/x) - n.arctan(y0/x))

def _chord(x, y0, y1):
    """
    Compute the area of a triangle defined by the origin and two
    points, (x,y0) and (x,y1).  This is a signed area.  If y1 > y0
    then the area will be positive, otherwise it will be negative.
    """
    return 0.5 * x * (y1 - y0)

def _oneside(x, y0, y1, r):
    """
    Compute the area of intersection between a triangle and a circle.
    The circle is centered at the origin and has a radius of r.  The
    triangle has verticies at the origin and at (x,y0) and (x,y1).
    This is a signed area.  The path is traversed from y0 to y1.  If
    this path takes you clockwise the area will be negative.
    """

    if n.all((x==0)): return x

    sx = x.shape
    ans = n.zeros(sx, dtype=n.float)
    yh = n.zeros(sx, dtype=n.float)
    to = (abs(x) >= r)
    ti = (abs(x) < r)
    if n.any(to):
        ans[to] = _arc(x[to], y0[to], y1[to], r)
    if not n.any(ti):
        return ans

    yh[ti] = n.sqrt(r**2 - x[ti]**2)

    i = ((y0 <= -yh) & ti)
    if n.any(i):

        j = ((y1 <= -yh) & i)
        if n.any(j):
            ans[j] = _arc(x[j], y0[j], y1[j], r)

        j = ((y1 > -yh) & (y1 <= yh) & i)
        if n.any(j):
            ans[j] = _arc(x[j], y0[j], -yh[j], r) + \
                     _chord(x[j], -yh[j], y1[j])

        j = ((y1 > yh) & i)
        if n.any(j):
            ans[j] = _arc(x[j], y0[j], -yh[j], r) + \
                     _chord(x[j], -yh[j], yh[j]) + \
                     _arc(x[j], yh[j], y1[j], r)

    i = ((y0 > -yh) & (y0 < yh) & ti)
    if n.any(i):

        j = ((y1 <= -yh) & i)
        if n.any(j):
            ans[j] = _chord(x[j], y0[j], -yh[j]) + \
                     _arc(x[j], -yh[j], y1[j], r)

        j = ((y1 > -yh) & (y1 <= yh) & i)
        if n.any(j):
            ans[j] = _chord(x[j], y0[j], y1[j])

        j = ((y1 > yh) & i)
        if n.any(j):
            ans[j] = _chord(x[j], y0[j], yh[j]) + \
                     _arc(x[j], yh[j], y1[j], r)
        
    i = ((y0 >= yh) & ti)
    if n.any(i):

        j = ((y1 <= -yh) & i)
        if n.any(j):
            ans[j] = _arc(x[j], y0[j], yh[j], r) + \
                     _chord(x[j], yh[j], -yh[j]) + \
                     _arc(x[j], -yh[j], y1[j], r)

        j = ((y1 > -yh) & (y1 <= yh) & i)
        if n.any(j):
            ans[j] = _arc(x[j], y0[j], yh[j], r) + \
                     _chord(x[j], yh[j], y1[j])

        j = ((y1 > yh) & i)
        if n.any(j):
            ans[j] = _arc(x[j], y0[j], y1[j], r)
        
    return ans

def _intarea(xc, yc, r, x0, x1, y0, y1):
    """
    Compute the area of overlap of a circle and a rectangle.
      xc, yc  :  Center of the circle.
      r       :  Radius of the circle.
      x0, y0  :  Corner of the rectangle.
      x1, y1  :  Opposite corner of the rectangle.
    """
    x0 = x0 - xc
    y0 = y0 - yc
    x1 = x1 - xc
    y1 = y1 - yc
    return _oneside(x1, y0, y1, r) + _oneside(y1, -x1, -x0, r) + \
           _oneside(-x0, -y1, -y0, r) + _oneside(-y0, x0, x1, r)

def pixwt(xc, yc, r, x, y):
    """
    Compute the fraction of a unit pixel that is interior to a circle.
    The circle has a radius r and is centered at (xc, yc).  The center
    of the unit pixel (length of sides = 1) is at (x, y).

    Divides the circle and rectangle into a series of sectors and
    triangles.  Determines which of nine possible cases for the
    overlap applies and sums the areas of the corresponding sectors
    and triangles.

    area = pixwt( xc, yc, r, x, y )

    xc, yc : Center of the circle, numeric scalars
    r      : Radius of the circle, numeric scalars
    x, y   : Center of the unit pixel, numeric scalar or vector
    """
    return _intarea(xc, yc, r, x-0.5, x+0.5, y-0.5, y+0.5)



# --- principal components analysis ---


def pca_cov(x, n_pc_max=None,
               skip_mean_subtract=False):
    """
    Get principal components.  x has realizations as rows, variables
    as columns.  Uses covariance method.  Principal components (each
    with length of # variables) are returned as column vectors.
    Eigenvalues are also returned.
    """


    n_real, n_var = x.shape

    if skip_mean_subtract:
        xms = x.copy()
    else:    
        # average across realizations
        x_mn = x.mean(axis=0)
        # remove mean
        xms = x-x_mn[n.newaxis,:]
    
    # compute covariance matrix
    cov = n.zeros((n_var,n_var), dtype=n.float)
    for dat in xms:
        dat.shape = (n_var,1)
        cov += n.dot(dat,dat.T)
    cov /= n_real

    # number of principal components
    n_pc = n_pc_max if ((n_pc_max is not None) and (n_var >= n_pc_max)) else n_var

    # get eigen decomposition of covariance matrix
    from scipy.linalg import eigh
    ev, P = eigh(cov, eigvals=(n_var-n_pc, n_var-1))

    return P, ev
    

def pca_nipals(x, n_pc_max=None, thresh=1e-8, tau_iter_max=300,
               ev_thresh=None,
               skip_mean_subtract=False):
    "Nonlinear iterative partial least-squares approach to PCA"
    # following http://folk.uio.no/henninri/pca_module/
    
    n_real, n_var = x.shape

    if skip_mean_subtract:
        xms = x.copy()
    else:    
        # average across realizations
        x_mn = x.mean(axis=0)
        # remove mean
        xms = x-x_mn[n.newaxis,:]

    # number of principal components
    n_pc = n_pc_max if ((n_pc_max is not None) and (n_var >= n_pc_max)) else n_var


    # NIPALS algorithm
    E = xms
    t = xms[:,0].copy()
    P = n.empty((n_var,n_pc), dtype=n.float)
    ev = n.empty(n_pc, dtype=n.float)
    for i in range(n_pc):
        #_log.debug("NIPALS principal component %d/%d" % (i, n_pc))
        tau_old = 0.
        for j in range(tau_iter_max):
            p = n.dot(E.T,t) / n.dot(t.T,t)
            p /= n.sqrt(n.dot(p.T,p))
            t = n.dot(E,p) #/ n.dot(p.T,p)
            tau = n.dot(t.T, t)
            if n.abs(1.-tau_old/tau) < thresh:
                break
            else:
                tau_old = tau
        E -= n.outer(t, p)
        # store
        P[:,i] = p
        ev[i] = tau
        # ev thresh check
        if ev_thresh is not None:
            if i==0: ev0 = tau
            elif tau/ev0 < ev_thresh: break
    if i != n_pc-1: # result of ev_thresh
        P = P[:,0:i+1]
        ev = ev[0:i+1]

    return P, ev

pca_nipals_py = pca_nipals
        
#from pca import pca_nipals # NOTE overrides above

def test_pca():


    # construct a dataset
    n_var = 100
    n_real = 10

    xx = n.linspace(0, n_real, n_var)

    x = n.empty((n_real, n_var), dtype=n.float)
    for i in range(n_real):
        x[i,:] = n.exp(-(xx-i)**2/2.)

    n_pc_max = 10
    Pc, evc = pca_cov(x, n_pc_max=n_pc_max)
    Pn, evn = pca_nipals(x, n_pc_max=n_pc_max)

    import pylab
    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)

    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    for i in range(n_pc_max):
        ax1.plot(Pc[:,n_pc_max-1-i])
        ax2.plot(Pn[:,i])

    pylab.draw()
    pylab.show()
    

def test_pca2():

    #---------------------------------------------------------------------------
    # This code loads IPython but modifies a few things if it detects it's running
    # embedded in another IPython session (helps avoid confusion)
    try:
        get_ipython
    except NameError:
        banner=exit_msg=''
    else:
        banner = '*** Nested interpreter ***'
        exit_msg = '*** Back in main IPython ***'
    from IPython.terminal.embed import InteractiveShellEmbed
    # Now create the IPython shell instance. Put ipshell() anywhere in your code
    # where you want it to open.
    ipshell = InteractiveShellEmbed(banner1=banner, exit_msg=exit_msg)
    #------------------------------------------------------------------------------


    # construct a dataset
    n_var = 100
    n_real = 10

    xx = n.linspace(0, n_real, n_var)

    x = n.empty((n_real, n_var), dtype=n.float)
    for i in range(n_real):
        x[i,:] = n.exp(-(xx-i)**2/2.)

    n_pc_max = 10
    P1, ev1 = pca_nipals(x, n_pc_max=n_pc_max)
    import pca
    P2, ev2 = pca.pca_nipals(x, n_pc_max=n_pc_max)

    #ipshell()
    print(n.abs(ev1/ev2-1.).max())
    print(n.abs(P1/P2-1.).max())
    #assert n.allclose(P1,P2)
    #assert n.allclose(ev1,ev2)


# -------

def monotone_spline(x, y):
    "monotonic cubic spline"
    # http://en.wikipedia.org/wiki/Monotone_cubic_interpolation
    
    # compute slopes (step 1)
    delta = (y[1:]-y[:-1]) / (x[1:]-x[:-1])

    # initialize tangents (step 2)
    m = n.zeros(len(y), dtype=n.float)
    m[1:-1] = (delta[:-1]+delta[1:])/2.
    m[0] = delta[0]
    m[-1] = delta[-1]

    # check for flat (step 3)
    w = n.nonzero(delta==0.)[0]
    m[w] = 0.
    m[w+1] = 0.


    # for piecewise monotone (step 4)
    wg = n.nonzero(delta!=0.)[0]

    a = m[:-1]/delta
    b = m[1:]/delta
    ww = n.nonzero((a[wg]<0.) | (b[wg]<0.))[0]
    m[wg[ww]] = 0.
    m[wg[ww]+1] = 0.

    # prevent overshoot (step 5)
    t = 3./n.sqrt(a**2+b**2)
    ww = n.nonzero(t[wg]<1.)[0]
    m[wg[ww]] = t[wg[ww]]*a[wg[ww]]*delta[wg[ww]]
    m[wg[ww]+1] = t[wg[ww]]*b[wg[ww]]*delta[wg[ww]]

    # define cubic functions
    h00 = lambda t: (1.+2.*t)*(1.-t)**2
    h10 = lambda t: t*(1.-t)**2
    h01 = lambda t: (3.-2.*t)*t**2
    h11 = lambda t: (-1.+t)*t**2

    # construct interpolating function
    def eval_spline(xx):

        # find lower and upper bound indices for each data point
        k_lower, k_upper = [], []
        for xc in xx:
            ws = n.nonzero(x <= xc)[0]
            if len(ws) == 0: raise IndexError
            k_lower.append(ws[n.argmax(x[ws])])
            ws = n.nonzero(x >= xc)[0]
            if len(ws) == 0: raise IndexError
            k_upper.append(ws[n.argmin(x[ws])])
        k_lower = n.array(k_lower)
        k_upper = n.array(k_upper)

        # interpolation parameters
        h = x[k_upper]-x[k_lower]
        t = (xx - x[k_lower]) / h
        t[h==0.] = 0. # at data point

        # perform interpolation
        return   y[k_lower]*h00(t) + \
               h*m[k_lower]*h10(t) + \
                 y[k_upper]*h01(t) + \
               h*m[k_upper]*h11(t)

    return eval_spline


def test_monotone_spline():

    y = n.array([0.1, 0.2, 0.7, 0.71, 0.999, 1.])
    y = n.concatenate((y, y[-2::-1]))
    x = n.arange(len(y), dtype=n.float)
    interp_fn = monotone_spline(x, y)

    xx = n.linspace(x.min(), x.max(), 100)
    yy = interp_fn(xx)

    import pylab
    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    ax.scatter(x, y)
    ax.plot(xx, yy)

    pylab.draw()
    pylab.show()
    

if __name__=='__main__':
    #test_pca()
    #test_pca2()

    test_monotone_spline()
