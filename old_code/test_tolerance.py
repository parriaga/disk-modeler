from hr4796a import OffsetRingModel
from hr4796a import KLIPPolModelComparator
import matplotlib.pyplot as plot
import numpy
import pickle
import pyfits



def test_tolerance():
    mc = pickle.load(open('hr4796-fitter-mcmc.dat'))
    nonmodel = pyfits.getdata('S20140422K-KLmodes-KL5-masked.fits')
    im = mc.ringmodel
    print mc.scale
    scale = 1
    intim, polim = im.get_model_ring_image([[281,281],[281,281]],[[140,140],[140,140]], scale, drad0 = 1., n_step = 200, fast = True)
    print intim
    mc.ringmodel.parms['int_parms'][:] = 1.e8
    intim[0] = mc.filter_im_I(intim[0], 0)
    plot.imshow(intim[0] - nonmodel)
    plot.colorbar()
    plot.show()
    mc.ringmodel.parms['gamma_in'] = mc.ringmodel.parms['gamma_in']+.00001
    mc.ringmodel.parms['int_parms'][:] = mc.ringmodel.parms['int_parms'][:] + 9.e8
    intim_2, polim_2 = im.get_model_ring_image([[281,281],[281,281]],[[140,140],[140,140]], scale, drad0 = 1., n_step = 200, fast = True)
    intim_2[0] = mc.filter_im_I(intim_2[0], 0)
    plot.imshow(intim_2[0])
    plot.colorbar()
    plot.show()
    
    plot.imshow(polim_2[0] - polim[0])
    plot.colorbar()
    plot.show()
    
#    intim_2[0][150:200, 50:150] = 0
#    intim[0][150:200, 50:150] = 0
 
    diff = intim_2[0] - intim[0]
    diff[numpy.where(numpy.abs(diff) > .0001)] = 0
   
    plot.imshow(intim_2[0] - intim[0])
    plot.colorbar()
    plot.show()

    plot.imshow(diff)
    plot.colorbar()
    plot.show()

    return polim, polim_2
    

