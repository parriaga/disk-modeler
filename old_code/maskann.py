import pyfits
import numpy
import matplotlib.pyplot as plot








def annulus_mask(imsize,  center, outerrad = None, innerrad = None):
    ''' 
    Returns a of 0s with 1s in an annulus from innerrad to outerrad centered around
    center
    Inputs: imsize, center
       Optional: outerrad, innerad
    Outputs: mask
    '''
    masked = numpy.zeros(imsize) 
    ny, nx = masked.shape
    y, x = numpy.ogrid[0:ny,0:nx]
    y -= center[0]
    x -= center[1]
    R = numpy.sqrt(y**2+x**2)
    if innerrad is None:
        wh = numpy.where(R < outerrad)# or image == numpy.nan)
        masked[wh] = 1
        wh = numpy.where(masked == 0)
        masked[wh] = numpy.nan

        return masked
    else:
        wh = numpy.where((R < outerrad) & (R > innerrad))
        masked[wh] = 1
        wh = numpy.where(masked == 0)
        masked[wh] = numpy.nan
        return masked


#a = pyfits.open('HR4796A-K1-Qr.fits')
a = pyfits.open('S20140422K-forfm-KLmodes-KL2.fits')

mask = annulus_mask([281, 281], [140,140],innerrad = 12, outerrad = 90)
a[1].data =  a[1].data * mask
#a.writeto('HR4796A-K1-Qr-masked.fits')
a.writeto('S20140422K-forfm-KLmodes-KL2-masked.fits')

