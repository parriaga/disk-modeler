import matplotlib.pyplot as plot
import pyfits
import numpy
import scipy.interpolate as interpol
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from scipy.ndimage.filters import gaussian_filter
import pylab
import deproject
from hr4796a import KLIPPolModelComparator
from hr4796a import OffsetRingModel


def updateparms():
    mc = pickle.load(open('hr4796-fitter-Kint-mcmc.dat'))
    mc.update_int_parms(40)
    mc.ringmodel.parms['int_parms'][32] *= 3.8
    mc.ringmodel.parms['int_parms'][30:32] *= 3
    mc.show_model()


def updateparmsj():
    mc = pickle.load(open('hr4796-fitter-Jintonly-mcmc.dat'))
    mc.update_int_parms(40)
    mc.ringmodel.parms['int_parms'][32] *= 2
    mc.ringmodel.parms['int_parms'][30:32] *= 2
    mc.show_model()




def test_circle():
    '''
    Testing NEzr to nu. 
    '''
    omega = numpy.pi / 4.
    Omega = numpy.deg2rad(-67.7)
    I =  77. *numpy.pi / 180.
    offset = 3
    #Generate circle
    nu = numpy.linspace(-numpy.pi, numpy.pi, 100)
    r = numpy.zeros(100.) + 70.
    #Transform into sky plane
    pN, pE, pz, r_star = rnu_to_NEzr(r, nu, omega, Omega, I, offset)
    plot.scatter(pN, pE)
    plot.show()
    #Transform back into disk plane
    r2, nu2 = NEzr_to_rnu(pN, pE, omega, Omega, I, offset)
    x = r2 * numpy.cos(nu2)
    y = r2 * numpy.sin(nu2)
    plot.scatter(r2, nu2)
    plot.show()
    plot.scatter(x, y)
    plot.show()
    xx, yy = polcoords()
    plot.scatter(xx - 140, yy - 140)
    plot.scatter(pN, pE)
    plot.show()
    rr, nunu = NEzr_to_rnu(xx, yy, omega, Omega, I, offset)




def test_ellipse():
    '''
    Testing Nezr to nu
    '''
    omega = numpy.pi / 4.
    Omega = numpy.pi / 4.
    I =  77. *numpy.pi / 180.
    offset = 0
    a = 60.
    e = .3 
    #Generate ellipse
    nu = numpy.linspace(-numpy.pi, numpy.pi, 100)
    r = a * ( 1. - e**2.) / ( 1 + e * numpy.cos(nu))
    x1 = r * numpy.cos(nu)
    y1 = r * numpy.sin(nu)
    plot.scatter(x1, y1)
    #Put into sky plane
    pN, pE, pz, r_star = rnu_to_NEzr(r, nu, omega, Omega, I, offset)
    r2, nu2 = NEzr_to_rnu(pN, pE, omega, Omega, I, offset)
    x = r2 * numpy.cos(nu2)
    y = r2 * numpy.sin(nu2)
    plot.scatter(x, y, color = 'g')
    plot.show()

    




def rnu_to_NEzr(r, nu, omega, Omega, I, offset):
    ''' 
    Transforms from disk plane to sky plane
        Inputs: (in radians) r, nu, omega Omega I, offset
        Outputs: pN, pE, omega, Omega, I, offset
    '''
    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]

    "geometric transformation"
    sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
    
    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si
    
    pN -= oN 
    pE -= oE 
    pz -= oz
    
    r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
    
    return pN, pE, pz, r_star

def NEzr_to_rnu(pN, pE, omega, Omega, I, offset):
    '''
    Transforms from sky plane to disk plane
        Inputs: pN, pE, omega, Omega, I, offset (in radians)
        Outputs: r, nu
    '''
    # compute some useful quantities
    so, co = numpy.sin(omega), numpy.cos(omega)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    alpha = (pN + oN) / (pE + oE)
    #Geometric 
    nu = numpy.arctan2((cO - alpha * sO), ci * ( sO + alpha * cO))
    sinu, conu = numpy.sin(nu), numpy.cos(nu)
    r = (pN + oN) / (cO * conu - sO * sinu * ci)
    nu = nu - numpy.pi / 4 
    return r, nu



def tilted_ellipse_residual(p, x, y):
    ''' 
    Residuals for leastsq ellipse
    Parameters:
    p = I, Omega, a, e, phi
    x, y = set of points in sky plane
    returns residuals
    '''
    I, Omega, a, e, phi = p
    # omega and offset zero. 
    rad, nu = NEzr_to_rnu(x, y, 0, Omega, I, 0)
    rpar = a * (1 - e**2.) / ( 1. - e * numpy.cos(nu + phi))
    resid = (numpy.abs(rpar) - numpy.abs(rad))**2. 
    return resid

    

def genellipse(p, x, y):
    '''
    Generates ellipse with parameters p
    '''
    I, Omega, a, e, phi = p
    # omega and offset zero. 
    rad, nu = NEzr_to_rnu(x, y, 0, Omega, I, 0)
    rpar = a * (1 - e**2.) / ( 1. + e * numpy.cos(nu + phi))
    x1, y1 = poltocart(rad, nu)
    print rad
    print rpar
    plot.scatter(x1, y1, color = 'g')
    xell, yell = poltocart(rpar, nu)
    plot.scatter(xell, yell)
    plot.show()
    return xell, yell




def tilted_circle_residual(p, x, y):
    r, I, Omega, offset, omega = p
    #Transform our points into disk plane
    rad, nu = NEzr_to_rnu(x, y, omega, Omega, I, offset)
    #Generate disk-plane circle
    nuttest = numpy.linspace(-numpy.pi, numpy.pi, 100)
    rtest = numpy.zeros(100.) + r
    pN, pE, pz, r_star = rnu_to_NEzr(r, nu, omega, Omega, I, offset)
    #Take residuals
    resid = numpy.abs(rad) - r
    return resid





def leastsq_tilted_circle(filt = 'K', comparison = True):
    # Get data points from polcoords
    x, y,rinit, thinit = radial_K(filt)
    # Form guess
    p0 = [78, numpy.deg2rad(77), numpy.deg2rad(-67),1.5, numpy.deg2rad(-22)]
    # Run leastsq
    p = leastsq(tilted_circle_residual, p0, args = (x, y))
    r, I, Omega, offset, omega = p[0]
    nu = numpy.linspace(-numpy.pi, numpy.pi, 100)
    rr = numpy.zeros(100.) + r
    pN, pE, pz, r_star = rnu_to_NEzr(rr, nu, omega, Omega, I, offset)
    im = pyfits.getdata('HR4796A-K1-Qr.fits')
    plot.imshow(im,vmin = 0, vmax = 100)
    plot.plot(pN + 140, pE + 140,  color = 'g', linewidth = 2)
    plot.scatter(x, y)
    plot.show()
    print 'Radius:', r
    print 'I', I * 180. / numpy.pi
    print 'Omega', Omega * 180 / numpy.pi
    print 'offset', offset



    if comparison is True:
        model = deproject.test_flat_ring_model
        x_test, y_test, rinit_test, thi_test = radial_K('K_test')
        p0 = [78, numpy.deg2rad(77), numpy.deg2rad(-67),1.5, numpy.deg2rad(-22)]
        # Run leastsq
        p_test = leastsq(tilted_circle_residual, p0, args = (x_test, y_test))
        r_test, I_test, Omega_test, offset_test, omega_test = p[0]
        nu_test = numpy.linspace(-numpy.pi, numpy.pi, 100)
        rr = numpy.zeros(100.) + r
        pN_test, pE_test, pz_test, r_star_test = rnu_to_NEzr(rr, nu, omega_test, Omega_test, I_test, offset_test)
        grid_test = deprojmap(p_test[0], model)
        

    ###TODO: Sort residuals by nu
#    r, nu = NEzr_to_rnu(x, y, omega, Omega, I, offset)
#    
#    nu = nu * 180. / numpy.pi + 45 - omega * 180. / numpy.pi
    
 #   nu[numpy.where(nu > 180)] = nu[numpy.where( nu > 180)] - 360
    resids = tilted_circle_residual(p[0], x, y)

    p[0][0] = 100.
    print p[0]    
    grid = deprojmap(p[0], im)

    print rinit
    print omega * 180. / numpy.pi
    thinit = thinit * 180. / numpy.pi - 45 - 22.3
    if comparison is True:
        plot.subplot('211')
        plot.imshow(numpy.transpose(grid_test),extent = [0, 360,-r,100. - r], vmin = 0, vmax = 100)
        plot.colorbar()
        plot.scatter(thinit, residuals)
        plot.plot(numpy.arange(360), numpy.zeros(360), color = 'orange', linewidth = 2)
        plot.subplot('212')
    plot.imshow(numpy.transpose(grid), extent = [0, 360,-r,100. - r], vmin = 0, vmax = 100)

    plot.colorbar()

    plot.scatter(thinit, resids, color = 'grey')
    #Plot zeroes
    ran = numpy.arange(360) 
    plot.plot(ran, numpy.zeros(len(ran)), color = 'orange', linewidth = 2)
    plot.show()


    return grid
    





    
def leastsq_tilted_ellipse(filt = 'K'):
    # Get data points from polcoords
    x, y = radial_K(filt)
    # Form guess
    p0 = [numpy.deg2rad(78), numpy.deg2rad(-67), 70, 0.1, numpy.deg2rad(20.)]
    # Run leastsq
    p = leastsq(tilted_ellipse_residual, p0, args = (x, y))
    I, Omega, a, e, phi = p[0]
    nu = numpy.linspace(-numpy.pi, numpy.pi, 100)
    rpar = a * (1 - e**2.) / ( 1. - e * numpy.cos(nu + phi))
    pN, pE, pz, r_star = rnu_to_NEzr(rpar, nu, 0, Omega, I, 0)
    im = pyfits.getdata('HR4796A-K1-Qr.fits')
    plot.imshow(im,vmin = 0, vmax = 100)
    plot.plot(pN + 140, pE + 140,  color = 'g', linewidth = 2)
    plot.scatter(x, y)
    plot.show()
    print p
    


def profiles(imtype = 'K'):
    
#    p = [ 77.2,            1.32852643,   -1.1031072,    -3.26029403,   -1.32528547]
    p = [ 100.,            1.32852643,   -1.1031072,    -3.26029403,   -1.32528547]
    if imtype is None:
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    if imtype is 'J':
        im = pyfits.getdata('HR4796A-J-Qr.fits')
    if imtype is 'K':
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    if imtype is 'Ktot':
        im = pyfits.getdata('S20140422K-KLmodes-KL5-masked.fits')
    if imtype is 'Jtot':
        
        im = pyfits.getdata('S20140422J-KLmodes-KL5-masked.fits')
    print 'what'
    grid = deprojmap(p, im)
    print 'fuck'
    # Bright extraneous patch
    grid[:, 0:50] = 0
    grid = grid[1:]
    grid = numpy.vstack((grid[285:],grid[0:170]))
    plot.imshow(grid, vmin = 0, vmax = 100)
    
    plot.colorbar()
    plot.show()

#    grid = deprojmap(p, im)
    sh = grid.shape
    lincoll = []
    xbcoll = []
    gam1coll = []
    gam2coll = []
    binning = 8.
    for i in numpy.arange(sh[0] / binning) - 1:
        lin = grid[i*binning:(i+1) *binning, :]
        lin = numpy.nansum(lin, axis = 0)
        lin = lin[60:90]
        plot.plot(lin)
        lincoll.append(lin)
        
        p = sbpl_fit(lin)
        xb, x0, y0, gam1, gam2, c = p
        gam1coll.append(gam1)
        gam2coll.append(gam2)
        xbcoll.append(xb)
    plot.show()
    plot.plot(gam1coll)
    plot.title('gam1 as a function of azi')
    plot.show()
    plot.plot(gam2coll)
    plot.title('gam2 as a function of azi')
    plot.show()
    plot.plot(xbcoll)
    plot.title('xb as a function of azi')
    plot.show()

    return lincoll



def sbpl_fit(lin, show = False):
    '''
    Fits a smooth broken power law to a line (lin)
    parameters returned are
    [x_b, x_0, y_0, gam1, gam2, c]
    x_b is the break point
    y_0 is the width parameter
    gam1 is the power law before the break point
    gam2 is the power law after the break point
    '''
    lin = lin / numpy.max(lin)
    p0 = [23., 1., .04,1.,-2., .1]
    p = leastsq(sbpl_residual, p0, args = (lin))
    x_b, x_0, y_0, gam1, gam2,  c = p[0]
#    print p[0]
    if show == True:
        yp = smooth_broken_power_law(numpy.arange(len(lin),dtype = float), x_b, x_0, y_0, gam1, gam2, 0) + c
        plot.plot(numpy.arange(len(lin)), yp)
        plot.scatter(numpy.arange(len(lin)),lin)
        plot.show()
    return p[0]

def sbpl_residual(p, lin):
    xpoints = numpy.arange(len(lin)) + 1.
    x_b, x_0, y_0, gam1, gam2,c = p
    ypoints = smooth_broken_power_law(xpoints, x_b, x_0, y_0, gam1, gam2, 0) + c
#    plot.plot(ypoints)
#    plot.plot(lin)
#    plot.show()
#    print ypoints - lin
#    print p
    return ypoints - lin


def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = numpy.zeros_like(x)
        w = numpy.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = numpy.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y


    
def deprojmap(p, im, samp = 1, show = True):
    maxrad = 140
    maxphi = 360 
    samp = 4
    center = [140,140]
    shape = im.shape
    r, I, Omega, offset, omega = p
    r, phi = numpy.meshgrid(numpy.arange(maxrad * samp ) / samp + 1, numpy.arange(maxphi * samp) / samp )
    r_u = []
    phi_u = []
    vals = []
    for i in numpy.arange(shape[0]):
        for j in numpy.arange(shape[1]):
            if not numpy.isnan(im[i,j]):
                r_c, phi_c = NEzr_to_rnu(i - center[0], j - center[1], omega, Omega, I, offset)
                if ~numpy.isnan(r_c) and ~numpy.isnan(phi_c):
                    if r_c < 0:
                        r_c = numpy.abs(r_c)
                        if phi_c > 0:
                        
                            phi_c = - numpy.pi + numpy.abs(phi_c)
                        else: 
                            phi_c = numpy.pi - numpy.abs(phi_c)
                    phi_c *= 180. / numpy.pi
                    if phi_c < 0:
                        phi_c +=360.

                    if r_c * numpy.sin(phi_c) != 0:
                        r_u.append(r_c)
                        phi_u.append(phi_c)
                        if im[i, j] < 1.e-2:
                            vals.append(0.)
                        else:
                            vals.append(im[i, j])

    grid = interpol.griddata(numpy.array([r_u, phi_u]).transpose(), vals, (r, phi), method = 'cubic')
    if show == True:
        plot.clf()
        plot.imshow(grid.transpose(), vmin = 0, vmax = 110, extent=[0,360,0,140])
        plot.show()
    return grid




import numpy as n
def array_labels(mc):
    r = mc.ringmodel.parms['r']
    offset = mc.ringmodel.parms['offset']
    #offset *= 10 # TEMP                                                              
    I = -mc.ringmodel.parms['I']
    omega = mc.ringmodel.parms['omega']
    Omega = mc.ringmodel.parms['Omega']

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]                                             
    oE = offset * (sO*co+cO*so*ci) # [AU]                                             
    oz = offset * so*si # [AU]                                                        
    def get_NE(nu):
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]                                          
        pE = r * (sO*conu+cO*sonu*ci) # [AU]                                          
        pz = r * sonu*si

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)

        # scattering angle                                                            
        phi_sca = n.arccos(pz/r_star) # [rad]                                         

        return pN, pE, phi_sca

    ## # draw line of nodes                                                           
    ## nu = n.array((-omega, n.pi-omega))                                             
    ## pN, pE, phi_sca = get_NE(nu)                                                   
    ## ax.plot(pE, pN, ls='--', c='k', lw=1., zorder=1)                               

    # calculate ellipse                                                               
    nu = n.linspace(0., 2.*n.pi, 100, endpoint=True)
    pN, pE, phi_sca = get_NE(nu)

    # draw ellipse                                    

    # mark phi_sca angles                                                             
    #                                                                                 

    ft = 1.4
    facts = n.array((0.95, 1.3))

    # nus for which theta_sca = min, max                                              
    from scipy.optimize import fmin, brentq
    nus = n.array([fmin(lambda nu: get_NE(nu)[2], 0.)[0],
                   fmin(lambda nu: -get_NE(nu)[2], 0.)[0]]) % (2.*n.pi)
    nu_min = nus.min()
    nu_max = nus.max()
    print "min", nu_min*180./n.pi, get_NE(nu_min)[2]*180./n.pi                       
    print "max", nu_max*180./n.pi, get_NE(nu_max)[2]*180./n.pi                       

    phi_scas = n.arange(13., 160., 10)
    nuret = []
    phiret = []
    for phi in phi_scas:
        def comp(nu):
            d = (get_NE(nu)[2]-phi*n.pi/180.) % (2.*n.pi)
            if d > n.pi: d -= 2.*n.pi
            return d
        # which nus does this correspond to?
        nu1 = brentq(comp, nu_min, nu_max) % (2.*n.pi)
        nu2 = brentq(comp, nu_max, nu_min+2.*n.pi) % (2.*n.pi)
        nuret.append(nu1)
        nuret.append(nu2)
        phiret.append(phi)
        phiret.append(phi)
        
    return numpy.array(nuret), numpy.array(phiret)




import pickle
from scipy.ndimage.filters import gaussian_filter
def kpol_paperfig(imtype = 'K', saveparams = 'hr4796-fitter-kpol-leastsq2.dat'):
    if imtype is None:
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    if imtype is 'J':
        im = pyfits.getdata('HR4796A-J-Qr.fits')
    if imtype is 'K':
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    if imtype is 'Ktot':
        im = pyfits.getdata('S20140422K-KLmodes-KL5-masked.fits')
    if imtype is 'Jtot':
        im = pyfits.getdata('S20140422J-KLmodes-KL5-masked.fits')
    if imtype is 'K_test':
#        im = deproject.test_flat_ring_model()
        im = pyfits.getdata('testring.fits')


    model = pyfits.getdata('kpolmodel.fits')



    parms = pickle.load(open(saveparams))
    I = parms['I']
    Omega = parms['Omega']
    omega = parms['omega']
    offset = parms['offset']

    
    nu = numpy.linspace(0., 2. * numpy.pi, 360 * 4.) + numpy.pi /4. + 22.3 * numpy.pi / 180.
    jphi_sca = to_phisca(nu, parms) #* 180. / numpy.pi 
    minloc = numpy.where(jphi_sca == numpy.min(jphi_sca))[0] 
    if minloc < 360. * 2:
        minloc += 360. * 2.
    else:
        minloc -= 360. * 2.
    print minloc
    nu = nu[::-1]
    grid = numpy.flipud(deprojmap([100, I, -Omega, offset, -22.3 * 180 / numpy.pi], im, show = False))
    grid = numpy.vstack((grid, grid))

#    minloc = numpy.shape(grid)[0] - minloc
#    grid = numpy.vstack((grid[minloc[0]:], grid[:minloc[0]]))
#    new_nu = numpy.append(nu[minloc[0]:], nu[3:minloc[0]])


    modelgrid = numpy.flipud(deprojmap([100, I, -Omega, offset, -22.3 * 180 / numpy.pi], model))
    modelgrid = numpy.vstack((modelgrid, modelgrid))


#    modelgrid = numpy.flipud(numpy.vstack((modelgrid[minloc[0]:], modelgrid[3:minloc[0]])))

    binning = 4.
    nbins = numpy.floor(360. * 8 / binning)
    hafval = binning / 2.
    heights = numpy.zeros(nbins)
    heights_mod = numpy.zeros(nbins)
    for i in numpy.arange(nbins):
#    for i, lin in enumerate(grid):
        # To do - Try binning
        modlin = modelgrid[i* binning: (i+1)*binning]
        modlin = numpy.nansum(modlin, axis = 0)
        lin = grid[i* binning: (i+1)*binning]
        lin = numpy.nansum(lin, axis = 0)
        
        
        heights[i] = numpy.nansum(lin[60*4:90*4]) / binning
        heights_mod[i] = numpy.nansum(modlin[60*4:90*4]) / binning

    nuaxis = numpy.linspace(0, 360, nbins / 2)
    nuaxis = numpy.append(nuaxis, nuaxis)
    f, (ax1, ax2, ax3) = plot.subplots(3, sharex = True)
    f.set_size_inches((7, 3.5))
    f.subplots_adjust(hspace = 0)
    #   f.subplots_adjust(right=0.85)
#    cbar_ax = f.add_axes([.9, .15, .03, .7])
    ax1.imshow(grid.T, vmin = 0, vmax = 100, extent = [0,360 * 2, 0, 140])
    ax1.set_ylabel('Radius (AU')
    modax = ax2.imshow(modelgrid.T, vmin = 0, vmax = 100,  extent = [0, 360 * 2, 0, 140])
    ax2.set_ylabel('Radius AU')
#    ax3.plot(numpy.linspace(-180, 180, len(heights)), numpy.array(heights), label = 'Data')
#    ax3.plot(numpy.linspace(-180, 180, len(heights)), numpy.array(heights_mod), label = 'Model')
    ax3.plot(numpy.linspace(0, 360 * 2, len(heights)), numpy.array(heights), label = 'Data')
    ax3.plot(numpy.linspace(0, 360 * 2, len(heights)), numpy.array(heights_mod), label = 'Model')
    ax3.legend()
    mc = pickle.load(open('hr4796-fitter-jpol-mcmc.dat'))
    nuax, phiax = array_labels(mc)
    nuax = numpy.append(nuax, nuax + 2. *numpy.pi)
    phiax = numpy.append(phiax, phiax)
    print nuax * 180. / numpy.pi
    print phiax #* 180. / numpy.pi
    
    plot.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    left='off',      # ticks along the bottom edge are off
    right='off',         # ticks along the top edge are off
    labelleft='off') # labels along the bottom edge are off
    plot.xticks(nuax * 180. / 3.14 + 22.3, phiax, rotation='vertical')
    plot.show()
    

    
def color_profiles(imtype = 'J'):
    save_pol = 'hr4796-fitter-jpol-leastsq2.dat'    
    polmodel = pyfits.getdata('jpolmodel.fits')
    intmodel = pyfits.getdata('tot_j_model.fits') 

    ksave_pol = 'hr4796-fitter-kpol-leastsq2.dat'    
    kpolmodel = pyfits.getdata('kpolmodel.fits')
    kintmodel = pyfits.getdata('tot_k_model.fits')    


    jparms = pickle.load(open(save_pol))
    I = jparms['I']
    Omega = jparms['Omega']
    omega = jparms['omega']
    offset = jparms['offset']
    nbins = 360 / 3
    kparms = pickle.load(open(ksave_pol))
    kI = kparms['I']
    kOmega = kparms['Omega']
    komega = kparms['omega']
    koffset = kparms['offset']


    polgrid = numpy.flipud(deprojmap([100, I, -Omega, offset, -22.3 * numpy.pi / 180.], polmodel, show = False))
    polgrid = polgrid[4:]
    intgrid = numpy.flipud(deprojmap([100, I, -Omega, offset, -22.3 * numpy.pi / 180.], intmodel, show = False)) * 1.4
    intgrid = intgrid[4:]
    kpolgrid = deprojmap([100, I, -Omega, offset, -22.3 * numpy.pi / 180.], kpolmodel, show = False)
    kintgrid = deprojmap([100, kI, -kOmega, koffset, komega], kintmodel, show = False)


    binning = 3
    intheight = numpy.zeros(1440. / binning)
    polheight = numpy.zeros(1440. / binning)
    kintheight = numpy.zeros(1440. / binning)
    kpolheight = numpy.zeros(1440. / binning)
    nbins = 1440 / binning
    for i in range(1440 / binning):
            
        intheight[i] = numpy.nansum(intgrid[i* binning: (i+1)*binning, 70*3:130*3]) / 3.
        polheight[i] = numpy.nansum(polgrid[i* binning: (i+1)*binning, 70*3:130*3]) / 3.
        kintheight[i] = numpy.nansum(kintgrid[i* binning: (i+1)*binning, 70*3:130*3]) / 3.
        kpolheight[i] = numpy.nansum(kpolgrid[i* binning: (i+1)*binning, 70*3:130*3]) / 3.
    

    polgrid = numpy.vstack((polgrid, polgrid))
    intgrid = numpy.vstack((intgrid, intgrid))
    polheight = numpy.append(polheight, polheight)
    intheight = numpy.append(intheight, intheight)

    mc = pickle.load(open('hr4796-fitter-jpol-mcmc.dat'))
    nuax, phiax = array_labels(mc)
    nuax = numpy.append(nuax, nuax + 2. *numpy.pi)
    phiax = numpy.append(phiax, phiax)

    f, (ax1, ax2, ax3, ax4) = plot.subplots(4, sharex = True)
    ax1.imshow(polgrid.T, vmin = 0, vmax = 150, extent = [0,720, 0, 140])
    modax = ax2.imshow(intgrid.T, vmin = 0, vmax = 400,  extent = [0,720, 0, 140])
    ax3.plot(numpy.linspace(0.,720, len(polheight)), polheight, label = 'Pol')
    ax3.plot(numpy.linspace(0.,720, len(polheight)), intheight, label = 'Tot')
    ax3.legend()
    ax4.plot(numpy.linspace(0.,720, len(polheight)), polheight/intheight, label = 'Pol frac')
    plot.xticks(nuax * 180. / 3.14 + 22.3, phiax, rotation='vertical')
#    ax1.set_xlim([0,360])
    plot.show()
    return 
    f, (ax1, ax2, ax3, ax4) = plot.subplots(4, sharex = True)
    f.set_size_inches((7, 3.5))
    f.subplots_adjust(hspace = 0)
    f.subplots_adjust(right=0.85)
    cbar_ax = f.add_axes([.9, .15, .03, .7])
    ax1.imshow(polgrid.T, vmin = 0, vmax = 400, extent = [0,360, 0, 140])
    modax = ax2.imshow(intgrid.T, vmin = 0, vmax = 400,  extent = [0,360, 0, 140])
    ax3.plot(numpy.linspace(0.,720, nbins), polheight, label = 'Pol')
    ax3.plot(numpy.linspace(0.,720, nbins), intheight, label = 'Tot')
    ax3.legend()
    ax4.plot(numpy.linspace(0.,720, nbins), polheight/intheight, label = 'Pol frac')
    f.colorbar(modax, cax = cbar_ax)
    ax1.set_xlim([0,360])
    plot.show()

    f, (ax1, ax2, ax3, ax4) = plot.subplots(4, sharex = True)
    f.set_size_inches((7, 3.5))
    f.subplots_adjust(hspace = 0)
    f.subplots_adjust(right=0.85)
    cbar_ax = f.add_axes([.9, .15, .03, .7])
    ax1.imshow(kpolgrid.T, vmin = 0, vmax = 150, extent = [0,360, 0, 140])
    modax = ax2.imshow(kintgrid.T, vmin = 0, vmax = 400,  extent = [0,360, 0, 140])
    ax3.plot(numpy.linspace(0.,360, nbins), kpolheight, label = 'Pol')
    ax3.plot(numpy.linspace(0.,360, nbins), kintheight, label = 'Tot')
    ax3.legend()
    ax4.plot(numpy.linspace(0.,360, nbins), kpolheight/kintheight, label = 'Pol frac')
    ax4.plot(numpy.linspace(0.,360, nbins), polheight/intheight, label = 'Pol frac')
    f.colorbar(modax, cax = cbar_ax)
    ax1.set_xlim([0,360])
    plot.show()
    


    




    

def to_phisca(nu, paramdict):
    r = paramdict['r']
    offset = paramdict['offset']
    I = paramdict['I']
    omega = -22.3 * numpy.pi / 180.#paramdict['omega']
    Omega = paramdict['Omega']
    so, co = numpy.sin(omega), numpy.cos(omega)
    sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    
    #Euler angle transformation
    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si
    
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    
    #Subtract offset angles
    pN -= oN
    pE -= oE
    pz -= oz
    
    r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
    
    # scattering angle
    phi_sca = numpy.arccos(pz/r_star) # [rad]
    return phi_sca



import pickle
from scipy.ndimage.filters import gaussian_filter
def paperfig_profiles(imtype = 'J', saveparams = 'hr4796-fitter-jpol-leastsq2.dat'):
    kim = pyfits.getdata('HR4796A-K1-Qr.fits')
    jim = pyfits.getdata('HR4796A-J-Qr.fits')
    jmodel = pyfits.getdata('jpolmodel.fits')
    kmodel = pyfits.getdata('kpolmodel.fits')

    plot.imshow(jim - jmodel, vmin = -5, vmax = 5)
    plot.show()
    plot.imshow(jim - kmodel, vmin = -5, vmax = 5)
    plot.show()

    parms = pickle.load(open(saveparams))
    I = parms['I']
    Omega = parms['Omega']
    omega = parms['omega']
    offset = parms['offset']

    jgrid = deprojmap([100, I, -Omega, offset, omega], jim, show = False)
    jgrid = numpy.vstack((jgrid[230:], jgrid[1:230]))
    kgrid = deprojmap([100, I, -Omega, offset, omega], kim, show = False)
    kgrid = numpy.vstack((kgrid[230:], kgrid[1:230]))
    jmodelgrid = deprojmap([100, I, -Omega, offset, omega], jmodel, show = False)
    jmodelgrid = numpy.vstack((jmodelgrid[230:], jmodelgrid[1:230]))
    kmodelgrid = deprojmap([100, I, -Omega, offset, omega], kmodel, show = False)
    kmodelgrid = numpy.vstack((kmodelgrid[230:], kmodelgrid[1:230]))


    binning = 3.
    nbins = numpy.floor(360. / binning)
    hafval = binning / 2.
    jheights = numpy.zeros(nbins)
    jheights_mod = numpy.zeros(nbins)
    kheights = numpy.zeros(nbins)
    kheights_mod = numpy.zeros(nbins)

    for i in numpy.arange(nbins):
#    for i, lin in enumerate(grid):
        # To do - Try binning
        kmodlin = kmodelgrid[i* binning: (i+1)*binning]
        kmodlin = numpy.sum(kmodlin, axis = 0)
        jmodlin = jmodelgrid[i* binning: (i+1)*binning]
        jmodlin = numpy.sum(jmodlin, axis = 0)
        
        
        jlin = jgrid[i* binning: (i+1)*binning]
        jlin = numpy.sum(jlin, axis = 0)
        klin = kgrid[i* binning: (i+1)*binning]
        klin = numpy.sum(klin, axis = 0)
        
        
        jheights[i] = numpy.nansum(jlin[60:90])
        jheights_mod[i] = numpy.nansum(jmodlin[60:90])
        kheights[i] = numpy.nansum(klin[60:90])
        kheights_mod[i] = numpy.nansum(kmodlin[60:90])

            
    plot.plot(numpy.linspace(0.,360, nbins), jheights, label = 'J Data')
    plot.plot(numpy.linspace(0.,360, nbins), jheights_mod, label = 'J Model', linewidth = 3.0)
    plot.plot(numpy.linspace(0.,360, nbins), kheights, label = 'K Data')
    plot.plot(numpy.linspace(0.,360, nbins), kheights_mod, label = 'K Model',linewidth = 3.0)
    plot.legend()
    plot.show()





def radial_K(imtype = None, initparams = None):
    ''' 
    Only transforms from Cartesion to polar
    '''
    if initparams is None:
        #Guesses for geometric parameters 
        omega = -22.3 * numpy.pi / 180.
        Omega = -27.8 * numpy.pi / 180.
        I =  77. *numpy.pi / 180.
        offset = 0 #1.5
    else:
        r, I, Omega, offset, omega = initparams
        Omega = (numpy.pi/2. + Omega) 
        Omega = - numpy.abs(Omega)
    if imtype is None:
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    if imtype is 'J':
        im = pyfits.getdata('HR4796A-J-Qr.fits')
    if imtype is 'K':
        im = pyfits.getdata('HR4796A-K1-Qr.fits')
    if imtype is 'Ktot':
        im = pyfits.getdata('S20140422K-KLmodes-KL5-masked.fits')
    if imtype is 'Jtot':
        im = pyfits.getdata('S20140422J-KLmodes-KL5-masked.fits')
    if imtype is 'K_test':
#        im = deproject.test_flat_ring_model()
        im = pyfits.getdata('testring.fits')
    print 'shit'
    grid = deprojmap([100, I, Omega, offset, omega], im)

    return vals, r, nu
    print 'fuck'
    print grid
    # Bright extraneous patch
#    grid[:, 0:50] = 0
#    grid = grid[1:]

    plot.imshow(grid, vmin = 0, vmax = 100)
    plot.colorbar()
    plot.show()

    coords = []
    binning = 3.
    nbins = numpy.floor(360. / binning)
    hafval = binning / 2.
    
    for i in numpy.arange(nbins):
#    for i, lin in enumerate(grid):
        # To do - Try binning
        if binning * i < 170. or binning * i > 285:
            lin = grid[i* binning: (i+1)*binning]
            lin = numpy.sum(lin, axis = 0)
            lin = lin[60:90]
            if imtype is 'K_test':
                plot.plot(lin)
                plot.show()
            print i
            p = sbpl_fit(lin)
            ma = p[0] + 60
            coords.append([binning * (i + .5), ma])
    coords = numpy.array(coords)
    samp = 1
    plot.scatter(coords[:, 1], coords[:, 0])
    plot.show()
    th = ((coords[:, 0] / samp)) * numpy.pi / 180. + 22.3 * numpy.pi / 180. + numpy.pi / 4
    print th
    r = (coords[:, 1] / samp) 
#    if imtype is 'J' or imtype is 'K':
#        wh = numpy.where((th < (167. + 22.3 + 45) * numpy.pi / 180.) | (th > (292 + 22.3 + 45) * numpy.pi / 180.))
        
#    else:
#        wh = numpy.where((r >68) & (r < 88))        
#    r = r[wh]
#    th = th[wh]
    y, x, z, r_star = rnu_to_NEzr(r, th, omega, Omega, I, offset)
    plot.imshow(im, vmin = 0, vmax = 100)
    plot.scatter(x + 140, y + 140)
    plot.show() 
    return x, y, r, th 






def poltocart(r, theta):
    shifty = r * numpy.sin(theta)
    shiftx = r * numpy.cos(theta)
    yy = shifty + center[0]
    xx = shiftx + center[0]
    return yy, xx





#################################################################
###############################################################
###############################################################

#Some parameters
#center = [140,140]
center = [0,0]
maxrad = 84
imsize = [281, 281]



def test():
    im = pyfits.getdata('HR4796A-K1-Qr-masked.fits')
    rbins = 16
    thetabins = 30
    sampleazi(im, rbins, thetabins)



