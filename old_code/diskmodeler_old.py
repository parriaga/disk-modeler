#!/usr/bin/env python
#
# $Id$
#
# Michael Fitzgerald (mpfitz@ucla.edu) 2014-2-5
#
# code for modeling HR 4796A
#

## #---------------------------------------------------------------------------
## # This code loads IPython but modifies a few things if it detects it's running
## # embedded in another IPython session (helps avoid confusion)
try:
    get_ipython
except NameError:
    banner=exit_msg=''
else:
    banner = '*** Nested interpreter ***'
    exit_msg = '*** Back in main IPython ***'
from IPython.terminal.embed import InteractiveShellEmbed
## # Now create the IPython shell instance. Put ipshell() anywhere in your code
## # where you want it to open. 
ipshell = InteractiveShellEmbed(banner1=banner, exit_msg=exit_msg)
## #------------------------------------------------------------------------------




import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
import copy
from pyklip.fmlib.diskfm import DiskFM
_log = logging.getLogger('hr4796a')
import matplotlib.pyplot as plt 





def get_tck(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:n_param+k] = params[0:k]

    return t, c, k

def get_tck2(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:k] = params[-k:]
    c[k:n_param+k] = params

    return t, c, k



def get_tck180(p, k=3):
    '''
    get periodic spline parameters for mirrored past 180deg
    Inputs:
       p:   the anchor points for the spline
       k:   the order of the spline fit
    Outputs:
       t:   the spline's knot vector
       c:   the spline coefficients 
    '''

    # mirror first cell
    ik = int(n.ceil(k/2.))
    params = n.concatenate((p[0:ik], p[k-ik-1::-1], p[ik:]))

    n_param = len(params)

    xx_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/(2*n_param-k-1)

    n_knot = 2*n_param-1 + k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:2*n_param-1] = params[-2::-1]

    return t, c, k

    

from scipy.interpolate import splev
eval_pspl = lambda phi, tck: splev(phi % (2.*n.pi), tck)

def model_fn(phi, params, **kwargs):
    tck = get_tck(params, **kwargs)
    return eval_pspl(phi, tck)

def model_fn2(phi, params, **kwargs):
    tck = get_tck2(params, **kwargs)
    return eval_pspl(phi, tck)

def model_fn180(phi, params, **kwargs):
    tck = get_tck180(params, **kwargs)
    return eval_pspl(phi, tck)




def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y


###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################

class diskObject(object):
    def __init__(self, fname, initparms, scale, label, is_fixed, basisfnames = None, psf_imname = None, 
                 polint = 'pol',  numbasis = None, psf_method = 'inputim', data_image_filenames = None):
        '''
        Disk object generates a filtered model disk based on parameters
        
        Args: 
            fname: Filename of data image
            initparms: Dictionary of initial values for parameters
            scale: 
            label: Label for this disk for the dictionary of parameters
            basisfnames: Name of the basis filenames if disk is total intensity
            psf_imname: Name for PSF image to be used  
            data_image_filenames: Used for KLIP-FM, filenames of the original data used to form the basis 
            FIXME finish here
        '''
        
        self.polint = polint.lower()
        self.psf_method = psf_method
        self.psf_imname = psf_imname
        self.scale = scale
        self.label = label
        self.is_fixed = is_fixed

        self.read_files(fname, basisfnames, numbasis, psf_imname, data_image_filenames)
        self.parms = initparms
        self.parmlist = initparms.keys()        

        self.parm_lens = dict([(k,1) for k in self.parmlist])
        self.parm_lens[label + 'psf_parms'] = 1
        self.parm_lens['int_parms'] = len(self.parms[label + 'int_parms'])
        
        stdim, R = self.get_stdims()
        self.stdim = stdim
        self.R = R
        
        self.current_residuals = None #FIXME 

    def update_int_parms(self, n_int_parm):
        "update intensity parameters with new number of parameters"

        if n_int_parm == self.parm_lens['int_parms']:
            return

        from scipy.optimize import leastsq
        nu = n.linspace(0., 2.*n.pi, 100) # [rad]
        phi_sca = n.linspace(0., n.pi, 50) # [rad]
        curr = self._get_intensity(nu, phi_sca)
        def fit_fn(p):
            self.parms[self.label + 'int_parms'] = n.exp(p)
            mod_int = self._get_intensity(nu, phi_sca)
            return curr-mod_int
        self.parms[self.label + 'int_parms'] = n.ones(n_int_parm, dtype=n.float)*2e8
        self.parm_lens['int_parms'] = n_int_parm
        sp = n.log(self.parms[self.label + 'int_parms'])
        p_opt, ier = leastsq(fit_fn, sp.copy())
        self.parms[self.label + 'int_parms'] = n.exp(p_opt)





    def update_parms(self, new_parms):
        ischanged = False
        # check to see if value has changed
        for key in new_parms.keys():
            if 'int_parms' in key:
                if key == self.label + 'int_parms':
                    if n.any(new_parms[key] != self.parms[key]):
                        ischanged = True
            elif 'psf_parms' in key:
                if key == self.label + 'psf_parms':
                    if n.any(new_parms[key] != self.parms[key]):
                        ischanged = True
            elif self.parms[key] != new_parms[key]:
                ischanged = True
            else:
                pass
        self.parms.update(new_parms)
        return ischanged
        
    def read_files(self, fname, basisfnames, numbasis, psf_imname, data_image_filenames):
        import glob
        import pyklip.instruments.GPI as gpi
        fits = pyfits.open(fname)
        self.fim = pyfits.getdata(fname)
        hdr = fits[0].header
        self.image_shape = n.shape(self.fim)
        if self.polint == 'pol':
            self.center = [140. + hdr.get('SPOT_DX'), 140. + hdr.get('SPOT_DY')]
            
        if self.polint == 'int' or self.polint == 'tot':
            assert basisfnames is not None, 'Requires basis files for KLIP-FM' 
            assert numbasis is not None, 'Requires numbasis'
            
            # FIXME
            self.center = [140, 140]
#            self.center = [hdr.get('PSFCENTX'), hdr.get('PSFCENTY')]
            # FIXMEE
            numbasis = 1
            
            fnames = sorted(glob.glob(data_image_filenames))
            self.klip_dataset = gpi.GPIData(fnames)
#            print self.image_shape
            model_disk = n.zeros(self.image_shape)  #dummy model disk

            self.fm_object = DiskFM(n.array([len(fnames), self.image_shape[0], self.image_shape[1]]),
                                    n.array([numbasis]), self.klip_dataset, model_disk,
                                    basis_filename = basisfnames,
                                    load_from_basis = True)


        if self.psf_method == 'inputim':
            assert self.psf_imname is not None
            self.psf_im = pyfits.getdata(self.psf_imname)
        else: 
            # FIXME
            pass

    def get_stdims(self):
        from image import radial_stdev_profile

        # assumes all images are same size and are aligned. 
        self.tfim = self.fim  #FIXME if ever dealing with different sized
        rprof, sprof = radial_stdev_profile(self.fim, [int(self.center[0]), int(self.center[1])])
        y, x = n.mgrid[0:self.fim.shape[0],
                       0:self.fim.shape[1]]
        y -= int(self.center[0])
        x -= int(self.center[1])
        R = n.sqrt(x**2+y**2)
        stdim = n.zeros_like(self.fim)
        
        # Turn profiles into image of standard dev as a function of radius. 
        for r, s in zip(rprof, sprof):
            wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
            stdim[wr] = s

        return stdim, R
    
    def update_stdim(self):
        from image import radial_stdev_profile
        # assumes all images are same size and are aligned. 
        self.tfim = self.fim  #FIXME if ever dealing with different sized
        
        mims = self.get_model()
        resids = self.fim - mims

        rprof, sprof = radial_stdev_profile(resids, [int(self.center[0]), int(self.center[1])])
        y, x = n.mgrid[0:self.fim.shape[0],
                       0:self.fim.shape[1]]
        y -= int(self.center[0])
        x -= int(self.center[1])
        R = n.sqrt(x**2+y**2)
        stdim = n.zeros_like(self.fim)
        
        # Turn profiles into image of standard dev as a function of radius. 
        for r, s in zip(rprof, sprof):
            wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
            stdim[wr] = s
        self.stdim = stdim         

    def _get_intensity(self, nu, phi_sca):
        int_parms = self.parms[self.label + 'int_parms']
        omega = self.parms['omega']
        nup = omega+nu
        intensity = eval_pspl(nup, get_tck(int_parms))
        return intensity

    def print_parms(self):
        for p in self.parmlist:
            if self.parms[p] is None:
                _log.info("%s\t%s" % (p, str(self.parms[p])))
                continue
            if self.parm_lens[p] == 1:
                _log.info("%s\t%f" % (p, self.parms[p]))
            else:
                _log.info("%s\t%s" % (p, str(self.parms[p])))
    def parametric_model_ring(self, n_step = 100):
             
        """
        Parametric representation of an eccentric ring.

        Inputs:
          r           [AU]  radius from ring center
          r_out       [AU]  outer radius
          offset      [AU]  offset size in disk plane
          I           [rad] inclination
          omega       [rad] argument of pericenter (for offset direction)
          Omega       [rad] longitude of ascending node
          int_parms         parameters for intensity function, which returns flux/rad
        """

        r = self.parms['r']
        offset = self.parms['offset']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']
        int_parms = self.parms[self.label + 'int_parms']

        # get parameteric construction of ring position
        nu = n.linspace(0., 2.*n.pi, n_step, endpoint=False) # [rad]

        so, co = n.sin(omega), n.cos(omega)
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)
        
        #Euler angle transformation
        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        oz = offset * so*si # [AU]

        #Subtract offset angles
        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)
        
        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        # projected radius
        r_proj = n.sqrt(pN**2+pE**2) # [AU]

        # projected PA
        PA_proj = n.arctan2(pE, pN)

        # get parameteric construction of ring intensity
        intensities = self._get_intensity(nu, phi_sca) / r_star**2


        return nu, r_star, phi_sca, r_proj, PA_proj, intensities


    def get_model_ring_image(self,n_step=200,
                             drad0=2., # [AU]
                             fast=True ,
                             fast_samp=2, #lower is faster
                             show=False):   
        # FIXME fast = False does not work yet

        scale = self.scale
        self.psf_parms = self.parms[self.label + 'psf_parms']
        gamma_in = self.parms['gamma_in']
        gamma = self.parms['gamma']
        beta = self.parms['beta']
        offs = n.array((self.parms['offsy'], self.parms['offsx']))
        offset = self.parms['offset']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']

        psf_im = self.psf_im ####
        
        # get position/intensity profile
        nu, r, phi_sca, r_proj, PA_proj, intensity = self.parametric_model_ring(n_step=n_step)
        dnu = nu[1]-nu[0]
        assert nu[2]-nu[1] == dnu

        r_in, r_mid, r_out = self.parms['r_in'], self.parms['r'], self.parms['r_out']

        # FIXME
        if r_in is None:
            r_in = r_mid
        if r_out is None:
            r_out = r_mid

        n_step_rad = int((r_out-r_in)/drad0)
        drad = (r_out-r_in)/n_step_rad
        
        if n_step_rad > 1:
            r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid
        else:
            n_step_rad = 1
            r_factors = n.array((1.,))

        # broken power-law
        # NOTE  including number of steps so integral over intensity is conserved..
        i_factors = smooth_broken_power_law(r_factors, 1., 1., 1., -gamma_in, -gamma, beta)/r_factors**2/n_step_rad

        # ellipse center at a_in
        so, co = n.sin(omega), n.cos(omega)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)
        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        xecen = -oE
        yecen = -oN


        def get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp):
            intensity = (i_factors[n.newaxis,:]*intensity[:,n.newaxis]).flatten()

            if fast:
                # fast method does not due subpixel positioning
                im = n.zeros(2*n.array(shape)*fast_samp, dtype=n.float)

                for k, (yy, xx) in enumerate(zip(n.round(ypos*fast_samp).astype(n.int),
                                                 n.round(xpos*fast_samp).astype(n.int))):
                    if (yy < 0) or (yy >= 2*shape[0]*fast_samp-1): continue
                    if (xx < 0) or (xx >= 2*shape[1]*fast_samp-1): continue
                    im[yy,xx] += intensity[k]*dnu

                zim = fft2(im)

                # convolve with PSF
                zim *= zp

                # back to image domain
                im = ifft2(zim).real

                # rebin
                im = im.reshape(2*shape[0], fast_samp, 2*shape[1], fast_samp).sum(axis=(1,3))
            else:
                zim = n.zeros(2*n.array(shape), dtype=n.complex) #FIXME this doesn't work

                for k, (yy, xx) in enumerate(zip(ypos, xpos)):
                    zim += intensity[k]*dnu 
                    
                    zim *= n.exp(-2.*n.pi*1j * (yy*ky[:,n.newaxis]+xx*kx[n.newaxis,:]))

                    

                # convolve with PSF
                zim *= zp

                # back to image domain
                im = ifft2(zim).real


            # undo zero padding region
            im = im[0:shape[0], 0:shape[1]]
            #im /= n_step_rad
            return im


        shape = self.image_shape
        cen = self.center
        psf_sig = self.psf_parms
        sxpos = cen[1]+offs[1] # star x position
        sypos = cen[0]+offs[0] # star y position


        # positions along ellipses
        xpos = sxpos+(r_factors[n.newaxis,:]*(-r_proj*n.sin(PA_proj)+xecen)[:,n.newaxis]-xecen)/scale
        ypos = sypos+(r_factors[n.newaxis,:]*(r_proj*n.cos(PA_proj)-yecen)[:,n.newaxis]+yecen)/scale
        xpos = xpos.flatten()
        ypos = ypos.flatten()

        # FIXME not sure gaussian or inputim  
 
        from numpy.fft import fft2, ifft2, fftshift, fftfreq
        ky, kx = fftfreq(2*shape[0]*fast_samp), fftfreq(2*shape[1]*fast_samp)
        if self.psf_method == 'gaussian':
            if fast:
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*(psf_sig*fast_samp)**2
            else:
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*psf_sig**2
            zp = n.exp(-0.5*r2)
        elif self.psf_method == 'airy':
            if fast:
                ky, kx = fftfreq(2*shape[0]*fast_samp), fftfreq(2*shape[1]*fast_samp)
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*(psf_sig*fast_samp)**2
            else:
                ky, kx = fftfreq(2*shape[0]), fftfreq(2*shape[1])
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*psf_sig**2
            zp = n.clip(1.-n.sqrt(r2), 0., 1.)
        elif self.psf_method == 'inputim':
            if fast:
                transfmd = fftshift(fft2(fftshift(self.psf_im)))
                canvas = n.zeros((2 * shape[0] * fast_samp, 2 * shape[1] * fast_samp))
                psfshape = n.shape(self.psf_im)
                canvas[shape[0] * fast_samp - psfshape[0] / 2: shape[0] * fast_samp + psfshape[0]/2, 
                       shape[1] * fast_samp - psfshape[1] / 2: shape[1] * fast_samp + psfshape[1]/2] = transfmd
                zp = fftshift(canvas)
            else:
                zp = fftshift(fft2(self.psf_im))
        else:
            raise NotImplementedError


        im = get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp)

        return im


    def update_stdmap(self):
        "use residuals from current model to recalculate stdev map"

        # get full image model
        shapes = self.fim.shape
        #now only have one dsk
        xcens = int(self.center[0])
        ycens = int(self.center[1])
        
        mim  = self.get_model()
        resids = self.fim - mim
        from image import radial_stdev_profile
        rprof, sprof = radial_stdev_profile(resids, [xcens, ycens])
        # compute thumbnail stdmap
        stdmap = n.zeros(self.tfim.shape, dtype=n.float)
        for r, s in zip(rprof, sprof):
            wr = n.nonzero((self.R >= r-0.5) & (self.R < r+0.5))
            stdmap[wr] = s
        return stdmap


    def show_model(self, vmin = -120, vmax = 200):
        # Scale factors GPI FIXME put these somewhere else
        pixelscale = .014            # arcseconds / pix
        dist = 73.                   # parsecs,cn
        pix_au = dist * pixelscale   # AU / pix
        ylims = 70. * pix_au
        xlims = 90. * .7 * pix_au
        cenx = 140
        ceny = 140
        extent = n.array([-ceny,ceny,-cenx,cenx]) * pix_au
        calibk = 5.7e-8             # Jy/ADU/coadd
        calibk_as = calibk * 1000. / pixelscale**2.  # Jy/ADU/coadd/as^2
        calibj = 1.03e-7 * .73
        calibj_as = calibj * 1000. / pixelscale**2.
        
        # colors 
        colormap_idl_bluewhite = mpl.colors.LinearSegmentedColormap('IDL_BlueWhite',
                                                                    {'red':   ((0.0, 0.0, 0.0),
                                                                               (0.756, 0.0, 0.0),
                                                                               (1.0, 1.0, 1.0)),
                                                                     
                                                                     
                                                                     'green': ((0.0, 0.0, 0.0),
                                                                               (0.38, 0.0, 0.0),
                                                                               (1.00, 1.0, 1.0)),
                                                                     
                                                                     'blue':  ((0.00, 0.0, 0.0),
                                                                               (0.737, 1.0, 1.0),
                                                                               (1.00, 1.0, 1.0))
                                                                 })
        colormap_idl_bluewhite.set_bad('black')
        
        #plot parameters
        label_size = 10.5
        mpl.rcParams['xtick.labelsize'] = label_size
        mpl.rcParams['ytick.labelsize'] = label_size
        fig,(ax1, ax2, ax3) = plt.subplots(1, 3)
        fig.set_size_inches(9,4.5)
        plot_kwargs =  {'extent': extent,
                        'cmap': colormap_idl_bluewhite}

        mim = self.get_model()

        # FIXME real title
        fig.suptitle('K-band Polarized Intensity',fontsize = 15)
        im = ax1.imshow(self.tfim,  vmin = vmin, vmax = vmax, **plot_kwargs)
        ax1.set_xlim([-xlims, xlims])
        ax1.set_ylim([-90, 90])
        ax1.set_title('Data')
        ax2.imshow(mim,vmin = vmin, vmax = vmax, **plot_kwargs)
        ax2.set_ylim([-90, 90])
        ax2.set_xlim([-xlims, xlims])
        ax2.set_title('Model')
        ax3.imshow(self.tfim - mim,vmin = vmin, vmax = vmax, **plot_kwargs) #make this divided by stdim and show color bar
        ax3.set_ylim([-90, 90])
        ax3.set_xlim([-xlims, xlims])
        ax3.set_title('Residuals')
        fig.subplots_adjust(wspace = .2)
        
        fig.subplots_adjust(bottom = .23)

        cbar_ax = fig.add_axes([0.15,0.12,0.7,0.03])
        cb = fig.colorbar(im, cax = cbar_ax, orientation = 'horizontal', label = 'mJy / arcsec$^2$')
        plt.show()

        
    def get_model(self):
        mim = self.get_model_ring_image()
        if self.polint == 'int' or self.polint == 'tot':
            self.fm_object.update_disk(mim)
            mim = n.mean(self.fm_object.fm_parallelized()[0], axis = 0)
        return mim
    
    def get_residuals(self, ischanged = True):
        import copy
        import matplotlib.pyplot as plot
        if ischanged is True:
            fmims = self.get_model()
            r = ((ma.array(self.tfim)-ma.array(fmims))/ma.array(self.stdim)).compressed()
            self.current_residuals = copy.deepcopy(r)
            return r
        elif self.current_residuals is None:
            fmims = self.get_model()
            r = ((ma.array(self.tfim)-ma.array(fmims))/ma.array(self.stdim)).compressed()
            self.current_residuals = copy.deepcopy(r)
            return r
        else:
            return copy.deepcopy(self.current_residuals)
            
###############################################################################
###############################################################################
###############################################################################
###############################################################################
###############################################################################



class ModelComparator(object):
    def __init__(self, config_filename, leastsq_save = None):
        # fast mode for model computation
        fast = True
        fast_samp = 2
 
        # Set mCMC parameters
        n_step = 200 # steps along ring for intensity
        drad0 = 2. # [AU] stepsize along radial direction


        self.config_filename = config_filename
        self.restore_parms(config_filename = config_filename, leastsq_parms = leastsq_save)

        self.first = True

    def get_gaussian_priors(self):
        "1-d gaussian prior residual array"
        parmdict, parmlist = self.get_freemodel_parminfo()
        priors = []
        for p, v in parmdict.iteritems():
            ## if p == 'r_in':
            ##     priors.append((v-73.)/10.)
            ## if p in ('a','r'):
            ##     priors.append((v-79.2)/10.)
            ## elif p in ('a_out','r_out'):
            ##     priors.append((v-83.)/5.)
            ## elif p=='I':
            if p=='I':
                priors.append((v*180./n.pi-76.7)/5.)
            elif p=='gamma_in':
                priors.append(v/15.)
            elif p=='gamma':
                priors.append(v/5.)
            elif p in ('r'):
                priors.append((v-77.4)/10.)
            elif p in ('offsy','offsx'):
                priors.append(v/self.pix_uncert)
            elif p == 'psf_parms':
                vv = (v-5.)/1.
                if self.n_images == 1:
                    priors.append(vv)
                else:
                    priors.extend(vv.tolist())
        return n.array(priors)

    def update_all_int_parms(self, len_array):
        for length, diskobj in zip(len_array, self.diskobjs):
            label = diskobj.label
            diskobj.update_int_parms(length)
            self.parms[label + 'int_parms'] = diskobj.parms[label + 'int_parms']
            self.parm_lens[label + 'int_parms'] = length


    @staticmethod
    def freemodel_to_fit_parminfo(freemodel_parmdict, freemodel_parmlist):
        "transform model parameters to fit parameters, to go to leastsq"
        fit_parmlist = []
        fit_parmdict = {}
        for k in freemodel_parmlist:
            v = freemodel_parmdict[k]
            if 'psf_parms' in k or 'int_parms' in k or 'beta' in k: 
                fit_parmdict[k] = n.log(v)
                fit_parmlist.append(k)
            elif k == 'e':
                assert 'omega' in freemodel_parmlist
                e, omega = v, freemodel_parmdict['omega']
                esino, ecoso = e*n.sin(omega), e*n.cos(omega)
                fit_parmdict['esino'] = esino
                fit_parmdict['ecoso'] = ecoso
                fit_parmlist.extend(['esino', 'ecoso'])
            elif k == 'offset':
                assert 'omega' in freemodel_parmlist
                offset, omega = v, freemodel_parmdict['omega']
                osino, ocoso = offset*n.sin(omega), offset*n.cos(omega)
                fit_parmdict['osino'] = osino
                fit_parmdict['ocoso'] = ocoso
                fit_parmlist.extend(['osino', 'ocoso'])
            elif k == 'omega':
                continue
            else:
                fit_parmdict[k] = v
                fit_parmlist.append(k)
        return fit_parmdict, fit_parmlist

    def save_parms(self, fn):
        with open(fn, 'w') as f:
            pickle.dump(self.parms, f, 2)
            pickle.dump(self.is_fixed, f, 2)
            pickle.dump(self.parm_lens, f, 2)            

    def restore_parms(self, config_filename = None, leastsq_parms = None, mc_chain = None):
        import configparser

        if config_filename == None:
            config_filename = self.config_filename


        if leastsq_parms is not None:
            with open(leastsq_parms, 'rb') as f:
                parms = pickle.load(f)
                is_fixed = pickle.load(f)
                parm_lens = pickle.load(f)
                parm_names = pickle.load(f)


        with open(config_filename) as fp:
            config = configparser.ConfigParser()
            config.readfp(fp)

            # GPI specific stuff
            dist = 72.8 # [pc]   # put in config file
            pix_scale = .01414 # [arcsec/pix] Konopacky et al. 2014
            pix_uncert = 0.3 # [pix]  uncertainty in pixel positioning of star
            scale = dist*pix_scale # [AU/pix]
            # MCMC parameters
            self.n_walker = int(config.get('Control Parameters', 'NumWalkers'))
            self.n_sample = int(config.get('Control Parameters', 'NumSample'))
            self.n_burn = int(config.get('Control Parameters', 'NumBurn'))

            # whether to fix the stellar position to the satspot location
            fix_star = config.getboolean('General', 'FixStarPosition')
        
            def str_to_bool(s):
                '''
                Turns a string s of 'True' or 'False' into a boolean 
                True/False
                '''
                if s == 'True':
                    return True
                elif s == 'False':
                    return False
                else:
                    raise ValueError # evil ValueError that doesn't tell you what the wrong value was
            
            if leastsq_parms is not None:
                self.parms = parms
                self.is_fixed = is_fixed
                self.parm_lens = parm_lens
                self.parm_names = parm_names
            else:
                # Generate dictionary of general parameters
                parms = {'r':float(config.get('General', 'InitR')),
                         'r_in':float(config.get('General', 'InitRin')),
                         'r_out':float(config.get('General', 'InitRout')),
                         'gamma_in':float(config.get('General', 'InitGammain')),
                         'gamma':float(config.get('General', 'InitGamma')),
                         'beta':float(config.get('General', 'InitBeta')),
                         'offset':float(config.get('General', 'InitOffset')),
                         'offsx':float(config.get('General', 'InitOffsetX')),
                         'offsy':float(config.get('General', 'InitOffsetY')),
                         'I':float(config.get('General', 'InitI')) * n.pi / 180.,
                         'Omega':float(config.get('General', 'InitOmega')) * n.pi / 180.,
                         'omega':float(config.get('General', 'Initomega2')) * n.pi / 180.}


                # Generate dictionary of fixed or not fixed
                is_fixed = {'r':str_to_bool(config.get('General', 'IsFixedR')),
                            'r_in':str_to_bool(config.get('General', 'IsFixedRin')),
                            'r_out':str_to_bool(config.get('General', 'IsFixedRout')),
                            'gamma_in':str_to_bool(config.get('General', 'IsFixedGammain')),
                            'gamma':str_to_bool(config.get('General', 'IsFixedGamma')),
                            'beta':str_to_bool(config.get('General', 'IsFixedBeta')),
                            'offset':str_to_bool(config.get('General', 'IsFixedOffset')),
                            'offsx':str_to_bool(config.get('General', 'IsFixedOffsetX')),
                            'offsy':str_to_bool(config.get('General', 'IsFixedOffsetY')),
                            'I':str_to_bool(config.get('General', 'IsFixedI')),
                            'Omega':str_to_bool(config.get('General', 'IsFixedOmega')),
                            'omega':str_to_bool(config.get('General', 'IsFixedomega2'))}
                
                
                
                parmlist = parms.keys()
                self.parm_lens = dict([(k,1) for k in parmlist])
            # Add disk-specific parameters
            self.diskobjs = []
            self.labels = config.sections()[2:] # Disk labels
            for label in self.labels:
                if leastsq_parms is None:
                    parms[label + 'int_parms'] = str_to_list(config.get(label, 'InitIntParms'))
                    is_fixed[label + 'int_parms'] = str_to_bool(config.get(label, 'IsFixedIntParms'))
                    self.parm_lens[label + 'int_parms'] = len(parms[label + 'int_parms'])
                    # FIXME test me
                    if config.has_option(label, 'InitPSFParms') is True:
                        parms[label + 'psf_parms'] = str_to_list(config.get(label, 'InitPSFParms'))
                        assert config.has_option(label, 'IsFixedPSFParms') is True
                        is_fixed[label + 'psf_parms'] = str_to_bool(config.get(label, 'IsFixedPSFParms'))
                    else:
                        parms[label + 'psf_parms'] = None
                        is_fixed[label + 'psf_parms'] = True
            for label in self.labels:
                polint = config.get(label, 'PolInt')
                
                # Make disks
                fnames = config.get(label, 'ImName')            
                
                if config.has_option(label, 'PSFImageFile') is True:
                    psf_imname = config.get(label, 'PSFImageFile')
                else: 
                    psf_imname = None
                    
                if str_to_bool(config.get(label, 'KLIPFM')) == True:
                    assert config.has_option(label, 'KLIPFMBasisPattern') is True
                    assert config.has_option(label, 'KLIPFMDataImages') is True
                    assert config.has_option(label, 'NumBasis') is True
                    basisfnames = config.get(label, 'KLIPFMBasisPattern')
                    data_image_filenames = config.get(label, 'KLIPFMDataImages')
                    numbasis = float(config.get(label, 'NumBasis'))
                else:
                    basisfnames = None
                    data_image_filenames = None
                    numbasis = None
                
                diskobj = diskObject(fnames, copy.deepcopy(parms), scale, label, is_fixed, psf_imname = psf_imname, polint = polint, basisfnames = basisfnames, numbasis = numbasis, data_image_filenames = data_image_filenames)
                
                self.diskobjs.append(diskobj)

            self.parms = parms
            self.is_fixed = is_fixed
            if leastsq_parms is None:
                self.parm_names = self.parms.keys()
            self.parm_lens = parm_lens

                

    def print_parms(self):
        for p in self.parm_names:
            if self.parms[p] is None:
                print ("%s\t%s" % (p, str(self.parms[p])))
                _log.info("%s\t%s" % (p, str(self.parms[p])))
                continue
            if self.parm_lens[p] == 1:
                print ("%s\t%s" % (p, str(self.parms[p])))
                _log.info("%s\t%f" % (p, self.parms[p]))
            else:
                print ("%s\t%s" % (p, str(self.parms[p])))
                _log.info("%s\t%s" % (p, str(self.parms[p])))
        print ''
        print ''
        print ''

    def get_fit_parms(self, get_parmlist=False): #FIXME 
        "take free parameters and construct 1-d array"

        # get current model parameter dictionary
        freemodel_parmdict, freemodel_parmlist = self.get_freemodel_parminfo() #mifxme

        # transform model parameter dictionary to fit parameter dictionary
        fit_parmdict, fit_parmlist = self.freemodel_to_fit_parminfo(freemodel_parmdict, freemodel_parmlist)

        # construct 1-d array from fit dictionary
        parms = []
        parm_lens = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if n.isscalar(v): v = (v,)
            parms.append(v)
            parm_lens[k] = len(v)
        if get_parmlist:
            return n.concatenate(parms), fit_parmlist, parm_lens
        else:
            return n.concatenate(parms)


    def get_all_residuals(self, p):
        # Update parameters
        tf_arr = self.set_fit_parms(p)
        if self.first is True:
            tf_arr = [True for tf in tf_arr]
        print tf_arr
        residuals = n.array([])
        for ringmodel, tf in zip(self.diskobjs, tf_arr):
            r = ringmodel.get_residuals(ischanged = tf)
            residuals = n.concatenate((r, residuals))
        self.first = False
        return residuals

    def do_leastsq(self):
        # Starting parameters
        sp = self.get_fit_parms()
        
        def fit_fn(p):
            resid = self.get_all_residuals(p)
            priors = self.get_gaussian_priors()
            self.print_parms()
            return n.concatenate((resid, priors))
        from scipy.optimize import leastsq


        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              #args=extra_args,                                       
                                              #epsfcn=1e-7,                                           
                                              #factor=3.,                                             
                                              epsfcn=1e-3,
                                              full_output=True,
                                              )
        _log.info("%d: %s" % (ier, mesg))
        
        self.set_fit_parms(p_opt)

        self.leastsq_vals = (p_opt, cov, info, mesg, ier)


    def get_freemodel_parminfo(self):
        parmdict = {}
        parmlist = []
        
        for k in self.parm_names:
            if self.is_fixed[k]: continue
            parmdict[k] = self.parms[k]
            parmlist.append(k)

        return parmdict, parmlist

    def update_all_parms(self, parm_dict):
        tf_arr = []
        for ringmodel in self.diskobjs:
            tf = ringmodel.update_parms(parm_dict)
            tf_arr.append(tf)
        self.parms.update(parm_dict)
        return tf_arr

    def set_fit_parms(self, fit_parms): 
        "use free parameters to update internal values"
        current_fit_parms, fit_parmlist, fit_parm_lens = self.get_fit_parms(get_parmlist=True)

        # unpack fit parameter array into dictionary
        i = 0
        fit_parmdict = {}
        for p in fit_parmlist:
            pl = fit_parm_lens[p]
            if pl == 1:
                fit_parmdict[p] = fit_parms[i]
            else:
                fit_parmdict[p] = fit_parms[i:i+pl]
            i += pl
        assert i == len(fit_parms)

        # transform fit parameter dictionary into model parameter dictionary
        freemodel_parmdict, freemodel_parmlist = self.fit_to_freemodel_parminfo(fit_parmdict, fit_parmlist)
        
        # FIXME need to send model parameters to every ring
        # udpate model parameters
        ischanged_arr = self.update_all_parms(freemodel_parmdict)

        #        ischanged_arr = []
        #        for ringmodel in self.diskobjs:
        #            ischanged = ringmodel.update_parms(freemodel_parmdict)
        #            ischanged_arr.append(ischanged)
        return ischanged_arr



    @staticmethod
    def fit_to_freemodel_parminfo(fit_parmdict, fit_parmlist):
        "transform fit parameters to model parameters"
        freemodel_parmlist = []
        freemodel_parmdict = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if 'psf_parms' in k or 'int_parms' in k or 'beta' in k:  # FIXME check
                freemodel_parmdict[k] = n.exp(v)
                freemodel_parmlist.append(k)
            elif k == 'esino':
                assert 'ecoso' in fit_parmlist
                esino, ecoso = v, fit_parmdict['ecoso']
                e = n.sqrt(esino**2+ecoso**2)
                omega = n.arctan2(esino, ecoso)
                freemodel_parmdict['e'] = e
                freemodel_parmdict['omega'] = omega
                freemodel_parmlist.extend(['e', 'omega'])
            elif k == 'ecoso':
                continue
            elif k == 'osino':
                assert 'ocoso' in fit_parmlist
                osino, ocoso = v, fit_parmdict['ocoso']
                offset = n.sqrt(osino**2+ocoso**2)
                omega = n.arctan2(osino, ocoso)
                freemodel_parmdict['offset'] = offset
                freemodel_parmdict['omega'] = omega
                freemodel_parmlist.extend(['offset', 'omega'])
            elif k == 'ocoso':
                continue
            else:
                freemodel_parmdict[k] = v
                freemodel_parmlist.append(k)
        return freemodel_parmdict, freemodel_parmlist



    def run_mcmc(self, fname = None):
        p_opt = self.get_fit_parms()
        print p_opt
        n_dim = len(p_opt)
        rs = n.random.RandomState(seed=34523)
        wz = n.nonzero(p_opt==0.)[0]
        pos = []
        for i in range(self.n_walker):
            p = p_opt*(1. + 3e-2*rs.randn(n_dim))
            p[wz] = 1e-1*rs.randn(len(wz))
            pos.append(p)
        if has_mpi:
            kw = {'pool':pool}
        else:
            import multiprocessing as mp
            n_process = mp.cpu_count()     
            kw = {'threads':n_process}
        import emcee
        import copy
        
        
        print pos

        sampler = emcee.EnsembleSampler(self.n_walker, n_dim, log_like,
                                        args=(self,), 
                                        **kw)

        sampler.run_mcmc(pos, self.n_sample,
                         rstate0=rs.get_state())

        self.chain = sampler.chain
        self.lnprobability = sampler.lnprobability
        # save best parms
        if fname is not None:
            f = open(fname, 'wb')
            pickle.dump(self.chain, f)
            pickle.dump(self.lnprobability)
            pickle.dump(self.parm_names)
            pickle.dump(self.parms)
            pickle.dump(self.is_fixed)
            pickle.dump(self.parm_lens)
            
        
def log_like(p, mc):

    "top-level function for MCMC"
    
    resid = mc.get_all_residuals(p)
    chi2 = n.sum(resid**2)
    priors = mc.get_gaussian_priors()
    
    chi2p = n.sum(priors**2)


    # get current model parameter dictionary
    parms = mc.parms
    print parms


    from itertools import chain
    int_parms_list = [parms[k] for k in parms.keys() if 'int_parms' in k]
    int_parms = list(chain.from_iterable(int_parms_list))
        
    omega = parms['omega']
    Omega = parms['Omega']
    I = parms['I']
    #a = parms['a']
    #a_out = parms['a_out']

    # bounds
    if 'e' in parms:
        if (parms['e']>=1.): return -n.inf


#FIXMEFIXME
#    if mc.ringmodel.psf_method == 'gaussian':
#        if n.any(psf_parms <= 10.): return -n.inf
#        if n.any(psf_parms > 21.): return -n.inf
#    elif mc.ringmodel.psf_method == 'airy':
#        if n.any(psf_parms <= 1.): return -n.inf
#        if n.any(psf_parms > 15.): return -n.inf

    if n.any(n.isnan(int_parms)): return -n.inf
    # FIXME 

    if ~n.all(n.isfinite(int_parms)): return -n.inf

    if (omega < -n.pi) or (omega > n.pi): return -n.inf
    if (Omega < 0) or (Omega > 2.*n.pi): return -n.inf
    ## if (Omega < 200.*n.pi/180.) or (Omega > 215.*n.pi/180.): return -n.inf

    if (I < 0.) or (I > n.pi): return -n.inf

    #if (a < 70.) or (a > 85.): return -n.inf
    if ('a' in parms) and ('a_out' in parms):
        if parms['a_out'] < parms['a']: return -n.inf

    if 'r' in parms:
        if parms['r'] < 0.: return -n.inf
    if 'r_in' in parms:
        if parms['r_in'] < 0.: return -n.inf
    if 'r_out' in parms:
        if parms['r_out'] < 0.: return -n.inf

    ## if ('r' in parms) and ('r_out' in parms):
    ##     if parms['r_out'] < parms['r']: return -n.inf
    ## if ('r' in parms) and ('r_in' in parms):
    ##     if parms['r_in'] > parms['r']: return -n.inf
    if ('r_in' in parms) and ('r_out' in parms):
        if parms['r_in'] > parms['r_out']: return -n.inf

    if 'r_in' in parms:
        if parms['r_in'] < 60.: return -n.inf
    if 'r_out' in parms:
        if parms['r_out'] > 100.: return -n.inf

    if 'a_out' in parms:
        if parms['a_out'] > 100.: return -n.inf

    return -(chi2+chi2p)/2.



def str_to_list(string):
    l = n.array([float(b) for b in string.split(',')])
    return l


def testing(conf):
    import copy
    x = ModelComparator(conf, leastsq_save = 'leastsq_kboth_run1.p')
    x.update_all_int_parms([10,10])
    x.run_mcmc()
    return

    parms = copy.deepcopy(x.parms)
    x.update_all_parms(parms)
    
    totintdisk = x.diskobjs[1]
    label1 = totintdisk.label


    parms[label1 + 'int_parms'] = copy.deepcopy(parms[label1 + 'int_parms'] ) * 5
    
    x.update_all_parms(parms)
    parms['omega'] = copy.deepcopy(parms['omega'] ) * 5
    x.update_all_parms(parms)
    return

    print 'True'
    parms['omega'] *= 3
    print totintdisk.update_parms(parms)
    print totintdisk.parms

    


    return
    polintdisk = x.diskobjs[0]
    label0 = polintdisk.label

    totintdisk = x.diskobjs[1]
    label1 = totintdisk.label
    parms = x.parms
    import copy
    print 'False'
    print totintdisk.update_parms(parms)
    print totintdisk.parms

    print 'True'
    parms2 = copy.deepcopy(parms)
    parms2[label1 + 'int_parms'] *=3

    print totintdisk.update_parms(parms2)
    print totintdisk.parms
    
    print 'True'
    parms[label0 + 'int_parms'] *= 3
    print totintdisk.update_parms(parms)
    print totintdisk.parms

    print 'True'
    parms['omega'] *= 3
    print totintdisk.update_parms(parms)
    print totintdisk.parms


#    totintdisk.show_model()
    return

    x.do_leastsq()
    x.run_mcmc()
    x.save_parms('mcmc_parms.p')
    f = open('mcmc_chain.p', 'wb')
    pickle.dump(x.chain, f)
    pickle.dump(x.lnprobability, f)
    return



def run_from_conf(conf):
    def str_to_bool(s):
        '''
        Turns a string s of 'True' or 'False' into a boolean 
        True/False
        '''
        if s == 'True':
            return True
        elif s == 'False':
            return False
        else:
            raise ValueError # evil ValueError that doesn't tell you what the wrong value was



    config = configparser.ConfigParser()
    config.readfp(conf)
    doleastsq = str_to_bool(config.get('Control Parameters', 'DoLeastSq'))
    domcmc = str_to_bool(config.get('Control Parameters', 'DoMCMC'))
    if doleastsq is True:
        mc = ModelComparator(conf)
        fname = config.get('Control Parameters', 'LeastSqFname')
        mc.do_leastsq()
        mc.save_parms(fname)
    if domcmc is True:
        # Needs to load from a leastsq
        fname = config.get('Control Parameters', 'LoadedLeastSqFname')
        mc = ModelComparator(conf, leastsq_save = fname)
        mcmc_fname = config.get('Control Parameters', 'MCMCFname')
        mc.run_mcmc(fname = mcmc_fname)
        
        #FIXME
        
def run_leastsq(conf):
    mc = ModelComparator(conf)
    mc.do_leastsq()
    mc.save_parms('leastsq_kboth.p')

def load_from_save(conf, fname):
    mc = ModelComparator(conf, leastsq_save = fname)
    kpol_do = mc.diskobjs[0]
    kpol_do.show_model()


if __name__=='__main__':
    #logging.basicConfig(level=logging.INFO,
    logging.basicConfig(level=logging.DEBUG,
                        format='%(name)-12s: %(levelname)-8s %(message)s',
                        )

try:
    from mpi4py import MPI
    from emcee.utils import MPIPool
    size = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()
    name = MPI.Get_processor_name()

    _log = logging.getLogger("hr4796a.%s.%d" % (name, rank))

    _log.debug("I am process %d of %d on %s." % (rank, size, name))

    if size == 1:
        _log.info("Use mpirun -np <N> python <filename.py> with N>=2 to run in MPI mode.")
        has_mpi = False
        pool = None
    else:

        has_mpi = True

        #pool = MPIPool()
        pool = MPIPool(loadbalance=True)
        pool = MPIPool(debug=True) # TEMP
        if not pool.is_master():
            _log.debug('I am not the master... waiting.')
            pool.wait()
            sys.exit(0)
        else:
            _log.debug('I am the master... continuing.')

except ImportError:
    has_mpi = False
    pool = None



if __name__=='__main__':
    conf = sys.argv[1]
    assert len(conf) != 0, "Requires config file"

#    run_leastsq(conf)

    testing(conf)

#    fname = 'leastsq_ktot.p'
#    load_from_save(conf,fname)

 

    # cleanup
    if pool is not None:
        pool.close()

