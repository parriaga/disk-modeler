#KLIP Forward Modelling
import pyklip.klip as klip
import pyklip.instruments.GPI as gpi
import numpy 
import scipy.linalg as la
from scipy.stats import norm
import scipy.ndimage as ndimage
import glob
import matplotlib.pyplot as plot
import copy
import pyfits
from time import time
import ctypes
import itertools
import multiprocessing as mp
import pickle
from pyklip.parallelized import _arraytonumpy
import pyklip.parallelized as par
from sys import stdout


#KLIP Forward Modelling

def klip_math(sci, refs, models, numbasis, return_basis=False): #Zack
    """
    linear algebra of KLIP with linear perturbation 
    disks and point sources
    
    Args:
        sci: array of length p containing the science data
        refs: N x p array of the N reference PSFs that 
                  characterizes the extended source with p pixels
        numbasis: number of KLIP basis vectors to use (can be an int or an array of ints of length b)  
        return_basis: If true, return KL basis vectors (used when onesegment==True)

    Returns:
        sub_img_rows_selected: array of shape (p,b) that is the PSF subtracted data for each of the b KLIP basis
                               cutoffs. If numbasis was an int, then sub_img_row_selected is just an array of length p

    """
    sci_mean_sub = sci - numpy.nanmean(sci)
    sci_nanpix = numpy.where(numpy.isnan(sci_mean_sub))
    sci_mean_sub[sci_nanpix] = 0


    refs_mean_sub = refs - numpy.nanmean(refs, axis=1)[:, None]
    refs_mean_sub[numpy.where(numpy.isnan(refs_mean_sub))] = 0
    
    
    models_mean_sub = models - numpy.nanmean(models, axis=1)[:,None]
    models_mean_sub[numpy.where(numpy.isnan(models_mean_sub))] = 0

    #if covar_psfs is None, disregarded scaling term:
#    covar_psfs = numpy.dot(refs_mean_sub,refs_mean_sub.T)  

    covar_psfs = numpy.cov(refs_mean_sub)
    covar_psfs *= (numpy.size(sci)-1)

    tot_basis = covar_psfs.shape[0]
    numbasis = numpy.clip(numbasis - 1, 0, tot_basis-1)
    max_basis = numpy.max(numbasis) + 1
        
    evals, evecs = la.eigh(covar_psfs, eigvals = (tot_basis-max_basis, tot_basis-1))
    evals = numpy.copy(evals[::-1])
    evecs = numpy.copy(evecs[:,::-1])
    evals_toreturn = copy.deepcopy(evals)
    evecs_toreturn = copy.deepcopy(evecs) #evals and evecs okay


    KL_basis = numpy.dot(refs_mean_sub.T,evecs)
    KL_basis = KL_basis * (1. / numpy.sqrt(evals))[None,:]

    sci_mean_sub_rows = numpy.tile(sci_mean_sub, (max_basis,1))
    sci_rows_selected = numpy.tile(sci_mean_sub, (numpy.size(numbasis),1))
    
    sci_nanpix = numpy.where(numpy.isnan(sci_mean_sub_rows))
    sci_mean_sub_rows[sci_nanpix] = 0
    sci_nanpix = numpy.where(numpy.isnan(sci_rows_selected))
    sci_rows_selected[sci_nanpix] = 0


    CAdeltaI = numpy.dot(refs_mean_sub,models_mean_sub.T) + numpy.dot(models_mean_sub,refs_mean_sub.T)

    Nrefs = evals.shape[0]
    KL_perturb = numpy.zeros(KL_basis.shape)
    Pert1 = numpy.zeros(KL_basis.shape)
    Pert2 = numpy.zeros(KL_basis.shape)
    CoreffsKL = numpy.zeros((Nrefs,Nrefs))
    Mult = numpy.zeros((Nrefs,Nrefs))
    cross = numpy.zeros((Nrefs,Nrefs))
    for i in range(Nrefs):
        for j in range(Nrefs):
            cross[i,j] = numpy.transpose(evecs[:,j]).dot(CAdeltaI).dot(evecs[:,i])
        
    for i in range(Nrefs):
        for j in range(Nrefs):
            if j == i:
                Mult[i,j] = -1./(2.*evals[j])
            else:
                Mult[i,j] = numpy.sqrt(evals[j]/evals[i])/(evals[i]-evals[j])
        CoreffsKL[i,:] = cross[i]*Mult[i]

     
    for j in range(Nrefs):
        Pert1[:,j] = numpy.dot(CoreffsKL[j],KL_basis.T).T
        Pert2[:,j] = (1./numpy.sqrt(evals[j])*numpy.transpose(evecs[:,j]).dot(models_mean_sub))
        KL_perturb[:,j] = (Pert1[:,j]+Pert2[:,j])
    
    KL_pert = KL_perturb + KL_basis
    
    inner_products = numpy.dot(sci_mean_sub_rows, KL_pert)
    lower_tri = numpy.tril(numpy.ones([max_basis,max_basis]))
    inner_products = inner_products * lower_tri
    klip = numpy.dot(inner_products[numbasis,:], KL_pert.T)
    
    sub_img_rows_selected = sci_rows_selected - klip
    sub_img_rows_selected[sci_nanpix] = numpy.nan
    
    if return_basis is True:
#        return sub_img_rows_selected.transpose(), KL_basis.transpose()
        return sub_img_rows_selected.transpose(), KL_basis, evals_toreturn, evecs_toreturn
    else:
        return sub_img_rows_selected.transpose()
		

import paperfigs
def makebasisvecs(filt = 'test'):
    basispattern = 'basis_images/basis'
    if filt == 'J':
        fnames = sorted(glob.glob('april_j/*podc*'))
    if filt == 'K1' or filt == 'K':
        fnames = sorted(glob.glob('april_k/*podc*'))

    if filt == 'test':
        fnames = sorted(glob.glob('/home/parriaga/fmtest/hdfiles/*'))

    a = gpi.GPIData(fnames)
    imgs = a.input
    centers = a.centers #[[140,140] for i in range(len(a.centers))]

    parangs = a.PAs
    IWA = 11. # ad hoc

#    model = pyfits.getdata('model0.fits')  # Doesn't matter
#    model = paperfigs.test2()[0][0]

    model = pyfits.getdata('tot_k_model.fits')
    models = numpy.zeros(numpy.shape(imgs))
    for i, pa in enumerate(parangs):
        canvas = copy.deepcopy(model)
        canvas = klip.rotate(canvas, pa, [140, 140],  flipx = True)
        canvas[numpy.where(numpy.isnan(canvas))] = 0.
        models[i] = canvas

    a,b = klip_adi(imgs, models, centers, parangs, IWA, annuli = 1, subsections = 1, minrot = 15,numbasis = [1,2,3,4,5,6,7,8,9,10],savebasis = False, basispattern = basispattern)

    print numpy.shape(a)
    print numpy.shape(b)
    a = a[0]
    b = b[0]
    for i in range(len(a)):
#        plot.imshow(a[i])
#        plot.show()
        a[i] = klip.rotate(a[i],parangs[i] , [140,140], flipx = True)
        b[i] = klip.rotate(b[i],parangs[i] , [140,140], flipx = True)

    im = pyfits.getdata('klipfmcheck-KLmodes-all.fits')[0]
    plot.imshow(numpy.nanmean(a, axis = 0) - im,vmin = -40, vmax = 40)
    plot.colorbar()
    plot.show()

    plot.imshow(numpy.nanmean(b, axis = 0))
    plot.colorbar()
    plot.show()
    plot.subplot(221)
    plot.title('KLIP-fm model')
    plot.imshow(numpy.nanmean(b, axis = 0),vmin = -40, vmax = 200)
    plot.subplot(222)
    plot.title('Model')
    plot.imshow(model,vmin = -40, vmax = 200)
    plot.subplot(223)
    plot.title('Science im')
    plot.imshow(im,vmin = -40, vmax = 200)
    plot.subplot(224)
    plot.title('Resids')
    plot.imshow(im-model,vmin = -40, vmax = 40)
    plot.show()
    



def testing(filt = 'K'):
    fnames = sorted(glob.glob('/home/parriaga/fmtest/hdfiles/*'))
    a = gpi.GPIData(fnames)
    imgs = a.input
    centers = a.centers #[[140,140] for i in range(len(a.centers))]

    parangs = a.PAs
    IWA = 11. # ad hoc

#    model = pyfits.getdata('model0.fits')  # Doesn't matter
#    model = paperfigs.test2()[0][0]

    model = pyfits.getdata('tot_k_model.fits')
    models = numpy.zeros(numpy.shape(imgs))
    for i, pa in enumerate(parangs):
        canvas = copy.deepcopy(model)
        canvas = klip.rotate(canvas, pa, [140, 140],  flipx = True)
        canvas[numpy.where(numpy.isnan(canvas))] = 0.
        models[i] = canvas

    a,b = klip_adi(imgs, models, centers, parangs, IWA, annuli = 1, subsections = 1, minrot = 15,numbasis = [1,2],savebasis = False)

    print numpy.shape(a)
    print numpy.shape(b)
    b = b[0]
    a = a[0]
    for i in range(len(a)):
#        plot.imshow(a[i])
#        plot.show()
        a[i] = klip.rotate(a[i],parangs[i] , [140,140], flipx = True)
        b[i] = klip.rotate(b[i],parangs[i] , [140,140], flipx = True)

    im = pyfits.getdata('/home/parriaga/fmtest/injected_klip-KLmodes-all.fits')[0]
    plot.subplot(211)
    plot.imshow(im)
    plot.subplot(212)
    plot.imshow(numpy.nanmean(b, axis = 0))
    plot.show()
    






def klip_adi(imgs, models, centers, parangs, IWA, annuli=5, subsections=4, movement=3, numbasis=None, aligned_center=None, minrot=0, savebasis = True, basispattern = 'basis_fm_K/basis'): #Zack
    """
    KLIP PSF Subtraction using angular differential imaging, perturbed

    Args:
        imgs: array of 2D images for ADI. Shape of array (N,y,x)
        models: array of 2D models for ADI corresponding to imgs. Shape of array (N,y,x)
        centers: N by 2 array of (x,y) coordinates of image centers
        parangs: N legnth array detailing parallactic angle of each image
        IWA: inner working angle (in pixels)
        anuuli: number of annuli to use for KLIP
        subsections: number of sections to break each annuli into
        movement: minimum amount of movement (in pixels) of an astrophysical source
                  to consider using that image for a refernece PSF
        numbasis: number of KL basis vectors to use (can be a scalar or list like). Length of b
        aligned_center: array of 2 elements [x,y] that all the KLIP subtracted images will be centered on for image
                        registration
        minrot: minimum PA rotation (in degrees) to be considered for use as a reference PSF (good for disks)

    Returns:
        sub_imgs: array of [array of 2D images (PSF subtracted)] using different number of KL basis vectors as
                    specified by numbasis. Shape of (b,N,y,x). Exception is if b==1. Then sub_imgs has the first
                    array stripped away and is shape of (N,y,x).
    """
    if numbasis is None:
        totalimgs = imgs.shape[0]
        numbasis = numpy.arange(1,totalimgs + 5,5)
    else:
        if hasattr(numbasis,"__len__"):
            numbasis = numpy.array(numbasis)
        else:
            numbasis = numpy.array([numbasis])
        
    if aligned_center is None:
        aligned_center = [int(imgs.shape[2]//2),int(imgs.shape[1]//2)]
        
    allnans = numpy.where(numpy.isnan(imgs))
    KLs = []
    evals = []
    evecs = []
    #annuli
    dims = imgs.shape
    x,y = numpy.meshgrid(numpy.arange(dims[2] * 1.0),numpy.arange(dims[1]*1.0))
    nanpix = numpy.where(numpy.isnan(imgs[0]))
    OWA = numpy.sqrt(numpy.min((x[nanpix] - centers[0][0]) ** 2 + (y[nanpix] - centers[0][1]) ** 2))
    dr = float(OWA - IWA) / (annuli) 
    
    rad_bounds = [(dr * rad + IWA, dr * (rad + 1) + IWA) for rad in range(annuli)]
    rad_bounds[annuli - 1] = (rad_bounds[annuli - 1][0], imgs[0].shape[0] / 2)

    dphi = 2 * numpy.pi / subsections
    phi_bounds = [[dphi * phi_i - numpy.pi, dphi * (phi_i + 1) - numpy.pi] for phi_i in range(subsections)]
    phi_bounds[-1][1] = 2. * numpy.pi
    
    sub_imgs = numpy.zeros([dims[0], dims[1] * dims[2], numbasis.shape[0]])
    fm_imgs = numpy.zeros([dims[0], dims[1] * dims[2], numbasis.shape[0]])
    
    #begin KLIP process for each image
    for img_num, pa in enumerate(parangs):
        recenteredimgs = numpy.array([klip.align_and_scale(frame, aligned_center, oldcenter) for frame, oldcenter in zip(imgs, centers)])
        recenteredmodels = numpy.array([klip.align_and_scale(frame, aligned_center, oldcenter) for frame, oldcenter in zip(models, centers)])
#        recenteredimgs = imgs
#        recenteredmodels = models
        #create coordinate system 
        r = numpy.sqrt((x - aligned_center[0]) ** 2 + (y - aligned_center[1]) ** 2)
        phi = numpy.arctan2(y - aligned_center[1], x - aligned_center[0])

        #flatten img dimension
        flattenedimgs = recenteredimgs.reshape((dims[0], dims[1] * dims[2]))
        flattenedmodels = recenteredmodels.reshape((dims[0],dims[1] * dims[2]))
#        print flattenedimgs[~numpy.isnan(flattenedimgs)]
        r.shape = (dims[1] * dims[2])
        phi.shape = (dims[1] * dims[2])
        KL_basis_arr = []
        evals_arr = []
        evecs_arr = []
        goodinds = []
        coords = []
        #iterate over the different sections
        for radstart, radend in rad_bounds:
            KL_basis_phi_arr = []
            evals_phi_arr = []
            evecs_phi_arr = []
            goodinds_phi = []
            coords_phi = []
        

            evals_phi_arr = []
            for phistart, phiend in phi_bounds:
                #grab the pixel location of the section we are going to anaylze
                section_ind = numpy.where((r >= radstart) & (r < radend) & (phi >= phistart) & (phi < phiend))
                if numpy.size(section_ind) == 0:
                    continue
                #grab the files suitable for reference PSF
                avg_rad = (radstart + radend) / 2.0
                moves = klip.estimate_movement(avg_rad, parang0=pa, parangs=parangs)
                file_ind = numpy.where((moves >= movement) & (numpy.abs(parangs - pa) > minrot))

                if numpy.size(file_ind) < 2:
                    print("less than 2 reference PSFs available, skipping...")
                    print((sub_imgs[img_num, section_ind]).shape)
                    print(numpy.zeros(numpy.size(section_ind)).shape)
#                    sub_imgs[img_num, section_ind] = numpy.zeros(numpy.size(section_ind))
                    continue
                images_math = flattenedimgs[file_ind[0], :]
                images_math = images_math[:, section_ind[0]]

                


                models_math = flattenedmodels[file_ind[0], :]
                models_math = models_math[:, section_ind[0]]
                if savebasis:
                    sub_img, KL_basis,delta_KL, evals, evecs = klip_math_JB(flattenedimgs[img_num, section_ind][0], images_math,numbasis, models_ref =models_math,savebasis = True,spec_included = True) 
#                    delta_KL_int = numpy.sum(delta_KL, axis = 1)
                    fm_psf, klipped_oversub, klipped_selfsub = calculate_fm(delta_KL, KL_basis, numbasis, flattenedimgs[img_num, section_ind][0], flattenedmodels[img_num, section_ind[0]]) 
#                    sub_imgs[img_num, section_ind, :] = numpy.array([fm_psf.transpose()])

                    fm_imgs[img_num, section_ind, :] = numpy.array([fm_psf.transpose()])
                    sub_imgs[img_num, section_ind, :] = sub_img

                    KL_basis_phi_arr.append(KL_basis)
                    evals_phi_arr.append(evals)
                    evecs_phi_arr.append(evecs)
                    goodinds_phi.append(file_ind)
                    coords_phi.append(section_ind)
                else:
                    sub_img, KL_basis, delta_KL, evals, evecs = klip_math_JB(flattenedimgs[img_num, section_ind][0], images_math,numbasis,model_sci = flattenedmodels[img_num, section_ind[0]], models_ref =models_math, spec_included = True)
                    sub_imgs[img_num, section_ind, :] = sub_img

                    #sub_img, KL_basis, evals, evecs = klip_math(flattenedimgs[img_num, section_ind][0], images_math, models_math, numbasis, return_basis = True)
                    fm_psf, klipped_oversub, klipped_selfsub = calculate_fm(delta_KL, KL_basis, numbasis, flattenedimgs[img_num, section_ind][0], flattenedmodels[img_num, section_ind[0]]) 
                    fm_imgs[img_num, section_ind, :] = numpy.array([fm_psf.transpose()])
            if savebasis:
                KL_basis_arr.append(KL_basis_phi_arr)
                evals_arr.append(evals_phi_arr)
                evecs_arr.append(evecs_phi_arr)
                
                goodinds.append(goodinds_phi)
                coords.append(coords_phi)
        if savebasis:
            f = open(basispattern + str(img_num) + '.p', 'wb')
            pickle.dump(KL_basis_arr, f)
            pickle.dump(evals_arr, f)
            pickle.dump(evecs_arr,f) 
            pickle.dump(goodinds, f)
            pickle.dump(coords, f)
    #move number of KLIP modes as leading axis (i.e. move from shape (N,y,x,b) to (b,N,y,x)
    print numpy.shape(sub_imgs)
    print numpy.shape(fm_imgs)
    sub_imgs = numpy.rollaxis(sub_imgs.reshape((dims[0], dims[1], dims[2], numbasis.shape[0])), 3)
    fm_imgs = numpy.rollaxis(fm_imgs.reshape((dims[0], dims[1], dims[2], numbasis.shape[0])), 3)
    print numpy.shape(sub_imgs)
    print numpy.shape(fm_imgs)

    #if we only passed in one value for numbasis (i.e. only want one PSF subtraction), strip off the number of basis)
    if sub_imgs.shape[0] == 1:
        sub_imgs = sub_imgs[0]
        fm_imgs = fm_imgs[0]
    #restore bad pixels
    #FIXME
#    sub_imgs[:,allnans[0], allnans[1], allnans[2]] = numpy.nan

    #derotate images
#    imgs_list = []
#    for a in sub_imgs:
#        imgs_list.append(numpy.array([klip.rotate(img, pa, (140,140), center) for img,pa,center in zip(sub_imgs, parangs, centers)]))
#    subimgs = numpy.asarray(imgs_list)
#print numpy.shape(subimgs)
#plot.imshow(numpy.mean(subimgs.T, axis = 0))
#    plot.show()
    
    #all of the image centers are now at aligned_center
    centers[:,0] = aligned_center[0]
    centers[:,1] = aligned_center[1]
    
    return sub_imgs, fm_imgs
    

def calculate_fm(delta_KL_nospec, original_KL, numbasis, sci, model_sci, inputflux = None):
    """
    Calculate what the PSF looks up post-KLIP using knowledge of the input PSF, assumed spectrum of the science target,
    and the partially calculated KL modes (\Delta Z_k^\lambda in Laurent's paper). If inputflux is None,
    the spectral dependence has already been folded into delta_KL_nospec (treat it as delta_KL)

    Args:
        delta_KL_nospec: perturbed KL modes but without the spectral info. delta_KL = spectrum x delta_Kl_nospec.
                         Shape is (numKL, wv, pix). If inputflux is None, delta_KL_nospec = delta_KL
        orignal_KL: unpertrubed KL modes (array of size [numbasis, numpix])
        numbasis: array of KL mode cutoffs
                If numbasis is [0] the number of KL modes to be used is automatically picked based on the eigenvalues.
        sci: array of size p representing the science data
        model_sci: array of size p corresponding to the PSF of the science frame
        input_spectrum: array of size wv with the assumed spectrum of the model

    Returns:
        fm_psf: array of shape (b,p) showing the forward modelled PSF
        klipped_oversub: array of shape (b, p) showing the effect of oversubtraction as a function of KL modes
        klipped_selfsub: array of shape (b, p) showing the effect of selfsubtraction as a function of KL modes
        Note: psf_FM = model_sci - klipped_oversub - klipped_selfsub to get the FM psf as a function of K Lmodes
              (shape of b,p)
    """
    max_basis = original_KL.shape[0]
    if numbasis[0]==0:
        numbasis_index = [max_basis-1]
    else:
        numbasis_index = numpy.clip(numbasis - 1, 0, max_basis-1)

    # remove means and nans from science image
    #print("c")
    sci_mean_sub = sci - numpy.nanmean(sci)
    sci_nanpix = numpy.where(numpy.isnan(sci_mean_sub))
    sci_mean_sub[sci_nanpix] = 0
    sci_mean_sub_rows = numpy.tile(sci_mean_sub, (max_basis,1))
    #sci_rows_selected = numpy.tile(sci_mean_sub, (numpy.size(numbasis),1))


    # science PSF models, ready for FM
    # /!\ JB: If subtracting the mean. It should be done here. not in klip_math since we don't use model_sci there.
    model_sci_mean_sub = model_sci # should be subtracting off the mean?
    model_nanpix = numpy.where(numpy.isnan(model_sci_mean_sub))
    model_sci_mean_sub[model_nanpix] = 0
    model_sci_mean_sub_rows = numpy.tile(model_sci_mean_sub, (max_basis,1))
    # model_rows_selected = numpy.tile(sci_mean_sub, (numpy.size(numbasis),1)) # don't need this because of python behavior where I don't need to duplicate rows


    # calculate perturbed KL modes based on spectrum
    if inputflux is not None:
        delta_KL = numpy.dot(inputflux, delta_KL_nospec) # this will take the last dimension of input_spectrum (wv) and sum over the second to last dimension of delta_KL_nospec (wv)
    else:
        delta_KL = delta_KL_nospec

    # Forward model the PSF
    # 3 terms: 1 for oversubtracton (planet attenauted by speckle KL modes),
    # and 2 terms for self subtraction (planet signal leaks in KL modes which get projected onto speckles)
    oversubtraction_inner_products = numpy.dot(model_sci_mean_sub_rows, original_KL.T)
    selfsubtraction_1_inner_products = numpy.dot(sci_mean_sub_rows, delta_KL.T)
    selfsubtraction_2_inner_products = numpy.dot(sci_mean_sub_rows, original_KL.T)

    lower_tri = numpy.tril(numpy.ones([max_basis,max_basis]))
    oversubtraction_inner_products = oversubtraction_inner_products * lower_tri
    selfsubtraction_1_inner_products = selfsubtraction_1_inner_products * lower_tri
    selfsubtraction_2_inner_products = selfsubtraction_2_inner_products * lower_tri

    klipped_oversub = numpy.dot(numpy.take(oversubtraction_inner_products, numbasis_index, axis=0), original_KL)
    klipped_selfsub = numpy.dot(numpy.take(selfsubtraction_1_inner_products, numbasis_index, axis=0), original_KL) + \
                      numpy.dot(numpy.take(selfsubtraction_2_inner_products,numbasis_index, axis=0), delta_KL)

    return model_sci - klipped_oversub - klipped_selfsub, klipped_oversub, klipped_selfsub




def klip_math_JB(sci, refs, numbasis, covar_psfs=None, model_sci=None, models_ref=None, spec_included=False, spec_from_model=False, savebasis = True):
    """
    linear algebra of KLIP with linear perturbation
    disks and point source

    Args:
        sci: array of length p containing the science data
        refs: N x p array of the N reference images that
                  characterizes the extended source with p pixels
        numbasis: number of KLIP basis vectors to use (can be an int or an array of ints of length b)
                If numbasis is [0] the number of KL modes to be used is automatically picked based on the eigenvalues.
        covar_psfs: covariance matrix of reference images (for large N, useful). Normalized following numpy normalization in numpy.cov documentationoo
        # The following arguments must all be passed in, or none of them for klip_math to work
        models_ref: N x p array of the N models corresponding to reference images. Each model should be normalized to unity (no flux information)
        model_sci: array of size p corresponding to the PSF of the science frame
        Sel_wv: wv x N array of the the corresponding wavelength for each reference PSF
        input_spectrum: array of size wv with the assumed spectrum of the model


    Returns:
        sub_img_rows_selected: array of shape (p,b) that is the PSF subtracted data for each of the b KLIP basis
                               cutoffs. If numbasis was an int, then sub_img_row_selected is just an array of length p
        KL_basis: array of KL basis (shape of [numbasis, p])
        If models_ref is passed in (not None):
            delta_KL_nospec: array of shape (b, wv, p) that is the almost perturbed KL modes just missing spectral info
        Otherwise:
            evals: array of eigenvalues (size of max number of KL basis requested aka nummaxKL)
            evecs: array of corresponding eigenvectors (shape of [p, nummaxKL])


    """
    # remove means and nans
    sci_mean_sub = sci - numpy.nanmean(sci)
    sci_nanpix = numpy.where(numpy.isnan(sci_mean_sub))
    sci_mean_sub[sci_nanpix] = 0

    refs_mean_sub = refs - numpy.nanmean(refs, axis=1)[:, None]
    refs_mean_sub[numpy.where(numpy.isnan(refs_mean_sub))] = 0

    # calculate the covariance matrix for the reference PSFs
    # note that numpy.cov normalizes by p-1 to get the NxN covariance matrix
    # we have to correct for that since that's not part of the equation in the KLIP paper
    if covar_psfs is None:
        # call numpy.cov to make the covariance matrix
        covar_psfs = numpy.cov(refs_mean_sub)
    # fix normalization of covariance matrix
    covar_psfs *= (numpy.size(sci)-1)

    # calculate the total number of KL basis we need based on the number of reference PSFs and number requested
    tot_basis = covar_psfs.shape[0]

    if numbasis[0] == 0:
        evals, evecs = la.eigh(covar_psfs, eigvals = (tot_basis-numpy.min([100,tot_basis-1]), tot_basis-1))
        evals = numpy.copy(evals[::-1])
        evecs = numpy.copy(evecs[:,::-1])
        # import matplotlib.pyplot as plt
        # plt.plot(numpy.log10(evals))
        # plt.show()

        max_basis = find_id_nearest(evals/evals[2],10**-1.25)+1
        print(max_basis)
        evals = evals[:max_basis]
        evecs = evecs[:,:max_basis]
    else:
        numbasis = numpy.clip(numbasis - 1, 0, tot_basis-1)
        max_basis = numpy.max(numbasis) + 1

        # calculate eigenvectors/values of covariance matrix
        evals, evecs = la.eigh(covar_psfs, eigvals = (tot_basis-max_basis, tot_basis-1))
        evals = numpy.copy(evals[::-1])
        evecs = numpy.copy(evecs[:,::-1])

    # project on reference PSFs to generate KL modes
    KL_basis = numpy.dot(refs_mean_sub.T,evecs)
    KL_basis = KL_basis * (1. / numpy.sqrt(evals))[None,:]
    KL_basis = KL_basis.T # flip dimensions to be consistent with Laurent's paper

    # prepare science frame for KLIP subtraction
    sci_mean_sub_rows = numpy.tile(sci_mean_sub, (max_basis,1))
    sci_rows_selected = numpy.tile(sci_mean_sub, (numpy.size(numbasis),1))

    sci_nanpix = numpy.where(numpy.isnan(sci_mean_sub_rows))
    sci_mean_sub_rows[sci_nanpix] = 0
    sci_nanpix = numpy.where(numpy.isnan(sci_rows_selected))
    sci_rows_selected[sci_nanpix] = 0

    # run KLIP on this sector and subtract the stellar PSF
    inner_products = numpy.dot(sci_mean_sub_rows, KL_basis.T)
    lower_tri = numpy.tril(numpy.ones([max_basis,max_basis]))
    inner_products = inner_products * lower_tri

    # JB, this code is broken here. It will always do this if the first numbasis is 1.
    # if numbasis[0] == 0:
    if False:
        klip = numpy.dot(inner_products[[max_basis-1],:], KL_basis)
    else:
        klip = numpy.dot(inner_products[numbasis,:], KL_basis)


    sub_img_rows_selected = sci_rows_selected - klip
    sub_img_rows_selected[sci_nanpix] = numpy.nan


    if models_ref is not None:

        print 'wat'
        print 'KLbasis'
        print numpy.shape(KL_basis)
        print 'refsmean'
        print numpy.shape(refs_mean_sub)
        print 'models-ref'
        print numpy.shape(models_ref)
        print 'evals'
        print numpy.shape(evals)
        print 'evecs'
        print numpy.shape(evecs)
                
        if spec_included:
            delta_KL = perturb_specIncluded(evals, evecs, KL_basis, refs_mean_sub, models_ref)
            return sub_img_rows_selected.transpose(), KL_basis,  delta_KL, evals, evecs
        elif spec_from_model:
            delta_KL_nospec = perturb_nospec_modelsBased(evals, evecs, KL_basis, refs_mean_sub, models_ref)
            return sub_img_rows_selected.transpose(), KL_basis,  delta_KL_nospec
        else:
            if savebasis:
                delta_KL_nospec = pertrurb_nospec(evals, evecs, KL_basis, refs_mean_sub, models_ref)
                return sub_img_rows_selected.transpose(), KL_basis,  numpy.sum(delta_KL_nospec, axis = 1), evals, evecs


    else:

        return sub_img_rows_selected.transpose(), KL_basis, evals, evecs











def pertrurb_nospec(evals, evecs, original_KL, refs, models_ref):
    """
    Perturb the KL modes using a model of the PSF but with no assumption on the spectrum. Useful for planets

    Args:
        evals: array of eigenvalues of the reference PSF covariance matrix (array of size numbasis)
        evecs: corresponding eigenvectors (array of size [p, numbasis])
        orignal_KL: unpertrubed KL modes (array of size [numbasis, p])
        Sel_wv: wv x N array of the the corresponding wavelength for each reference PSF
        refs: N x p array of the N reference images that
                  characterizes the extended source with p pixels
        models_ref: N x p array of the N models corresponding to reference images. Each model should be normalized to unity (no flux information)
        model_sci: array of size p corresponding to the PSF of the science frame

    Returns:
        delta_KL_nospec: perturbed KL modes but without the spectral info. delta_KL = spectrum x delta_Kl_nospec.
                         Shape is (numKL, wv, pix)
    """

    max_basis = original_KL.shape[0]
    N_ref = refs.shape[0]
    N_pix = original_KL.shape[1]

    refs_mean_sub = refs - numpy.nanmean(refs, axis=1)[:, None]
    refs_mean_sub[numpy.where(numpy.isnan(refs_mean_sub))] = 0

    models_mean_sub = models_ref # - numpy.nanmean(models_ref, axis=1)[:,None] should this be the case?
    models_mean_sub[numpy.where(numpy.isnan(models_mean_sub))] = 0

    # science PSF models
    #model_sci_mean_sub = model_sci # should be subtracting off the mean?
    #model_nanpix = numpy.where(numpy.isnan(model_sci_mean_sub))
    #model_sci_mean_sub[model_nanpix] = 0

    # perturbed KL modes
    delta_KL_nospec = numpy.zeros([max_basis, N_ref, N_pix]) # (numKL,N_ref,N_pix)

    #plt.figure(1)
    #plt.plot(evals)
    #ax = plt.gca()
    #ax.set_yscale('log')
    #plt.show()

    models_mean_sub_X_refs_mean_sub_T = models_mean_sub.dot(refs_mean_sub.transpose())
    # calculate perturbed KL modes. TODO: make this NOT a freaking for loop
    for k in range(max_basis):
        Zk = numpy.reshape(original_KL[k,:],(1,original_KL[k,:].size))
        Vk = (evecs[:,k])[:,None]

        DeltaZk_noSpec = -(1/numpy.sqrt(evals[k]))*(Vk*models_mean_sub_X_refs_mean_sub_T).dot(Vk).dot(Zk)+Vk*models_mean_sub
        # TODO: Make this NOT a for loop
        diagVk_X_models_mean_sub_X_refs_mean_sub_T = Vk*models_mean_sub_X_refs_mean_sub_T
        models_mean_sub_X_refs_mean_sub_T_X_Vk = models_mean_sub_X_refs_mean_sub_T.dot(Vk)
        for j in range(k):
            Zj = original_KL[j, :][None,:]
            Vj = evecs[:, j][:,None]
            DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk_X_models_mean_sub_X_refs_mean_sub_T.dot(Vj) + Vj*models_mean_sub_X_refs_mean_sub_T_X_Vk).dot(Zj)
        for j in range(k+1, max_basis):
            Zj = original_KL[j, :][None,:]
            Vj = evecs[:, j][:,None]
            DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk_X_models_mean_sub_X_refs_mean_sub_T.dot(Vj) + Vj*models_mean_sub_X_refs_mean_sub_T_X_Vk).dot(Zj)
        delta_KL_nospec[k] = DeltaZk_noSpec/numpy.sqrt(evals[k])
    '''
    if 0:  # backup slow version
        # calculate perturbed KL modes. TODO: make this NOT a freaking for loop
        for k in range(max_basis):
            # Define Z_{k}. Variable name: kl_basis_noPl[k,:], Shape: (1, N_pix)
            Zk = numpy.reshape(original_KL[k,:],(1,original_KL[k,:].size))
            # Define V_{k}. Variable name: eigvec_noPl[:,k], Shape: (1, N_ref)
            Vk = numpy.reshape(evecs[:,k],(evecs[:,k].size,1))
            # Define bolt{V}_{k}. Variable name: diagV_k = numpy.diag(eigvec_noPl[:,k]), Shape: (N_ref,N_ref)
            diagVk = numpy.diag(evecs[:,k])


            DeltaZk_noSpec = -(1/numpy.sqrt(evals[k]))*diagVk.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vk).dot(Zk)+diagVk.dot(models_mean_sub)
            # TODO: Make this NOT a for loop
            for j in range(k):
                Zj = numpy.reshape(original_KL[j, :], (1, original_KL[j, :].size))
                Vj = numpy.reshape(evecs[:, j], (evecs[:, j].size,1))
                diagVj = numpy.diag(evecs[:, j])
                DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vj) + diagVj.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vk)).dot(Zj)
            for j in range(k+1, max_basis):
                Zj = numpy.reshape(original_KL[j, :], (1, original_KL[j, :].size))
                Vj = numpy.reshape(evecs[:, j], (evecs[:, j].size,1))
                diagVj = numpy.diag(evecs[:, j])
                DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vj) + diagVj.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vk)).dot(Zj)

            delta_KL_nospec[k] = (Sel_wv/numpy.sqrt(evals[k])).dot(DeltaZk_noSpec)
    '''

    return delta_KL_nospec







def perturb_specIncluded(evals, evecs, original_KL, refs, models_ref):
    """
    Perturb the KL modes using a model of the PSF but with the spectrum included in the model. Quicker than the others

    Args:
        evals: array of eigenvalues of the reference PSF covariance matrix (array of size numbasis)
        evecs: corresponding eigenvectors (array of size [p, numbasis])
        orignal_KL: unpertrubed KL modes (array of size [numbasis, p])
        refs: N x p array of the N reference images that
                  characterizes the extended source with p pixels
        models_ref: N x p array of the N models corresponding to reference images.
                    Each model should contain spectral informatoin
        model_sci: array of size p corresponding to the PSF of the science frame

    Returns:
        delta_KL_nospec: perturbed KL modes. Shape is (numKL, wv, pix)
    """

    max_basis = original_KL.shape[0]
    N_ref = refs.shape[0]
    N_pix = original_KL.shape[1]

    refs_mean_sub = refs - numpy.nanmean(refs, axis=1)[:, None]
    # JB: check if commenting that is fine
    #refs_mean_sub[numpy.where(numpy.isnan(refs_mean_sub))] = 0

    models_mean_sub = models_ref # - numpy.nanmean(models_ref, axis=1)[:,None] should this be the case?
    # JB: check if commenting that is fine
    #models_mean_sub[numpy.where(numpy.isnan(models_mean_sub))] = 0

    #print(evals.shape,evecs.shape,original_KL.shape,refs.shape,models_ref.shape)

    evals_tiled = numpy.tile(evals,(max_basis,1))
    numpy.fill_diagonal(evals_tiled,numpy.nan)
    evals_sqrt = numpy.sqrt(evals)
    evalse_inv_sqrt = 1./evals_sqrt
    evals_ratio = (evalse_inv_sqrt[:,None]).dot(evals_sqrt[None,:])
    beta_tmp = 1./(evals_tiled.transpose()- evals_tiled)
    #print(evals)
    beta_tmp[numpy.diag_indices(numpy.size(evals))] = -0.5/evals
    beta = evals_ratio*beta_tmp

    C_partial = models_mean_sub.dot(refs_mean_sub.transpose())
    C = C_partial+C_partial.transpose()
    #C =  models_mean_sub.dot(refs_mean_sub.transpose())+refs_mean_sub.dot(models_mean_sub.transpose())
    alpha = (evecs.transpose()).dot(C).dot(evecs)

    delta_KL = (beta*alpha).dot(original_KL)+(evalse_inv_sqrt[:,None]*evecs.transpose()).dot(models_mean_sub)


    return delta_KL
