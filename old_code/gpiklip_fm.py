import pyfits
import glob
import numpy
import constants as const
#import shift
import matplotlib.pyplot as plot
import scipy.ndimage as ndimage
import copy
import pyklip.instruments.GPI as gpi
import os
import image
import pickle
import image
import multiprocessing as mp
from astropy import wcs



def klipmodel(im, globname='april_k/*podc*',basisname = 'basis_fm_K/basis'):
    x = sorted(glob.glob(globname))
    a = gpi.GPIData(x)
    sciims = a.input
    pas = a.PAs
    cents = [[140,140] for i in range(len(a.centers))]
    test = perform_klip_adi(sciims, im, pas, cents, basispattern = basisname)
    return test




def kliptest():
    x = sorted(glob.glob('april_j/*podc*'))
    a = gpi.GPIData(x)
    model = pyfits.getdata('model0.fits')
    sciims = a.input
    pas = a.PAs
    cents = [[140,140] for i in range(len(a.centers))]
    
    test = perform_klip_adi(sciims, model, pas, cents, basispattern = 'basis_fm_J/basis')
    pyfits.writeto('test1.fits', test)
    plot.imshow(test)
    plot.show()
    test2 = perform_klip_adi(sciims, model * 10, pas, cents, basispattern = 'basis_fm_J/basis')
    pyfits.writeto('test2.fits', test2)
    plot.imshow(test)
    plot.show()
    plot.imshow(test2 - test)
    plot.colorbar()
    plot.show()


def test_loadin():
    x = sorted(glob.glob('april_j/*podc*'))
    a = gpi.GPIData(x)
    model = pyfits.getdata('model0.fits')
    sciims = a.input
    pas = a.PAs
    cents = [[140,140] for i in range(len(a.centers))]
    KLbasisvals_all, evals_all, evecs_all, goodinds_all, coords_all  = load_basis_info(ind = 1)

    test1 = perform_klip_adi(sciims, model, pas, cents,basisvals_all = KLbasisvals_all,evals_all = evals_all, evecs_all = evecs_all, coords_all = coords_all, goodinds_all = goodinds_all)
    test2 = perform_klip_adi(sciims, model * 2., pas, cents,basisvals_all = KLbasisvals_all,evals_all = evals_all, evecs_all = evecs_all, coords_all = coords_all, goodinds_all = goodinds_all)
    plot.subplot(211)
    plot.imshow(test1)
    plot.colorbar()
    plot.subplot(212)
    plot.imshow(test2)
    plot.colorbar()
    plot.show()



def load_basis_info(ind = None, basispattern = None):
    if basispattern is None:
        assert ind is not None
        if ind == 0:
            basispattern = '/home/parriaga/hr4796a_new/basis_fm_K/basis'
        else:
            basispattern = '/home/parriaga/hr4796a_new/basis_fm_J/basis'

    KLbasisvals_all = []
    evals_all = []
    evecs_all = []
    goodinds_all = []
    coords_all = []

    gl = glob.glob(basispattern + '*')
    
    nims = len(gl)
    for imnum in range(nims):
        picklename = basispattern + str(imnum) + '.p'
        picklefile = open(picklename)
        KLbasisvals_arr = pickle.load(picklefile)
        evals_arr = pickle.load(picklefile)
        evecs_arr = pickle.load(picklefile)
        goodinds_arr = pickle.load(picklefile)
        coords_arr = pickle.load(picklefile)
        KLbasisvals_all.append(KLbasisvals_arr)
        evals_all.append(evals_arr)
        evecs_all.append(evecs_arr)
        goodinds_all.append(goodinds_arr)
        coords_all.append(coords_arr)
    return KLbasisvals_all, evals_all, evecs_all, goodinds_all, coords_all





def perform_klip_adi(scienceims, model, pas, cents, basispattern = None, basisvals_all = None, evals_all = None, evecs_all = None, coords_all = None, goodinds_all = None, ind = None):
    """
    Load aligned images from file.  Load KLIP modes from file.
    Perform KLIP-based ADI on aligned images and save the output.
    ims - model images
    nrs - number of radii
    nphis - numpy of phi 
    ims - images aligned to center
    pas - pa of each image
    """
#    print scienceims[0][~numpy.isnan(scienceims[0])]
#    plot.imshow(scienceims[0])
#    plot.colorbar()
#    plot.show()
    if basispattern is None:
        if ind is not None:
            if ind == 0:
                basispattern = '/home/parriaga/hr4796a_red/basis/basisK'
            else:
                basispattern = '/home/parriaga/hr4796a_red/basis/basisJ'
    modelims = numpy.zeros(numpy.shape(scienceims))
    modelsize = numpy.shape(model)
    for i, pa in enumerate(pas):
        canvas = copy.deepcopy(model)
        canvas = kliprotate(canvas, pa, [140, 140],  flipx = True)
        canvas[numpy.where(numpy.isnan(canvas))] = 0.
        modelims[i] = canvas
    imsh = numpy.shape(modelims)
    flatmodelims = numpy.reshape(modelims, [imsh[0], imsh[1] * imsh[2]])
    flatscienceims = numpy.reshape(scienceims, [imsh[0], imsh[1] * imsh[2]])
    sub_ims = numpy.zeros(flatmodelims.shape, dtype = numpy.float) + numpy.nan    
    for imnum in range(len(pas)):
        if basispattern is not None:
            picklename = basispattern + str(imnum) + '.p'
            picklefile = open(picklename)
            KLbasisvals_arr = pickle.load(picklefile)
            evals_arr = pickle.load(picklefile)
            evecs_arr = pickle.load(picklefile)
            goodinds_arr = pickle.load(picklefile)
            coords_arr = pickle.load(picklefile)
        else:
            #FIXME
            KLbasisvals_arr = basisvals_all[imnum]
            evals_arr = evals_all[imnum]
            evecs_arr = evecs_all[imnum]
            coords_arr = coords_all[imnum]
            goodinds_arr = goodinds_all[imnum]


        for p in range(len(KLbasisvals_arr)):
            for r in range(len(KLbasisvals_arr[0])):
                KLbasisvals = KLbasisvals_arr[p][r]
                evecs = evecs_arr[p][r]
                evals = evals_arr[p][r]
                goodinds = goodinds_arr[p][r]
                coords = coords_arr[p][r]

#                sciencedata = flatscienceims[imnum, coords][0]
#                modeldata = flatmodelims[imnum, coords][0]
                sciencedata = flatscienceims[imnum, coords[0]]
                modeldata = flatmodelims[imnum, coords[0]]


#                modeldata = modeldata[:, coords[0]]

                refims = flatscienceims[goodinds[0], :]
                refims = refims[:, coords[0]]
                refims[numpy.where(numpy.isnan(refims))] = 0

                modelref = flatmodelims[goodinds[0], :]
                modelref = modelref[:, coords[0]]
                modelref[numpy.where(numpy.isnan(modelref))] = 0


                #Need to normalize and give a spectrum to do properly
                delta_KL_nospec = perturb_specIncluded(evals, evecs, KLbasisvals, refims, modelref)# / numpy.max(refs_mean_sub), modeldata)


                fm_psf, klipped_oversub, klipped_selfsub = calculate_fm(delta_KL_nospec, KLbasisvals, numpy.array([2]), sciencedata, modeldata)#flattenedimgs[img_num, section_ind][0], flattenedmodels[img_num, section_ind[0]]) 
                sub_ims[imnum, coords] = fm_psf


                # image subtraction
    sub_ims = numpy.reshape(sub_ims, imsh)
    for i in range(len(sub_ims)):
        sub_ims[i] = kliprotate(sub_ims[i],pas[i] , [140,140], flipx = True)
    collapse = numpy.mean(sub_ims, axis = 0)
    return collapse







                        

def perturb_specIncluded(evals, evecs, original_KL, refs, models_ref):
    """
    Perturb the KL modes using a model of the PSF but with the spectrum included in the model. Quicker than the others

    Args:
        evals: array of eigenvalues of the reference PSF covariance matrix (array of size numbasis)
        evecs: corresponding eigenvectors (array of size [p, numbasis])
        orignal_KL: unpertrubed KL modes (array of size [numbasis, p])
        refs: N x p array of the N reference images that
                  characterizes the extended source with p pixels
        models_ref: N x p array of the N models corresponding to reference images.
                    Each model should contain spectral informatoin
        model_sci: array of size p corresponding to the PSF of the science frame

    Returns:
        delta_KL_nospec: perturbed KL modes. Shape is (numKL, wv, pix)
    """

    max_basis = original_KL.shape[0]
    N_ref = refs.shape[0]
    N_pix = original_KL.shape[1]

    refs_mean_sub = refs - numpy.nanmean(refs, axis=1)[:, None]
    # JB: check if commenting that is fine
    #refs_mean_sub[numpy.where(numpy.isnan(refs_mean_sub))] = 0

    models_mean_sub = models_ref # - numpy.nanmean(models_ref, axis=1)[:,None] should this be the case?
    # JB: check if commenting that is fine
    #models_mean_sub[numpy.where(numpy.isnan(models_mean_sub))] = 0

    #print(evals.shape,evecs.shape,original_KL.shape,refs.shape,models_ref.shape)

    evals_tiled = numpy.tile(evals,(max_basis,1))
    numpy.fill_diagonal(evals_tiled,numpy.nan)
    evals_sqrt = numpy.sqrt(evals)
    evalse_inv_sqrt = 1./evals_sqrt
    evals_ratio = (evalse_inv_sqrt[:,None]).dot(evals_sqrt[None,:])
    beta_tmp = 1./(evals_tiled.transpose()- evals_tiled)
    #print(evals)
    beta_tmp[numpy.diag_indices(numpy.size(evals))] = -0.5/evals
    beta = evals_ratio*beta_tmp

    C_partial = models_mean_sub.dot(refs_mean_sub.transpose())
    C = C_partial+C_partial.transpose()
    #C =  models_mean_sub.dot(refs_mean_sub.transpose())+refs_mean_sub.dot(models_mean_sub.transpose())
    alpha = (evecs.transpose()).dot(C).dot(evecs)

    delta_KL = (beta*alpha).dot(original_KL)+(evalse_inv_sqrt[:,None]*evecs.transpose()).dot(models_mean_sub)


    return delta_KL



def pertrurb_nospec(evals, evecs, original_KL, refs, models_ref):
    """
    Perturb the KL modes using a model of the PSF but with no assumption on the spectrum. Useful for planets

    Args:
        evals: array of eigenvalues of the reference PSF covariance matrix (array of size numbasis)
        evecs: corresponding eigenvectors (array of size [p, numbasis])
        orignal_KL: unpertrubed KL modes (array of size [numbasis, p])
        Sel_wv: wv x N array of the the corresponding wavelength for each reference PSF
        refs: N x p array of the N reference images that
                  characterizes the extended source with p pixels
        models_ref: N x p array of the N models corresponding to reference images. Each model should be normalized to unity (no flux information)
        model_sci: array of size p corresponding to the PSF of the science frame

    Returns:
        delta_KL_nospec: perturbed KL modes but without the spectral info. delta_KL = spectrum x delta_Kl_nospec.
                         Shape is (numKL, wv, pix)
    """

    max_basis = original_KL.shape[0]
    N_ref = refs.shape[0]
    N_pix = original_KL.shape[1]

    refs_mean_sub = refs - numpy.nanmean(refs, axis=1)[:, None]
    refs_mean_sub[numpy.where(numpy.isnan(refs_mean_sub))] = 0

    models_mean_sub = models_ref # - numpy.nanmean(models_ref, axis=1)[:,None] should this be the case?
    models_mean_sub[numpy.where(numpy.isnan(models_mean_sub))] = 0

    # science PSF models
    #model_sci_mean_sub = model_sci # should be subtracting off the mean?
    #model_nanpix = numpy.where(numpy.isnan(model_sci_mean_sub))
    #model_sci_mean_sub[model_nanpix] = 0

    # perturbed KL modes
    delta_KL_nospec = numpy.zeros([max_basis, N_ref, N_pix]) # (numKL,N_ref,N_pix)

    #plt.figure(1)
    #plt.plot(evals)
    #ax = plt.gca()
    #ax.set_yscale('log')
    #plt.show()

    models_mean_sub_X_refs_mean_sub_T = models_mean_sub.dot(refs_mean_sub.transpose())
    # calculate perturbed KL modes. TODO: make this NOT a freaking for loop
    for k in range(max_basis):
        Zk = numpy.reshape(original_KL[k,:],(1,original_KL[k,:].size))
        Vk = (evecs[:,k])[:,None]

        DeltaZk_noSpec = -(1/numpy.sqrt(evals[k]))*(Vk*models_mean_sub_X_refs_mean_sub_T).dot(Vk).dot(Zk)+Vk*models_mean_sub
        # TODO: Make this NOT a for loop
        diagVk_X_models_mean_sub_X_refs_mean_sub_T = Vk*models_mean_sub_X_refs_mean_sub_T
        models_mean_sub_X_refs_mean_sub_T_X_Vk = models_mean_sub_X_refs_mean_sub_T.dot(Vk)
        for j in range(k):
            Zj = original_KL[j, :][None,:]
            Vj = evecs[:, j][:,None]
            DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk_X_models_mean_sub_X_refs_mean_sub_T.dot(Vj) + Vj*models_mean_sub_X_refs_mean_sub_T_X_Vk).dot(Zj)
        for j in range(k+1, max_basis):
            Zj = original_KL[j, :][None,:]
            Vj = evecs[:, j][:,None]
            DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk_X_models_mean_sub_X_refs_mean_sub_T.dot(Vj) + Vj*models_mean_sub_X_refs_mean_sub_T_X_Vk).dot(Zj)

        delta_KL_nospec[k] = DeltaZk_noSpec/numpy.sqrt(evals[k])
    '''
    if 0:  # backup slow version
        # calculate perturbed KL modes. TODO: make this NOT a freaking for loop
        for k in range(max_basis):
            # Define Z_{k}. Variable name: kl_basis_noPl[k,:], Shape: (1, N_pix)
            Zk = numpy.reshape(original_KL[k,:],(1,original_KL[k,:].size))
            # Define V_{k}. Variable name: eigvec_noPl[:,k], Shape: (1, N_ref)
            Vk = numpy.reshape(evecs[:,k],(evecs[:,k].size,1))
            # Define bolt{V}_{k}. Variable name: diagV_k = numpy.diag(eigvec_noPl[:,k]), Shape: (N_ref,N_ref)
            diagVk = numpy.diag(evecs[:,k])


            DeltaZk_noSpec = -(1/numpy.sqrt(evals[k]))*diagVk.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vk).dot(Zk)+diagVk.dot(models_mean_sub)
            # TODO: Make this NOT a for loop
            for j in range(k):
                Zj = numpy.reshape(original_KL[j, :], (1, original_KL[j, :].size))
                Vj = numpy.reshape(evecs[:, j], (evecs[:, j].size,1))
                diagVj = numpy.diag(evecs[:, j])
                DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vj) + diagVj.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vk)).dot(Zj)
            for j in range(k+1, max_basis):
                Zj = numpy.reshape(original_KL[j, :], (1, original_KL[j, :].size))
                Vj = numpy.reshape(evecs[:, j], (evecs[:, j].size,1))
                diagVj = numpy.diag(evecs[:, j])
                DeltaZk_noSpec += numpy.sqrt(evals[j])/(evals[k]-evals[j])*(diagVk.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vj) + diagVj.dot(models_mean_sub).dot(refs_mean_sub.transpose()).dot(Vk)).dot(Zj)

            delta_KL_nospec[k] = (Sel_wv/numpy.sqrt(evals[k])).dot(DeltaZk_noSpec)
    '''

    return delta_KL_nospec



def kliprotate(img, angle, center, new_center=None, flipx=True, astr_hdr=None):
    """
    Rotate an image by the given angle about the given center.
    Optional: can shift the image to a new image center after rotation. Also can reverse x axis for those left
              handed astronomy coordinate systems

    Inputs:
        img: a 2D image
        angle: angle CCW to rotate by (degrees)
        center: 2 element list [x,y] that defines the center to rotate the image to respect to
        new_center: 2 element list [x,y] that defines the new image center after rotation
        flipx: default is True, which reverses x axis.
        astr_hdr: wcs astrometry header for the image
    Outputs:
        resampled_img: new 2D image
    """
    #convert angle to radians
    angle_rad = numpy.radians(angle)

    #create the coordinate system of the image to manipulate for the transform
    dims = img.shape
    x, y = numpy.meshgrid(numpy.arange(dims[1], dtype=numpy.float32), numpy.arange(dims[0], dtype=numpy.float32))

    #if necessary, move coordinates to new center
    if new_center is not None:
        dx = new_center[0] - center[0]
        dy = new_center[1] - center[1]
        x -= dx
        y -= dy

    #flip x if needed to get East left of North
    if flipx is True:
        x = center[0] - (x - center[0])
    
    #do rotation. CW rotation formula to get a CCW of the image
    xp = (x-center[0])*numpy.cos(angle_rad) + (y-center[1])*numpy.sin(angle_rad) + center[0]
    yp = -(x-center[0])*numpy.sin(angle_rad) + (y-center[1])*numpy.cos(angle_rad) + center[1]


    #resample image based on new coordinates
    #scipy uses y,x convention when meshgrid uses x,y
    #stupid scipy functions can't work with masked arrays (NANs)
    #and trying to use interp2d with sparse arrays is way to slow
    #hack my way out of this by picking a really small value for NANs and try to detect them after the interpolation
    #then redo the transformation setting NaN to zero to reduce interpolation effects, but using the mask we derived
    minval = numpy.min([numpy.nanmin(img), 0.0])
    nanpix = numpy.where(numpy.isnan(img))
    medval = numpy.median(img[numpy.where(~numpy.isnan(img))])
    img_copy = numpy.copy(img)
    img_copy[nanpix] = minval * 5.0
    resampled_img_mask = ndimage.map_coordinates(img_copy, [yp, xp], cval=minval * 5.0)
    img_copy[nanpix] = medval
    resampled_img = ndimage.map_coordinates(img_copy, [yp, xp], cval=numpy.nan)
    resampled_img[numpy.where(resampled_img_mask < minval)] = numpy.nan

    #edit the astrometry header if given to compensate for orientation
    if astr_hdr is not None:
        _rotate_wcs_hdr(astr_hdr, angle, flipx=flipx)

    return resampled_img



def calculate_fm(delta_KL_nospec, original_KL, numbasis, sci, model_sci, inputflux = None):
    """
    Calculate what the PSF looks up post-KLIP using knowledge of the input PSF, assumed spectrum of the science target,
    and the partially calculated KL modes (\Delta Z_k^\lambda in Laurent's paper). If inputflux is None,
    the spectral dependence has already been folded into delta_KL_nospec (treat it as delta_KL)

    Args:
        delta_KL_nospec: perturbed KL modes but without the spectral info. delta_KL = spectrum x delta_Kl_nospec.
                         Shape is (numKL, wv, pix). If inputflux is None, delta_KL_nospec = delta_KL
        orignal_KL: unpertrubed KL modes (array of size [numbasis, numpix])
        numbasis: array of KL mode cutoffs
                If numbasis is [0] the number of KL modes to be used is automatically picked based on the eigenvalues.
        sci: array of size p representing the science data
        model_sci: array of size p corresponding to the PSF of the science frame
        input_spectrum: array of size wv with the assumed spectrum of the model

    Returns:
        fm_psf: array of shape (b,p) showing the forward modelled PSF
        klipped_oversub: array of shape (b, p) showing the effect of oversubtraction as a function of KL modes
        klipped_selfsub: array of shape (b, p) showing the effect of selfsubtraction as a function of KL modes
        Note: psf_FM = model_sci - klipped_oversub - klipped_selfsub to get the FM psf as a function of K Lmodes
              (shape of b,p)
    """
    max_basis = original_KL.shape[0]
    if numbasis[0]==0:
        numbasis_index = [max_basis-1]
    else:
        numbasis_index = numpy.clip(numbasis - 1, 0, max_basis-1)

    # remove means and nans from science image
    #print("c")
    sci_mean_sub = sci - numpy.nanmean(sci)
    sci_nanpix = numpy.where(numpy.isnan(sci_mean_sub))
    sci_mean_sub[sci_nanpix] = 0
    sci_mean_sub_rows = numpy.tile(sci_mean_sub, (max_basis,1))
    #sci_rows_selected = numpy.tile(sci_mean_sub, (numpy.size(numbasis),1))


    # science PSF models, ready for FM
    # /!\ JB: If subtracting the mean. It should be done here. not in klip_math since we don't use model_sci there.
    model_sci_mean_sub = model_sci # should be subtracting off the mean?
    model_nanpix = numpy.where(numpy.isnan(model_sci_mean_sub))
    model_sci_mean_sub[model_nanpix] = 0
    model_sci_mean_sub_rows = numpy.tile(model_sci_mean_sub, (max_basis,1))
    # model_rows_selected = numpy.tile(sci_mean_sub, (numpy.size(numbasis),1)) # don't need this because of python behavior where I don't need to duplicate rows


    # calculate perturbed KL modes based on spectrum
    if inputflux is not None:
        delta_KL = numpy.dot(inputflux, delta_KL_nospec) # this will take the last dimension of input_spectrum (wv) and sum over the second to last dimension of delta_KL_nospec (wv)
    else:
        delta_KL = delta_KL_nospec

    # Forward model the PSF
    # 3 terms: 1 for oversubtracton (planet attenauted by speckle KL modes),
    # and 2 terms for self subtraction (planet signal leaks in KL modes which get projected onto speckles)
    oversubtraction_inner_products = numpy.dot(model_sci_mean_sub_rows, original_KL.T)
    selfsubtraction_1_inner_products = numpy.dot(sci_mean_sub_rows, delta_KL.T)
    selfsubtraction_2_inner_products = numpy.dot(sci_mean_sub_rows, original_KL.T)

    lower_tri = numpy.tril(numpy.ones([max_basis,max_basis]))
    oversubtraction_inner_products = oversubtraction_inner_products * lower_tri
    selfsubtraction_1_inner_products = selfsubtraction_1_inner_products * lower_tri
    selfsubtraction_2_inner_products = selfsubtraction_2_inner_products * lower_tri

    klipped_oversub = numpy.dot(numpy.take(oversubtraction_inner_products, numbasis_index, axis=0), original_KL)
    klipped_selfsub = numpy.dot(numpy.take(selfsubtraction_1_inner_products, numbasis_index, axis=0), original_KL) + \
                      numpy.dot(numpy.take(selfsubtraction_2_inner_products,numbasis_index, axis=0), delta_KL)

    return model_sci - klipped_oversub - klipped_selfsub, klipped_oversub, klipped_selfsub



