import numpy
import glob
import pyklip.instruments.GPI as gpi
import pyklip.parallelized as par
import pyfits
import pickle

def test_J():
    x = glob.glob('/home/parriaga/hr4796a_new/april_j/*podc*.fits')
    x = sorted(x)
    a = gpi.GPIData(x)
    par.klip_dataset(a, outputdir = '.', fileprefix = 'S20140422J', annuli = 1, minrot = 5, subsections = 1, numbasis = 1, mode = 'ADI', numthreads = 1, savebasis = True)
    alignedcube = 'S20140422J-KLmodes-all.fits'
    newfile = 'S20140422J-KLmodes-KL5.fits'
    dataset = pyfits.open(alignedcube)
    dat = dataset[1].data[0]
    dat[0:40] = numpy.nan
    dat[-40:] = numpy.nan
    dat[:, 0:40] = numpy.nan
    dat[:,-40:] = numpy.nan
    dataset[1].data = dat
    dataset.writeto(newfile)
    nrads = 1
    nsubs = 1
    nims = len(x)
    fnamere = 'basis/basisJ'
    fnames = [[['basis/s' + str(r) + 'a' + str(p) + 'i' + str(i) + 'basis.p' for r in range(1)] for p in range(1)] for i in range(nims)]
    for i in range(nims):
        bigarr = []
        bigcoordsarr = []
        for rad in range(nrads):
            subarr = []
            coordsarr = []
            for subs in range(nsubs):
                fname = fnames[i][subs][rad]
                f = open(fname)
                vals = pickle.load(f)
                coords = pickle.load(f)
                goodinds = pickle.load(f)
                subarr.append(vals)
                coordsarr.append(coords)
            bigarr.append(subarr)
            bigcoordsarr.append(coordsarr)            
        fname = fnamere + str(i) + '.p'
        dumper = open(fname, 'wb')
        pickle.dump(bigarr, dumper)
        pickle.dump(bigcoordsarr, dumper)
        pickle.dump(goodinds, dumper)
    




def test_K(fm = True):
    x = glob.glob('/home/parriaga/hr4796a_new/april_k/*podc*.fits')
    x = sorted(x)
    a = gpi.GPIData(x)

    par.klip_dataset(a, outputdir = '.', fileprefix = 'S20140422K', annuli = 1, minrot = 5, subsections = 1, numbasis = 1, mode = 'ADI', numthreads = 1)
    alignedcube = 'S20140422K-KLmodes-all.fits'
    newfile = 'S20140422K-KLmodes-KL1.fits'
    dataset = pyfits.open(alignedcube)

    dat = dataset[1].data[0]
#    dat[0:40] = numpy.nan
#    dat[-40:] = numpy.nan
#    dat[:, 0:40] = numpy.nan
#    dat[:,-40:] = numpy.nan
    dataset[1].data = dat
    dataset.writeto(newfile)
    
    if fm != True:
        nrads = 1
        nsubs = 1
        fnamere = 'basis/basisK'
        nims = len(x)
        fnames = [[['basis/s' + str(r) + 'a' + str(p) + 'i' + str(i) + 'basis.p' for r in range(1)] for p in range(1)] for i in range(nims)]
        for i in range(nims):
            bigarr = []
            bigcoordsarr = []
            for rad in range(nrads):
                subarr = []
                coordsarr = []
                for subs in range(nsubs):
                    fname = fnames[i][subs][rad]
                    f = open(fname)
                    vals = pickle.load(f)
                    coords = pickle.load(f)
                    goodinds = pickle.load(f)
                    subarr.append(vals)
                    coordsarr.append(coords)
                bigarr.append(subarr)
                bigcoordsarr.append(coordsarr)            
            fname = fnamere + str(i) + '.p'
            dumper = open(fname, 'wb')
            pickle.dump(bigarr, dumper)
            pickle.dump(bigcoordsarr, dumper)
            pickle.dump(goodinds, dumper)
