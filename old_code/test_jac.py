# code for testing jacobian of transformation between disk coordinates
# and sky plane
#
# Mike Fitzgerald (mpfitz@ucla.edu)  2015-9-18

import numpy as n
import matplotlib as mpl
import pylab

# geometric parameters
omega = 30.*n.pi/180. # [rad]
Omega = 80.*n.pi/180. # [rad]
I = 0#80.*n.pi/180. # [rad]
offset = 0#8. # [pix]

# compute some useful quantities
so, co = n.sin(omega), n.cos(omega)
sO, cO = n.sin(Omega), n.cos(Omega)
si, ci = n.sin(I), n.cos(I)
oN = offset * (cO*co-sO*so*ci) # [AU]
oE = offset * (sO*co+cO*so*ci) # [AU]
oz = offset * so*si # [AU]


def rnu_to_NEzr(r, nu):
    "geometric transformation"
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si

    pN -= oN
    pE -= oE
    pz -= oz

    r_star = n.sqrt(pN**2 + pE**2 + pz**2)

    return pN, pE, pz, r_star

# create grid for disk
rmin, rmax = 40., 60. # [pix], [pix]
nr = 50
dr = (rmax-rmin)/nr
ds = 2.*n.pi*rmax/4000 # [pix] arc length

# transform from disk coords to sky plane
# NOTE  thrown away, only for making grid
pN, pE, pz, r_star = rnu_to_NEzr(rmax, n.linspace(0., 2.*n.pi, 100))

#print pN
#print pE
#print pz
#print r_star

# create grid for receiving flux
N, E = n.mgrid[int(pN.min())-10:int(pN.max())+10,
               int(pE.min())-10:int(pE.max())+10]
N = N.astype(n.float)
E = E.astype(n.float)
Nmin, dN = N.min(), N[1,0]-N[0,0]
Emin, dE = E.min(), E[0,1]-E[0,0]
print Nmin, dN
print Emin, dE


# compute observed image
obsim = n.zeros(N.shape, dtype=n.float)
sig = 1./2.3548 # [pix] gaussian width


obsim = (((N**2. + E**2.) > rmin**2) & ((N**2 + E**2) < rmax**2)).astype(n.float)
print n.max(obsim)

#rr = n.linspace(rmin, rmax, nr)
#for r in rr:
#    nnu = int(n.round(2.*n.pi*r/ds))
#    nus = n.linspace(0., 2.*n.pi, nnu)##
#
    # transform from disk coords to sky plane
#    pN, pE, pz, r_star = rnu_to_NEzr(r, nus)

#    for nn, ee, rs in zip(pN, pE, r_star):
#        obsim += 1./rs**2 * n.exp(-((nn-N)**2+(ee-E)**2)/2./sig**2)
    
# show observed image
fig = pylab.figure(0)
fig.hold(True)
fig.clear()
ax = fig.add_subplot(111)
ax.imshow(obsim,
          extent=(E.min(), E.max(),
                  N.min(), N.max()),
          interpolation='nearest',
          )
ax.set_xlim(E.max(), E.min())
ax.set_xlabel(r"$\alpha$ [pix]")
ax.set_ylabel(r"$\delta$ [pix]")
pylab.draw()
pylab.show()


# interpolation grid
rimin, rimax = rmin-5., rmax+5. # [pix], [pix]
nri, nnui = 20, 50
dri = (rimax-rimin)/nri
rri, nuis = n.mgrid[0:nri, 0:nnui]
rri = rri*dri+rimin # [pix]
nuis = nuis*2.*n.pi/nnui

# coordinates in observed image
pN, pE, pz, r_star = rnu_to_NEzr(rri, nuis)

# perform interpolation
from scipy.ndimage import map_coordinates
iy = (pN-Nmin)/dN
ix = (pE-Emin)/dE
im = map_coordinates(obsim, (iy, ix))

# multiply by jacobian
detJ = rri*ci
im *= detJ

# multiply by r_star^2
im *= r_star**2

# show recovered map
fig = pylab.figure(1)
fig.clear()
fig.hold(True)
ax = fig.add_subplot(111)
ax.imshow(im,
          extent=(nuis.min()*180./n.pi,
                  nuis.max()*180./n.pi,
                  rri.min(), rri.max()),
          interpolation='nearest',
          )
ax.set_aspect('auto')
ax.set_xlabel(r"$\nu$ [deg]")
ax.set_ylabel(r"$r$ [pix]")
pylab.draw()
pylab.show()
