import pyklip.parallelized as par
import pyklip.klip as klip
import glob
import pyklip.instruments.GPI as gpi
import pyfits
import matplotlib.pyplot as plot
import numpy
from hr4796a import OffsetRingModel
from hr4796a import KLIPPolModelComparator
import pickle
from scipy.interpolate import splrep, splev
import pyfits









eval_pspl = lambda phi, tck: splev(phi % (2.*numpy.pi), tck)
def polstuff():
    n_step = 200
    nu = numpy.linspace(-numpy.pi, 2. * numpy.pi, n_step)
    
    jmc = pickle.load(open('hr4796-fitter-jpol-mcmc.dat'))
    jchain = jmc.chain
    nwalker,nchain,nparm = jchain.shape
    jchain = jchain.reshape(nwalker * nchain, nparm)
    jprob = jmc.lnprobability
    jbestparms = jchain[numpy.argmax(jprob)]
    jmc.set_fit_parms(jbestparms)
    jparms = jmc.ringmodel.parms
    jpolmodel = jmc.get_model()[1]
    pyfits.writeto('jpolmodel.fits', jpolmodel[0])


    kmc = pickle.load(open('hr4796-fitter-kpol-mcmc.dat'))
    kchain = kmc.chain
    kprob = kmc.lnprobability
    nwalker,nchain,nparm = kchain.shape
    kchain = kchain.reshape(nwalker * nchain, nparm)
    kbestparms = kchain[numpy.argmax(kprob)]
    kmc.set_fit_parms(kbestparms)
    kparms = kmc.ringmodel.parms
    kpolmodel = kmc.get_model()[1]
    pyfits.writeto('kpolmodel.fits', kpolmodel[0])
    




from image import radial_profile
def rpol_map():
    jstokes = pyfits.getdata('S20140422S0265_podc_stokesdc.fits')
    kstokes = pyfits.getdata('S20140422S0308_podc_stokesdc.fits')
    jq = jstokes[3]
    kq = kstokes[3]
    jq[numpy.abs(jq) > 200] = 0
    kq[numpy.abs(kq) > 200] = 0
    r,prof,jradq = radial_profile(jq, [140,140],return_im = True)
    r,prof,kradq = radial_profile(kq, [140,140],return_im = True)
    pyfits.writeto('jradialq.fits', numpy.array(jradq))
    pyfits.writeto('kradialq.fits', numpy.array(kradq))

    


def get_stdim(rprof):
    y, x = n.mgrid[0:fim_P.shape[0],
                   0:fim_P.shape[1]]
    y -= cen[0]
    x -= cen[1]
    R = n.sqrt(x**2+y**2)
    stdmap = n.zeros_like(fim_P)
    for r, s in zip(rprof, sprof):
        wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
        stdmap[wr] = s
        

    # extract relevant area                                                                                                                                                 
    stdim_P = stdmap[w_s]
    
    return tfim_P_orig, stdim_P


def injection_check():
    x = glob.glob('april_k/*podc*.fits')
    a = gpi.GPIData(x)
    frames = a.input
    cents = a.centers
    pas = a.PAs
    head = a.wcs
    model = pyfits.getdata('tot_k_model.fits')  / 2.5
    inject_from_image(frames, cents, model, head, 33.)
    a.input = frames
    par.klip_dataset(a, fileprefix = 'injected_k', annuli = 1, subsections = 1, minrot = 10, numbasis = [2], mode = 'ADI')


def injection_flux_check():
    x = glob.glob('april_k/*podc*.fits')
    a = gpi.GPIData(x)
    frames = a.input
    pas = a.PAs
    head = a.wcs
    cents = a.centers
    model = pyfits.getdata('tot_k_model.fits') 
    inject_from_image(frames, cents, model , head, 150)
    a.input = frames
    par.klip_dataset(a, fileprefix = 'injected_aggressive_many', annuli = 5, subsections = 5, minrot = 10, numbasis = [1,2,3,4,5,10,20,50], mode = 'ADI')



def check():
    a = pyfits.getdata('injected_k-KLmodes-all.fits')
    b = pyfits.getdata('tot_k_model_klip.fits') #/ 2.5
    brot = klip.rotate(b, 90-33, [140,140],flipx = True)
    plot.imshow(a[0] + brot,vmin = -20, vmax = 175)
    plot.show()


def test3():
    mc = pickle.load(open('actually_working_fits/hr4796-fitter-Jintonly-mcmc.dat'))
    scale = 1.
    im = mc.ringmodel
    model = im.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = .3, n_step = 600, fast = True)
    print numpy.shape(model)
    plot.imshow(model[0][0])
    plot.show()
    pyfits.writeto('unconvolved_j.fits', model[0][0])


def covert_pa_to_image_polar(pa, astr_hdr):
    """
    Given a parallactic angle (angle from N to Zenith rotating in the Eastward direction), calculate what
    polar angle theta (angle from +X CCW towards +Y) it corresponds to

    Input:
        pa: parallactic angle in degrees
        astr_hdr: wcs astrometry header (astropy.wcs)

    Output:
        theta: polar angle in degrees
    """
    rot_det = astr_hdr.wcs.cd[0,0] * astr_hdr.wcs.cd[1,1] - astr_hdr.wcs.cd[0,1] * astr_hdr.wcs.cd[1,0]
    if rot_det < 0:
        rot_sgn = -1.
    else:
        rot_sgn = 1.
    #calculate CCW rotation from +Y to North in radians
    rot_YN = numpy.arctan2(rot_sgn * astr_hdr.wcs.cd[0,1],rot_sgn * astr_hdr.wcs.cd[0,0])
    #now that we know where north it, find the CCW rotation from +Y to find location of planet
    rot_YPA = rot_YN - rot_sgn*pa*numpy.pi/180. #radians
    #rot_YPA = rot_YN + pa*numpy.pi/180. #radians

    theta = rot_YPA * 180./numpy.pi + 90.0 #degrees
    return theta

    


def inject_from_image(frames, cents, model, headers, pa):
    """
    Input:
        frames - set of images onto which disk will be injected
        cents - center of each image
        mim - a centered model image, unrotated 
        headers - fits headers
    Outputs:
        Injected frames written to frames
    """
    modelsize = numpy.shape(model)
    for frame, cent, hdr in zip(frames, cents, headers):
        # Change position angle into frame of image
        theta = -covert_pa_to_image_polar(pa , hdr)
        #Create canvas
        canvas = numpy.zeros(numpy.shape(frame))
        canvas[0:modelsize[0], 0:modelsize[1]] = model
        #Put centered disk
        canvas = klip.rotate(canvas, theta, [modelsize[1] / 2., modelsize[0] / 2.], new_center = [cent[0], cent[1]])
        canvas[numpy.where(numpy.isnan(canvas))] = 0.
        #Rotate model
        frame += canvas
    




def test1():
    test1 = pyfits.getdata('test1.fits')
    model = pyfits.getdata('model0.fits')
    im = pyfits.getdata('S20140422J-KLmodes-KL5-masked.fits')
    
    
    f, axarr = plot.subplots(2,2)
    f.suptitle('J band total intensity fits')
    
    axarr[0,0].set_title('Unfiltered Model')
    axarr[0,0].imshow(model,vmin = -30, vmax = 150, cmap = 'hot')
    
    axarr[0,1].set_title('KLIP filtered model')
    axarr[0,1].imshow(test1, vmin = -30, vmax = 150, cmap = 'hot')
    
    axarr[1,0].set_title('Data')
    axarr[1,0].imshow(im, vmin = -30, vmax = 150, cmap = 'hot')
    
    axarr[1,1].set_title('Residuals')
    axarr[1,1].imshow(im - test1, vmin = -30, vmax = 150, cmap = 'hot')
    
    plot.show()





import gpiklip_fm 
from hr4796a import ModelComparator
def test2():
#    psfim = pyfits.getdata('psf_k_short.fits')
#    im = OffsetRingModel(psf_method = 'gaussian')
#    save_fn = 'hr4796-fitter-Kintonly-leastsq2.dat'

#    im.restore_parms(save_fn)

    mc = pickle.load(open('../dat_files/hr4796-fitter-Kint-mcmc.dat'))
    im = mc.ringmodel
    scale = 1.
    model = im.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = 1., n_step = 200, fast = True)
    print model

#    pyfits.writeto('tot_k_model.fits', model[0])
    print numpy.shape(model)
    plot.imshow(model[0][0])
    plot.colorbar()
    plot.show()
    conv = gpiklip_fm.klipmodel(model[0][0], globname = '../reduced_data/april_k/*podc*', basisname = '../basis_files/basis_fm_K/basis')
    pyfits.writeto('tot_k_model_klip.fits', conv)
#    plot.show()
    return model



def get_tck(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*numpy.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (numpy.arange(n_knot)-k)*dt

    c = numpy.zeros(n_knot, dtype=numpy.float)
    c[0:n_param] = params
    c[n_param:n_param+k] = params[0:k]

    return t, c, k





def polprof():
    save_fnk = 'hr4796-fitter-kpol-mcmc.dat'
    save_fnj = 'hr4796-fitter-jpol-mcmc.dat'
    parms_k = pickle.load(open(save_fnk))
    parms_j = pickle.load(open(save_fnj))
    



def polint():
    save_fn_tot = 'hr4796-fitter-jintpsf-leastsq1.dat'
    save_fn_pol = 'hr4796-fitter-jpol-leastsq2.dat'
    n_step = 200



    parms_tot = pickle.load(open(save_fn_tot))
    parms_pol = pickle.load(open(save_fn_pol))

    totint = parms_tot['int_parms']
    polint = parms_pol['pol_int_parms']
    omega = parms_tot['omega']
    r = parms_tot['r']


    nup = numpy.linspace(0, 3.*numpy.pi, n_step) + omega
    deg =(nup-omega) * 180. / 3.14 + 45.

    polintensities = eval_pspl(nup, get_tck(polint)) / r**2. / n_step
    totintensities = eval_pspl(nup, get_tck(totint)) / r**2. / n_step
    plot.plot(nup, polintensities, label = 'Polarized')
    plot.plot(nup, totintensities, label = 'Total')
    plot.legend()
    plot.show()
    plot.plot(nup, polintensities/totintensities)
    plot.show()


def to_phisca(nu, paramdict):
    r = paramdict['r']
    offset = paramdict['offset']
    I = paramdict['I']
    omega = paramdict['omega']
    Omega = paramdict['Omega']
    so, co = numpy.sin(omega), numpy.cos(omega)
    sonu, conu = numpy.sin(omega+nu), numpy.cos(omega+nu)
    sO, cO = numpy.sin(Omega), numpy.cos(Omega)
    si, ci = numpy.sin(I), numpy.cos(I)
    
    #Euler angle transformation
    pN = r * (cO*conu-sO*sonu*ci) # [AU]
    pE = r * (sO*conu+cO*sonu*ci) # [AU]
    pz = r * sonu*si
    
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    
    #Subtract offset angles
    pN -= oN
    pE -= oE
    pz -= oz
    
    r_star = numpy.sqrt(pN**2 + pE**2 + pz**2)
    
    # scattering angle
    phi_sca = numpy.arccos(pz/r_star) # [rad]
    return phi_sca







################################################################################
###############################################################################
def updateparms():
    mc = pickle.load(open('hr4796-fitter-Kint-mcmc_run1.dat'))
    mc.update_int_parms(40)
    p = mc.get_fit_parms()
    mc.ringmodel.parms['int_parms'][32] *= 3.8
    mc.ringmodel.parms['int_parms'][14:18] *= 1.30

    mc.ringmodel.parms['int_parms'][30:32] *= 2.6
    mc.ringmodel.parms['int_parms'][21:23] *= 1.5
    mc.ringmodel.parms['int_parms'][1:3] *= 1.5
    mc.ringmodel.parms['r'] -= 1.3
    mc.ringmodel.parms['r_out'] -= .6
    mc.ringmodel.parms['Omega'] -= 1. * 3.14 / 180.
    mc.ringmodel.parms['int_parms'][:] *= .9
    mc.ringmodel.parms['psf_parms'] -=2.
#    mc.ringmodel.parms['int_parms'][:] *= 1.1
#    mc.ringmodel.parms['psf_parms'] -=.6

    mask = pyfits.getdata('testring.fits')
    ma = numpy.where(mask < .6)
  
    scale = 1
    model = mc.ringmodel.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = 1., n_step = 200, fast = True)
    model[0][0][ma] = 0
    conv = gpiklip_fm.klipmodel(model[0][0], globname = 'april_k/*podc*', basisname = 'basis_fm_K/basis')
    true = pyfits.getdata('S20140422K-forfm-KLmodes-KL2-masked.fits')
    print numpy.nansum((true - conv)**2.)
    plot.subplot(131)
    plot.imshow(true , vmin = -50, vmax = 150, cmap = 'hot')
    plot.subplot(132)
    plot.imshow(conv, vmin = -50, vmax = 150, cmap = 'hot')
    plot.subplot(133)
    plot.imshow(true - conv,cmap = 'hot')
    plot.show()
    plot.imshow(true - conv,cmap = 'hot', vmin = -30, vmax = 30)
    plot.show()
    pyfits.writeto('tot_k_model.fits', model[0][0])
#    print numpy.shape(model)
#    plot.imshow(model[0][0])
#    plot.colorbar()
    pyfits.writeto('tot_k_model_klip.fits', conv)


def updateparmsj():
    mc = pickle.load(open('hr4796-fitter-Jintonly-mcmc.dat'))
    mc.update_int_parms(40)
    mc.ringmodel.parms['int_parms'][32] *= 2
    mc.ringmodel.parms['int_parms'][30:32] *= 2
    mc.ringmodel.parms['int_parms'][15:29] *= 1.1
#    mc.ringmodel.parms['int_parms'][21:23] *= 1.3
#    mc.ringmodel.parms['int_parms'][1:3] *= 1.3

#    mc.show_model()



    im = mc.ringmodel
    scale = 1.
    model = im.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = 1., n_step = 200, fast = True)
    conv = gpiklip_fm.klipmodel(model[0][0], globname = 'april_j/*podc*', basisname = 'basis_fm_J/basis')
    true = pyfits.getdata('S20140422J-forfm-KLmodes-KL2-masked.fits')
    plot.subplot(131)
    plot.imshow(true , vmin = -50, vmax = 200, cmap = 'hot')
    plot.subplot(132)
    plot.imshow(conv, vmin = -50, vmax = 200, cmap = 'hot')
    plot.subplot(133)
    plot.imshow(true - conv, vmin = -50, vmax = 200,cmap = 'hot')
    plot.show()

    pyfits.writeto('tot_j_model.fits', model[0][0])
    print numpy.shape(model)
    plot.imshow(model[0][0])
    plot.colorbar()
    conv = gpiklip_fm.klipmodel(model[0][0], globname = 'april_j/*podc*', basisname = 'basis_fm_J/basis')
    pyfits.writeto('tot_j_model_klip.fits', conv)
