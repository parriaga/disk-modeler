#!/usr/bin/env python
#
# $Id$
#
# Michael Fitzgerald (mpfitz@ucla.edu) 2014-2-5
#
# code for modeling HR 4796A
#

## #---------------------------------------------------------------------------
## # This code loads IPython but modifies a few things if it detects it's running
## # embedded in another IPython session (helps avoid confusion)
try:
    get_ipython
except NameError:
    banner=exit_msg=''
else:
    banner = '*** Nested interpreter ***'
    exit_msg = '*** Back in main IPython ***'
from IPython.terminal.embed import InteractiveShellEmbed
## # Now create the IPython shell instance. Put ipshell() anywhere in your code
## # where you want it to open. 
ipshell = InteractiveShellEmbed(banner1=banner, exit_msg=exit_msg)
## #------------------------------------------------------------------------------

import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
_log = logging.getLogger('hr4796a')


def test_spline():
    '''
    Generates data points of a sin wave and generates the spline interpolation of it
    Uses splev to get the knots and coefficients 
    '''
    from scipy.interpolate import splrep, splev

    # compute a test set of data points
    x = n.linspace(0., 2.*n.pi, 10)
    y = n.sin(x)

    # get spline interpolator info
    t, c, k = splrep(x, y)

    # get spline interpolation
    xx = n.linspace(0., 2.*n.pi, 100)
    yy = splev(xx, (t,c,k))

    # show results
    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    ax.scatter(x, y, c='k')
    ax.plot(xx, yy, c='b')
    ax.scatter(t, c, c='r')

    pylab.draw()
    pylab.show()

    #ipshell() # TEMP



def get_tck(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:n_param+k] = params[0:k]

    return t, c, k

def get_tck2(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:k] = params[-k:]
    c[k:n_param+k] = params

    return t, c, k



def get_tck180(p, k=3):
    '''
    get periodic spline parameters for mirrored past 180deg
    Inputs:
       p:   the anchor points for the spline
       k:   the order of the spline fit
    Outputs:
       t:   the spline's knot vector
       c:   the spline coefficients 
    '''

    # mirror first cell
    ik = int(n.ceil(k/2.))
    params = n.concatenate((p[0:ik], p[k-ik-1::-1], p[ik:]))

    n_param = len(params)

    xx_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/(2*n_param-k-1)

    n_knot = 2*n_param-1 + k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:2*n_param-1] = params[-2::-1]

    return t, c, k

    

from scipy.interpolate import splev
eval_pspl = lambda phi, tck: splev(phi % (2.*n.pi), tck)

def model_fn(phi, params, **kwargs):
    tck = get_tck(params, **kwargs)
    return eval_pspl(phi, tck)

def model_fn2(phi, params, **kwargs):
    tck = get_tck2(params, **kwargs)
    return eval_pspl(phi, tck)

def model_fn180(phi, params, **kwargs):
    tck = get_tck180(params, **kwargs)
    return eval_pspl(phi, tck)




def show_spline(t, c, k, fignum=0):

    nu = n.linspace(t.min(), t.max(), 100)

    fig = pylab.figure(fignum)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    nn = len(t)
    assert len(t) == len(c)

    colors = mpl.rcParams['axes.color_cycle']
    n_c = len(colors)

    for i in range(nn):
        cp = n.zeros_like(c)
        cp[i] = c[i]
        v = splev(nu, (t, cp, k))
        ax.plot(nu, v,
                c=colors[i % n_c],
                ls='--',
                )
        j = n.argmax(v)
        ax.plot((t[i], nu[j]),
                (c[i], v[j]),
                c=colors[i % n_c],
                ls=':',
                )
                

    v = splev(nu, (t, c, k))
    ax.plot(nu, v, c='k', ls='-')
    v2 = splev(nu % (2.*n.pi), (t, c, k))
    ax.plot(nu, v2, c='k', ls='-.')
    ax.scatter(t, c, c='r')

    w = n.nonzero((nu>=0.) & (nu <= 2.*n.pi))[0]
    ax.set_ylim(n.min((v[w].min(), c.min(), 0.)),
                n.max((v[w].max(), c.max())),
                )
    ax.set_autoscale_on(False)
    for loc in (0., n.pi, 2.*n.pi):
        ax.axvline(loc, c='k')

    pylab.draw()
    pylab.show()
    


def test_pspl_model(test_180=False):

    k = 3
    if test_180:
        n_d = 6
        n_p = 6
    else:
        n_d = 8
        n_p = 8
    n_d2 = 10*n_d

    rs = n.random.RandomState(seed=2345)
    if test_180:
        phi = n.linspace(0., n.pi, n_d, endpoint=True)
        r = 3. + rs.randn(n_d)*.4
    else:
        phi = n.linspace(0., 2.*n.pi, n_d, endpoint=False)
        r = 3. + rs.randn(n_d)*.4
        ## r = 3. + rs.randn(n_d/2+1)*.4
        ## r = n.concatenate((r, r[-2:0:-1]))
    x, y = r*n.cos(phi), r*n.sin(phi)

    if test_180:
        mod_fn = model_fn180
        tck_fn = get_tck180
    else:
        mod_fn = model_fn
        tck_fn = get_tck


    from scipy.optimize import leastsq
    def fit_fn(p):
        return r - mod_fn(phi, p, k=k)
    sp = 3.*n.ones(n_p, dtype=n.float)
    p_opt, cov, infodict, msg, ier = leastsq(fit_fn,
                                             sp.copy(),
                                             full_output=True,
                                             )
    print ier, msg



    t, c, k = tck_fn(p_opt, k=k)
    ## print "dt = %f" % (t[1]-t[0])

    tx, ty = c*n.cos(t), c*n.sin(t)

    phi2 = n.linspace(0., 2.*n.pi, n_d2)
    rr = mod_fn(phi2, p_opt)
    xx, yy = rr*n.cos(phi2), rr*n.sin(phi2)


    # show results
    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)

    ax = fig.add_subplot(111)

    ax.scatter(x, y, c='k')
    ax.plot(xx, yy, c='b')

    w = n.nonzero(c)
    ax.plot(tx[w], ty[w], c='r', marker='o')

    ax.set_aspect('equal')

    pylab.draw()
    pylab.show()


    fignum = 2 if test_180 else 1
    show_spline(t, c, k, fignum=fignum)


    #ipshell()
    


def test_psf_model():
    '''
    Test of a gaussian psf. Now deprecated by generated psf
    '''
    from numpy.fft import fft2, ifft2, fftshift, fftfreq

    shape = (128, 128) # image shape
    cen = (n.array(shape)/2.).astype(n.int)


    lam = 2.12e-6 # [m]
    D = 7.7701 # [m]  telescope pupil size
    D *= 9.571/10. # reduction by lyot mask
    pix_scl = 0.01414 # [arcsec/pix]
    as_per_rad = 206265. # [arcsec/rad]

    kscl = lam/D*as_per_rad/pix_scl
    #kscl = 10. # FIXME

    print kscl

    # create zero-padded image
    zim = n.zeros((2*shape[0], 2*shape[1]), dtype=n.float)
    zim[cen[0],cen[1]] = 1.

    # create PSF
    ky, kx = fftfreq(2*shape[0]), fftfreq(2*shape[1])
    r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*kscl**2

    zp = n.clip(1.-n.sqrt(r2), 0., 1.)


    # image to fourier domain
    fzim = fft2(zim)

    # convolved with PSF
    fzim *= zp

    # back to image domain
    im = ifft2(fzim).real

    # undo zero padding region
    im = im[0:shape[0], 0:shape[1]]

    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    ax.imshow(im,
              interpolation='nearest',
              cmap=mpl.cm.gray,
              )

    ax.set_autoscale_on(False)

    ax.scatter((cen[1],), (cen[0],), c='w')

    ax.add_patch(mpl.patches.Circle(cen[::-1],
                                    radius=kscl,
                                    facecolor='none', 
                                                edgecolor='w',
                                    ))

    pylab.draw()
    pylab.show()


    print zim.sum(), im.sum()

    #ipshell() # TEMP


def test_flat_ring_model():
    im = OffsetRingModel()
    im.parms['r_in'] = im.parms['r'] - 10
    im.parms['r_out'] = im.parms['r'] + 10
    im.parms['offset'] = 0
#    im.parms['I'] = 0
    scale = 1.
    model = im.get_model_ring_image([[281,281]],[[140,140]], scale, drad0 = 2, n_step = 200, fast = True)
    return model


def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y

def test_sbpl():

    r = 10.**n.linspace(-1., 3., 500)

    r_0 = 1.
    y_0 = 1.

    r_b = 10.

    gam1 = 10.
    gam2 = -3.



    fig = pylab.figure(0)
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    betas = n.concatenate(((0.,), 10.**n.linspace(-1., 1., 5)))
    for beta in betas:
        ax.plot(r, smooth_broken_power_law(r, r_b, r_0, y_0, gam1, gam2, beta))
    ax.set_xscale('log')
    ax.set_yscale('log')

    pylab.draw()
    pylab.show()



class OffsetRingModel(object):
    '''
    Class for a circular ring that is offset 
    Inputs:
         n_int_parms: The number of anchor points for the intensity spline
         n_images:    The number of wavebands used
         psf_method:  Convolution method. Options are 'gaussian' 'airy' and 'inputim'
         init_pol:    Whether or not to generate a pol image
         psf_ims:     If psf method is inputim, these are the psf images
    '''
    def __init__(self, n_int_parms=9, n_images=1,
                 psf_method='gaussian',
                 init_pol=False, psf_ims = None):

        self.n_images = n_images
        self.psf_method = psf_method
        self.psf_ims = psf_ims

        self.parmlist = ['r', 'r_in', 'r_out', 'gamma_in', 'gamma', 'beta',
                         'offset', 'omega', 'Omega', 'I',
                         'psf_parms', 'offsx', 'offsy',
                         'int_parms', 'pol_int_parms',
                         ]

        self.parms = dict([(k, None) for k in self.parmlist])
        self.parms.update({'r':78.90, # [AU]
                           'r_in':None, # [AU]
                           'r_out':None, # [AU]
                           'gamma_in':0.,
                           'gamma':0.,
                           'beta':0.,
                           'offset':1.5, # [AU]
#                           'omega':-22.3*n.pi/180., # [rad]
#                           'Omega':27.8*n.pi/180., # [rad]
#                           'I':77.*n.pi/180., # [rad]
                           ### TEST
                           'omega':-18*n.pi/180., # [rad]
                           'Omega':33*n.pi/180., # [rad]
                           'I':70.*n.pi/180., # [rad]

                           'psf_parms':n.ones(n_images, dtype=n.float)*21.7, # [pix]
                           'offsy':0., # [pix]
                           'offsx':0., # [pix]
                           'int_parms':n.ones(n_int_parms*n_images, dtype=n.float)*2e7,
                           'pol_int_parms':None,
                           })

        self.is_fixed = dict([(k,False) for k in self.parmlist])
        self.is_fixed['gamma_in'] = True
        self.is_fixed['gamma'] = True
        self.is_fixed['beta'] = True
        self.is_fixed['r_out'] = True
        self.is_fixed['r_in'] = True
        self.is_fixed['pol_int_parms'] = True

        self.parm_lens = dict([(k,1) for k in self.parmlist])
        self.parm_lens['psf_parms'] = n_images
        self.parm_lens['int_parms'] = n_int_parms*n_images

        if init_pol:
            self.parms['pol_int_parms'] = n.ones(n_int_parms*n_images, dtype=n.float) * 1e6
            self.parm_lens['pol_int_parms'] = n_int_parms*n_images
            self.is_fixed['pol_int_parms'] = False

    def save_parms(self, fn):
        with open(fn, 'w') as f:
            pickle.dump(self.parms, f, 2)
            pickle.dump(self.is_fixed, f, 2)
            pickle.dump(self.parm_lens, f, 2)

    def restore_parms(self, fn):
        with open(fn) as f:
            self.parms = pickle.load(f)
            self.is_fixed = pickle.load(f)
            self.parm_lens = pickle.load(f)


    def _get_intensity(self, nu, phi_sca):
        int_parms = self.parms['int_parms']
        omega = self.parms['omega']
        nup = omega+nu
        if self.n_images > 1:
            intensities = [eval_pspl(nup, get_tck(ip)) for ip in n.reshape(int_parms, (self.n_images,len(int_parms)/self.n_images))]
        else:
            intensities = [eval_pspl(nup, get_tck(int_parms))]
        return intensities
    
    def _get_pol_intensity(self, nu, phi_sca):
        pol_int_parms = self.parms['pol_int_parms']
        omega = self.parms['omega']
        nup = omega+nu
        if pol_int_parms is not None:
            if self.n_images > 1:
                pol_intensities = [eval_pspl(nup, get_tck(pip)) for pip in n.reshape(pol_int_parms, (self.n_images, len(pol_int_parms)/self.n_images))]
            else:
                pol_intensities = [eval_pspl(nup, get_tck(pol_int_parms))]
            return pol_intensities
        else: return None

    def print_parms(self):
        for p in self.parmlist:
            if self.parms[p] is None:
                _log.info("%s\t%s" % (p, str(self.parms[p])))
                continue
            if self.parm_lens[p] == 1:
                _log.info("%s\t%f" % (p, self.parms[p]))
            else:
                _log.info("%s\t%s" % (p, str(self.parms[p])))

    def model_ring(self, n_step=500):
        """
        Parametric representation of an eccentric ring.

        Inputs:
          r           [AU]  radius from ring center
          r_out       [AU]  outer radius
          offset      [AU]  offset size in disk plane
          I           [rad] inclination
          omega       [rad] argument of pericenter (for offset direction)
          Omega       [rad] longitude of ascending node
          int_parms         parameters for intensity function, which returns flux/rad
        """

        r = self.parms['r']
        offset = self.parms['offset']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']
        int_parms = self.parms['int_parms']
        pol_int_parms = self.parms['pol_int_parms']


        # get parameteric construction of ring position
        #

        nu = n.linspace(0., 2.*n.pi, n_step, endpoint=False) # [rad]

        so, co = n.sin(omega), n.cos(omega)
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)
        
        #Euler angle transformation
        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        oz = offset * so*si # [AU]

        #Subtract offset angles
        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)
        
        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        # projected radius
        r_proj = n.sqrt(pN**2+pE**2) # [AU]

        # projected PA
        PA_proj = n.arctan2(pE, pN)

        # get parameteric construction of ring intensity
        intensities = self._get_intensity(nu, phi_sca) / r_star**2

        # get parameteric construction of ring intensity
        if pol_int_parms is not None:
            pol_intensities = self._get_pol_intensity(nu, phi_sca) / r_star**2
        else:
            pol_intensities = None

        return nu, r_star, phi_sca, r_proj, PA_proj, intensities, pol_intensities



    def get_model_ring_image(self, shapes, cens, scale,
                             n_step=200,
                             drad0=2., # [AU]
                             fast=False,
                             fast_samp=4,
                             show=False):


        psf_parms = self.parms['psf_parms']
        gamma_in = self.parms['gamma_in']
        gamma = self.parms['gamma']
        beta = self.parms['beta']
        offs = n.array((self.parms['offsy'], self.parms['offsx']))
        offset = self.parms['offset']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']
        psf_ims = self.psf_ims
        
        # get position/intensity profile
        nu, r, phi_sca, r_proj, PA_proj, intensities, pol_intensities = \
            self.model_ring(n_step=n_step)
        dnu = nu[1]-nu[0]
        assert nu[2]-nu[1] == dnu

        r_in, r_mid, r_out = self.parms['r_in'], self.parms['r'], self.parms['r_out']

        # FIXME
        if r_in is None:
            r_in = r_mid
        if r_out is None:
            r_out = r_mid

        n_step_rad = int((r_out-r_in)/drad0)
        drad = (r_out-r_in)/n_step_rad
        
        if n_step_rad > 1:
            r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid
        else:
            n_step_rad = 1
            r_factors = n.array((1.,))

        # broken power-law
        # NOTE  including number of steps so integral over intensity is conserved..
        i_factors = smooth_broken_power_law(r_factors, 1., 1., 1., -gamma_in, -gamma, beta)/r_factors**2/n_step_rad

        # ellipse center at a_in
        so, co = n.sin(omega), n.cos(omega)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)
        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        xecen = -oE
        yecen = -oN


        def get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp):
            intensity = (i_factors[n.newaxis,:]*intensity[:,n.newaxis]).flatten()

            if fast:
                # fast method does not due subpixel positioning
                im = n.zeros(2*n.array(shape)*fast_samp, dtype=n.float)

                for k, (yy, xx) in enumerate(zip(n.round(ypos*fast_samp).astype(n.int),
                                                 n.round(xpos*fast_samp).astype(n.int))):
                    if (yy < 0) or (yy >= 2*shape[0]*fast_samp-1): continue
                    if (xx < 0) or (xx >= 2*shape[1]*fast_samp-1): continue
                    im[yy,xx] += intensity[k]*dnu

                zim = fft2(im)

                # convolve with PSF
                zim *= zp

                # back to image domain
                im = ifft2(zim).real

                # rebin
                im = im.reshape(2*shape[0], fast_samp, 2*shape[1], fast_samp).sum(axis=(1,3))
            else:
                zim = n.zeros(2*n.array(shape), dtype=n.complex)
                for k, (yy, xx) in enumerate(zip(ypos, xpos)):
                    zim += intensity[k]*dnu * \
                           n.exp(-2.*n.pi*1j*(yy*ky[:,n.newaxis]+xx*kx[n.newaxis,:]))

                # convolve with PSF
                zim *= zp

                # back to image domain
                im = ifft2(zim).real


            # undo zero padding region
            im = im[0:shape[0], 0:shape[1]]
            #im /= n_step_rad
            return im


        ims = []
        if pol_intensities is not None:
            pol_ims = []
        for i in range(self.n_images):
            shape = shapes[i]
            cen = cens[i]
            intensity = intensities[i]
            if self.n_images == 1:
                psf_sig = psf_parms
            else:
                psf_sig = psf_parms[i]
            if pol_intensities is not None:
                pol_intensity = pol_intensities[i]
            if self.psf_method == 'inputim':
                assert psf_ims is not None
                psf_im = psf_ims[i]
            sxpos = cen[1]+offs[1] # star x position
            sypos = cen[0]+offs[0] # star y position


            # positions along ellipses
            xpos = sxpos+(r_factors[n.newaxis,:]*(-r_proj*n.sin(PA_proj)+xecen)[:,n.newaxis]-xecen)/scale
            ypos = sypos+(r_factors[n.newaxis,:]*(r_proj*n.cos(PA_proj)-yecen)[:,n.newaxis]+yecen)/scale
            xpos = xpos.flatten()
            ypos = ypos.flatten()


            from numpy.fft import fft2, ifft2, fftshift, fftfreq
            if fast:
                print shape
                print psf_sig
                #quickfix 

                ky, kx = fftfreq(2*shape[0]*fast_samp), fftfreq(2*shape[1]*fast_samp)
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*(psf_sig*fast_samp)**2
            else:
                ky, kx = fftfreq(2*shape[0]), fftfreq(2*shape[1])
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*psf_sig**2
            if self.psf_method == 'gaussian':
                zp = n.exp(-0.5*r2)
            elif self.psf_method == 'airy':
                zp = n.clip(1.-n.sqrt(r2), 0., 1.)
            elif self.psf_method == 'inputim':
                if fast:
                    transfmd = fftshift(fft2(fftshift(psf_im)))
                    canvas = n.zeros((2. * shape[0] * fast_samp, 2. * shape[1] * fast_samp))
                    psfshape = n.shape(psf_im)
                    canvas[shape[0] * fast_samp - psfshape[0] / 2: shape[0] * fast_samp + psfshape[0]/2, 
                           shape[1] * fast_samp - psfshape[1] / 2: shape[1] * fast_samp + psfshape[1]/2] = transfmd
                    zp = fftshift(canvas)
                else:
                    zp = fftshift(fft2(psf_im))
            else:
                raise NotImplementedError


            ims.append(get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp))
            if pol_intensities is not None:
                pol_ims.append(get_model_im(pol_intensity, shape, cen, xpos, ypos, kx, ky, zp))

            if show:
                extent = [-(cen[1]+offs[1])*scale, (shape[1]-1-(cen[1]+offs[1]))*scale,
                          -(cen[0]+offs[0])*scale, (shape[0]-1-(cen[0]+offs[1]))*scale]


                fig = pylab.figure(i)
                fig.clear()
                fig.hold(True)
                if pol_intensities is not None:
                    ax = fig.add_subplot(121)
                    ax2 = fig.add_subplot(122)
                else:
                    ax = fig.add_subplot(111)
                ax.imshow(ims[i],
                          cmap=mpl.cm.gray,
                          interpolation='nearest',
                          extent=extent,
                          )
                ax.set_autoscale_on(False)
                ax.scatter((0.,), (0.,), c='w')
                ax.scatter((-xecen,), (yecen,), c='g')
                ax.set_aspect('equal')

                if pol_intensities is not None:
                    ax2.imshow(pol_ims[i],
                               cmap=mpl.cm.gray,
                               interpolation='nearest',
                               extent=extent,
                               )
                    ax2.set_autoscale_on(False)
                    ax2.scatter((0.,), (0.,), c='w')
                    ax2.scatter((-xecen,), (yecen,), c='g')
                    ax2.set_aspect('equal')

                pylab.draw()
                pylab.show()

        if pol_intensities is not None:
            return ims, pol_ims
        else:
            return ims



def test_image_offset():
    n_images = 2
    rm = OffsetRingModel(n_images=n_images,
                         psf_method='airy',
                         )
    import constants as c
    shape = (128,128)
    cen = (64, 64)
    scale = 50./shape[1] # [AU/pix]
    rm.parms['psf_parms'] = n.ones(n_images,dtype=n.float)*2. # [pix]
    rm.parms['r'] = 10. # [AU]
    rm.parms['r_out'] = 15. # [AU]
    rm.parms['r_in'] = 8. # [AU]
    rm.parms['gamma_in'] = -2.
    rm.parms['gamma'] = 10.
    rm.parms['offset'] = 2.
    #rm.parms['offset'] = 0.
    rm.parms['I'] = 60.*c.dtor # [rad]
    #rm.parms['I'] = 0.*c.dtor # [rad]
    rm.parms['omega'] = 90.*c.dtor # [rad]
    rm.parms['Omega'] = 30.*c.dtor # [rad]
    rm.parms['int_parms'] = n.array([10., 0., 0., 0., 0., 0.,
                                     10., 0., 0., 0., 0., 0.])
    rm.parm_lens['int_parms'] = len(rm.parms['int_parms'])
    #int_parms = n.ones(10, dtype=n.float)
    #rm.parms['int_parms'] = n.sin(n.linspace(0., n.pi/2., 10, endpoint=False))**3
    n_step = 1500
    drad0 = 2.

    im = rm.get_model_ring_image(n_images*[shape], n_images*[cen], scale,
                                 n_step=n_step,
                                 drad0=drad0,
                                 show=True)[0]
    #print im.sum()

    e = rm.parms['offset']/rm.parms['r']
    from kepler import get_apparent_orbit_ellipse
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(rm.parms['r'], e, rm.parms['I'], rm.parms['omega'], rm.parms['Omega'])
    ax = pylab.gca()
    ax.add_artist(mpl.patches.Ellipse((-E0,N0),
                                      width=2.*rm.parms['r']*n.cos(rm.parms['I']),
                                      height=2.*rm.parms['r'],
                                      angle=rm.parms['Omega']*180./n.pi,
                                      color='r',
                                      fill=False,
                                      ))

    pylab.draw()
    pylab.show()


_KeplerianRingModelBase = OffsetRingModel
class KeplerianRingModel(_KeplerianRingModelBase):

    def __init(self, **kwargs):
        # call base class init
        _KeplerianRingModelBase.__init__(self, **kwargs)

        # remove obsolete parameters
        deleted_parms = ['r', 'r_out', 'offset']
        for p in deleted_parms:
            self.parmlist.remove(p)
            del self.parms[p]
            del self.is_fixed[p]
            del self.parm_lens[p]


        # add new parameters
        added_parms = ['a', 'a_out', 'e',
                       ]
        self.parmlist.extend(added_parms)
        self.parms.update({'a':78.90, # [AU]
                           'a_out':None, # [AU]
                           'e':0.082,
                           })
        for p in added_parms:
            self.is_fixed[p] = False
            self.parm_lens[p] = 1

        self.is_fixed['a_out'] = True

    def model_ring(self, n_step=500):
        """
        Parametric representation of an eccentric ring.

        Inputs:
          a           [AU]  semimajor axis
          a_out       [AU]  outer semimajor axis
          e                 eccentricity
          I           [rad] inclination
          omega       [rad] argument of pericenter
          Omega       [rad] longitude of ascending node
          int_parms         parameters for intensity function, which returns flux/rad
        """

        a = self.parms['a']
        #a_out = self.parms['a_out']
        e = self.parms['e']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']
        int_parms = self.parms['int_parms']
        pol_int_parms = self.parms['pol_int_parms']


        # get parameteric construction of ring position
        #

        # eccentric anomaly
        #E = n.linspace(0., 2.*n.pi, n_step, endpoint=False) # [rad]

        # true anomaly
        #nu = 2.*n.arctan2(n.sqrt(1.+e)*n.sin(E/2.), n.sqrt(1.-e)*n.cos(E/2.)) # [rad]
        nu = n.linspace(0., 2.*n.pi, n_step, endpoint=False) # [rad]
        #nu = n.linspace(0., .1, n_step, endpoint=False) # [rad] # TEMP

        # physical radius from star
        r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]

        so, co = n.sin(omega), n.cos(omega)
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        # scattering angle
        phi_sca = n.arccos(pz/r) # [rad]

        # projected radius
        r_proj = n.sqrt(pN**2+pE**2) # [AU]

        # projected PA
        PA_proj = n.arctan2(pE, pN)

        # get parameteric construction of ring intensity
        intensity = self._get_intensity(nu, phi_sca) / r**2

        # get parameteric construction of ring intensity
        if pol_int_parms is not None:
            pol_intensity = self._get_pol_intensity(nu, phi_sca) / r**2
        else:
            pol_intensity = None

        return nu, r, phi_sca, r_proj, PA_proj, intensity, pol_intensity


    def get_model_ring_image(self, shapes, cens, scale,
                             n_step=200, drad0=2.,
                             fast=True,
                             fast_samp=4,
                             show=False):

        psf_parms = self.parms['psf_parms']
        gamma = self.parms['gamma']
        # FIXME  update double power law
        offs = n.array((self.parms['offsy'], self.parms['offsx']))
            
        # construct image
        im = n.zeros(shape, dtype=n.float)

        # get position/intensity profile
        nu, r, phi_sca, r_proj, PA_proj, intensities, pol_intensities = \
            self.model_ring(n_step=n_step)
        dnu = nu[1]-nu[0]
        assert nu[2]-nu[1] == dnu

        a, a_out = self.parms['a'], self.parms['a_out']

        # FIXME  convert to drad0
        if a_out is None:
            da = 0.
            n_step_rad = 1
        else:
            da = a_out-a
            if da < 0.: da = 0.

        if n_step_rad > 1:
            r_factors = n.linspace(1., 1.+da/a, n_step_rad)
        else:
            r_factors = n.array((1.,))

        # ellipse center at a_in
        from kepler import get_apparent_orbit_ellipse
        ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(self.parms['a'], self.parms['e'], self.parms['I'], self.parms['omega'], self.parms['Omega'])
        xecen = E0
        yecen = N0



        def get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp):
            intensity = (((r_factors**(-gamma))*(1./r_factors**2))[n.newaxis,:]*intensity[:,n.newaxis]).flatten()

            if fast:
                # fast method does not due subpixel positioning
                im = n.zeros(2*n.array(shape)*fast_samp, dtype=n.float)

                for k, (yy, xx) in enumerate(zip(n.round(ypos*fast_samp).astype(n.int),
                                                 n.round(xpos*fast_samp).astype(n.int))):
                    if (yy < 0) or (yy >= 2*shape[0]*fast_samp-1): continue
                    if (xx < 0) or (xx >= 2*shape[1]*fast_samp-1): continue
                    im[yy,xx] += intensity[k]*dnu

                zim = fft2(im)

                # convolve with PSF
                zim *= zp

                # back to image domain
                im = ifft2(zim).real

                # rebin
                im = im.reshape(2*shape[0], fast_samp, 2*shape[1], fast_samp).sum(axis=(1,3))

            else:
                zim = n.zeros(2*n.array(shape), dtype=n.complex)
                for k, (yy, xx) in enumerate(zip(ypos, xpos)):
                    zim += intensity[k]*dnu * \
                           n.exp(-2.*n.pi*1j*(yy*ky[:,n.newaxis]+xx*kx[n.newaxis,:]))

                # convolve with PSF
                zim *= zp

                # back to image domain
                im = ifft2(zim).real


            # undo zero padding region
            im = im[0:shape[0], 0:shape[1]]
            im /= n_step_rad

            return im


        ims = []
        if pol_intensities is not None:
            pol_ims = []
        for i in range(self.n_images):
            shape = shapes[i]
            cen = cens[i]
            intensity = intensities[i]
            if self.n_images == 1:
                psf_sig = psf_parms
            else:
                psf_sig = psf_parms[i]
            if pol_intensities is not None:
                pol_intensity = pol_intensities[i]


            sxpos = cen[1]+offs[1] # star x position
            sypos = cen[0]+offs[0] # star y position

            # positions along ellipses
            xpos = sxpos+(r_factors[n.newaxis,:]*(-r_proj*n.sin(PA_proj)+xecen)[:,n.newaxis]-xecen)/scale
            ypos = sypos+(r_factors[n.newaxis,:]*(r_proj*n.cos(PA_proj)-yecen)[:,n.newaxis]+yecen)/scale
            xpos = xpos.flatten()
            ypos = ypos.flatten()


            from numpy.fft import fft2, ifft2, fftshift, fftfreq
            if fast:
                ky, kx = fftfreq(2*shape[0]*fast_samp), fftfreq(2*shape[1]*fast_samp)
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*(psf_sig*fast_samp)**2
            else:
                ky, kx = fftfreq(2*shape[0]), fftfreq(2*shape[1])
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*psf_sig**2
            if self.psf_method == 'gaussian':
                zp = n.exp(-0.5*r2)
            elif self.psf_method == 'airy':
                zp = n.clip(1.-r2, 0., 1.)
            else:
                raise NotImplementedError




            ims.append(get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp))
            if pol_intensities is not None:
                pol_ims.append(get_model_im(pol_intensity, shape, cen, xpos, ypos, kx, ky, zp))

            if show:
                extent = [-(cen[1]+offs[1])*scale, (shape[1]-1-(cen[1]+offs[1]))*scale,
                          -(cen[0]+offs[0])*scale, (shape[0]-1-(cen[0]+offs[1]))*scale]

                fig = pylab.figure(i)
                fig.clear()
                fig.hold(True)
                if pol_intensities is not None:
                    ax = fig.add_subplot(121)
                    ax = fig.add_subplot(122)
                else:
                    ax = fig.add_subplot(111)
                ax.imshow(ims[i],
                          cmap=mpl.cm.gray,
                          interpolation='nearest',
                          extent=extent,
                          )
                ax.set_autoscale_on(False)
                ax.scatter((0.,), (0.,), c='w')
                ax.set_aspect('equal')

                if pol_intensities is not None:
                    ax2.imshow(pol_ims[i],
                               cmap=mpl.cm.gray,
                               interpolation='nearest',
                               extent=extent,
                               )
                    ax2.set_autoscale_on(False)
                    ax2.scatter((0.,), (0.,), c='w')
                    ax2.set_aspect('equal')

                pylab.draw()
                pylab.show()

        if pol_intensities is not None:
            return ims, pol_ims
        else:
            return ims



# FIXME  make this a class factory applied to OffsetRingModel and KeplerianRingModel?
_PhaseKeplerianRingModelBase = KeplerianRingModel
class PhaseKeplerianRingModel(_PhaseKeplerianRingModelBase):

    def __init__(self, n_int_parms=5,
                 init_pol=False):
        # call base class init
        _PhaseKeplerianRingModelBase.__init__(self, n_int_parms=n_int_parms, init_pol=init_pol)
        self.parms['int_parms'] = n.ones(n_int_parms, dtype=n.float) * 5e8
        self.parm_lens['int_parms'] = n_int_parms
        if init_pol:
            self.parms['pol_int_parms'] = n.ones(n_int_parms, dtype=n.float) * 2e7
            self.parm_lens['pol_int_parms'] = n_int_parms
            self.is_fixed['pol_int_parms'] = False

    def _get_intensity(self, nu, phi_sca):
        int_parms = self.parms['int_parms']
        intensity = eval_pspl(phi_sca, get_tck180(int_parms))
        return intensity

    def _get_pol_intensity(self, nu, phi_sca):
        pol_int_parms = self.parms['pol_int_parms']
        if pol_int_parms is not None:
            pol_intensity = eval_pspl(phi_sca, get_tck180(pol_int_parms))
            return pol_intensity
        else: return None


def test_image_keplerian():
    n_images = 2
    rm = KeplerianRingModel(n_images=n_images,
                            )
    import constants as c
    shape = (128,128)
    cen = (64, 64)
    scale = 50./shape[1] # [AU/pix]
    rm.parms['psf_parms'] = n.ones(n_images,dtype=n.float)*10. # [pix]
    rm.parms['a'] = 10. # [AU]
    rm.parms['a_out'] = 15. # [AU]
    ## e = 0.
    ## I = 0.*c.dtor # [rad]
    ## omega = 0.*c.dtor # [rad]
    ## Omega = 0.*c.dtor # [rad]
    rm.parms['e'] = 0.6
    rm.parms['I'] = 70.*c.dtor # [rad]
    rm.parms['omega'] = 45.*c.dtor # [rad]
    rm.parms['Omega'] = 30.*c.dtor # [rad]
    #int_parms = n.ones(10, dtype=n.float)
    #rm.parms['int_parms'] = n.sin(n.linspace(0., n.pi/2., 10, endpoint=False))**3
    n_step = 1500

    im = rm.get_model_ring_image(n_images*[shape], n_images*[cen], scale,
                                 n_step=n_step, show=True)[0]
    #print im.sum()
    from kepler import get_apparent_orbit_ellipse
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(rm.parms['a'], rm.parms['e'], rm.parms['I'], rm.parms['omega'], rm.parms['Omega'])
    ax = pylab.gca()
    ax.add_artist(mpl.patches.Ellipse((-E0,N0),
                                      width=2.*ap, height=2.*bp,
                                      angle=-phi*180./n.pi,
                                      color='r',
                                      fill=False,
                                      ))
   
    pylab.draw()
    pylab.show()



from scipy.ndimage import gaussian_filter


class ModelComparator(object):
    "model comparator for total intensity"

    def __init__(self, ringmodel, cens, w_ss, labels, ims_I, 
                 mask_rad=38., # [pix]
                 fast_samp=4,
                 fast=True,
                 **kwargs):
        self.n_images = ringmodel.n_images
        self.ringmodel = ringmodel
        self.cens = cens
        self.w_ss = w_ss
        self.labels = labels

        ## # high-pass filter
        ## self.fims = [self.filter_im_I(im_I) for im_I in ims_I]
        self.fims = ims_I

        # get stdev profile
        from image import radial_stdev_profile
        def get_info(fim, cen, w_s):
            rprof, sprof = radial_stdev_profile(fim, cen)
            y, x = n.mgrid[0:fim.shape[0],
                           0:fim.shape[1]]
            y -= cen[0]
            x -= cen[1]
            R = n.sqrt(x**2+y**2)
            stdmap = n.zeros_like(fim)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
                stdmap[wr] = s


            # extract relevant area
            tfim_orig = fim[w_s]
            stdim = stdmap[w_s]
            tcen = cen - n.array((w_s[0].start,w_s[1].start))

            return tfim_orig, stdim, tcen, R

        self.tfims_orig, self.stdims, self.tcens, self.Rs = [], [], [], []
        for i in range(self.n_images):
            tfim_orig, stdim, tcen, R = get_info(self.fims[i], cens[i], w_ss[i])
            self.tfims_orig.append(tfim_orig)
            self.stdims.append(stdim)
            self.tcens.append(tcen)
            self.Rs.append(R)

        self.set_tfims(mask_rad=mask_rad)


        self.n_step = 500
        self.drad0 = 2. # [AU]
        self.dist = 72.8 # [pc]
        self.pix_scale = .01414 # [arcsec/pix] Konopacky et al. 2014
        self.pix_uncert = 0.3 # [pix]  uncertainty in pixel positioning of star
        self.scale = self.dist*self.pix_scale # [AU/pix]

        self.fast = fast
        self.fast_samp = fast_samp


    @staticmethod
    def filter_im_I(im, i, fwhm=20.):
        im = ma.array(im)
        sig = fwhm/2.3548
        fim = im - gaussian_filter(im.filled(0.), sig)
        return fim


    def set_tfims(self, mask_rad=None, **kwargs):
        self.tfims = copy.deepcopy(self.tfims_orig)
        if mask_rad is not None:
            for i in range(self.n_images):
                # mask near star
                y, x = n.mgrid[0:self.tfims[i].shape[0],
                               0:self.tfims[i].shape[1]]
                y -= self.tcens[i][0]
                x -= self.tcens[i][1]
                self.Rs[i] = n.sqrt(x**2+y**2)

                self.tfims[i][n.nonzero(self.Rs[i] < mask_rad)] = ma.masked
        


    def get_freemodel_parminfo(self):
        parmdict = {}
        parmlist = []
        for k in self.ringmodel.parmlist:
            if self.ringmodel.is_fixed[k]: continue
            parmdict[k] = self.ringmodel.parms[k]
            parmlist.append(k)
        return parmdict, parmlist

    @staticmethod
    def fit_to_freemodel_parminfo(fit_parmdict, fit_parmlist):
        "transform fit parameters to model parameters"
        freemodel_parmlist = []
        freemodel_parmdict = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if k in ('psf_parms', 'int_parms', 'pol_int_parms', 'beta'):
                freemodel_parmdict[k] = n.exp(v)
                freemodel_parmlist.append(k)
            elif k == 'esino':
                assert 'ecoso' in fit_parmlist
                esino, ecoso = v, fit_parmdict['ecoso']
                e = n.sqrt(esino**2+ecoso**2)
                omega = n.arctan2(esino, ecoso)
                freemodel_parmdict['e'] = e
                freemodel_parmdict['omega'] = omega
                freemodel_parmlist.extend(['e', 'omega'])
            elif k == 'ecoso':
                continue
            elif k == 'osino':
                assert 'ocoso' in fit_parmlist
                osino, ocoso = v, fit_parmdict['ocoso']
                offset = n.sqrt(osino**2+ocoso**2)
                omega = n.arctan2(osino, ocoso)
                freemodel_parmdict['offset'] = offset
                freemodel_parmdict['omega'] = omega
                freemodel_parmlist.extend(['offset', 'omega'])
            elif k == 'ocoso':
                continue
            else:
                freemodel_parmdict[k] = v
                freemodel_parmlist.append(k)
        return freemodel_parmdict, freemodel_parmlist

    @staticmethod
    def freemodel_to_fit_parminfo(freemodel_parmdict, freemodel_parmlist):
        "transform model parameters to fit parameters"
        fit_parmlist = []
        fit_parmdict = {}
        for k in freemodel_parmlist:
            v = freemodel_parmdict[k]
            if k in ('psf_parms', 'int_parms', 'pol_int_parms', 'beta'):
                fit_parmdict[k] = n.log(v)
                fit_parmlist.append(k)
            elif k == 'e':
                assert 'omega' in freemodel_parmlist
                e, omega = v, freemodel_parmdict['omega']
                esino, ecoso = e*n.sin(omega), e*n.cos(omega)
                fit_parmdict['esino'] = esino
                fit_parmdict['ecoso'] = ecoso
                fit_parmlist.extend(['esino', 'ecoso'])
            elif k == 'offset':
                assert 'omega' in freemodel_parmlist
                offset, omega = v, freemodel_parmdict['omega']
                osino, ocoso = offset*n.sin(omega), offset*n.cos(omega)
                fit_parmdict['osino'] = osino
                fit_parmdict['ocoso'] = ocoso
                fit_parmlist.extend(['osino', 'ocoso'])
            elif k == 'omega':
                continue
            else:
                fit_parmdict[k] = v
                fit_parmlist.append(k)
        return fit_parmdict, fit_parmlist

    def get_fit_parms(self, get_parmlist=False):
        "take free parameters and construct 1-d array"

        # get current model parameter dictionary
        freemodel_parmdict, freemodel_parmlist = self.get_freemodel_parminfo()

        # transform model parameter dictionary to fit parameter dictionary
        fit_parmdict, fit_parmlist = self.freemodel_to_fit_parminfo(freemodel_parmdict, freemodel_parmlist)

        # construct 1-d array from fit dictionary
        parms = []
        parm_lens = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if n.isscalar(v): v = (v,)
            parms.append(v)
            parm_lens[k] = len(v)
        if get_parmlist:
            return n.concatenate(parms), fit_parmlist, parm_lens
        else:
            return n.concatenate(parms)

    def set_fit_parms(self, fit_parms):
        "use free parameters to update internal values"
        current_fit_parms, fit_parmlist, fit_parm_lens = self.get_fit_parms(get_parmlist=True)

        # unpack fit parameter array into dictionary
        i = 0
        fit_parmdict = {}
        for p in fit_parmlist:
            pl = fit_parm_lens[p]
            if pl == 1:
                fit_parmdict[p] = fit_parms[i]
            else:
                fit_parmdict[p] = fit_parms[i:i+pl]
            i += pl
        assert i == len(fit_parms)

        # transform fit parameter dictionary into model parameter dictionary
        freemodel_parmdict, freemodel_parmlist = self.fit_to_freemodel_parminfo(fit_parmdict, fit_parmlist)

        # udpate model parameters
        self.ringmodel.parms.update(freemodel_parmdict)



    def get_model(self, shapes=None, cens=None):
        "compute a model filtered image"
        fmims = []
        for i in range(self.n_images):
            if shapes is None:
                shapes = [tfim.shape for tfim in self.tfims]
            if cens is None:
                cens = self.tcens
            # compute model image
            mims = self.ringmodel.get_model_ring_image(shapes, cens, self.scale, n_step=self.n_step, drad0=self.drad0, fast=self.fast, fast_samp=self.fast_samp)
            # filter model image
            fmims = [self.filter_im_I(mim, i) for i, mim in enumerate(mims)]
        return fmims

    def get_residuals(self, p):
        "1-d residual array"
        self.set_fit_parms(p)
        fmims = self.get_model()
        r = ((ma.array(self.tfims)-ma.array(fmims))/ma.array(self.stdims)).compressed()
        return r


    def get_gaussian_priors(self):
        "1-d gaussian prior residual array"
        parmdict, parmlist = self.get_freemodel_parminfo()
        priors = []
        for p, v in parmdict.iteritems():
            ## if p == 'r_in':
            ##     priors.append((v-73.)/10.)
            ## if p in ('a','r'):
            ##     priors.append((v-79.2)/10.)
            ## elif p in ('a_out','r_out'):
            ##     priors.append((v-83.)/5.)
            ## elif p=='I':
            if p=='I':
                priors.append((v*180./n.pi-76.7)/5.)
            elif p=='gamma_in':
                priors.append(v/15.)
            elif p=='gamma':
                priors.append(v/5.)
            elif p in ('r'):
                priors.append((v-77.4)/10.)
            elif p in ('offsy','offsx'):
                priors.append(v/self.pix_uncert)
            elif p == 'psf_parms':
                vv = (v-5.)/1.
                if self.n_images == 1:
                    priors.append(vv)
                else:
                    priors.extend(vv.tolist())
        return n.array(priors)


    def do_leastsq(self):

        # get starting parameters based on current model state
        sp = self.get_fit_parms()

        def fit_fn(p):
            resid = self.get_residuals(p)
            priors = self.get_gaussian_priors()
            self.ringmodel.print_parms()
            return -n.concatenate((resid, priors))

        # run fit
        from scipy.optimize import leastsq
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              #args=extra_args,
                                              #epsfcn=1e-7,
                                              #factor=3.,
                                              epsfcn=1e-3,
                                              full_output=True,
                                              )
        _log.info("%d: %s" % (ier, mesg))
        #p_opt = sp # TEMP

        self.leastsq_vals = (p_opt, cov, info, mesg, ier)

        return self.leastsq_vals



    def report_leastsq(self):
        "print out best-fit parameters with uncertainties"

        assert hasattr(self, 'leastsq_vals')
        p_opt, cov, info, mesg, ier = self.leastsq_vals
        try:
            var = n.diag(cov)
        except:
            _log.error('no covariance matrix!')
            return

        current_fit_parms, fit_parmlist, fit_parm_lens = self.get_fit_parms(get_parmlist=True)
        fit_parms = p_opt


        # unpack fit parameter array into dictionary
        i = 0
        fit_parmdict = {}
        fit_parmvardict = {}
        ecosoesino = []
        ocosoosino = []
        for p in fit_parmlist:
            if p in ('ecoso','esino'): ecosoesino.append(i)
            if p in ('ocoso','osino'): ocosoosino.append(i)
            pl = fit_parm_lens[p]
            if pl == 1:
                fit_parmdict[p] = fit_parms[i]
                fit_parmvardict[p] = var[i]
            else:
                fit_parmdict[p] = fit_parms[i:i+pl]
                fit_parmvardict[p] = var[i:i+pl]
            i += pl
        assert i == len(fit_parms)


        # transform fit parameters to model parameters
        freemodel_parmlist = []
        freemodel_parmdict = {}
        freemodel_parmvardict = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if k in ('psf_parms', 'int_parms', 'pol_int_parms'):
                freemodel_parmdict[k] = n.exp(v)
                freemodel_parmvardict[k] = n.exp(v)**2 * fit_parmvardict[k]
                freemodel_parmlist.append(k)
            elif k == 'esino':
                assert 'ecoso' in fit_parmlist
                esino, ecoso = v, fit_parmdict['ecoso']
                e = n.sqrt(esino**2+ecoso**2)
                omega = n.arctan2(esino, ecoso)
                freemodel_parmdict['e'] = e
                freemodel_parmdict['omega'] = omega

                c = cov[ecosoesino[0],ecosoesino[1]]
                freemodel_parmvardict['e'] = (ecoso**2 * fit_parmvardict['ecoso'] + esino**2 * fit_parmvardict['esino'] + 2.*ecoso*esino*c)/e**2

                freemodel_parmvardict['omega'] = (esino**2 * fit_parmvardict['ecoso'] + ecoso**2 * fit_parmvardict['esino'] - 2.*ecoso*esino*c)/e**4

                freemodel_parmlist.extend(['e', 'omega'])
            elif k == 'ecoso':
                continue
            elif k == 'osino':
                assert 'ocoso' in fit_parmlist
                osino, ocoso = v, fit_parmdict['ocoso']
                offset = n.sqrt(osino**2+ocoso**2)
                omega = n.arctan2(osino, ocoso)
                freemodel_parmdict['offset'] = offset
                freemodel_parmdict['omega'] = omega

                c = cov[ocosoosino[0],ocosoosino[1]]
                freemodel_parmvardict['offset'] = (ocoso**2 * fit_parmvardict['ocoso'] + osino**2 * fit_parmvardict['osino'] + 2.*ocoso*osino*c)/offset**2

                freemodel_parmvardict['omega'] = (osino**2 * fit_parmvardict['ocoso'] + ocoso**2 * fit_parmvardict['osino'] - 2.*ocoso*osino*c)/offset**4

                freemodel_parmlist.extend(['offset', 'omega'])
            elif k == 'ocoso':
                continue
            else:
                freemodel_parmdict[k] = v
                freemodel_parmvardict[k] = fit_parmvardict[k]
                freemodel_parmlist.append(k)


        # which parameters to report
        #parms = ['psf_parms', 'offsy', 'offsx', 'r', 'r_out', 'gamma_in', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        # FIXME
        parms = ['offsy', 'offsx', 'r', 'r_in', 'r_out', 'gamma_in', 'gamma', 'beta', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        for p in copy.copy(parms):
            if p not in self.ringmodel.parmlist:
                parms.pop(parms.index(p))
                continue
            if self.ringmodel.is_fixed[p]:
                parms.pop(parms.index(p))
        angle_parms = ('I','omega','Omega')
        for parm in parms:
            fact = 180./n.pi if parm in angle_parms else 1.
            _log.info("%s:\t%f\t+/- %f" % (parm,
                                           freemodel_parmdict[parm]*fact,
                                           n.sqrt(freemodel_parmvardict[parm])*fact,
                                           ))



    def update_stdmap(self):
        "use residuals from current model to recalculate stdev map"

        # get full image model
        shapes = [fim.shape for fim in self.fims]
        cens = self.cens
        mims = self.get_model(shapes=shapes, cens=cens)
        resids = [fim-mim for fim, mim in zip(self.fims, mims)]
        from image import radial_stdev_profile
        for i in range(self.n_images):
            rprof, sprof = radial_stdev_profile(resids[i], self.cens[i],
                                                #robust=False,
                                                )

            # compute thumbnail stdmap
            stdmap = n.zeros(self.tfims[i].shape, dtype=n.float)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((self.Rs[i] >= r-0.5) & (self.Rs[i] < r+0.5))
                stdmap[wr] = s
            self.stdims[i][:] = stdmap


    def update_int_parms(self, n_int_parm):
        "update intensity parameters with new number of parameters"

        if n_int_parm*self.n_images == len(self.ringmodel.parms['int_parms']):
            return

        fit_pol = self.ringmodel.parms['pol_int_parms'] is not None


        from scipy.optimize import leastsq
        nu = n.linspace(0., 2.*n.pi, 100) # [rad]
        phi_sca = n.linspace(0., n.pi, 50) # [rad]
        curr_int = self.ringmodel._get_intensity(nu, phi_sca)
        if fit_pol:
            curr_pol_int = self.ringmodel._get_pol_intensity(nu, phi_sca)
            curr = n.concatenate(curr_int+curr_pol_int)
        else:
            curr = n.concatenate(curr_int)
        def fit_fn(p):
            if fit_pol:
                np = len(p)
                self.ringmodel.parms['int_parms'] = n.exp(p[0:np/2])
            else:
                self.ringmodel.parms['int_parms'] = n.exp(p)
            mod_int = self.ringmodel._get_intensity(nu, phi_sca)
            if fit_pol:
                self.ringmodel.parms['pol_int_parms'] = n.exp(p[np/2:])
                mod_pol_int = self.ringmodel._get_pol_intensity(nu, phi_sca)
                return curr-n.concatenate(mod_int+mod_pol_int)
            else:
                return curr-n.concatenate(mod_int)
        self.ringmodel.parms['int_parms'] = n.ones(n_int_parm*self.n_images, dtype=n.float)*2e8
        self.ringmodel.parm_lens['int_parms'] = n_int_parm*self.n_images
        if fit_pol:
            self.ringmodel.parms['pol_int_parms'] = n.ones(n_int_parm*self.n_images, dtype=n.float)*2e7
            self.ringmodel.parm_lens['pol_int_parms'] = n_int_parm*self.n_images
            sp = n.log(n.concatenate((self.ringmodel.parms['int_parms'], self.ringmodel.parms['pol_int_parms'])))
        else:
            sp = n.log(self.ringmodel.parms['int_parms'])
        p_opt, ier = leastsq(fit_fn, sp.copy())
        if fit_pol:
            np = len(p_opt)
            self.ringmodel.parms['int_parms'] = n.exp(p_opt[0:np/2])
            self.ringmodel.parms['pol_int_parms'] = n.exp(p_opt[np/2:])
        else:
            self.ringmodel.parms['int_parms'] = n.exp(p_opt)


    def show_model(self):
        fmims = self.get_model()

        # chi2 best fit
        impad_l = 0.6 # [in]
        impad_r = 0.1 # [in]
        impad_b = 0.6 # [in]
        impad_t = 0.4 # [in]
        imwidth = 1.5 # [in]



        for i in range(self.n_images):

            imheight = imwidth * float(self.tfims[i].shape[0])/float(self.tfims[i].shape[1]) # [in]
            fig_width = impad_l + 3*imwidth + impad_r
            fig_height = impad_b + imheight + impad_t
            figsize = (fig_width, fig_height)

            fig = pylab.figure(i, figsize=figsize)
            fig.clear()
            fig.hold(True)
            fig.suptitle(self.labels[i])
            axs = []
            for j in range(3):
                ax = fig.add_axes([(impad_l+j*imwidth)/fig_width,
                                   impad_b/fig_height,
                                   imwidth/fig_width,
                                   imheight/fig_height,
                                   ])
                axs.append(ax)
            kw = {'vmin':-1e2,
                  'vmax':1e3,
                  'interpolation':'nearest',
                  'cmap':mpl.cm.gist_stern,
                  }
            print fmims[i]
            axs[0].imshow(self.tfims[i], **kw)
            axs[1].imshow(fmims[i], **kw)
            axs[2].imshow(self.tfims[i]-fmims[i], **kw)

#            pyfits.writeto('model' + str(i) + '.fits', n.array(fmims[i]))
            # mark star
            for ax in axs:
                ax.set_autoscale_on(False)
                ax.scatter((self.tcens[i][1],), (self.tcens[i][0],), c='k')

            for ax in axs[1:]:
                pylab.setp(ax.get_yticklabels(), visible=False)

            pylab.draw()
            pylab.show()


    def run_MCMC(self, n_walker, n_sample):
        p_opt = self.get_fit_parms()
        n_dim = len(p_opt)
        rs = n.random.RandomState(seed=34523)
        wz = n.nonzero(p_opt==0.)[0]
        pos = []
        for i in range(n_walker):
            p = p_opt*(1. + 3e-2*rs.randn(n_dim))
            p[wz] = 1e-1*rs.randn(len(wz))
            pos.append(p)
        if has_mpi:
            kw = {'pool':pool}
        else:
            import multiprocessing as mp
            n_process = mp.cpu_count()
            kw = {'threads':n_process}
        import emcee
        sampler = emcee.EnsembleSampler(n_walker, n_dim, log_like,
                                        args=(self,),
                                        **kw)
        sampler.run_mcmc(pos, n_sample,
                         rstate0=rs.get_state())

        self.chain = sampler.chain
        self.lnprobability = sampler.lnprobability



    def show_mcmc_results(self, n_burn, mask_rad, pol_mask_rad):
        import constants as c

        chain = self.chain
        self.ringmodel.print_parms()
        ## mim = get_model(p_opt, *extra_args[2:])

        n_walker, n_sample, n_dim = chain.shape

        # process chains
        parmlist = self.ringmodel.parmlist
        pchain = []
        for p in chain.reshape(n_walker*n_sample, n_dim):
            self.set_fit_parms(p)
            vals = [self.ringmodel.parms[k] for k in parmlist]
            pchain.append(tuple(vals))
        names = ','.join(parmlist)
        formats = ['f' if self.ringmodel.parm_lens[k]==1 else 'O' for k in parmlist]
        pchain = n.rec.fromrecords(pchain,
                                   names=names,
                                   formats=formats,
                                   )
        pchain.shape = n_walker, n_sample


        do_pol = self.ringmodel.parms['pol_int_parms'] is not None
        

        # show individual chains
        fig = pylab.figure(self.n_images+1)
        fig.clear()
        fig.hold(True)

        #parms = ['psf_parms', 'offsy', 'offsx', 'r', 'r_out', 'gamma_in', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        # FIXME
        parms = ['offsy', 'offsx', 'r', 'r_in', 'r_out', 'gamma_in', 'gamma',  'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega'] #'beta',
        for p in copy.copy(parms):
            if p not in self.ringmodel.parmlist:
                parms.pop(parms.index(p))
                continue
            if self.ringmodel.is_fixed[p]:
                parms.pop(parms.index(p))
        n_parms = len(parms)

        angle_parms = ('I','omega','Omega')

        axs = [fig.add_subplot(n_parms, 1, i+1) for i in range(n_parms)]

        for j, ax in enumerate(axs):
            if parms[j] in ('I','omega','Omega'):
                fact = 180./n.pi
            else: fact = 1.
            for i in range(n_walker):
                ax.plot(pchain[parms[j]][i,:]*fact, c='k', alpha=.03)
            ax.set_autoscale_on(False)
            ax.set_xlim(0, n_sample)
            if j != n_parms-1:
                pylab.setp(ax.get_xticklabels(), visible=False)
            ax.set_ylabel(parms[j].replace('_','\_'))
            ax.axvline(n_burn, ls=':', c='k')
        pylab.draw()
        pylab.show()



        # show 1 and 2d distributions
        samples = []
        labels = []
        for parm in parms:
            if parm in angle_parms:
                fact = 180./n.pi
            else: fact = 1.
            samples.append(pchain[parm][:,n_burn:].flatten()*fact)
            labels.append(parm.replace('_','\_'))
        samples = n.array(samples).T


        factor = 1.0           # size of one side of one panel
        lbdim = 0.5 * factor   # size of left/bottom margin
        trdim = 0.05 * factor  # size of top/right margin
        whspace = 0.05         # w/hspace size
        plotdim = factor * n_parms + factor * (n_parms - 1.) * whspace
        dim = lbdim + plotdim + trdim


        fig = pylab.figure(figsize=(dim, dim), num=self.n_images+2)
        fig.clear()
        fig, axes = pylab.subplots(n_parms, n_parms, figsize=(dim, dim), num=self.n_images+2)
        import triangle
        frac = 0.98
        #frac = 1.
        triangle.corner(samples, labels=labels, fig=fig,
                        extents=(n.ones(n_parms, dtype=n.float)*frac).tolist(),
                        )


        pylab.draw()
        pylab.show()


        # FIXME
        #  below needs to be adjusted for polarization




        ## # show intensity vs. scattering angle
        ## fig = pylab.figure(self.n_images+4, figsize=(8,4))
        ## fig.clear()
        ## fig.hold(True)
        ## ax1 = fig.add_subplot(121, polar=True)
        ## if do_pol:
        ##     ax2 = fig.add_subplot(222)
        ##     ax3 = fig.add_subplot(224, sharex=ax2)
        ## else:
        ##     ax2 = fig.add_subplot(122)

        ## nu = n.linspace(0., 2.*n.pi, 100)#, endpoint=False)

        ## w = n.s_[::20]
        ## #w = n.s_[::300]
        ## #w = n.s_[::1000]
        ## ## phi_scas = []
        ## ## sc_ints = []
        ## if do_pol:
        ##     zipped = zip(pchain['a'][:,n_burn:].flatten()[w],
        ##                  pchain['e'][:,n_burn:].flatten()[w],
        ##                  pchain['I'][:,n_burn:].flatten()[w],
        ##                  pchain['omega'][:,n_burn:].flatten()[w],
        ##                  pchain['Omega'][:,n_burn:].flatten()[w],
        ##                  pchain['int_parms'][:,n_burn:].flatten()[w],
        ##                  pchain['pol_int_parms'][:,n_burn:].flatten()[w],
        ##                  )
        ## else:
        ##     zipped = zip(pchain['a'][:,n_burn:].flatten()[w],
        ##                  pchain['e'][:,n_burn:].flatten()[w],
        ##                  pchain['I'][:,n_burn:].flatten()[w],
        ##                  pchain['omega'][:,n_burn:].flatten()[w],
        ##                  pchain['Omega'][:,n_burn:].flatten()[w],
        ##                  pchain['int_parms'][:,n_burn:].flatten()[w],
        ##                  )
    
        ## for vals in zipped:
        ##     if do_pol:
        ##         a, e, I, omega, Omega, int_parms, pol_int_parms = vals
        ##     else:
        ##         a, e, I, omega, Omega, int_parms = vals

        ##     # TEMP
        ##     #if (a<75.) or (a>85.): continue

        ##     # FIXME  this depends on model (circular vs. keplerian)

        ##     # physical radius from star
        ##     r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]
        ##     # scattering angle
        ##     sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        ##     sO, cO = n.sin(Omega), n.cos(Omega)
        ##     si, ci = n.sin(I), n.cos(I)
        ##     pN = r*(cO*conu-sO*sonu*ci)
        ##     pE = r*(sO*conu+cO*sonu*ci)
        ##     pz = r * sonu*si
        ##     phi_sca = n.arccos(pz/r) # [rad]
        ##     r_proj = n.sqrt(pN**2+pE**2) # [AU]
        ##     # intensity at given location
        ##     self.ringmodel.parms['int_parms'] = int_parms
        ##     intensity = self.ringmodel._get_intensity(nu, phi_sca) / r**2
        ##     if do_pol:
        ##         pol_intensity = self.ringmodel._get_pol_intensity(nu, phi_sca) / r**2
        ##     # rescale by stellar distance squared 
        ##     wi = n.nonzero(r_proj > mask_rad)[0]
        ##     sc_int = intensity * r**2
        ##     if do_pol:
        ##         wpi = n.nonzero(r_proj > pol_mask_rad)[0]
        ##         sc_pol_int = pol_intensity * r**2



        ##     #ax.scatter(phi_sca, sc_int, facecolor='k', alpha=0.003, marker='.', edgecolor='none')
        ##     kw = {'alpha':0.003, 'marker':'.', 'edgecolor':'none'}
        ##     ax1.scatter(phi_sca[wi], sc_int[wi],
        ##                 facecolor='k', **kw)
        ##     ax1.scatter(2.*n.pi-phi_sca[wi], sc_int[wi],
        ##                 facecolor='k', **kw)
        ##     if do_pol:
        ##         ax1.scatter(phi_sca[wpi], sc_pol_int[wpi],
        ##                     facecolor='r', **kw)
        ##         ax1.scatter(2.*n.pi-phi_sca[wpi], sc_pol_int[wpi],
        ##                     facecolor='r', **kw)

        ##     ax2.scatter(phi_sca[wi]*c.rtod, sc_int[wi],
        ##                 facecolor='k', **kw)
        ##     if do_pol:
        ##         ax2.scatter(phi_sca[wpi]*c.rtod, sc_pol_int[wpi],
        ##                     facecolor='r', **kw)
        ##         ax3.scatter(phi_sca[wi]*c.rtod, sc_pol_int[wi]/sc_int[wi],
        ##                     facecolor='b', **kw)

        ## if do_pol:
        ##     axs = (ax2, ax3)
        ## else:
        ##     axs = (ax2,)
        ## for ax in axs:
        ##     ax.set_xlim(0., 180.)
        ##     ax.axvline(90., ls=':', c='k')
        ## ax2.set_ylim(0., 6e8)
        ## if do_pol:
        ##     ax3.set_ylim(0., 0.55)
        ##     pylab.setp(ax2.get_xticklabels(), visible=False)
        ## ax2.set_ylabel('Intensity')
        ## if do_pol:
        ##     ax3.set_ylabel('Pol. frac.')
        ##     ax3.set_xlabel(r"$\theta$")
        ## else:
        ##     ax2.set_xlabel(r"$\theta$")


        ## ##     phi_scas.append(phi_sca)
        ## ##     sc_ints.append(sc_int)
        ## ## phi_scas = n.concatenate(phi_scas)
        ## ## sc_ints = n.concatenate(sc_ints)
        ## ## with open('sc_ints.dat', 'w') as f:
        ## ##     pickle.dump(phi_scas, f, 2)
        ## ##     pickle.dump(sc_ints, f, 2)

        ## ## # overplot a scaled Rayleigh phase function
        ## ## scl = 3.7e9
        ## ## ax.plot(nu, scl * 3./16./n.pi * (1.+n.cos(nu)**2), c='g')

        ## ## # overplot a scaled Mie phase function
        ## ## m = 1.33+1e-8j
        ## ## x = 6.963e-1
        ## ## phi_sca, sc_int1 = get_mie(x,m)
        ## ## x = 2.99
        ## ## phi_sca, sc_int2 = get_mie(x,m)
        ## ## sc_int = 1.007e9 * sc_int1 + 1.39e8 * sc_int2
        ## ## ax.plot(phi_sca, sc_int, c='b')
        ## ## ax.plot(2.*n.pi-phi_sca, sc_int, c='b')


        ## #ax.set_xlabel(r"$\phi_\mathrm{sca}$ [deg]")
        ## #ax.set_ylabel(r"Intensity $\times r^2$")
        ## #ax.set_xlim(0., n.pi)

        ## pylab.draw()
        ## pylab.show()


    def report_mcmc_results(self, n_burn):
        "print out best-fit parameters with uncertainties"

        chain = self.chain
        n_walker, n_sample, n_dim = chain.shape

        # process chains
        parmlist = self.ringmodel.parmlist
        pchain = []
        for p in chain.reshape(n_walker*n_sample, n_dim):
            self.set_fit_parms(p)
            vals = [self.ringmodel.parms[k] for k in parmlist]
            pchain.append(tuple(vals))
        names = ','.join(parmlist)
        formats = ['f' if self.ringmodel.parm_lens[k]==1 else 'O' for k in parmlist]
        pchain = n.rec.fromrecords(pchain,
                                   names=names,
                                   formats=formats,
                                   )
        pchain.shape = n_walker, n_sample


        # which parameters to report
        #parms = ['psf_parms', 'offsy', 'offsx', 'r', 'r_out', 'gamma_in', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        # FIXME
        parms = ['offsy', 'offsx', 'r', 'r_in', 'r_out', 'gamma_in', 'gamma', 'beta', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        for p in copy.copy(parms):
            if p not in self.ringmodel.parmlist:
                parms.pop(parms.index(p))
                continue
            if self.ringmodel.is_fixed[p]:
                parms.pop(parms.index(p))
        n_parms = len(parms)
        angle_parms = ('I','omega','Omega')


        # get good samples in proper units
        samples = []
        for parm in parms:
            if parm in angle_parms:
                fact = 180./n.pi
            else: fact = 1.
            samples.append(pchain[parm][:,n_burn:].flatten()*fact)
        samples = n.array(samples)

        _log.info("mean, stdev of MCMC calculation results")
        for parm, vals in zip(parms, samples):
            _log.info("%s:\t%f\t+/- %f" % (parm, vals.mean(), vals.std()))
            print("%s:\t%f\t+/- %f" % (parm, vals.mean(), vals.std()))


        # projected center
        # FIXME  this depends on model
        ## import constants as c
        ## a = samples[parms.index('a'),:]
        ## e = samples[parms.index('e'),:]
        ## inc = samples[parms.index('I'),:]*c.dtor # [rad]
        ## omega = samples[parms.index('omega'),:]*c.dtor # [rad]
        ## Omega = samples[parms.index('Omega'),:]*c.dtor # [rad]

        ## # compute Thiele-Innes elements
        ## A = a*(n.cos(omega)*n.cos(Omega)-n.sin(omega)*n.sin(Omega)*n.cos(inc))
        ## B = a*(n.cos(omega)*n.sin(Omega)+n.sin(omega)*n.cos(Omega)*n.cos(inc))
        ## F = a*(-n.sin(omega)*n.cos(Omega)-n.cos(omega)*n.sin(Omega)*n.cos(inc))
        ## G = a*(-n.sin(omega)*n.sin(Omega)+n.cos(omega)*n.cos(Omega)*n.cos(inc))

        ## # compute coefficients for general ellipse
        ## a = (F**2+A**2/(1.-e**2))
        ## b = -(F*G+A*B/(1.-e**2))
        ## c = (G**2+B**2/(1.-e**2))
        ## d = e*F*(B*F-A*G)
        ## f = -e*G*(B*F-A*G)
        ## g = -(1.-e**2)*(B*F-A*G)**2

        ## # solve for ellipse parameters
        ## E0 = (c*d-b*f) / (b**2-a*c)
        ## N0 = (a*f-b*d) / (b**2-a*c)

        ## for parm, vals in zip(('E0', 'N0'), (E0, N0)):
        ##     print "%s:\t%f\t+/- %f" % (parm, vals.mean(), vals.std())
        
        #ipshell() # TEMP
        

class PolModelComparator(ModelComparator):
    
    def __init__(self, ringmodel, cens, w_ss, labels, ims_I, ims_P,
                 mask_rad=38., # [pix]
                 **kwargs):

        for im_I, im_P in zip(ims_I, ims_P):
            assert n.all(im_I.shape == im_P.shape)

        ## # high-pass filter
        ## self.fims_P = [self.filter_im_P(im_P) for im_P in ims_P]
        self.fims_P = ims_P

        # get stdev profile
        from image import radial_stdev_profile
        def get_info(fim_P, cen, w_s):
            rprof, sprof = radial_stdev_profile(fim_P, cen)
            y, x = n.mgrid[0:fim_P.shape[0],
                           0:fim_P.shape[1]]
            y -= cen[0]
            x -= cen[1]
            R = n.sqrt(x**2+y**2)
            stdmap = n.zeros_like(fim_P)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
                stdmap[wr] = s


            # extract relevant area
            tfim_P_orig = fim_P[w_s]
            stdim_P = stdmap[w_s]

            return tfim_P_orig, stdim_P

        self.tfims_P_orig, self.stdims_P = [], []
        for i in range(len(ims_I)):
            tfim_P_orig, stdim_P = get_info(self.fims_P[i], cens[i], w_ss[i])
            self.tfims_P_orig.append(tfim_P_orig)
            self.stdims_P.append(stdim_P)

        ModelComparator.__init__(self, ringmodel, cens, w_ss, labels, ims_I, mask_rad=mask_rad, **kwargs)

    @staticmethod
    def filter_im_P(im, i, fwhm=20.):
        im = ma.array(im)
        sig = fwhm/2.3548
        fim = im - gaussian_filter(im.filled(0.), sig)
        return fim


    def set_tfims(self, mask_rad=None, pol_mask_rad=None, **kwargs):
        ModelComparator.set_tfims(self, mask_rad=mask_rad, **kwargs)

        self.tfims_P = copy.deepcopy(self.tfims_P_orig)
        if (mask_rad is not None) or (pol_mask_rad is not None):
            if pol_mask_rad is None:
                pol_mask_rad = mask_rad
            for i in range(self.n_images):
                self.tfims_P[i][n.nonzero(self.Rs[i] < pol_mask_rad)] = ma.masked

    def get_model(self, shapes=None, cens=None):
        "compute a model filtered image"
        if shapes is None:
            shapes = [tfim.shape for tfim in self.tfims]
        if cens is None:
            cens = self.tcens
        # compute model image
        mims, mims_P = self.ringmodel.get_model_ring_image(shapes, cens, self.scale, n_step=self.n_step, drad0=self.drad0, fast=self.fast, fast_samp=self.fast_samp)
        # filter model image
        fmims = [self.filter_im_I(mim, i) for i, mim in enumerate(mims)]
        fmims_P = mims_P #[self.filter_im_P(mim_P, i) for i, mim_P in enumerate(mims_P)]
        return fmims, fmims_P

    def get_residuals(self, p):
        "1-d residual array"
        self.set_fit_parms(p)
        fmims, fmims_P = self.get_model()
        r = ((ma.array(self.tfims)-ma.array(fmims))/ma.array(self.stdims)).compressed()
        r_P = ((ma.array(self.tfims_P)-ma.array(fmims_P))/ma.array(self.stdims_P)).compressed()
        print 'chisq'
#        print n.sum(r_P**2.) #+ n.sum(r**2.)
        return r_P
#        return n.concatenate((r, r_P))

    def update_stdmap(self):
        "use residuals from current model to recalculate stdev map"

        # get full image model
        shapes = [fim.shape for fim in self.fims]
        cens = self.cens
        mims, mims_P = self.get_model(shapes=shapes, cens=cens)
        resids = [fim-mim for fim, mim in zip(self.fims, mims)]
        resids_P = [fim_P-mim_P for fim_P, mim_P in zip(self.fims_P, mims_P)]
        from image import radial_stdev_profile
        for i in range(self.n_images):
            rprof, sprof = radial_stdev_profile(resids[i], self.cens[i])
            rprof_P, sprof_P = radial_stdev_profile(resids_P[i], self.cens[i])

            # compute thumbnail stdmap
            stdmap = n.zeros(self.tfims[i].shape, dtype=n.float)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((self.Rs[i] >= r-0.5) & (self.Rs[i] < r+0.5))
                stdmap[wr] = s
            self.stdims[i][:] = stdmap
            stdmap_P = n.zeros(self.tfims_P[i].shape, dtype=n.float)
            for r, s in zip(rprof_P, sprof_P):
                wr = n.nonzero((self.Rs[i] >= r-0.5) & (self.Rs[i] < r+0.5))
                stdmap_P[wr] = s
            self.stdims_P[i][:] = stdmap_P


    def show_model(self):
        fmims, fmims_P = self.get_model()
        # chi2 best fit
        impad_l = 0.6 # [in]
        impad_r = 0.1 # [in]
        impad_b = 0.6 # [in]
        impad_t = 0.4 # [in]
        imheight = 3. # [in]


        for k in range(self.n_images):

            ## extent = [-self.tcen[1]*self.scale,
            ##           (self.tfim.shape[1]-1-self.tcen[1])*self.scale,
            ##           -self.tcen[0]*self.scale,
            ##           (self.tfim.shape[0]-1-self.tcen[0])*self.scale]

            extent = [(self.tcens[k][1]+self.ringmodel.parms['offsx'])*self.scale,
                      -(self.tfims[k].shape[1]-1-(self.tcens[k][1]+self.ringmodel.parms['offsx']))*self.scale,
                      -(self.tcens[k][0]+self.ringmodel.parms['offsy'])*self.scale,
                      (self.tfims[k].shape[0]-1-(self.tcens[k][0]+self.ringmodel.parms['offsy']))*self.scale]

            imwidth = imheight * float(self.tfims[k].shape[1])/float(self.tfims[k].shape[0]) # [in]
            fig_width = impad_l + 3*imwidth + impad_r
            fig_height = impad_b + 2*imheight + impad_t
            figsize = (fig_width, fig_height)

            fig = pylab.figure(k, figsize=figsize)
            fig.clear()
            fig.hold(True)

            fig.suptitle(self.labels[k])

            axs = []
            for i in range(6):
                j = i / 3
                ax = fig.add_axes([(impad_l+(i%3)*imwidth)/fig_width,
                                   (impad_b+(1-j)*imheight)/fig_height,
                                   imwidth/fig_width,
                                   imheight/fig_height,
                                   ])
                axs.append(ax)
            vmax = n.nanmax(fmims[k])
            kw = {'vmin':-0.1*vmax,
                  'vmax':1.1*vmax,
                  'interpolation':'nearest',
                  'cmap':mpl.cm.gist_stern,
                  'extent':extent,
                  }
            axs[0].imshow(self.tfims[k], **kw)
            axs[1].imshow(fmims[k], **kw)
            axs[2].imshow(self.tfims[k]-fmims[k], **kw)
            vmax = n.nanmax(fmims_P[k])
            kw['vmin'] = -0.1*vmax
            kw['vmax'] = 1.1*vmax
            axs[3].imshow(self.tfims_P[k], **kw)
            axs[4].imshow(fmims_P[k], **kw)
            axs[5].imshow((self.tfims_P[k]-fmims_P[k]) / self.stdims_P[k], **kw)

            # mark star
            for ax in axs:
                ax.set_autoscale_on(False)
                #ax.scatter((self.tcen[1],), (self.tcen[0],), c='k')
                ax.scatter((0.,), (0.,), c='k')
                ## ax.scatter((self.ringmodel.parms['offsx']*self.scale,),
                ##            (self.ringmodel.parms['offsy']*self.scale,),
                ##            c='b')

            # FIXME  update for offset ring
            ## # mark major axis
            ## #
            ## nu = n.array((0., n.pi))
            ## a = self.ringmodel.parms['a']
            ## e = self.ringmodel.parms['e']
            ## I = self.ringmodel.parms['I']
            ## omega = self.ringmodel.parms['omega']
            ## Omega = self.ringmodel.parms['Omega']

            ## # physical radius from star
            ## r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]

            ## so, co = n.sin(omega), n.cos(omega)
            ## sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
            ## sO, cO = n.sin(Omega), n.cos(Omega)
            ## si, ci = n.sin(I), n.cos(I)

            ## pN = r * (cO*conu-sO*sonu*ci) # [AU]
            ## pE = r * (sO*conu+cO*sonu*ci) # [AU]

            ## for ax in (axs[1], axs[4]):
            ##     ax.plot(pE, pN, c='g')
            ##     ax.scatter((pE[0],), (pN[0],), c='g')


            ## # mark line of nodes
            ## #
            ## nu = n.array((-omega, n.pi-omega))
            ## r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]
            ## so, co = n.sin(omega), n.cos(omega)
            ## sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
            ## sO, cO = n.sin(Omega), n.cos(Omega)
            ## si, ci = n.sin(I), n.cos(I)
            ## pN = r * (cO*conu-sO*sonu*ci) # [AU]
            ## pE = r * (sO*conu+cO*sonu*ci) # [AU]
            ## for ax in (axs[1], axs[4]):
            ##     ax.plot(pE, pN, c='m', ls='--')
            ##     ax.scatter((pE[0],), (pN[0],), c='m')


            # axis labels
            for ax in axs[1:3]+axs[4:6]:
                pylab.setp(ax.get_yticklabels(), visible=False)
            for ax in axs[0:3]:
                pylab.setp(ax.get_xticklabels(), visible=False)

            pylab.draw()
            pylab.show()
            pylab.imshow((self.tfims_P[0]-fmims_P[0]) / self.stdims_P[0])
            print n.sum(((self.tfims_P[0]-fmims_P[0]) / self.stdims_P[0])**2.)
            pylab.colorbar()
            pylab.show()



d = os.path.expanduser('~/work/gpi/')
if d not in sys.path: sys.path.append(d)
import gpiklip
import glob
import instruments.GPI as gpi
_KLIPPolModelComparatorBase = PolModelComparator 
class KLIPPolModelComparator(_KLIPPolModelComparatorBase):

    def __init__(self, im_save_fns, klip_save_fns, polmodeonly = True, *args, **kwargs):
        _KLIPPolModelComparatorBase.__init__(self, *args, **kwargs)

        # load PA and KLIP mode information
        self.polmodeonly = polmodeonly
        self.pas, self.cents, self.basisvals_all, self.coords_all, self.goodinds_all = [], [], [], [], []
        for im_save_fn, klip_save_fn in zip(im_save_fns, klip_save_fns):
            basisvals, coords, goodinds = gpiklip.load_klip_save_data(basispattern = klip_save_fn)
            self.basisvals_all.append(basisvals)
            self.coords_all.append(coords)
            self.goodinds_all.append(goodinds)
            image_arr = glob.glob(im_save_fn)
            image_arr = sorted(image_arr)
            ims_dataset = gpi.GPIData(image_arr)
            self.cents.append(ims_dataset.centers)
            self.pas.append(ims_dataset.PAs)

    def filter_im_I(self, im, i, **kwargs):
        
        if self.polmodeonly == True:
            return im
        else:
            fim = gpiklip.perform_klip_adi(im,
                                           self.pas[i],
                                           self.cents[i],
                                           basisvals_all = self.basisvals_all[i],
                                           coords_all = self.coords_all[i],
                                           goodinds_all = self.goodinds_all[i])
            return fim
#        fim = im
        # NOTE  testing high-pass filter of klip images
        # NOTE  Do not need, KLIP fm is nice
#        mfim = ma.masked_where(n.isnan(fim), fim)
#        fim2 = _KLIPPolModelComparatorBase.filter_im_I(mfim, i, **kwargs)
#        return fim2




def log_like(p, mc):
    "top-level function for MCMC"

    # residuals
    resid = mc.get_residuals(p)
    # chi2
    chi2 = n.sum(resid**2)
    # priors
    priors = mc.get_gaussian_priors()
    chi2p = n.sum(priors**2)

    mc.ringmodel.print_parms()

    # get current model parameter dictionary
    parms = mc.ringmodel.parms
    #e = parms['e']
    psf_parms = parms['psf_parms']
    int_parms = parms['int_parms']
    pol_int_parms = parms['pol_int_parms']
    omega = parms['omega']
    Omega = parms['Omega']
    I = parms['I']
    #a = parms['a']
    #a_out = parms['a_out']

    # bounds
    if 'e' in parms:
        if (parms['e']>=1.): return -n.inf

    if mc.ringmodel.psf_method == 'gaussian':
        if n.any(psf_parms <= 10.): return -n.inf
        if n.any(psf_parms > 21.): return -n.inf
    elif mc.ringmodel.psf_method == 'airy':
        if n.any(psf_parms <= 1.): return -n.inf
        if n.any(psf_parms > 15.): return -n.inf

    if n.any(n.isnan(int_parms)): return -n.inf
    if n.any(int_parms > 1e15): return -n.inf
    if ~n.all(n.isfinite(int_parms)): return -n.inf

    if pol_int_parms is not None:
        if n.any(n.isnan(pol_int_parms)): return -n.inf
        if n.any(pol_int_parms > 1e15): return -n.inf
        if ~n.all(n.isfinite(pol_int_parms)): return -n.inf

    if (omega < -n.pi) or (omega > n.pi): return -n.inf
    if (Omega < 0) or (Omega > 2.*n.pi): return -n.inf
    ## if (Omega < 200.*n.pi/180.) or (Omega > 215.*n.pi/180.): return -n.inf

    if (I < 0.) or (I > n.pi): return -n.inf

    #if (a < 70.) or (a > 85.): return -n.inf
    if ('a' in parms) and ('a_out' in parms):
        if parms['a_out'] < parms['a']: return -n.inf

    if 'r' in parms:
        if parms['r'] < 0.: return -n.inf
    if 'r_in' in parms:
        if parms['r_in'] < 0.: return -n.inf
    if 'r_out' in parms:
        if parms['r_out'] < 0.: return -n.inf

    ## if ('r' in parms) and ('r_out' in parms):
    ##     if parms['r_out'] < parms['r']: return -n.inf
    ## if ('r' in parms) and ('r_in' in parms):
    ##     if parms['r_in'] > parms['r']: return -n.inf
    if ('r_in' in parms) and ('r_out' in parms):
        if parms['r_in'] > parms['r_out']: return -n.inf

    if 'r_in' in parms:
        if parms['r_in'] < 60.: return -n.inf
    if 'r_out' in parms:
        if parms['r_out'] > 100.: return -n.inf

    if 'a_out' in parms:
        if parms['a_out'] > 100.: return -n.inf

#    return -(chi2+chi2p)/2.
    return - chi2


def estimate_debiased_P(stokes_im):
    """
    Estimate and remove the Serkowski bias in linearly polarized
    intensity P=sqrt(Q^2+U^2).

    Estimates noise in each of Q and U using local stdev, the uses
    these to compute the bias.  Subtracts this bias from P and returns
    the latter quantity.
    """

    # get local stdev estimates of variance in each of Q and U



    # If Q and U have the same variance, then the distribution of P is
    # Rician.




def fit_image(redo=False, fit_pol=True):
    import constants as c
    
    n_step = 1000  # steps along ellipse for intensity
    #n_step = 4000  # steps along ellipse for intensity
    drad0 = 2.  # [AU]  stepsize along semimajor
    # MCMC parameters
    n_walker = 400
    #n_sample = 4000
    #n_burn = 3500
    #n_burn = 1000
    #n_sample = 2000
    #n_burn = 1500
    n_sample = 1000
    n_burn = 750
    #n_sample = 1000
    #n_burn = 500
    #n_sample = 800
    #n_burn = 500
    #n_sample = 100
    #n_burn = 50

    # whether to fix the stellar position to satspot location
    fix_star = True
    #fix_star = False

    # fast mode for model computation
    fast = True
    #fast = False
    fast_samp = 3
    #fast_samp = 4
    #fast_samp = 6
    #fast_samp = 1

    # load GPI image

    labels = ['K1']
    im_dir = os.path.expanduser('~/Dropbox (GPI)/HR4796A/hr4796a-gd/')
    im_fn = im_dir+'1404/best_subtract_vc2.fits.gz'
    im, hdr = fits.getdata(im_fn, header=True)
    cen = n.array((hdr['PSFCENTY'], hdr['PSFCENTX']))
    by, ey = int(cen[0])-83, int(cen[0])+83
    bx, ex = int(cen[1])-50, int(cen[1])+50
    w_s = n.s_[by:ey,bx:ex]


    im_I = im[0,:,:]
    im_P = n.sqrt(im[1,:,:]**2 + im[2,:,:]**2)
    im_I = ma.masked_where(n.isnan(im_I), im_I)
    im_P = ma.masked_where(n.isnan(im_P), im_P)

    # FIXME need to re-implement filtering of input images
    _log.error('images need to be filtered!')


    n_int_parms = 5
    #ringmodel = PhaseKeplerianRingModel(init_pol=True, n_int_parms=n_int_parms)
    ringmodel = KeplerianRingModel(init_pol=fit_pol, n_int_parms=n_int_parms)
    if fit_pol:
        mc = PolModelComparator(ringmodel, cen, w_s, labels, im_I, im_P, fast=fast, fast_samp=fast_samp)
    else:
        mc = ModelComparator(ringmodel, cen, w_s, labels, im_I, fast=fast, fast_samp=fast_samp)
    mc.n_step = n_step
    mc.drad0 = drad0
    #mc.ringmodel.parms['Omega'] = -3. * n.pi/180.
    mc.ringmodel.parms['int_parms'][:] = 5e8
    if fit_pol:
        mc.ringmodel.parms['pol_int_parms'][:] = 2e7


    if fix_star:
        # fix stellar position
        mc.ringmodel.is_fixed['offsx'] = True
        mc.ringmodel.is_fixed['offsy'] = True
    else:
        # small offset for parameter ranges
        mc.ringmodel.parms['offsx'] = 0.01
        mc.ringmodel.parms['offsy'] = 0.01

    mc.ringmodel.is_fixed['a_out'] = False
    mc.ringmodel.parms['a_out'] = mc.ringmodel.parms['a']+5.
    ## mc.ringmodel.is_fixed['a_out'] = True
    ## mc.ringmodel.parms['a_out'] = None
    ## mc.drad0 = 1.

    # run least squares
    save_fn = 'polfit.dat'
    if os.access(save_fn, os.R_OK) and not redo:
        mc.ringmodel.restore_parms(save_fn)
    else:
        mc.do_leastsq()
        mc.ringmodel.save_parms(save_fn)
        redo = True

    mc.report_leastsq()
    #assert False # TEMP

    ## # TEMP
    ## ipshell()
    ## assert False

    # update stdmap, reduce pol mask size
    mc.update_stdmap()
    #mask_rad = 38. # [pix]
    mask_rad = 50. # [pix]
    pol_mask_rad = 12. # [pix]
    mc.set_tfim(mask_rad=mask_rad, pol_mask_rad=pol_mask_rad)

    # scale the errors...
    p = mc.get_fit_parms()
    r = mc.get_residuals(p)
    n_data = len(r)
    # scale by pixels per resolution element
    #  dia = 1.22 * lambda / D * (arcsec/rad) / (arcsec/pix)
    n_data /= n.pi * (1.22 * 2.05e-6 / 8. * 2.06e5 / mc.pix_scale / 2.)**2
    n_parm = len(p)
    n_dof = n_data-n_parm
    chi2nu = (r**2).sum()/n_dof
    fact = n.sqrt(chi2nu)
    #fact *= 10 # TEMP
    mc.stdim *= fact
    if fit_pol:
        mc.stdim_P *= fact


    ## # TEMP
    ## ipshell()
    ## assert False


    # run least squares with updated stdmap, more pol params
    save_fn = 'polfit2.dat'
    if os.access(save_fn, os.R_OK) and not redo:
        mc.ringmodel.restore_parms(save_fn)
    else:
        n_int_parm = 10
        mc.ringmodel.parms['int_parms'] = n.ones(n_int_parm, dtype=n.float)*2e8
        mc.ringmodel.parm_lens['int_parms'] = n_int_parm
        if fit_pol:
            mc.ringmodel.parms['pol_int_parms'] = n.ones(n_int_parm, dtype=n.float)*2e7
            mc.ringmodel.parm_lens['pol_int_parms'] = n_int_parm
        mc.do_leastsq()
        mc.ringmodel.save_parms(save_fn)


    if hasattr(mc, 'leastsq_vals'):
        mc.report_leastsq()
#    mc.show_model()

    #assert False # TEMP
    

    # show least-squares fit phase curves
    #
    
    a = mc.ringmodel.parms['a']
    e = mc.ringmodel.parms['e']
    I = mc.ringmodel.parms['I']
    omega = mc.ringmodel.parms['omega']

    nu = n.linspace(0., 2.*n.pi, 100)#, endpoint=False)

    # physical radius from star
    r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]
    # scattering angle
    pz = r * n.sin(omega+nu)*n.sin(I)
    phi_sca = n.arccos(pz/r) # [rad]

    intensity = mc.ringmodel._get_intensity(nu, phi_sca) / r**2
    sc_int = intensity * r**2

    if fit_pol:
        pol_intensity = mc.ringmodel._get_pol_intensity(nu, phi_sca) / r**2
        pol_sc_int = pol_intensity * r**2


    fig = pylab.figure(4, figsize=(8,4))
    fig.clear()
    fig.hold(True)
    ax1 = fig.add_subplot(121, polar=True)

    #kw = {'marker':'.', 'edgecolor':'none'}
    kw = {}
    ax1.plot(phi_sca, sc_int,
             color='k', **kw)
    ax1.plot(2.*n.pi-phi_sca, sc_int,
             color='k', **kw)
    if fit_pol:
        ax1.plot(phi_sca, pol_sc_int,
                 color='r', **kw)
        ax1.plot(2.*n.pi-phi_sca, pol_sc_int,
                 color='r', **kw)

    if fit_pol:
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(224, sharex=ax2)
    else:
        ax2 = fig.add_subplot(122)

    #kw = {'marker':'.', 'edgecolor':'none'}
    ax2.plot(phi_sca*c.rtod, sc_int,
             color='k', **kw)
    ax2.set_autoscale_on(False)
    if fit_pol:
        ax2.plot(phi_sca*c.rtod, pol_sc_int,
                 color='r', **kw)

        ax3.plot(phi_sca*c.rtod, pol_sc_int/sc_int,
                 color='b', **kw)


    if fit_pol:
        axs = (ax2, ax3)
    else:
        axs = (ax2,)
    for ax in axs:
        ax.set_xlim(0., 180.)
        ax.axvline(90., ls=':', c='k')
    ax2.set_ylim(0., 8e8)
    if fit_pol:
        ax3.set_ylim(0., 0.6)
        pylab.setp(ax2.get_xticklabels(), visible=False)

    ax2.set_ylabel('Intensity')
    if fit_pol:
        ax3.set_ylabel('Pol. frac.')
        ax3.set_xlabel(r"$\theta$")
    else:
        ax2.set_xlabel(r"$\theta$")
    

    pylab.draw()
    pylab.show()


    if not fix_star:
        # small offset for parameter ranges
        mc.ringmodel.parms['offsx'] = 0.01
        mc.ringmodel.parms['offsy'] = 0.01


    save_fn = 'polfit3.dat'
    if os.access(save_fn, os.R_OK) and not redo:

        with open(save_fn) as f:
            mc = pickle.load(f)

    else:
        # run MCMC
        mc.run_MCMC(n_walker, n_sample)

        _log.debug('finished MCMC')

        with open(save_fn, 'w') as f:
            pickle.dump(mc, f, 2)


    # MCMC results
    mc.report_mcmc_results(n_burn)
    mc.show_mcmc_results(n_burn, mask_rad, pol_mask_rad)


    #ipshell()


def load_image_info(labels):
    im_dir = '/u/home/p/parriaga/hr4796a_new/'
    
    # Assume 2 labels mean K and J fit
    if len(labels) == 2:
        im_I_fns = [im_dir+'S20140422K-KLmodes-KL5-masked.fits',
                    im_dir+'S20140422J-KLmodes-KL5-masked.fits']
        im_P_fns = [im_dir+'HR4796A-K1-Qr-masked2.fits',
                    im_dir+'HR4796A-J-Qr-masked.fits']
        im_save_fns = [im_dir+'april_k/*podc*', im_dir + 'april_j/*podc*']
        klip_save_fns = ['basis/basisK', 'basis/basisJ']
        psf_save_fns = ['psf_k_short.fits', 'psf_j_short.fits']

    if len(labels) == 1.:
        if labels[0] == 'K1':
            im_I_fns = [im_dir + 'S20140422K_first-KLmodes-KL1-masked.fits']
            im_P_fns = [im_dir + 'HR4796A-K1-Qr-masked2.fits']
            psf_save_fns = ['psf_k_short.fits']
            im_save_fns = [im_dir+'april_k/*podc*']
            klip_save_fns = ['basis/basisK'] #FIXME after KLIP-fm
        if labels[0] == 'J':
            im_I_fns = [im_dir + 'S20140422J-KLmodes-KL5-masked.fits']
            im_P_fns = [im_dir + 'HR4796A-J-Qr-masked.fits']
            psf_save_fns = ['psf_j_short.fits']
            im_save_fns = [im_dir+'april_j/*podc*']
            klip_save_fns = ['basis/basisJ']
    return im_dir, labels, im_I_fns, im_P_fns, im_save_fns, klip_save_fns, psf_save_fns




def fit_images(show=True,
               redo_lsq1=False,
               redo_lsq2=False,
               redo_mcmc=False,
               redo_all=False,
               lsq_only=False,
               ):
    import constants as c

    for name in ('image', 'gpiklip'):
        logging.getLogger(name).setLevel(logging.INFO)

    redo_lsq1 = redo_lsq1 or redo_all
    redo_lsq2 = redo_lsq2 or redo_lsq1
    redo_mcmc = redo_mcmc or redo_lsq2

    n_step = 200 # steps along ring for intensity
    drad0 = 2. # [AU] stepsize along radial direction
    # MCMC parameters
    n_walker = 200
    n_sample = 400
    n_burn = 0


    # whether to fix the stellar position to the satspot location
    fix_star = True #False

    # fast mode for model computation
    fast = True
    fast_samp = 2

    fit_pol = True
    #Still going to load in I images, just will not do KLIP
    fit_pol_only = True
    labels = ['J']

    # load GPI image data
    im_dir, labels, im_I_fns, im_P_fns, im_save_fns, klip_save_fns, psf_save_fns = load_image_info(labels)
    psf_ims = []
    for psf_save_fn in psf_save_fns:
        psf_im = fits.getdata(psf_save_fn)
        psf_ims.append(psf_im)
    ims_I = []
    ims_P = []
    cens = []
    w_ss = []
    for im_I_fn, im_P_fn in zip(im_I_fns, im_P_fns):
        # get total intensity image
        _log.debug("loading %s" % im_I_fn)
        im_I = fits.getdata(im_I_fn) # [ADU/sec]
        im_I = ma.masked_where(n.isnan(im_I), im_I)
        cen_I = n.array((140, 140)) # NOTE  only for klip

        # get polarization image
        _log.debug("loading %s" % im_P_fn)
        im_P, hdr = fits.getdata(im_P_fn, header=True)
        im_P = ma.masked_where(n.isnan(im_P), im_P)
        im_P = ma.masked_where(im_P > 300., im_P)
        im_P = ma.masked_where(im_P < -30., im_P)
        cen_P = n.array((140, 140))
        if ('BUNIT' in hdr) and (hdr['BUNIT'] == 'ADU per coadd'):
            im_P /= hdr['ITIME'] # [ADU/coadd] -> [ADU/sec]

        # resample polarization image onto intensity grid
        ny, nx = im_I.shape
        yx = n.array(n.mgrid[0:ny,0:nx])
        yx -= cen_I[:,n.newaxis,n.newaxis]
        from image import median_replace
        from scipy.ndimage import map_coordinates
        syx = yx+cen_P[:,n.newaxis,n.newaxis]
        sim_P = map_coordinates(median_replace(im_P, boxsize=3, fast=True).filled(0.),
                                syx,
                                output=n.float64)
        thresh = 0.1
        smask = (map_coordinates(im_P.mask.astype(n.float), syx, cval=1.) > thresh)
        sim_P = ma.masked_where(smask, sim_P)
        cen = cen_I
        cens.append(cen)

        # accumulate images
        ims_I.append(im_I)
        if fit_pol:
            ims_P.append(sim_P)

        # get ROI parameters
        ## by, ey = int(cen[0])-83, int(cen[0])+83
        ## bx, ex = int(cen[1])-50, int(cen[1])+50
        ## w_s = n.s_[by:ey,bx:ex]
        w_s = n.s_[0:im_I.shape[0],0:im_I.shape[1]]
        w_ss.append(w_s)

    lam_data = {'Y' :1.05e-6, # [m]
                'J' :1.22e-6,# FIXME  these are all eyeballed
                'H' :1.65e-6,
                'K1':2.09e-6,
                'K2':2.25e-6,
                }
    D = 7.7701 # [m] telescope diameter
    D *= 9.571/10. # reduction by lyot
    pix_scl = 0.01414 # [arcsec/pix]
    psf_sigs = n.array([lam_data[band]/D*c.as_per_rad/pix_scl for band in labels])
    print psf_sigs
    print labels
    if fit_pol: Comparator = KLIPPolModelComparator
    else: Comparator = ModelComparator

    # filter images
    # NOTE  don't filter klip-processed model images, since already filtered
    ims_I = [ModelComparator.filter_im_I(im_I, i) for i, im_I in enumerate(ims_I)]

    n_int_parms = 35
    ringmodel = OffsetRingModel(n_int_parms=n_int_parms,
                                init_pol=fit_pol,
                                n_images=len(ims_I),
                                psf_method='inputim',
                                psf_ims = psf_ims
                                )
    args = (ringmodel, cens, w_ss, labels, ims_I)
    kwargs = {'fast':fast,
              'fast_samp':fast_samp,
              }
    if fit_pol:
        args = (im_save_fns, klip_save_fns, fit_pol_only) + args + (ims_P,)
    mc = Comparator(*args, **kwargs)
    mc.n_step = n_step
    mc.drad0 = drad0
    mc.ringmodel.parms['psf_parms'][:] = psf_sigs
    mc.ringmodel.is_fixed['psf_parms'] = True
    mc.ringmodel.parms['int_parms'][:] = 1.e9
    if fit_pol:
        mc.ringmodel.parms['pol_int_parms'][:] = 7.e8
        mc.ringmodel.is_fixed['pol_int_parms'] =False
        if fit_pol_only:
            mc.ringmodel.is_fixed['int_parms'] = True 
        else:
            mc.ringmodel.is_fixed['int_parms'] = False
    if fix_star:
        mc.ringmodel.is_fixed['offsx'] = True
        mc.ringmodel.is_fixed['offsy'] = True
    else:
        # small offset for parameter ranges
        mc.ringmodel.parms['offsx'] = 0.01
        mc.ringmodel.parms['offsy'] = 0.01
    mc.ringmodel.parms['offsx'] = 0.004
    mc.ringmodel.parms['offsy'] = -0.014


    # Radial parameters
    # Basically fitting an exponential which vegins at r power law gamma that extends to rout
    save_prefix = 'hr4796-fitter-jpol-'
    save_fn = save_prefix+'leastsq2.dat'
    mc.ringmodel.restore_parms(save_fn)
#    mc.ringmodel.parms['r_out'] = mc.ringmodel.parms['r']+20.
    n_int_parm = 30
    mc.update_int_parms(n_int_parm)


    mc.ringmodel.is_fixed['r_out'] = True
    mc.ringmodel.parms['r_in'] = None
    mc.ringmodel.is_fixed['r_in'] = True
    mc.ringmodel.is_fixed['r'] = True 
    mc.ringmodel.is_fixed['gamma_in'] = True
    mc.ringmodel.parms['gamma'] = 20.
    mc.ringmodel.is_fixed['gamma'] = True #False
    mc.ringmodel.parms['beta'] = 0.
    mc.ringmodel.is_fixed['beta'] = True 


    # Morphology parameters
    mc.ringmodel.is_fixed['omega'] = True
    mc.ringmodel.is_fixed['Omega'] = True 
    mc.ringmodel.is_fixed['offset'] = True
    mc.ringmodel.is_fixed['I'] = True


    mask_rad = 20. # [pix]
    pol_mask_rad = 10. # [pix]
    mc.set_tfims(mask_rad=mask_rad, pol_mask_rad=pol_mask_rad)

    
    # run least-squares
    save_prefix = 'hr4796-fitter-jpolintparms-'
    save_fn = save_prefix+'leastsq1.dat'
    if os.access(save_fn, os.R_OK) and not redo_lsq1:
        _log.info('loading first leastsq')
        mc.ringmodel.restore_parms(save_fn)
    else:
        _log.info('running first leastsq')
        mc.do_leastsq()
        mc.ringmodel.save_parms(save_fn)
    if hasattr(mc, 'leastsq_vals'):
        mc.report_leastsq()
    if show:
        mc.show_model()

    #assert False # TEMP

    # update stdmap, reduce pol mask size
    mc.update_stdmap()
    mask_rad = 15. # [pix]
    pol_mask_rad = 12. # [pix]
    mc.set_tfims(mask_rad=mask_rad, pol_mask_rad=pol_mask_rad)

    # scale the errors...
    p = mc.get_fit_parms()
    r = mc.get_residuals(p)
    n_data = len(r)
    # scale by pixels per resolution element
    #  dia = 1.22 * lambda / D * (arcsec/rad) / (arcsec/pix)
    # FIXME  this only valid for K1
    n_data /= n.pi * (1.22 * 2.05e-6 / 8. * 2.06e5 / mc.pix_scale / 2.)**2
    n_parm = len(p)
    n_dof = n_data-n_parm
    chi2nu = (r**2).sum()/n_dof
    fact = n.sqrt(chi2nu)
    for i in range(mc.n_images):
        mc.stdims[i] *= fact
        if fit_pol:
            mc.stdims_P[i] *= fact

    # get new intensity parameters
    n_int_parm = 40
    mc.update_int_parms(n_int_parm)

    # fit for slopes
    mc.ringmodel.is_fixed['gamma_in'] = True
    mc.ringmodel.is_fixed['gamma'] = True
    mc.ringmodel.is_fixed['beta'] = True
#    mc.ringmodel.parms['beta'] = 1e-3 #Uncomment if not fit in first run
#    mc.ringmodel.parms['beta'] = 0.


    # run least squares with updated stdmap
    save_fn = save_prefix+'leastsq2.dat'
    if os.access(save_fn, os.R_OK) and not redo_lsq2:
        _log.info('loading second leastsq')
        mc.ringmodel.restore_parms(save_fn)
    else:
        _log.info('running second leastsq')
        mc.do_leastsq()
        mc.ringmodel.save_parms(save_fn)

    if hasattr(mc, 'leastsq_vals'):
        mc.report_leastsq()
    if show:
        mc.show_model()

    if lsq_only: return

    mc.ringmodel.is_fixed['pol_int_parms'] =False
    mc.ringmodel.is_fixed['int_parms'] = True

#    mc.ringmodel.is_fixed['r_out'] = True
#    mc.ringmodel.is_fixed['r_in'] = 

    n_int_parm = 35
    mc.update_int_parms(n_int_parm)



    # run MCMC
    save_fn = save_prefix+'mcmc.dat'
    if os.access(save_fn, os.R_OK) and not redo_mcmc:
        _log.info('loading MCMC')
        with open(save_fn) as f:
            mc = pickle.load(f)
    else:
        _log.info('running MCMC')
        mc.run_MCMC(n_walker, n_sample)
        _log.debug('finished MCMC')

        with open(save_fn, 'w') as f:
            pickle.dump(mc, f, 2)
    chain = mc.chain[:,n_burn:,:]
    lnprobability = mc.lnprobability[:,n_burn:]
    n_walker, n_sample, n_parm = chain.shape

    # set model to max-likelihood parameters
    mc.set_fit_parms(chain.reshape(n_walker*n_sample,n_parm)[n.argmax(lnprobability)])
    mc.ringmodel.print_parms()
    if show:
        mc.show_model()

    # MCMC results
    mc.report_mcmc_results(n_burn)
    if show:
        mc.show_mcmc_results(n_burn, mask_rad, pol_mask_rad)

    #ipshell() # TEMP


def get_euler_angs(R):
   # from Savransky & Kasdin
   A = R.copy()

   i, j = 2, 0

   if j==0: A[1,2] = -A[1,2]
   if j==1: A[2,0] = -A[2,0]
   if j==2: A[0,1] = -A[0,1]

   p = 3 - (i+j)

   s2 = n.sqrt(A[i,p]**2 + A[i,j]**2)
   t1 = n.arctan2(A[i,j]/s2, A[i,p]/s2)
   t2 = n.arctan2(s2, A[i,i])
   t3 = n.arctan2(A[j,i]/s2, A[p,i]/s2)

   return t1, t2, t3

def get_transformation(omega, Omega, I, omega2, Omega2, I2):
   "compute revised Euler angles with reference plane rotations"

   # transformation matrix
   #sO, cO = n.sin(Omega), n.cos(Omega)
   #so, co = n.sin(omega), n.cos(omega)
   # TEMP
   so, co = n.sin(Omega), n.cos(Omega)
   sO, cO = n.sin(omega), n.cos(omega)
   si, ci = n.sin(I), n.cos(I)
   x1 = cO*co-sO*so*ci
   x2 = sO*co+cO*so*ci
   x3 = si*so
   y1 = -cO*so-sO*co*ci
   y2 = -sO*so+cO*co*ci
   y3 = si*co
   z1 = si*sO
   z2 = -si*cO
   z3 = ci
   R = n.array([[x1, x2, x3],
                [y1, y2, y3],
                [z1, z2, z3]])
   #R = n.diag(n.ones(3)) # TEMP
   R = R.T


   # rotate line of nodes
   th = -omega2
   sth, cth = n.sin(th), n.cos(th)
   Rz = n.array([[cth, sth, 0.],
                 [-sth, cth, 0.],
                 [0., 0., 1.]])
   R = n.dot(R, Rz)

   # rotate so there is inclination
   th = I2
   sth, cth = n.sin(th), n.cos(th)
   Rx = n.array([[1., 0., 0.],
                 [0., cth, -sth],
                 [0., sth, cth]])
   R = n.dot(R, Rx)

   # rotate so that line of nodes is to North
   th = -Omega2
   sth, cth = n.sin(th), n.cos(th)
   Rz = n.array([[cth, sth, 0.],
                 [-sth, cth, 0.],
                 [0., 0., 1.]])
   R = n.dot(R, Rz)

   # compute revised elements
   Omegap, Ip, omegap = get_euler_angs(R)

   return omegap, Omegap, Ip

def test_get_transformation():
    om, Om, I = 1., 2., 3.
    print om, Om, I
    print get_transformation(om, Om, I, 0., 0., 0.)
    print get_transformation(0., 0., 0., om, Om, I)


def make_proposal_figure():
    import constants as const

    from kepler import get_apparent_orbit, get_apparent_orbit_ellipse

    def laplace_factor(a_per, a_gr):
        from misc import laplace_coeff
        if a_per < a_gr:
            alf = a_per/a_gr
        else:
            alf = a_gr/a_per
        if alf == 0.: return 0.
        elif alf == 1.: return 1.
        else:
            return laplace_coeff(alf,3./2,2) / laplace_coeff(alf,3./2,1)

    n_phi = 20 # number of precession positions

    # perturber parameters
    a_per = 60. # [AU]
    e_per = 0.1
    I_per = 0. * const.dtor # [rad]
    omega_per = 0. * const.dtor # [rad]
    Omega_per = 0. * const.dtor # [rad]
    pomega_per = omega_per+Omega_per # [rad]


    # grain initial parameters
    a_gr = 80. # [AU]
    e_gr0 = 0.0
    I_gr0 = 1e-5 * const.dtor # [rad]
    omega_gr0 = 0. * const.dtor # [rad]
    Omega_gr0 = 1e-1 * const.dtor # [rad]
    #Omega_gr0 = 0. * const.dtor # [rad]
    pomega_gr0 = omega_gr0+Omega_gr0 # [rad]


    # compute forced elements
    e_f = laplace_factor(a_per, a_gr) * e_per
    z_f = e_f * n.exp(1j*pomega_per)
    y_f = I_per * n.exp(1j*Omega_per)


    # compute initial proper elements
    z_gr0 = e_gr0 * n.exp(1j*pomega_gr0)
    y_gr0 = I_gr0 * n.exp(1j*Omega_gr0)
    z_p0 = z_gr0 - z_f
    y_p0 = y_gr0 - y_f


    # precess proper elements
    phi = n.linspace(0., 2.*n.pi, n_phi, endpoint=False)
    z_p = z_p0 * n.exp(1j*phi)
    y_p = y_p0 * n.exp(-1j*phi)


    # compute elements
    z_gr = z_p + z_f
    y_gr = y_p + y_f
    e_gr = n.abs(z_gr)
    I_gr = n.abs(y_gr)
    pomega_gr = n.arctan2(z_gr.imag, z_gr.real) # [rad]
    Omega_gr = n.arctan2(y_gr.imag, y_gr.real) # [rad]
    omega_gr = pomega_gr-Omega_gr # [rad]



    xx = 100. # [AU]
    view_range = [xx, -xx, -xx, xx]
    aspect_ratio = n.abs(view_range[3]-view_range[2])/n.abs(view_range[1]-view_range[0])

    im_width = 2.5 # [in]
    left_pad = 0.7 # [in]
    cen_pad = 0.0 # [in]
    right_pad = 0.1 # [in]
    bottom_pad = 0.1 # [in]
    im_height = aspect_ratio * im_width # [in]
    top_pad = 0.1 # [in]
    fig_width = left_pad+2*im_width+cen_pad+right_pad # [in]
    fig_height = bottom_pad+im_height+top_pad # [in]
    figsize = (fig_width, fig_height)

    fig = pylab.figure(0, figsize=figsize)
    fig.clear()
    fig.hold(True)

    ax1 = fig.add_axes([left_pad/fig_width,
                        bottom_pad/fig_height,
                        im_width/fig_width,
                        im_height/fig_height])
    ax2 = fig.add_axes([(left_pad+im_width+cen_pad)/fig_width,
                        bottom_pad/fig_height,
                        im_width/fig_width,
                        im_height/fig_height])

    # orientation parameters for rotated view
    #omega_per2 = 45.*const.dtor # [rad]
    omega_per2 = -84.89*const.dtor # [rad]
    #Omega_per2 = 45.*const.dtor # [rad]
    Omega_per2 = 207.*const.dtor # [rad]
    #I_per2 = 80.*const.dtor # [rad]
    I_per2 = 76.96*const.dtor # [rad]

    # mark star
    for ax in (ax1, ax2):
        ax.scatter((0.,), (0.,), marker='*', c='g', zorder=10)

    for i, (e, I, omega, Omega) in enumerate(zip(e_gr, I_gr, omega_gr, Omega_gr)):
        #lw = 1.0 if (i==0) else 0.5
        lw = 0.25
        #alpha = (1.-float(i+1)/n_phi)**1.0
        alpha = 1.


        # face-on view
        ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a_gr, e, I, omega, Omega)
        ax1.add_artist(mpl.patches.Ellipse((E0,N0),
                                           width=2.*ap, height=2.*bp,
                                           angle=phi*180./n.pi,
                                           ec='b', fc='none',
                                           lw=lw,
                                           alpha=alpha,
                                           ))




        # rotated view
        #

        # perturber originally has pericenter to North
        assert Omega_per == 0.
        assert omega_per == 0.
        # perturber originally has no inclination
        assert I_per == 0.


        omegap, Omegap, Ip = get_transformation(omega, Omega, I, omega_per2, Omega_per2, I_per2)

        ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a_gr, e, Ip, omegap, Omegap)
        ax2.add_artist(mpl.patches.Ellipse((E0,N0),
                                           width=2.*ap, height=2.*bp,
                                           angle=phi*180./n.pi,
                                           ec='b', fc='none',
                                           lw=lw,
                                           alpha=alpha,
                                           ))


    # show perturber orbit
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a_per, e_per, I_per, omega_per, Omega_per)
    ax1.add_artist(mpl.patches.Ellipse((E0,N0),
                                       width=2.*ap, height=2.*bp,
                                       angle=phi*180./n.pi,
                                       ec='r', fc='none',
                                       lw=1.5,
                                       alpha=1.,
                                       ))
    ax1.annotate(r"Perturber", (40., -50.),
                 (30., -30.),
                 ha="left", va="center",
                 size=10,
                 multialignment='center',
                 arrowprops=dict(arrowstyle='->',
                                 fc="w", ec="k",
                                 connectionstyle="arc3,rad=-0.05",
                                 ),
                 )

    ax1.annotate(r"Dust belt", (65., 50.),
                 (75., 75.),
                 ha="center", va="center",
                 size=10,
                 multialignment='center',
                 arrowprops=dict(arrowstyle='->',
                                 fc="w", ec="k",
                                 connectionstyle="arc3,rad=-0.05",
                                 ),
                 )



    omegap, Omegap, Ip = get_transformation(omega_per, Omega_per, I_per, omega_per2, Omega_per2, I_per2)
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(a_per, e_per, Ip, omegap, Omegap)
    ax2.add_artist(mpl.patches.Ellipse((E0,N0),
                                       width=2.*ap, height=2.*bp,
                                       angle=phi*180./n.pi,
                                       ec='r', fc='none',
                                       lw=1.5,
                                       alpha=1.,
                                       ))

    # show circles for ADI for r = N*lam/D/dtheta
    N = 5.
    lam = 1.05e-4 # [cm]
    D = 8.0e2 # [cm]  FIXME  apodizer reduces this...
    dist = 73. # [pc]
    dthetas = n.array([3., 50.])*const.dtor # [rad]
    for i, dtheta in enumerate(dthetas):
        r = N*lam/D/dtheta * 206265. * dist # [AU]
        ax2.add_artist(mpl.patches.Circle((0.,0.),
                                          radius=r,
                                          fill=False,
                                          ec='k',
                                          lw=0.5,
                                          ))

    phi = 80.*const.dtor
    phi2 = 90.*const.dtor
    r2 = 70.
    ax2.annotate(r"Proposed ADI limit", (-r*n.cos(phi), r*n.sin(phi)),
                 (-r2*n.cos(phi2), r2*n.sin(phi2)),
                 ha="left", va="center",
                 size=10,
                 multialignment='center',
                 arrowprops=dict(arrowstyle='->',
                                 fc="w", ec="k",
                                 connectionstyle="arc3,rad=-0.05",
                                 ),
                 )

    # show circle for coronagraph IWA
    #r_mask = .156/2. # [arcsec]  Y-band
    r_mask = .184/2. # [arcsec]  J-band
    r_mask_AU = r_mask*dist
    ax2.add_artist(mpl.patches.Circle((0.,0.),
                                      radius=r_mask_AU,
                                      fill=False,
                                      hatch='xxxxxxx',
                                      ec='k',
                                      lw=0.5,
                                      ))


    # draw scale bar
    ys, xs = -75., 85. # [AU]
    ax2.plot((xs,xs-dist), (ys,ys),
             c='k',
             lw=1.5,
             marker='|',
             mec='k',
             mfc='k'
             )
    ax2.text(xs-dist/2., ys+5.,
             r"1\,$''$",
             va='bottom',
             ha='center',
             color='k',
             )


    for ax in (ax1, ax2):
        ax.axis(view_range)
        ax.set_aspect('equal')
    ##     ax.set_xlabel(r"$\Delta\alpha$ [AU]")
    ## ax1.set_ylabel(r"$\Delta\delta$ [AU]")
    ## pylab.setp(ax2.get_yticklabels(), visible=False)
    for ax in (ax1, ax2):
        pylab.setp(ax.get_xticklabels(), visible=False)
    pylab.setp(ax2.get_yticklabels(), visible=False)
    ax1.set_ylabel("AU")

    pylab.draw()
    pylab.show()
    pylab.savefig('geometry.pdf', dpi=300)
    pylab.savefig('geometry.png', dpi=300)


def get_mie(x, m, n_ang=None):
    if n_ang is None:
        n_ang = 1000

    from bhmie import bhmie
    s1, s2, qext, qsca, qback, gsca = bhmie(x, m, n_ang)

    phi_sca = n.linspace(0., n.pi, 2*n_ang-1, endpoint=False)

    s11 = 0.5*(n.abs(s1)**2 + n.abs(s2)**2)
    s12 = 0.5*(n.abs(s2)**2 - n.abs(s1)**2)
    sc_int = s11
    pol_int = s12

    # normalize
    from scipy.integrate import simps
    norm = simps(sc_int, phi_sca)
    sc_int /= norm
    pol_int /= norm

    return phi_sca, sc_int, pol_int


def show_mie(phi_sca, sc_int, pol_int=None):

    
    # show intensity vs. scattering angle
    fig = pylab.figure(5, figsize=(5,5))
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111, polar=True)

    ax.plot(phi_sca, sc_int, c='k')
    ax.plot(2.*n.pi-phi_sca, sc_int, c='k')
    if pol_int is not None:
        ax.plot(phi_sca, pol_int, c='k', ls='--')
        ax.plot(2.*n.pi-phi_sca, pol_int, c='k', ls='--')

    pylab.draw()
    pylab.show()


def fit_mie():

    with open('sc_ints.dat') as f:
        phi_sca = pickle.load(f)
        sc_int = pickle.load(f)

    from scipy.interpolate import interp1d
    def get_model_fn(p):
        "returns a function of scattering angle"
        #mre, x1, x2, sc1, sc2 = p
        x1, x2, sc1, sc2 = n.exp(p)

        #m = mre + 1e-8j
        m = 1.33 + 1e-8j

        phi_sca, sc_int1, pol_int1 = get_mie(x1, m)
        phi_sca, sc_int2, pol_int2 = get_mie(x2, m)

        sc_int = sc1*sc_int1 + sc2*sc_int2
        pol_int = n.abs(sc1*pol_int1 + sc2*pol_int2)

        return interp1d(phi_sca, sc_int), interp1d(phi_sca, pol_int)

    def fit_fn(p):
        sc_model_fn, pol_model_fn = get_model_fn(p)
        model_vals = sc_model_fn(phi_sca)
        return sc_int - model_vals

    from scipy.optimize import leastsq
    sp = n.log(n.array((0.5, 2.1, 4e8, 4e8)))
    p_opt, ier = leastsq(fit_fn, sp.copy(),
                         )
    print ier
    x1, x2, sc1, sc2 = n.exp(p_opt)
    print "%.4f\t%.4f\t%f\t%f\t%f" % (x1, x2, sc1, sc2, sc1/sc2)

    sc_model_fn, pol_model_fn = get_model_fn(p_opt)
    phi_sca = n.linspace(0., n.pi, 300, endpoint=False)
    sc_int = sc_model_fn(phi_sca)
    pol_int = pol_model_fn(phi_sca)
    show_mie(phi_sca, sc_int, pol_int=pol_int)

    ax = pylab.gca()
    m = 1.33 + 1e-8j
    phi_sca, sc_int1, pol_int1 = get_mie(x1, m)
    ax.plot(phi_sca, sc1*sc_int1, c='b')
    ax.plot(2.*n.pi-phi_sca, sc1*sc_int1, c='b')
    phi_sca, sc_int2, pol_int2 = get_mie(x2, m)
    ax.plot(phi_sca, sc2*sc_int2, c='r')
    ax.plot(2.*n.pi-phi_sca, sc2*sc_int2, c='r')

    pylab.draw()
    pylab.show()
    
    #ipshell()
    

def plot_radial_profile():
    mc = pickle.load(open('hr4796-fitter-mcmc.dat'))
    n_burn = 90
    chain = mc.chain[:, n_burn:, :]
    n_walker, n_sample, n_dim = chain.shape
    chain = chain.reshape(n_walker * n_sample, n_dim)
    pylab.subplot(211)
    for j, p in enumerate(chain):
        mc.set_fit_parms(p)
        gamma_in = mc.ringmodel.parms['gamma_in']
        gamma = mc.ringmodel.parms['gamma']
        beta = mc.ringmodel.parms['beta']
        r_in, r_mid, r_out = mc.ringmodel.parms['r_in'], mc.ringmodel.parms['r'], mc.ringmodel.parms['r_out']
        drad0 = .1
        n_step_rad = int((r_out-r_in)/drad0)
        drad = (r_out-r_in)/n_step_rad
        r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid
        i_factors = smooth_broken_power_law(r_factors, 1., 1., 1., -gamma_in, -gamma, beta)/r_factors**2/n_step_rad
        pylab.plot(r_factors * r_mid, i_factors)
        pylab.xlabel('Distance (AU)')
        pylab.ylabel('Intensity factors')
    psf_j_short = fits.getdata('psf_j_short.fits')
    psf_j = fits.getdata('psf_j.fits')
    psf_k_short = fits.getdata('psf_k_short.fits')
    psf_k = fits.getdata('psf_k.fits')
    psf_j_short = n.sum(psf_j_short, axis = 0)[272:292]
    psf_j = n.sum(psf_j, axis = 0)[272:292]
    psf_k_short = n.sum(psf_k_short, axis = 0)[272:292]
    psf_k = n.sum(psf_k, axis = 0)[272:292]
    pylab.subplot(212)
    pylab.plot(psf_j, label = 'J psf averaged')
    pylab.plot(psf_k, label = 'K psf averaged')
    pylab.plot(psf_j_short, label = 'J psf first frame')
    pylab.plot(psf_k_short, label = 'K psf first frame')
    pylab.legend()
    pylab.show()
    

def make_figure_observed_images(save_fn='HR4796A_images.pdf'):
    "make figure for observed images"


    # load image data
    im_dir, labels, im_I_fns, im_P_fns, im_save_fns, klip_save_fns, psf_save_fns = load_image_info()
    n_im = len(labels)
    ims_I = []
    ims_P = []
    extents_I = []
    extents_P = []
    scale = 0.01414 # [arcsec/pix]
    for im_I_fn, im_P_fn in zip(im_I_fns, im_P_fns):
        # get total intensity image
        _log.debug("loading %s" % im_I_fn)
        im_I = fits.getdata(im_I_fn) # [ADU/sec]
        im_I = ma.masked_where(n.isnan(im_I), im_I)
        cen_I = n.array((im_I.shape))/2. # NOTE  only for klip
        extent_I = [cen_I[1]*scale, -(im_I.shape[1]-1-cen_I[1])*scale,
                    -cen_I[0]*scale, (im_I.shape[0]-1-cen_I[0])*scale]

        # get polarization image
        _log.debug("loading %s" % im_P_fn)
        im, hdr = fits.getdata(im_P_fn, header=True)
        im_P = im #n.sqrt(im[1,:,:]**2 + im[2,:,:]**2)
        im_P = ma.masked_where(n.isnan(im_P), im_P)
        cen_P = n.array((140,140))
        if ('BUNIT' in hdr) and (hdr['BUNIT'] == 'ADU per coadd'):
            im_P /= hdr['ITIME'] # [ADU/coadd] -> [ADU/sec]
        extent_P = [cen_P[1]*scale, -(im_P.shape[1]-1-cen_P[1])*scale,
                    -cen_P[0]*scale, (im_P.shape[0]-1-cen_P[0])*scale]

        # accumulate images
        ims_I.append(im_I)
        ims_P.append(im_P)
        extents_I.append(extent_I)
        extents_P.append(extent_P)


    view_range = [0.8, -0.8, -1.3, 1.3] # [arcsec]


    # set up figure
    im_width = 2. # [in]
    axrat = (view_range[0]-view_range[1])/(view_range[3]-view_range[2]) # width/height
    im_height = im_width/axrat # [in]
    cb_width = 0.2 # [in]
    impad_l = 0.7 # [in]
    impad_cb = 0.1 # [in]
    impad_r = 0.7 # [in]
    impad_b = 0.5 # [in]
    impad_m = 0.1 # [in]
    impad_t = 0.3 # [in]

    fig_width = impad_l + n_im*im_width + impad_cb + cb_width + impad_r # [in]
    fig_height = impad_b + 2*im_height + impad_m + impad_t # [in]

    figsize = (fig_width, fig_height)

    fig = pylab.figure(0, figsize=figsize)
    fig.hold(True)
    fig.clear()

    # set up image axes
    axs_I, axs_P = [], []
    for i in range(n_im):
        ax_I = fig.add_axes([(impad_l+i*im_width)/fig_width,
                             (impad_b+im_height+impad_m)/fig_height,
                             im_width/fig_width,
                             im_height/fig_height])
        ax_P = fig.add_axes([(impad_l+i*im_width)/fig_width,
                             impad_b/fig_height,
                             im_width/fig_width,
                             im_height/fig_height])
        axs_I.append(ax_I)
        axs_P.append(ax_P)
    

    # set up colorbar axes
    axs_cb = []
    for i in range(2):
        ax_cb = fig.add_axes([(impad_l+n_im*im_width+impad_cb)/fig_width,
                              (impad_b+(1-i)*(im_height+impad_m))/fig_height,
                              cb_width/fig_width,
                              im_height/fig_height])
        ax_cb.yaxis.tick_right()
        ax_cb.yaxis.set_label_position('right')
        axs_cb.append(ax_cb)


    # draw images
    for i in range(n_im):
        vmax = 4.-i*2.5
        vmin = -.05*vmax
        imdata_I = axs_I[i].imshow(ims_I[n_im-1-i],
                                   interpolation='bicubic',
                                   #cmap=mpl.cm.gist_stern,
                                   cmap=mpl.cm.gnuplot,
                                   extent=extents_I[n_im-1-i],
                                   vmin=vmin,
                                   vmax=vmax,
                                   )
        axs_I[i].axis(view_range)

        imdata_P = axs_P[i].imshow(ims_P[n_im-1-i],
                                   interpolation='bicubic',
                                   cmap=mpl.cm.gist_heat,
                                   extent=extents_P[n_im-1-i],
                                   vmin=0.,
                                   vmax=0.7*vmax,
                                   )
        axs_P[i].axis(view_range)

    # draw colorbars
    pylab.colorbar(imdata_I, axs_cb[0])
    pylab.colorbar(imdata_P, axs_cb[1])

    # annotation
    #

    # tick locations and colors
    tick_col='w'
    for ax in axs_I+axs_P:
        # tick locations
        ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(0.5))
        ax.yaxis.set_major_locator(mpl.ticker.MultipleLocator(0.5))
        ax.xaxis.set_minor_locator(mpl.ticker.MultipleLocator(0.1))
        ax.yaxis.set_minor_locator(mpl.ticker.MultipleLocator(0.1))

        # tick colors
        ax.tick_params(axis='x', color=tick_col)
        ax.tick_params(axis='y', color=tick_col)
        ax.spines['bottom'].set_color(tick_col)
        ax.spines['top'].set_color(tick_col)
        ax.spines['left'].set_color(tick_col)
        ax.spines['right'].set_color(tick_col)

    # tick labels
    for i in range(n_im):
        pylab.setp(axs_I[i].get_xticklabels(), visible=False)
        if i != 0:
            pylab.setp(axs_I[i].get_yticklabels(), visible=False)
            pylab.setp(axs_P[i].get_yticklabels(), visible=False)
    for i in range(2):
        pylab.setp(axs_cb[i].get_xticklabels(), visible=False)

    # text labels
    for i in range(n_im):
        axs_I[i].set_title(labels[n_im-1-i])
    for ax in (axs_I[0], axs_P[0]):
        ax.set_ylabel(r"$\Delta\delta$ ($''$)")
    for ax in axs_P:
        ax.set_xlabel(r"$\Delta\alpha$ ($''$)")
    axs_cb[0].set_ylabel(r"$I$ ()")
    axs_cb[1].set_ylabel(r"$P=\sqrt{Q^2+U^2}$ ()")

    ## # TEMP
    ## for ax in axs_I+axs_P:
    ##     ax.axhline(.9, c='w')
    ##     ax.axhline(-.95, c='w')

    pylab.draw()
    pylab.show()
    if save_fn is not None:
        fig.savefig(save_fn, dpi=300)
        
        
def make_figure_profiles(save_fn = "HR4796A_profiles.pdf"):
    "make figure for extracted profiles"
    import constants as c

    # load mcmc data
    _log.info('loading MCMC data ...')
    mcmc_fn = 'hr4796-fitter-mcmc.dat'
    with open(mcmc_fn) as f:
        mc = pickle.load(f)

    im_dir, labels, im_I_fns, im_P_fns, im_save_fns, klip_save_fns,psf = load_image_info()

    n_burn = 50 # FIXME


    fig = pylab.figure(1)
    fig.hold(True)
    fig.clear()

    ax_I = fig.add_subplot(311)
    ax_P = fig.add_subplot(312)#, sharex=ax_I)
    ax_p = fig.add_subplot(313)#, sharex=ax_I)


    # burn
    chain = mc.chain[:,n_burn:,:]
    n_walker, n_sample, n_dim = chain.shape
    chain = chain.reshape(n_walker*n_sample, n_dim)

    nu = n.linspace(0., 2.*n.pi, 2000)#, endpoint=False)

    edges_phi = n.linspace(0., 180., 201)
    edges_I = n.linspace(0., 1e7, 101)
    edges_P = n.linspace(0., 3e6, 101)
    #edges_p = n.linspace(0., 1., 101)
    edges_p = n.linspace(0., 1.2, 101) # TEMP
    hist_I = n.zeros((len(edges_I)-1, len(edges_phi)-1, mc.n_images), dtype=n.float)
    hist_P = n.zeros((len(edges_P)-1, len(edges_phi)-1, mc.n_images), dtype=n.float)
    hist_p = n.zeros((len(edges_p)-1, len(edges_phi)-1, mc.n_images), dtype=n.float)


    fcs = ['r', 'b']
    kw = {'alpha':0.003, 'marker':'.', 'edgecolor':'none'}
    _log.info('building histograms ...')
    for j, p in enumerate(chain):
        if j % 1000 == 0: _log.debug("\tsample %d/%d" % (j, len(chain)))
        mc.set_fit_parms(p)
        r = mc.ringmodel.parms['r']
        offset = mc.ringmodel.parms['offset']
        I = mc.ringmodel.parms['I']
        omega = mc.ringmodel.parms['omega']
        Omega = mc.ringmodel.parms['Omega']

        # for OffsetRing
        so, co = n.sin(omega), n.cos(omega)
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        oz = offset * so*si # [AU]

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)
        r_proj = n.sqrt(pN**2+pE**2) # [AU]


        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        # get parameteric construction of ring intensity (not r_star scaled)
        intensities = mc.ringmodel._get_intensity(nu, phi_sca)
        pol_intensities = mc.ringmodel._get_pol_intensity(nu, phi_sca)


        #wi = n.nonzero(r_proj > mask_rad)[0]
        wi = n.s_[:] # FIXME
        wpi = n.s_[:] # FIXME


        for i, (sc_int, sc_pol_int) in enumerate(zip(intensities, pol_intensities)):
            h, d1, d2 = n.histogram2d(sc_int[wi], phi_sca[wi]*c.rtod,
                                      bins=(edges_I, edges_phi),
                                      )
            hist_I[:,:,i] += h
            h, d1, d2 = n.histogram2d(sc_pol_int[wpi], phi_sca[wpi]*c.rtod,
                                      bins=(edges_P, edges_phi),
                                      )
            hist_P[:,:,i] += h
            h, d1, d2 = n.histogram2d(sc_pol_int[wi]/sc_int[wi], phi_sca[wi]*c.rtod,
                                      bins=(edges_p, edges_phi),
                                      )
            hist_p[:,:,i] += h

    # divide each histogram column by its sum
    #  (this is because theta_sca is not sampled uniformly)
    sI = hist_I.sum(axis=0)
    sI[n.nonzero(sI==0)] = 1.
    hist_I /= sI[n.newaxis,:,:]
    sP = hist_P.sum(axis=0)
    sP[n.nonzero(sP==0)] = 1.
    hist_P /= sP[n.newaxis,:,:]
    sp = hist_p.sum(axis=0)
    sp[n.nonzero(sp==0)] = 1.
    hist_p /= sp[n.newaxis,:,:]

    # show histograms
    _log.info('plotting histograms ...')
    def proc_hist(in_hist):
        hist = n.ones(in_hist.shape[0:2]+(3,), dtype=n.float)
        hist[:,:,2] *= 1.-n.sqrt(in_hist[:,:,0]/in_hist[:,:,0].max())
        hist[:,:,1] *= 1.-n.sqrt(in_hist[:,:,0]/in_hist[:,:,0].max())
        hist[:,:,1] *= 1.-n.sqrt(in_hist[:,:,1]/in_hist[:,:,1].max())
        hist[:,:,0] *= 1.-n.sqrt(in_hist[:,:,1]/in_hist[:,:,1].max())
        return hist
    ax_I.imshow(proc_hist(hist_I),
                interpolation='nearest',
                #cmap=mpl.cm.gray_r,
                extent=[edges_phi[0], edges_phi[-1], edges_I[0], edges_I[-1]],
                )
    ax_P.imshow(proc_hist(hist_P),
                interpolation='nearest',
                #cmap=mpl.cm.gray_r,
                extent=[edges_phi[0], edges_phi[-1], edges_P[0], edges_P[-1]],
                )
    ax_p.imshow(proc_hist(hist_p),
                interpolation='nearest',
                #cmap=mpl.cm.gray_r,
                extent=[edges_phi[0], edges_phi[-1], edges_p[0], edges_p[-1]],
                )
    for ax in (ax_I, ax_P, ax_p):
        ax.set_autoscale_on(False)
        ax.set_aspect('auto')

    # make legend
    lines = [mpl.lines.Line2D((0,0),(1,1),linestyle='-',c=fc) for fc in fcs]
    ax_P.legend(lines[::-1], labels[::-1], loc=2)

    # annotation
    for ax in (ax_I, ax_P, ax_p):
        ax.axvline(90., ls=':', c='k')
    for ax in (ax_I, ax_P):
        pylab.setp(ax.get_xticklabels(), visible=False)
    ax_I.set_ylabel('$I$ ()')
    ax_P.set_ylabel('$P$ ()')
    ax_p.set_ylabel('$p=P/I$')
    ax_p.set_xlabel(r"$\theta$ ($^\circ$)")

    pylab.draw()
    pylab.show()
    if save_fn is not None:
        fig.savefig(save_fn, dpi=300)


def make_figure_colors(save_fn='HR4796A_colors.pdf'):
    "make figure for extracted color profiles"
    import constants as c

    # load mcmc data
    _log.info('loading MCMC data ...')
    mcmc_fn = 'hr4796-fitter-mcmc.dat'
    with open(mcmc_fn) as f:
        mc = pickle.load(f)

    im_dir, labels, im_I_fns, im_P_fns, im_save_fns, klip_save_fns, psf = load_image_info()

    n_burn = 50 # FIXME


    fig = pylab.figure(2)
    fig.hold(True)
    fig.clear()

    ax_I = fig.add_subplot(311)
    ax_P = fig.add_subplot(312)#, sharex=ax_I)
    ax_p = fig.add_subplot(313)#, sharex=ax_I)


    n_walker, n_sample, n_dim = mc.chain.shape

    # burn
    chain = mc.chain[:,n_burn:,:]
    n_walker, n_sample, n_dim = chain.shape
    chain = chain.reshape(n_walker*n_sample, n_dim)

    nu = n.linspace(0., 2.*n.pi, 2000)#, endpoint=False)

    edges_nu = n.linspace(0., 360., 201)
    edges_I = n.linspace(0., 10., 101)
    edges_P = n.linspace(0., 15., 101)
    edges_p = n.linspace(0., 10., 101)
    hist_I = n.zeros((len(edges_I)-1, len(edges_nu)-1), dtype=n.float)
    hist_P = n.zeros((len(edges_P)-1, len(edges_nu)-1), dtype=n.float)
    hist_p = n.zeros((len(edges_p)-1, len(edges_nu)-1), dtype=n.float)


    ## lines = []
    ## kw = {'alpha':0.003, 'marker':'.', 'edgecolor':'none', 'c':'k'}
    _log.info('building histograms ...')
    for j, p in enumerate(chain):
        if j % 1000 == 0: _log.debug("\tsample %d/%d" % (j, len(chain)))
        mc.set_fit_parms(p)
        r = mc.ringmodel.parms['r']
        offset = mc.ringmodel.parms['offset']
        I = mc.ringmodel.parms['I']
        omega = mc.ringmodel.parms['omega']
        Omega = mc.ringmodel.parms['Omega']

        # for OffsetRing
        so, co = n.sin(omega), n.cos(omega)
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        oz = offset * so*si # [AU]

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)
        r_proj = n.sqrt(pN**2+pE**2) # [AU]


        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        # get parameteric construction of ring intensity (not r_star scaled)
        intensities = mc.ringmodel._get_intensity(nu, phi_sca)
        pol_intensities = mc.ringmodel._get_pol_intensity(nu, phi_sca)


        nup = (nu + omega) % (2.*n.pi)

        #wi = n.nonzero(r_proj > mask_rad)[0]
        wi = n.s_[:] # FIXME
        wpi = n.s_[:] # FIXME


        col_I = intensities[1]/intensities[0]
        col_P = pol_intensities[1]/pol_intensities[0]
        col_p = col_P/col_I
        ## ## ax_I.scatter(phi_sca[wi]*c.rtod, col_I[wi], **kw)
        ## ## ax_P.scatter(phi_sca[wpi]*c.rtod, col_P[wpi], **kw)
        ## ## ax_p.scatter(phi_sca[wi]*c.rtod, col_p[wi], **kw)
        ## ax_I.scatter(nu[wi]*c.rtod, col_I[wi], **kw)
        ## ax_P.scatter(nu[wpi]*c.rtod, col_P[wpi], **kw)
        ## ax_p.scatter(nu[wi]*c.rtod, col_p[wi], **kw)
        h, d1, d2 = n.histogram2d(col_I[wi], nup[wi]*c.rtod,
                                  bins=(edges_I, edges_nu),
                                  )
        hist_I += h
        h, d1, d2 = n.histogram2d(col_P[wi], nup[wi]*c.rtod,
                                  bins=(edges_P, edges_nu),
                                  )
        hist_P += h
        h, d1, d2 = n.histogram2d(col_p[wi], nup[wi]*c.rtod,
                                  bins=(edges_p, edges_nu),
                                  )
        hist_p += h

    # show histograms
    ax_I.imshow(hist_I,
                interpolation='nearest',
                cmap=mpl.cm.gray_r,
                extent=[edges_nu[0], edges_nu[-1], edges_I[0], edges_I[-1]],
                )
    ax_P.imshow(hist_P,
                interpolation='nearest',
                cmap=mpl.cm.gray_r,
                extent=[edges_nu[0], edges_nu[-1], edges_P[0], edges_P[-1]],
                )
    ax_p.imshow(hist_p,
                interpolation='nearest',
                cmap=mpl.cm.gray_r,
                extent=[edges_nu[0], edges_nu[-1], edges_p[0], edges_p[-1]],
                )
    for ax in (ax_I, ax_P, ax_p):
        ax.set_autoscale_on(False)
        ax.set_aspect('auto')

    # annotation
    #ax_I.set_xlim(0., 180.)
    #ax_I.set_xlim(0., 360.)
    ## for ax in (ax_I, ax_P, ax_p):
    ##     ax.axvline(90., ls=':', c='k')
    ## for ax in (ax_P, ax_p):
    ##     ax.set_ylim(bottom=0.)
    for ax in (ax_I, ax_P):
        pylab.setp(ax.get_xticklabels(), visible=False)
    ax_I.set_ylabel("$I$ (%s/%s)" % (labels[1], labels[0]))
    ax_P.set_ylabel("$P$ (%s/%s)" % (labels[1], labels[0]))
    ax_p.set_ylabel("$p=P/I$ (%s/%s)" % (labels[1], labels[0]))
    ax_p.set_xlabel(r"$\nu+\omega$ ($^\circ$)")

    pylab.draw()
    pylab.show()
    if save_fn is not None:
        fig.savefig(save_fn, dpi=300)


def make_figure_cartoon(save_fn='HR4796A_cartoon.pdf'):
    "make figure for geometry cartoon"
    import constants as c

    fig = pylab.figure(3, figsize=(4,5))
    fig.clear()
    fig.hold(True)
    ax = fig.add_subplot(111)

    # choose highest-probability parameters
    # FIXME
    mcmc_fn = 'hr4796-fitter-mcmc.dat'
    with open(mcmc_fn) as f:
        mc = pickle.load(f)
    # n.argmax(mc.lnprobability)
    # TEMP
    ls_fn = 'hr4796-fitter-leastsq2.dat'
    mc.ringmodel.restore_parms(ls_fn)

    r = mc.ringmodel.parms['r']
    offset = mc.ringmodel.parms['offset']
    #offset *= 10 # TEMP
    I = -mc.ringmodel.parms['I']
    omega = mc.ringmodel.parms['omega']
    Omega = mc.ringmodel.parms['Omega']

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]
    oE = offset * (sO*co+cO*so*ci) # [AU]
    oz = offset * so*si # [AU]
    def get_NE(nu):
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)

        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        return pN, pE, phi_sca

    ## # draw line of nodes
    ## nu = n.array((-omega, n.pi-omega))
    ## pN, pE, phi_sca = get_NE(nu)
    ## ax.plot(pE, pN, ls='--', c='k', lw=1., zorder=1)

    # calculate ellipse
    nu = n.linspace(0., 2.*n.pi, 100, endpoint=True)
    pN, pE, phi_sca = get_NE(nu)

    # draw ellipse
    ax.plot(pE, pN, ls='-', c='k', lw=2., zorder=2)

    # mark phi_sca angles
    #

    ft = 1.4
    facts = n.array((0.95, 1.3))

    # nus for which theta_sca = min, max
    from scipy.optimize import fmin, brentq
    nus = n.array([fmin(lambda nu: get_NE(nu)[2], 0.)[0],
                   fmin(lambda nu: -get_NE(nu)[2], 0.)[0]]) % (2.*n.pi)
    nu_min = nus.min()
    nu_max = nus.max()
    #print "min", nu_min*180./n.pi, get_NE(nu_min)[2]*180./n.pi
    #print "max", nu_max*180./n.pi, get_NE(nu_max)[2]*180./n.pi

    phi_scas = n.arange(30., 180., 30.)
    for phi in phi_scas:
        def comp(nu):
            d = (get_NE(nu)[2]-phi*n.pi/180.) % (2.*n.pi)
            if d > n.pi: d -= 2.*n.pi
            return d
        # which nus does this correspond to?
        nu1 = brentq(comp, nu_min, nu_max) % (2.*n.pi)
        nu2 = brentq(comp, nu_max, nu_min+2.*n.pi) % (2.*n.pi)
        for nu in (nu1, nu2):
            pN, pE, phi_sca = get_NE(nu)
            ax.plot((2.-facts)*pE, (2.-facts)*pN, ls=':', c='k', zorder=3)
            tstr = r"$%d^\circ$" % phi
            ax.text((2.-ft)*pE, (2.-ft)*pN,
                    tstr,
                    #ha='left' if nu*c.rtod < 180. else 'right',
                    #va='top' if pN < 0. else 'bottom',
                    ha='center', va='center',
                    )

    # mark nu angles
    nus = n.arange(0., 360., 30.)
    for nu in nus:
        pN, pE, phi_sca = get_NE(nu*n.pi/180.)
        ax.plot(facts*pE, facts*pN, ls='-', c='k', zorder=3)
        tstr = r"$%d^\circ$" % nu
        if nu == 0.:
            tstr = r"$\nu = 0^\circ$"
            ha = 'center'
        else:
            ha='left' if nu > 180. else 'right'
        ax.text(ft*pE, ft*pN,
                tstr,
                ha=ha,
                va='top' if pN < 0. else 'bottom',
                )

    # mark star
    ax.scatter((0.,), (0.,), c='k', marker='*', s=80.)

    # mark projected offset
    #ax.scatter((-oE,), (-oN,), c='k', marker='o', s=20.)
    

    ax.set_aspect('equal')
    ax.set_xlim(60, -60)
    ax.set_axis_off()

    pylab.draw()
    pylab.show()
    if save_fn is not None:
        pylab.savefig(save_fn, dpi=300)

    #ipshell()



if __name__=='__main__':
    #logging.basicConfig(level=logging.INFO,
    logging.basicConfig(level=logging.DEBUG,
                        format='%(name)-12s: %(levelname)-8s %(message)s',
                        )

try:
    from mpi4py import MPI
    from emcee.utils import MPIPool
    size = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()
    name = MPI.Get_processor_name()

    _log = logging.getLogger("hr4796a.%s.%d" % (name, rank))

    _log.debug("I am process %d of %d on %s." % (rank, size, name))

    if size == 1:
        _log.info("Use mpirun -np <N> python <filename.py> with N>=2 to run in MPI mode.")
        has_mpi = False
        pool = None
    else:

        has_mpi = True

        #pool = MPIPool()
        pool = MPIPool(loadbalance=True)
        #pool = MPIPool(debug=True) # TEMP
        if not pool.is_master():
            _log.debug('I am not the master... waiting.')
            pool.wait()
            sys.exit(0)
        else:
            _log.debug('I am the master... continuing.')

except ImportError:
    has_mpi = False
    pool = None


if __name__=='__main__':
    #test_spline()
    #test_pspl_model()
    #test_pspl_model(test_180=True)
    #test_image_offset()
    #test_image_keplerian()
    #test_psf_model()
    #test_sbpl()

    ## fit_pol = True
    ## #fit_pol = False
    ## #redo = False
    ## redo = True
    ## fit_image(redo=redo, fit_pol=fit_pol)

    fit_images(redo_lsq1=False,
               redo_lsq2=False,
               redo_mcmc=True,
               lsq_only=False,
               show=True,
               )

#    leastsq_plot()
    #make_proposal_figure()


    ## x = 1.
    ## m = 1.+1e-8j
    ## phi_sca, sc_int = get_mie(x, m)
    ## show_mie(phi_sca, sc_int)

    #fit_mie()


    #make_figure_observed_images()
    #make_figure_observed_images(save_fn=None)
    #make_figure_profiles()
    #make_figure_profiles(save_fn=None)
    #make_figure_colors()
    #make_figure_colors(save_fn=None)
    #make_figure_cartoon()
    #make_figure_cartoon(save_fn=None)

    # cleanup
    if pool is not None:
        pool.close()

