import pyklip.instruments.GPI as gpi
import glob
import fm
import numpy
import matplotlib.pyplot as plot
import pyfits
import copy
import pyklip.parallelized as par
import pyklip.klip as klip
import scipy.ndimage as ndimage


import os
import image
import pickle
import image
import multiprocessing as mp
from astropy import wcs


def propogate():
    imgs = pyfits.getdata('fm.fits')
    plot.imshow(numpy.sum(imgs[0], axis=0), vmin = -1000,vmax = 1000)
    plot.colorbar()
    plot.show()

#    x = sorted(glob.glob('april_k/S20140422S0*'))
#    a = gpi.GPIData(x)
#    a.input = imgs[1]
#    par.klip_dataset(a, outputdir = '.', fileprefix = 'fm2', annuli = 1, subsections=1, numbasis = [1,5], mode = "ADI", minrot = 10)




def test1():
    x = sorted(glob.glob('april_k/S20140422S0*'))
    a = gpi.GPIData(x)
    images = a.input
    centers = a.centers
    parangs = a.PAs
    hdrs = a.wcs
    IWA = 11.
    modelims = numpy.zeros(numpy.shape(images))
#    model = pyfits.getdata('bestmodel_pol.fits') * 50.
    model = pyfits.getdata('model0.fits') 
    modelsize = numpy.shape(images)
    for i, pa in enumerate(parangs):
#        pa = parangs[i]
#        hdr = hdrs[i]
#        theta = -covert_pa_to_image_polar(pa + 90, hdr)
        canvas = copy.deepcopy(model)
        canvas = klip.rotate(canvas, pa , [140, 140], flipx = True)
        canvas[numpy.where(numpy.isnan(canvas))] = 0.
        modelims[i] += canvas
#        images[i] += canvas
        
    sub_ims = fm.klip_adi(images, modelims, centers, parangs, IWA, annuli = 1, subsections = 1, minrot = 5, numbasis =[1])
    for i in range(len(parangs)):
#        pa = parangs[i]
#        hdr = hdrs[i]
#        theta = -covert_pa_to_image_polar(pa + 90., hdr)
        sub_ims[i] = kliprotate(sub_ims[i], parangs[i], [140, 140], flipx = True)
    plot.imshow(numpy.nansum(sub_ims, axis = 0))
    plot.show()
    pyfits.writeto('simklip_reaflux.fits', numpy.nansum(sub_ims, axis = 0))
    return sub_ims
        



def kliprotate(img, angle, center, new_center=None, flipx=True, astr_hdr=None):
    """
    Rotate an image by the given angle about the given center.
    Optional: can shift the image to a new image center after rotation. Also can reverse x axis for those left
              handed astronomy coordinate systems

    Inputs:
        img: a 2D image
        angle: angle CCW to rotate by (degrees)
        center: 2 element list [x,y] that defines the center to rotate the image to respect to
        new_center: 2 element list [x,y] that defines the new image center after rotation
        flipx: default is True, which reverses x axis.
        astr_hdr: wcs astrometry header for the image
    Outputs:
        resampled_img: new 2D image
    """
    #convert angle to radians
    angle_rad = numpy.radians(angle)

    #create the coordinate system of the image to manipulate for the transform
    dims = img.shape
    x, y = numpy.meshgrid(numpy.arange(dims[1], dtype=numpy.float32), numpy.arange(dims[0], dtype=numpy.float32))

    #if necessary, move coordinates to new center
    if new_center is not None:
        dx = new_center[0] - center[0]
        dy = new_center[1] - center[1]
        x -= dx
        y -= dy

    #flip x if needed to get East left of North
    if flipx is True:
        x = center[0] - (x - center[0])
    
    #do rotation. CW rotation formula to get a CCW of the image
    xp = (x-center[0])*numpy.cos(angle_rad) + (y-center[1])*numpy.sin(angle_rad) + center[0]
    yp = -(x-center[0])*numpy.sin(angle_rad) + (y-center[1])*numpy.cos(angle_rad) + center[1]


    #resample image based on new coordinates
    #scipy uses y,x convention when meshgrid uses x,y
    #stupid scipy functions can't work with masked arrays (NANs)
    #and trying to use interp2d with sparse arrays is way to slow
    #hack my way out of this by picking a really small value for NANs and try to detect them after the interpolation
    #then redo the transformation setting NaN to zero to reduce interpolation effects, but using the mask we derived
    minval = numpy.min([numpy.nanmin(img), 0.0])
    nanpix = numpy.where(numpy.isnan(img))
    medval = numpy.median(img[numpy.where(~numpy.isnan(img))])
    img_copy = numpy.copy(img)
    img_copy[nanpix] = minval * 5.0
    resampled_img_mask = ndimage.map_coordinates(img_copy, [yp, xp], cval=minval * 5.0)
    img_copy[nanpix] = medval
    resampled_img = ndimage.map_coordinates(img_copy, [yp, xp], cval=numpy.nan)
    resampled_img[numpy.where(resampled_img_mask < minval)] = numpy.nan

    #edit the astrometry header if given to compensate for orientation
    if astr_hdr is not None:
        _rotate_wcs_hdr(astr_hdr, angle, flipx=flipx)

    return resampled_img



def covert_pa_to_image_polar(pa, astr_hdr):
    """                                                                                                                                          
    Given a parallactic angle (angle from N to Zenith rotating in the Eastward direction), calculate what                                        
    polar angle theta (angle from +X CCW towards +Y) it corresponds to                                                                           
                                                                                                                                                 
    Input:                                                                                                                                       
        pa: parallactic angle in degrees                                                                                                         
        astr_hdr: wcs astrometry header (astropy.wcs)                                                                                            
                                                                                                                                                 
    Output:                                                                                                                                      
        theta: polar angle in degrees                                                                                                            
    """
    rot_det = astr_hdr.wcs.cd[0,0] * astr_hdr.wcs.cd[1,1] - astr_hdr.wcs.cd[0,1] * astr_hdr.wcs.cd[1,0]
    if rot_det < 0:
        rot_sgn = -1.
    else:
        rot_sgn = 1.
    #calculate CCW rotation from +Y to North in radians                                                                                          
    rot_YN = numpy.arctan2(rot_sgn * astr_hdr.wcs.cd[0,1],rot_sgn * astr_hdr.wcs.cd[0,0])
    #now that we know where north it, find the CCW rotation from +Y to find location of planet                                                   
    rot_YPA = rot_YN - rot_sgn*pa*numpy.pi/180. #radians                                                                                         
    #rot_YPA = rot_YN + pa*numpy.pi/180. #radians                                                                                                

    theta = rot_YPA * 180./numpy.pi + 90.0 #degrees                                                                                              
    return theta


