from diskmodeler import ModelComparator
import numpy as n
import matplotlib.pyplot as plot
from matplotlib import pylab
import matplotlib as mpl
from matplotlib import gridspec
import pickle





conf = 'hr4796_config_allpol.ini'
leastsq_fname = 'leastsq_allpol.p'
mc = ModelComparator(conf, leastsq_save = leastsq_fname)


import matplotlib.pyplot as plt
plt.clf()
nu, r, phi_sca, r_proj, PA_proj, intensity_pol = mc.diskobjs[0].parametric_model_ring(n_step=200)
plt.plot(nu * 180. / n.pi, intensity_pol / n.max(intensity_pol), label = mc.diskobjs[0].label)
nu, r, phi_sca, r_proj, PA_proj, intensity_pol = mc.diskobjs[1].parametric_model_ring(n_step=200)
plt.plot(nu * 180. / n.pi, intensity_pol/ n.max(intensity_pol),label = mc.diskobjs[1].label)
nu, r, phi_sca, r_proj, PA_proj, intensity_pol = mc.diskobjs[2].parametric_model_ring(n_step=200)
plt.plot(nu * 180. / n.pi, intensity_pol/ n.max(intensity_pol),label = mc.diskobjs[2].label)
plt.title('Polarized intensity phase curve')
plt.xlabel('Phase angle')
plt.ylabel('Normalized Intensity')
plt.legend()
plt.show()



import matplotlib.pyplot as plt
plt.clf()
nu, r, phi_sca, r_proj, PA_proj, intensity_pol = mc.diskobjs[0].parametric_model_ring(n_step=200)
plt.plot(nu * 180. / n.pi, intensity_pol)
nu, r, phi_sca, r_proj, PA_proj, intensity_pol = mc.diskobjs[1].parametric_model_ring(n_step=200)
plt.plot(nu * 180. / n.pi, intensity_pol)
nu, r, phi_sca, r_proj, PA_proj, intensity_pol = mc.diskobjs[2].parametric_model_ring(n_step=200)
plt.plot(nu * 180. / n.pi, intensity_pol)
plt.show()
