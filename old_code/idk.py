#!/usr/bin/env python
#
# $Id$
#
# Michael Fitzgerald (mpfitz@ucla.edu) 2014-2-5
#
# code for modeling HR 4796A
#

## #---------------------------------------------------------------------------
## # This code loads IPython but modifies a few things if it detects it's running
## # embedded in another IPython session (helps avoid confusion)
try:
    get_ipython
except NameError:
    banner=exit_msg=''
else:
    banner = '*** Nested interpreter ***'
    exit_msg = '*** Back in main IPython ***'
from IPython.terminal.embed import InteractiveShellEmbed
## # Now create the IPython shell instance. Put ipshell() anywhere in your code
## # where you want it to open. 
ipshell = InteractiveShellEmbed(banner1=banner, exit_msg=exit_msg)
## #------------------------------------------------------------------------------

import os, sys, copy
import numpy as n
import numpy.ma as ma
import matplotlib as mpl
import pylab
from astropy.io import fits
import cPickle as pickle
import pyfits
import logging
_log = logging.getLogger('hr4796a')



def get_tck(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:n_param+k] = params[0:k]

    return t, c, k

def get_tck2(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)

    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param

    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:k] = params[-k:]
    c[k:n_param+k] = params

    return t, c, k



def get_tck180(p, k=3):
    '''
    get periodic spline parameters for mirrored past 180deg
    Inputs:
       p:   the anchor points for the spline
       k:   the order of the spline fit
    Outputs:
       t:   the spline's knot vector
       c:   the spline coefficients 
    '''

    # mirror first cell
    ik = int(n.ceil(k/2.))
    params = n.concatenate((p[0:ik], p[k-ik-1::-1], p[ik:]))

    n_param = len(params)

    xx_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/(2*n_param-k-1)

    n_knot = 2*n_param-1 + k + 1
    t = (n.arange(n_knot)-k)*dt

    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:2*n_param-1] = params[-2::-1]

    return t, c, k

    

from scipy.interpolate import splev
eval_pspl = lambda phi, tck: splev(phi % (2.*n.pi), tck)


def smooth_broken_power_law(x, x_b, x_0, y_0, gam1, gam2, beta,
                            x_min=None, x_max=None):
    '''
    A smoothly varying broken power law
    Inputs: 
       x:     x points for the y function
       x_b:   break location
       x_0:   scale parameter for the width of the function 
       y_0:   scale parameter for the height of the function
       gam1:  the power law that dominates before the break point
       gam2:  the power law that dominates after the break point
       beta:  smoothing factor
    Outputs:
       y:     function of x
    '''
    if beta < 0: raise ValueError

    if beta != 0:
        y = (1.+(x_0/x_b)**((gam1-gam2)/beta))**beta * y_0 * (x/x_0)**gam1 * ( 1. + (x/x_b)**((gam1-gam2)/beta) )**(-beta)
    else:
        y = n.zeros_like(x)
        w = n.nonzero(x <= x_b)
        y[w] = y_0 * (x[w]/x_0)**gam1
        w = n.nonzero(x > x_b)
        y[w] = y_0 * (x_b/x_0)**gam1 * (x[w]/x_b)**gam2

    # clip range
    if x_min is not None:
        y[x<x_min] = 0.
    if x_max is not None:
        y[x>x_max] = 0.
    return y


class OffsetRingModel(object):
    '''
    Class for a circular ring that is offset 
    Inputs:
         n_int_parms: The number of anchor points for the intensity spline
         n_images:    The number of wavebands used
         psf_method:  Convolution method. Options are 'gaussian' 'airy' and 'inputim'
         init_pol:    Whether or not to generate a pol image
         psf_ims:     If psf method is inputim, these are the psf images
    '''
    def __init__(self, n_int_parms=9, n_images=1,
                 psf_method='gaussian',
                 init_pol=False, psf_ims = None):

        self.n_images = n_images
        self.psf_method = psf_method
        self.psf_ims = psf_ims

        self.parmlist = ['r', 'r_in', 'r_out', 'gamma_in', 'gamma', 'beta',
                         'offset', 'omega', 'Omega', 'I',
                         'psf_parms', 'offsx', 'offsy',
                         'int_parms', 'pol_int_parms',
                         ]

        self.parms = dict([(k, None) for k in self.parmlist])
        self.parms.update({'r':78.90, # [AU]
                           'r_in':None, # [AU]
                           'r_out':None, # [AU]
                           'gamma_in':0.,
                           'gamma':0.,
                           'beta':0.,
                           'offset':1.5, # [AU]
                           'omega':-22.3*n.pi/180., # [rad]
                           'Omega':27.8*n.pi/180., # [rad]
                           'I':77.*n.pi/180., # [rad]
                           'psf_parms':n.ones(n_images, dtype=n.float)*21.7, # [pix]
                           'offsy':0., # [pix]
                           'offsx':0., # [pix]
                           'int_parms':n.ones(n_int_parms*n_images, dtype=n.float)*2e7,
                           'pol_int_parms':None,
                           })

        self.is_fixed = dict([(k,False) for k in self.parmlist])
        self.is_fixed['gamma_in'] = True
        self.is_fixed['gamma'] = True
        self.is_fixed['beta'] = True
        self.is_fixed['r_out'] = True
        self.is_fixed['r_in'] = True
        self.is_fixed['pol_int_parms'] = True

        self.parm_lens = dict([(k,1) for k in self.parmlist])
        self.parm_lens['psf_parms'] = n_images
        self.parm_lens['int_parms'] = n_int_parms*n_images

        if init_pol:
            self.parms['pol_int_parms'] = n.ones(n_int_parms*n_images, dtype=n.float) * 1e6
            self.parm_lens['pol_int_parms'] = n_int_parms*n_images
            self.is_fixed['pol_int_parms'] = False

    def save_parms(self, fn):
        with open(fn, 'w') as f:
            pickle.dump(self.parms, f, 2)
            pickle.dump(self.is_fixed, f, 2)
            pickle.dump(self.parm_lens, f, 2)

    def restore_parms(self, fn):
        with open(fn) as f:
            self.parms = pickle.load(f)
            self.is_fixed = pickle.load(f)
            self.parm_lens = pickle.load(f)


    def _get_intensity(self, nu, phi_sca):
        int_parms = self.parms['int_parms']
        omega = self.parms['omega']
        nup = omega+nu
        if self.n_images > 1:
            intensities = [eval_pspl(nup, get_tck(ip)) for ip in n.reshape(int_parms, (self.n_images,len(int_parms)/self.n_images))]
        else:
            intensities = [eval_pspl(nup, get_tck(int_parms))]
        return intensities
    
    def _get_pol_intensity(self, nu, phi_sca):
        pol_int_parms = self.parms['pol_int_parms']
        omega = self.parms['omega']
        nup = omega+nu
        if pol_int_parms is not None:
            if self.n_images > 1:
                pol_intensities = [eval_pspl(nup, get_tck(pip)) for pip in n.reshape(pol_int_parms, (self.n_images, len(pol_int_parms)/self.n_images))]
            else:
                pol_intensities = [eval_pspl(nup, get_tck(pol_int_parms))]
            return pol_intensities
        else: return None

    def print_parms(self):
        for p in self.parmlist:
            if self.parms[p] is None:
                _log.info("%s\t%s" % (p, str(self.parms[p])))
                continue
            if self.parm_lens[p] == 1:
                _log.info("%s\t%f" % (p, self.parms[p]))
            else:
                _log.info("%s\t%s" % (p, str(self.parms[p])))

    def model_ring(self, n_step=500):
        """
        Parametric representation of an eccentric ring.

        Inputs:
          r           [AU]  radius from ring center
          r_out       [AU]  outer radius
          offset      [AU]  offset size in disk plane
          I           [rad] inclination
          omega       [rad] argument of pericenter (for offset direction)
          Omega       [rad] longitude of ascending node
          int_parms         parameters for intensity function, which returns flux/rad
        """

        r = self.parms['r']
        offset = self.parms['offset']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']
        int_parms = self.parms['int_parms']
        pol_int_parms = self.parms['pol_int_parms']


        # get parameteric construction of ring position
        #

        nu = n.linspace(0., 2.*n.pi, n_step, endpoint=False) # [rad]

        so, co = n.sin(omega), n.cos(omega)
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)
        
        #Euler angle transformation
        pN = r * (cO*conu-sO*sonu*ci) # [AU]
        pE = r * (sO*conu+cO*sonu*ci) # [AU]
        pz = r * sonu*si

        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        oz = offset * so*si # [AU]

        #Subtract offset angles
        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)
        
        # scattering angle
        phi_sca = n.arccos(pz/r_star) # [rad]

        # projected radius
        r_proj = n.sqrt(pN**2+pE**2) # [AU]

        # projected PA
        PA_proj = n.arctan2(pE, pN)

        # get parameteric construction of ring intensity
        intensities = self._get_intensity(nu, phi_sca) / r_star**2

        # get parameteric construction of ring intensity
        if pol_int_parms is not None:
            pol_intensities = self._get_pol_intensity(nu, phi_sca) / r_star**2
        else:
            pol_intensities = None

        return nu, r_star, phi_sca, r_proj, PA_proj, intensities, pol_intensities



    def get_model_ring_image(self, shapes, cens, scale,
                             n_step=200,
                             drad0=2., # [AU]
                             fast=False,
                             fast_samp=4,
                             show=False):


        psf_parms = self.parms['psf_parms']
        gamma_in = self.parms['gamma_in']
        gamma = self.parms['gamma']
        beta = self.parms['beta']
        offs = n.array((self.parms['offsy'], self.parms['offsx']))
        offset = self.parms['offset']
        I = self.parms['I']
        omega = self.parms['omega']
        Omega = self.parms['Omega']
        psf_ims = self.psf_ims
        
        # get position/intensity profile
        nu, r, phi_sca, r_proj, PA_proj, intensities, pol_intensities = \
            self.model_ring(n_step=n_step)
        dnu = nu[1]-nu[0]
        assert nu[2]-nu[1] == dnu

        r_in, r_mid, r_out = self.parms['r_in'], self.parms['r'], self.parms['r_out']

        # FIXME
        if r_in is None:
            r_in = r_mid
        if r_out is None:
            r_out = r_mid

        n_step_rad = int((r_out-r_in)/drad0)
        drad = (r_out-r_in)/n_step_rad
        
        if n_step_rad > 1:
            r_factors = (r_in + n.arange(n_step_rad)*drad)/r_mid
        else:
            n_step_rad = 1
            r_factors = n.array((1.,))

        # broken power-law
        # NOTE  including number of steps so integral over intensity is conserved..
        i_factors = smooth_broken_power_law(r_factors, 1., 1., 1., -gamma_in, -gamma, beta)/r_factors**2/n_step_rad

        # ellipse center at a_in
        so, co = n.sin(omega), n.cos(omega)
        sO, cO = n.sin(Omega), n.cos(Omega)
        si, ci = n.sin(I), n.cos(I)
        oN = offset * (cO*co-sO*so*ci) # [AU]
        oE = offset * (sO*co+cO*so*ci) # [AU]
        xecen = -oE
        yecen = -oN


        def get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp):
            intensity = (i_factors[n.newaxis,:]*intensity[:,n.newaxis]).flatten()

            if fast:
                # fast method does not due subpixel positioning
                im = n.zeros(2*n.array(shape)*fast_samp, dtype=n.float)

                for k, (yy, xx) in enumerate(zip(n.round(ypos*fast_samp).astype(n.int),
                                                 n.round(xpos*fast_samp).astype(n.int))):
                    if (yy < 0) or (yy >= 2*shape[0]*fast_samp-1): continue
                    if (xx < 0) or (xx >= 2*shape[1]*fast_samp-1): continue
                    im[yy,xx] += intensity[k]*dnu

                zim = fft2(im)

                # convolve with PSF
#                zim *= zp

                # back to image domain
                im = ifft2(zim).real

                # rebin
                im = im.reshape(2*shape[0], fast_samp, 2*shape[1], fast_samp).sum(axis=(1,3))
            else:
                zim = n.zeros(2*n.array(shape), dtype=n.complex)
                for k, (yy, xx) in enumerate(zip(ypos, xpos)):
                    zim += intensity[k]*dnu * \
                           n.exp(-2.*n.pi*1j*(yy*ky[:,n.newaxis]+xx*kx[n.newaxis,:]))

                # convolve with PSF
#                zim *= zp

                # back to image domain
                im = ifft2(zim).real


            # undo zero padding region
            im = im[0:shape[0], 0:shape[1]]
            #im /= n_step_rad
            return im


        ims = []
        if pol_intensities is not None:
            pol_ims = []
        for i in range(self.n_images):
            shape = shapes[i]
            cen = cens[i]
            intensity = intensities[i]
            if self.n_images == 1:
                psf_sig = psf_parms
            else:
                psf_sig = psf_parms[i]
            if pol_intensities is not None:
                pol_intensity = pol_intensities[i]
            if self.psf_method == 'inputim':
                assert psf_ims is not None
                psf_im = psf_ims[i]
            sxpos = cen[1]+offs[1] # star x position
            sypos = cen[0]+offs[0] # star y position


            # positions along ellipses
            xpos = sxpos+(r_factors[n.newaxis,:]*(-r_proj*n.sin(PA_proj)+xecen)[:,n.newaxis]-xecen)/scale
            ypos = sypos+(r_factors[n.newaxis,:]*(r_proj*n.cos(PA_proj)-yecen)[:,n.newaxis]+yecen)/scale
            xpos = xpos.flatten()
            ypos = ypos.flatten()


            from numpy.fft import fft2, ifft2, fftshift, fftfreq
            if fast:
                ky, kx = fftfreq(2*shape[0]*fast_samp), fftfreq(2*shape[1]*fast_samp)
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*(psf_sig*fast_samp)**2
            else:
                ky, kx = fftfreq(2*shape[0]), fftfreq(2*shape[1])
                r2 = (ky[:,n.newaxis]**2+kx[n.newaxis,:]**2)*psf_sig**2
            if self.psf_method == 'gaussian':
                zp = n.exp(-0.5*r2)
            elif self.psf_method == 'airy':
                zp = n.clip(1.-n.sqrt(r2), 0., 1.)
            elif self.psf_method == 'inputim':
                if fast:
                    transfmd = fftshift(fft2(fftshift(psf_im)))
                    canvas = n.zeros((2. * shape[0] * fast_samp, 2. * shape[1] * fast_samp))
                    psfshape = n.shape(psf_im)
                    canvas[shape[0] * fast_samp - psfshape[0] / 2: shape[0] * fast_samp + psfshape[0]/2, 
                           shape[1] * fast_samp - psfshape[1] / 2: shape[1] * fast_samp + psfshape[1]/2] = transfmd
                    zp = fftshift(canvas)
                else:
                    zp = fftshift(fft2(psf_im))
            else:
                raise NotImplementedError


            ims.append(get_model_im(intensity, shape, cen, xpos, ypos, kx, ky, zp))
            if pol_intensities is not None:
                pol_ims.append(get_model_im(pol_intensity, shape, cen, xpos, ypos, kx, ky, zp))

            if show:
                extent = [-(cen[1]+offs[1])*scale, (shape[1]-1-(cen[1]+offs[1]))*scale,
                          -(cen[0]+offs[0])*scale, (shape[0]-1-(cen[0]+offs[1]))*scale]


                fig = pylab.figure(i)
                fig.clear()
                fig.hold(True)
                if pol_intensities is not None:
                    ax = fig.add_subplot(121)
                    ax2 = fig.add_subplot(122)
                else:
                    ax = fig.add_subplot(111)
                ax.imshow(ims[i],
                          cmap=mpl.cm.gray,
                          interpolation='nearest',
                          extent=extent,
                          )
                ax.set_autoscale_on(False)
                ax.scatter((0.,), (0.,), c='w')
                ax.scatter((-xecen,), (yecen,), c='g')
                ax.set_aspect('equal')

                if pol_intensities is not None:
                    ax2.imshow(pol_ims[i],
                               cmap=mpl.cm.gray,
                               interpolation='nearest',
                               extent=extent,
                               )
                    ax2.set_autoscale_on(False)
                    ax2.scatter((0.,), (0.,), c='w')
                    ax2.scatter((-xecen,), (yecen,), c='g')
                    ax2.set_aspect('equal')

                pylab.draw()
                pylab.show()

        if pol_intensities is not None:
            return ims, pol_ims
        else:
            return ims



def test_image_offset():
    n_images = 2
    rm = OffsetRingModel(n_images=n_images,
                         psf_method='airy',
                         )
    import constants as c
    shape = (128,128)
    cen = (64, 64)
    scale = 50./shape[1] # [AU/pix]
    rm.parms['psf_parms'] = n.ones(n_images,dtype=n.float)*2. # [pix]
    rm.parms['r'] = 10. # [AU]
    rm.parms['r_out'] = 15. # [AU]
    rm.parms['r_in'] = 8. # [AU]
    rm.parms['gamma_in'] = -2.
    rm.parms['gamma'] = 10.
    rm.parms['offset'] = 2.
    #rm.parms['offset'] = 0.
    rm.parms['I'] = 60.*c.dtor # [rad]
    #rm.parms['I'] = 0.*c.dtor # [rad]
    rm.parms['omega'] = 90.*c.dtor # [rad]
    rm.parms['Omega'] = 30.*c.dtor # [rad]
    rm.parms['int_parms'] = n.array([10., 0., 0., 0., 0., 0.,
                                     10., 0., 0., 0., 0., 0.])
    rm.parm_lens['int_parms'] = len(rm.parms['int_parms'])
    #int_parms = n.ones(10, dtype=n.float)
    #rm.parms['int_parms'] = n.sin(n.linspace(0., n.pi/2., 10, endpoint=False))**3
    n_step = 1500
    drad0 = 2.

    im = rm.get_model_ring_image(n_images*[shape], n_images*[cen], scale,
                                 n_step=n_step,
                                 drad0=drad0,
                                 show=True)[0]
    #print im.sum()

    e = rm.parms['offset']/rm.parms['r']
    from kepler import get_apparent_orbit_ellipse
    ap, bp, phi, E0, N0 = get_apparent_orbit_ellipse(rm.parms['r'], e, rm.parms['I'], rm.parms['omega'], rm.parms['Omega'])
    ax = pylab.gca()
    ax.add_artist(mpl.patches.Ellipse((-E0,N0),
                                      width=2.*rm.parms['r']*n.cos(rm.parms['I']),
                                      height=2.*rm.parms['r'],
                                      angle=rm.parms['Omega']*180./n.pi,
                                      color='r',
                                      fill=False,
                                      ))

    pylab.draw()
    pylab.show()







from scipy.ndimage import gaussian_filter


class ModelComparator(object):
    "model comparator for total intensity"

    def __init__(self, ringmodel, cens, w_ss, labels, ims_I, 
                 mask_rad=38., # [pix]
                 fast_samp=4,
                 fast=False, #True,
                 **kwargs):
        self.n_images = ringmodel.n_images
        self.ringmodel = ringmodel
        self.cens = cens
        self.w_ss = w_ss
        self.labels = labels

        ## # high-pass filter
        ## self.fims = [self.filter_im_I(im_I) for im_I in ims_I]
        self.fims = ims_I

        # get stdev profile
        from image import radial_stdev_profile
        def get_info(fim, cen, w_s):
            rprof, sprof = radial_stdev_profile(fim, cen)
            y, x = n.mgrid[0:fim.shape[0],
                           0:fim.shape[1]]
            y -= cen[0]
            x -= cen[1]
            R = n.sqrt(x**2+y**2)
            stdmap = n.zeros_like(fim)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
                stdmap[wr] = s


            # extract relevant area
            tfim_orig = fim[w_s]
            stdim = stdmap[w_s]
            tcen = cen - n.array((w_s[0].start,w_s[1].start))

            return tfim_orig, stdim, tcen, R

        self.tfims_orig, self.stdims, self.tcens, self.Rs = [], [], [], []
        for i in range(self.n_images):
            tfim_orig, stdim, tcen, R = get_info(self.fims[i], cens[i], w_ss[i])
            self.tfims_orig.append(tfim_orig)
            self.stdims.append(stdim)
            self.tcens.append(tcen)
            self.Rs.append(R)

        self.set_tfims(mask_rad=mask_rad)


        self.n_step = 500
        self.drad0 = 2. # [AU]
        self.dist = 72.8 # [pc]
        self.pix_scale = .01414 # [arcsec/pix] Konopacky et al. 2014
        self.pix_uncert = 0.3 # [pix]  uncertainty in pixel positioning of star
        self.scale = self.dist*self.pix_scale # [AU/pix]

        self.fast = fast
        self.fast_samp = fast_samp


    @staticmethod
    def filter_im_I(im, i, fwhm=20.):
        im = ma.array(im)
        sig = fwhm/2.3548
        fim = im - gaussian_filter(im.filled(0.), sig)
        return fim


    def set_tfims(self, mask_rad=None, **kwargs):
        self.tfims = copy.deepcopy(self.tfims_orig)
        if mask_rad is not None:
            for i in range(self.n_images):
                # mask near star
                y, x = n.mgrid[0:self.tfims[i].shape[0],
                               0:self.tfims[i].shape[1]]
                y -= self.tcens[i][0]
                x -= self.tcens[i][1]
                self.Rs[i] = n.sqrt(x**2+y**2)

                self.tfims[i][n.nonzero(self.Rs[i] < mask_rad)] = ma.masked
        


    def get_freemodel_parminfo(self):
        parmdict = {}
        parmlist = []
        for k in self.ringmodel.parmlist:
            if self.ringmodel.is_fixed[k]: continue
            parmdict[k] = self.ringmodel.parms[k]
            parmlist.append(k)
        return parmdict, parmlist

    @staticmethod
    def fit_to_freemodel_parminfo(fit_parmdict, fit_parmlist):
        "transform fit parameters to model parameters"
        freemodel_parmlist = []
        freemodel_parmdict = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if k in ('psf_parms', 'int_parms', 'pol_int_parms', 'beta'):
                freemodel_parmdict[k] = n.exp(v)
                freemodel_parmlist.append(k)
            elif k == 'esino':
                assert 'ecoso' in fit_parmlist
                esino, ecoso = v, fit_parmdict['ecoso']
                e = n.sqrt(esino**2+ecoso**2)
                omega = n.arctan2(esino, ecoso)
                freemodel_parmdict['e'] = e
                freemodel_parmdict['omega'] = omega
                freemodel_parmlist.extend(['e', 'omega'])
            elif k == 'ecoso':
                continue
            elif k == 'osino':
                assert 'ocoso' in fit_parmlist
                osino, ocoso = v, fit_parmdict['ocoso']
                offset = n.sqrt(osino**2+ocoso**2)
                omega = n.arctan2(osino, ocoso)
                freemodel_parmdict['offset'] = offset
                freemodel_parmdict['omega'] = omega
                freemodel_parmlist.extend(['offset', 'omega'])
            elif k == 'ocoso':
                continue
            else:
                freemodel_parmdict[k] = v
                freemodel_parmlist.append(k)
        return freemodel_parmdict, freemodel_parmlist

    @staticmethod
    def freemodel_to_fit_parminfo(freemodel_parmdict, freemodel_parmlist):
        "transform model parameters to fit parameters"
        fit_parmlist = []
        fit_parmdict = {}
        for k in freemodel_parmlist:
            v = freemodel_parmdict[k]
            if k in ('psf_parms', 'int_parms', 'pol_int_parms', 'beta'):
                fit_parmdict[k] = n.log(v)
                fit_parmlist.append(k)
            elif k == 'e':
                assert 'omega' in freemodel_parmlist
                e, omega = v, freemodel_parmdict['omega']
                esino, ecoso = e*n.sin(omega), e*n.cos(omega)
                fit_parmdict['esino'] = esino
                fit_parmdict['ecoso'] = ecoso
                fit_parmlist.extend(['esino', 'ecoso'])
            elif k == 'offset':
                assert 'omega' in freemodel_parmlist
                offset, omega = v, freemodel_parmdict['omega']
                osino, ocoso = offset*n.sin(omega), offset*n.cos(omega)
                fit_parmdict['osino'] = osino
                fit_parmdict['ocoso'] = ocoso
                fit_parmlist.extend(['osino', 'ocoso'])
            elif k == 'omega':
                continue
            else:
                fit_parmdict[k] = v
                fit_parmlist.append(k)
        return fit_parmdict, fit_parmlist

    def get_fit_parms(self, get_parmlist=False):
        "take free parameters and construct 1-d array"

        # get current model parameter dictionary
        freemodel_parmdict, freemodel_parmlist = self.get_freemodel_parminfo()

        # transform model parameter dictionary to fit parameter dictionary
        fit_parmdict, fit_parmlist = self.freemodel_to_fit_parminfo(freemodel_parmdict, freemodel_parmlist)

        # construct 1-d array from fit dictionary
        parms = []
        parm_lens = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if n.isscalar(v): v = (v,)
            parms.append(v)
            parm_lens[k] = len(v)
        if get_parmlist:
            return n.concatenate(parms), fit_parmlist, parm_lens
        else:
            return n.concatenate(parms)

    def set_fit_parms(self, fit_parms):
        "use free parameters to update internal values"
        current_fit_parms, fit_parmlist, fit_parm_lens = self.get_fit_parms(get_parmlist=True)

        # unpack fit parameter array into dictionary
        i = 0
        fit_parmdict = {}
        for p in fit_parmlist:
            pl = fit_parm_lens[p]
            if pl == 1:
                fit_parmdict[p] = fit_parms[i]
            else:
                fit_parmdict[p] = fit_parms[i:i+pl]
            i += pl
        assert i == len(fit_parms)

        # transform fit parameter dictionary into model parameter dictionary
        freemodel_parmdict, freemodel_parmlist = self.fit_to_freemodel_parminfo(fit_parmdict, fit_parmlist)

        # udpate model parameters
        self.ringmodel.parms.update(freemodel_parmdict)



    def get_model(self, shapes=None, cens=None):
        "compute a model filtered image"
        fmims = []
        for i in range(self.n_images):
            if shapes is None:
                shapes = [tfim.shape for tfim in self.tfims]
            if cens is None:
                cens = self.tcens
            # compute model image
            mims = self.ringmodel.get_model_ring_image(shapes, cens, self.scale, n_step=self.n_step, drad0=self.drad0, fast=self.fast, fast_samp=self.fast_samp)
            # filter model image
            fmims = [self.filter_im_I(mim, i) for i, mim in enumerate(mims)]
        return fmims

    def get_residuals(self, p):
        "1-d residual array"
        self.set_fit_parms(p)
        fmims = self.get_model()
        r = ((ma.array(self.tfims)-ma.array(fmims))/ma.array(self.stdims)).compressed()
        return r


    def get_gaussian_priors(self):
        "1-d gaussian prior residual array"
        parmdict, parmlist = self.get_freemodel_parminfo()
        priors = []
        for p, v in parmdict.iteritems():
            ## if p == 'r_in':
            ##     priors.append((v-73.)/10.)
            ## if p in ('a','r'):
            ##     priors.append((v-79.2)/10.)
            ## elif p in ('a_out','r_out'):
            ##     priors.append((v-83.)/5.)
            ## elif p=='I':
            if p=='I':
                priors.append((v*180./n.pi-76.7)/5.)
            elif p=='gamma_in':
                priors.append(v/15.)
            elif p=='gamma':
                priors.append(v/5.)
            elif p in ('offsy','offsx'):
                priors.append(v/self.pix_uncert)
            elif p == 'psf_parms':
                vv = (v-5.)/1.
                if self.n_images == 1:
                    priors.append(vv)
                else:
                    priors.extend(vv.tolist())
        return n.array(priors)


    def do_leastsq(self):

        # get starting parameters based on current model state
        sp = self.get_fit_parms()

        def fit_fn(p):
            resid = self.get_residuals(p)
            priors = self.get_gaussian_priors()
            self.ringmodel.print_parms()
            return n.concatenate((resid, priors))

        # run fit
        from scipy.optimize import leastsq
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              #args=extra_args,
                                              #epsfcn=1e-7,
                                              #factor=3.,
                                              epsfcn=1e-3,
                                              full_output=True,
                                              )
        _log.info("%d: %s" % (ier, mesg))
        #p_opt = sp # TEMP

        self.leastsq_vals = (p_opt, cov, info, mesg, ier)

        return self.leastsq_vals



    def report_leastsq(self):
        "print out best-fit parameters with uncertainties"

        assert hasattr(self, 'leastsq_vals')
        p_opt, cov, info, mesg, ier = self.leastsq_vals
        try:
            var = n.diag(cov)
        except:
            _log.error('no covariance matrix!')
            return

        current_fit_parms, fit_parmlist, fit_parm_lens = self.get_fit_parms(get_parmlist=True)
        fit_parms = p_opt


        # unpack fit parameter array into dictionary
        i = 0
        fit_parmdict = {}
        fit_parmvardict = {}
        ecosoesino = []
        ocosoosino = []
        for p in fit_parmlist:
            if p in ('ecoso','esino'): ecosoesino.append(i)
            if p in ('ocoso','osino'): ocosoosino.append(i)
            pl = fit_parm_lens[p]
            if pl == 1:
                fit_parmdict[p] = fit_parms[i]
                fit_parmvardict[p] = var[i]
            else:
                fit_parmdict[p] = fit_parms[i:i+pl]
                fit_parmvardict[p] = var[i:i+pl]
            i += pl
        assert i == len(fit_parms)


        # transform fit parameters to model parameters
        freemodel_parmlist = []
        freemodel_parmdict = {}
        freemodel_parmvardict = {}
        for k in fit_parmlist:
            v = fit_parmdict[k]
            if k in ('psf_parms', 'int_parms', 'pol_int_parms'):
                freemodel_parmdict[k] = n.exp(v)
                freemodel_parmvardict[k] = n.exp(v)**2 * fit_parmvardict[k]
                freemodel_parmlist.append(k)
            elif k == 'esino':
                assert 'ecoso' in fit_parmlist
                esino, ecoso = v, fit_parmdict['ecoso']
                e = n.sqrt(esino**2+ecoso**2)
                omega = n.arctan2(esino, ecoso)
                freemodel_parmdict['e'] = e
                freemodel_parmdict['omega'] = omega

                c = cov[ecosoesino[0],ecosoesino[1]]
                freemodel_parmvardict['e'] = (ecoso**2 * fit_parmvardict['ecoso'] + esino**2 * fit_parmvardict['esino'] + 2.*ecoso*esino*c)/e**2

                freemodel_parmvardict['omega'] = (esino**2 * fit_parmvardict['ecoso'] + ecoso**2 * fit_parmvardict['esino'] - 2.*ecoso*esino*c)/e**4

                freemodel_parmlist.extend(['e', 'omega'])
            elif k == 'ecoso':
                continue
            elif k == 'osino':
                assert 'ocoso' in fit_parmlist
                osino, ocoso = v, fit_parmdict['ocoso']
                offset = n.sqrt(osino**2+ocoso**2)
                omega = n.arctan2(osino, ocoso)
                freemodel_parmdict['offset'] = offset
                freemodel_parmdict['omega'] = omega

                c = cov[ocosoosino[0],ocosoosino[1]]
                freemodel_parmvardict['offset'] = (ocoso**2 * fit_parmvardict['ocoso'] + osino**2 * fit_parmvardict['osino'] + 2.*ocoso*osino*c)/offset**2

                freemodel_parmvardict['omega'] = (osino**2 * fit_parmvardict['ocoso'] + ocoso**2 * fit_parmvardict['osino'] - 2.*ocoso*osino*c)/offset**4

                freemodel_parmlist.extend(['offset', 'omega'])
            elif k == 'ocoso':
                continue
            else:
                freemodel_parmdict[k] = v
                freemodel_parmvardict[k] = fit_parmvardict[k]
                freemodel_parmlist.append(k)


        # which parameters to report
        #parms = ['psf_parms', 'offsy', 'offsx', 'r', 'r_out', 'gamma_in', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        # FIXME
        parms = ['offsy', 'offsx', 'r', 'r_in', 'r_out', 'gamma_in', 'gamma', 'beta', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        for p in copy.copy(parms):
            if p not in self.ringmodel.parmlist:
                parms.pop(parms.index(p))
                continue
            if self.ringmodel.is_fixed[p]:
                parms.pop(parms.index(p))
        angle_parms = ('I','omega','Omega')
        for parm in parms:
            fact = 180./n.pi if parm in angle_parms else 1.
            _log.info("%s:\t%f\t+/- %f" % (parm,
                                           freemodel_parmdict[parm]*fact,
                                           n.sqrt(freemodel_parmvardict[parm])*fact,
                                           ))



    def update_stdmap(self):
        "use residuals from current model to recalculate stdev map"

        # get full image model
        shapes = [fim.shape for fim in self.fims]
        cens = self.cens
        mims = self.get_model(shapes=shapes, cens=cens)
        resids = [fim-mim for fim, mim in zip(self.fims, mims)]
        from image import radial_stdev_profile
        for i in range(self.n_images):
            rprof, sprof = radial_stdev_profile(resids[i], self.cens[i],
                                                #robust=False,
                                                )

            # compute thumbnail stdmap
            stdmap = n.zeros(self.tfims[i].shape, dtype=n.float)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((self.Rs[i] >= r-0.5) & (self.Rs[i] < r+0.5))
                stdmap[wr] = s
            self.stdims[i][:] = stdmap


    def update_int_parms(self, n_int_parm):
        "update intensity parameters with new number of parameters"

        if n_int_parm*self.n_images == len(self.ringmodel.parms['int_parms']):
            return

        fit_pol = self.ringmodel.parms['pol_int_parms'] is not None


        from scipy.optimize import leastsq
        nu = n.linspace(0., 2.*n.pi, 100) # [rad]
        phi_sca = n.linspace(0., n.pi, 50) # [rad]
        curr_int = self.ringmodel._get_intensity(nu, phi_sca)
        if fit_pol:
            curr_pol_int = self.ringmodel._get_pol_intensity(nu, phi_sca)
            curr = n.concatenate(curr_int+curr_pol_int)
        else:
            curr = n.concatenate(curr_int)
        def fit_fn(p):
            if fit_pol:
                np = len(p)
                self.ringmodel.parms['int_parms'] = n.exp(p[0:np/2])
            else:
                self.ringmodel.parms['int_parms'] = n.exp(p)
            mod_int = self.ringmodel._get_intensity(nu, phi_sca)
            if fit_pol:
                self.ringmodel.parms['pol_int_parms'] = n.exp(p[np/2:])
                mod_pol_int = self.ringmodel._get_pol_intensity(nu, phi_sca)
                return curr-n.concatenate(mod_int+mod_pol_int)
            else:
                return curr-n.concatenate(mod_int)
        self.ringmodel.parms['int_parms'] = n.ones(n_int_parm*self.n_images, dtype=n.float)*2e8
        self.ringmodel.parm_lens['int_parms'] = n_int_parm*self.n_images
        if fit_pol:
            self.ringmodel.parms['pol_int_parms'] = n.ones(n_int_parm*self.n_images, dtype=n.float)*2e7
            self.ringmodel.parm_lens['pol_int_parms'] = n_int_parm*self.n_images
            sp = n.log(n.concatenate((self.ringmodel.parms['int_parms'], self.ringmodel.parms['pol_int_parms'])))
        else:
            sp = n.log(self.ringmodel.parms['int_parms'])
        p_opt, ier = leastsq(fit_fn, sp.copy())
        if fit_pol:
            np = len(p_opt)
            self.ringmodel.parms['int_parms'] = n.exp(p_opt[0:np/2])
            self.ringmodel.parms['pol_int_parms'] = n.exp(p_opt[np/2:])
        else:
            self.ringmodel.parms['int_parms'] = n.exp(p_opt)


    def show_model(self):
        fmims = self.get_model()

        # chi2 best fit
        impad_l = 0.6 # [in]
        impad_r = 0.1 # [in]
        impad_b = 0.6 # [in]
        impad_t = 0.4 # [in]
        imwidth = 1.5 # [in]



        for i in range(self.n_images):

            imheight = imwidth * float(self.tfims[i].shape[0])/float(self.tfims[i].shape[1]) # [in]
            fig_width = impad_l + 3*imwidth + impad_r
            fig_height = impad_b + imheight + impad_t
            figsize = (fig_width, fig_height)

            fig = pylab.figure(i, figsize=figsize)
            fig.clear()
            fig.hold(True)
            fig.suptitle(self.labels[i])
            axs = []
            for j in range(3):
                ax = fig.add_axes([(impad_l+j*imwidth)/fig_width,
                                   impad_b/fig_height,
                                   imwidth/fig_width,
                                   imheight/fig_height,
                                   ])
                axs.append(ax)
            kw = {'vmin':-1e2,
                  'vmax':1e3,
                  'interpolation':'nearest',
                  'cmap':mpl.cm.gist_stern,
                  }
            print fmims[i]
            axs[0].imshow(self.tfims[i], **kw)
            axs[1].imshow(fmims[i], **kw)
            axs[2].imshow(self.tfims[i]-fmims[i], **kw)

            pyfits.writeto('model' + str(i) + '.fits', n.array(fmims[i]))
            # mark star
            for ax in axs:
                ax.set_autoscale_on(False)
                ax.scatter((self.tcens[i][1],), (self.tcens[i][0],), c='k')

            for ax in axs[1:]:
                pylab.setp(ax.get_yticklabels(), visible=False)

            pylab.draw()
            pylab.show()


    def run_MCMC(self, n_walker, n_sample):
        p_opt = self.get_fit_parms()
        n_dim = len(p_opt)
        rs = n.random.RandomState(seed=34523)
        wz = n.nonzero(p_opt==0.)[0]
        pos = []
        for i in range(n_walker):
            p = p_opt*(1. + 3e-2*rs.randn(n_dim))
            p[wz] = 1e-1*rs.randn(len(wz))
            pos.append(p)
        if has_mpi:
            kw = {'pool':pool}
        else:
            import multiprocessing as mp
            n_process = mp.cpu_count()
            kw = {'threads':n_process}
        import emcee
        sampler = emcee.EnsembleSampler(n_walker, n_dim, log_like,
                                        args=(self,),
                                        **kw)
        sampler.run_mcmc(pos, n_sample,
                         rstate0=rs.get_state())

        self.chain = sampler.chain
        self.lnprobability = sampler.lnprobability



    def show_mcmc_results(self, n_burn, mask_rad, pol_mask_rad):
        import constants as c

        chain = self.chain
        self.ringmodel.print_parms()
        ## mim = get_model(p_opt, *extra_args[2:])

        n_walker, n_sample, n_dim = chain.shape

        # process chains
        parmlist = self.ringmodel.parmlist
        pchain = []
        for p in chain.reshape(n_walker*n_sample, n_dim):
            self.set_fit_parms(p)
            vals = [self.ringmodel.parms[k] for k in parmlist]
            pchain.append(tuple(vals))
        names = ','.join(parmlist)
        formats = ['f' if self.ringmodel.parm_lens[k]==1 else 'O' for k in parmlist]
        pchain = n.rec.fromrecords(pchain,
                                   names=names,
                                   formats=formats,
                                   )
        pchain.shape = n_walker, n_sample


        do_pol = self.ringmodel.parms['pol_int_parms'] is not None
        

        # show individual chains
        fig = pylab.figure(self.n_images+1)
        fig.clear()
        fig.hold(True)

        #parms = ['psf_parms', 'offsy', 'offsx', 'r', 'r_out', 'gamma_in', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        # FIXME
        parms = ['offsy', 'offsx', 'r', 'r_in', 'r_out', 'gamma_in', 'gamma',  'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega'] #'beta',
        for p in copy.copy(parms):
            if p not in self.ringmodel.parmlist:
                parms.pop(parms.index(p))
                continue
            if self.ringmodel.is_fixed[p]:
                parms.pop(parms.index(p))
        n_parms = len(parms)

        angle_parms = ('I','omega','Omega')

        axs = [fig.add_subplot(n_parms, 1, i+1) for i in range(n_parms)]

        for j, ax in enumerate(axs):
            if parms[j] in ('I','omega','Omega'):
                fact = 180./n.pi
            else: fact = 1.
            for i in range(n_walker):
                ax.plot(pchain[parms[j]][i,:]*fact, c='k', alpha=.03)
            ax.set_autoscale_on(False)
            ax.set_xlim(0, n_sample)
            if j != n_parms-1:
                pylab.setp(ax.get_xticklabels(), visible=False)
            ax.set_ylabel(parms[j].replace('_','\_'))
            ax.axvline(n_burn, ls=':', c='k')

        pylab.draw()
        pylab.show()



        # show 1 and 2d distributions
        samples = []
        labels = []
        for parm in parms:
            if parm in angle_parms:
                fact = 180./n.pi
            else: fact = 1.
            samples.append(pchain[parm][:,n_burn:].flatten()*fact)
            labels.append(parm.replace('_','\_'))
        samples = n.array(samples).T


        factor = 1.0           # size of one side of one panel
        lbdim = 0.5 * factor   # size of left/bottom margin
        trdim = 0.05 * factor  # size of top/right margin
        whspace = 0.05         # w/hspace size
        plotdim = factor * n_parms + factor * (n_parms - 1.) * whspace
        dim = lbdim + plotdim + trdim


        fig = pylab.figure(figsize=(dim, dim), num=self.n_images+2)
        fig.clear()
        fig, axes = pylab.subplots(n_parms, n_parms, figsize=(dim, dim), num=self.n_images+2)
        import triangle
        frac = 0.98
        #frac = 1.
        triangle.corner(samples, labels=labels, fig=fig,
                        extents=(n.ones(n_parms, dtype=n.float)*frac).tolist(),
                        )


        pylab.draw()
        pylab.show()


        # FIXME
        #  below needs to be adjusted for polarization




        ## # show intensity vs. scattering angle
        ## fig = pylab.figure(self.n_images+4, figsize=(8,4))
        ## fig.clear()
        ## fig.hold(True)
        ## ax1 = fig.add_subplot(121, polar=True)
        ## if do_pol:
        ##     ax2 = fig.add_subplot(222)
        ##     ax3 = fig.add_subplot(224, sharex=ax2)
        ## else:
        ##     ax2 = fig.add_subplot(122)

        ## nu = n.linspace(0., 2.*n.pi, 100)#, endpoint=False)

        ## w = n.s_[::20]
        ## #w = n.s_[::300]
        ## #w = n.s_[::1000]
        ## ## phi_scas = []
        ## ## sc_ints = []
        ## if do_pol:
        ##     zipped = zip(pchain['a'][:,n_burn:].flatten()[w],
        ##                  pchain['e'][:,n_burn:].flatten()[w],
        ##                  pchain['I'][:,n_burn:].flatten()[w],
        ##                  pchain['omega'][:,n_burn:].flatten()[w],
        ##                  pchain['Omega'][:,n_burn:].flatten()[w],
        ##                  pchain['int_parms'][:,n_burn:].flatten()[w],
        ##                  pchain['pol_int_parms'][:,n_burn:].flatten()[w],
        ##                  )
        ## else:
        ##     zipped = zip(pchain['a'][:,n_burn:].flatten()[w],
        ##                  pchain['e'][:,n_burn:].flatten()[w],
        ##                  pchain['I'][:,n_burn:].flatten()[w],
        ##                  pchain['omega'][:,n_burn:].flatten()[w],
        ##                  pchain['Omega'][:,n_burn:].flatten()[w],
        ##                  pchain['int_parms'][:,n_burn:].flatten()[w],
        ##                  )
    
        ## for vals in zipped:
        ##     if do_pol:
        ##         a, e, I, omega, Omega, int_parms, pol_int_parms = vals
        ##     else:
        ##         a, e, I, omega, Omega, int_parms = vals

        ##     # TEMP
        ##     #if (a<75.) or (a>85.): continue

        ##     # FIXME  this depends on model (circular vs. keplerian)

        ##     # physical radius from star
        ##     r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]
        ##     # scattering angle
        ##     sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
        ##     sO, cO = n.sin(Omega), n.cos(Omega)
        ##     si, ci = n.sin(I), n.cos(I)
        ##     pN = r*(cO*conu-sO*sonu*ci)
        ##     pE = r*(sO*conu+cO*sonu*ci)
        ##     pz = r * sonu*si
        ##     phi_sca = n.arccos(pz/r) # [rad]
        ##     r_proj = n.sqrt(pN**2+pE**2) # [AU]
        ##     # intensity at given location
        ##     self.ringmodel.parms['int_parms'] = int_parms
        ##     intensity = self.ringmodel._get_intensity(nu, phi_sca) / r**2
        ##     if do_pol:
        ##         pol_intensity = self.ringmodel._get_pol_intensity(nu, phi_sca) / r**2
        ##     # rescale by stellar distance squared 
        ##     wi = n.nonzero(r_proj > mask_rad)[0]
        ##     sc_int = intensity * r**2
        ##     if do_pol:
        ##         wpi = n.nonzero(r_proj > pol_mask_rad)[0]
        ##         sc_pol_int = pol_intensity * r**2



        ##     #ax.scatter(phi_sca, sc_int, facecolor='k', alpha=0.003, marker='.', edgecolor='none')
        ##     kw = {'alpha':0.003, 'marker':'.', 'edgecolor':'none'}
        ##     ax1.scatter(phi_sca[wi], sc_int[wi],
        ##                 facecolor='k', **kw)
        ##     ax1.scatter(2.*n.pi-phi_sca[wi], sc_int[wi],
        ##                 facecolor='k', **kw)
        ##     if do_pol:
        ##         ax1.scatter(phi_sca[wpi], sc_pol_int[wpi],
        ##                     facecolor='r', **kw)
        ##         ax1.scatter(2.*n.pi-phi_sca[wpi], sc_pol_int[wpi],
        ##                     facecolor='r', **kw)

        ##     ax2.scatter(phi_sca[wi]*c.rtod, sc_int[wi],
        ##                 facecolor='k', **kw)
        ##     if do_pol:
        ##         ax2.scatter(phi_sca[wpi]*c.rtod, sc_pol_int[wpi],
        ##                     facecolor='r', **kw)
        ##         ax3.scatter(phi_sca[wi]*c.rtod, sc_pol_int[wi]/sc_int[wi],
        ##                     facecolor='b', **kw)

        ## if do_pol:
        ##     axs = (ax2, ax3)
        ## else:
        ##     axs = (ax2,)
        ## for ax in axs:
        ##     ax.set_xlim(0., 180.)
        ##     ax.axvline(90., ls=':', c='k')
        ## ax2.set_ylim(0., 6e8)
        ## if do_pol:
        ##     ax3.set_ylim(0., 0.55)
        ##     pylab.setp(ax2.get_xticklabels(), visible=False)
        ## ax2.set_ylabel('Intensity')
        ## if do_pol:
        ##     ax3.set_ylabel('Pol. frac.')
        ##     ax3.set_xlabel(r"$\theta$")
        ## else:
        ##     ax2.set_xlabel(r"$\theta$")


        ## ##     phi_scas.append(phi_sca)
        ## ##     sc_ints.append(sc_int)
        ## ## phi_scas = n.concatenate(phi_scas)
        ## ## sc_ints = n.concatenate(sc_ints)
        ## ## with open('sc_ints.dat', 'w') as f:
        ## ##     pickle.dump(phi_scas, f, 2)
        ## ##     pickle.dump(sc_ints, f, 2)

        ## ## # overplot a scaled Rayleigh phase function
        ## ## scl = 3.7e9
        ## ## ax.plot(nu, scl * 3./16./n.pi * (1.+n.cos(nu)**2), c='g')

        ## ## # overplot a scaled Mie phase function
        ## ## m = 1.33+1e-8j
        ## ## x = 6.963e-1
        ## ## phi_sca, sc_int1 = get_mie(x,m)
        ## ## x = 2.99
        ## ## phi_sca, sc_int2 = get_mie(x,m)
        ## ## sc_int = 1.007e9 * sc_int1 + 1.39e8 * sc_int2
        ## ## ax.plot(phi_sca, sc_int, c='b')
        ## ## ax.plot(2.*n.pi-phi_sca, sc_int, c='b')


        ## #ax.set_xlabel(r"$\phi_\mathrm{sca}$ [deg]")
        ## #ax.set_ylabel(r"Intensity $\times r^2$")
        ## #ax.set_xlim(0., n.pi)

        ## pylab.draw()
        ## pylab.show()


    def report_mcmc_results(self, n_burn):
        "print out best-fit parameters with uncertainties"

        chain = self.chain
        n_walker, n_sample, n_dim = chain.shape

        # process chains
        parmlist = self.ringmodel.parmlist
        pchain = []
        for p in chain.reshape(n_walker*n_sample, n_dim):
            self.set_fit_parms(p)
            vals = [self.ringmodel.parms[k] for k in parmlist]
            pchain.append(tuple(vals))
        names = ','.join(parmlist)
        formats = ['f' if self.ringmodel.parm_lens[k]==1 else 'O' for k in parmlist]
        pchain = n.rec.fromrecords(pchain,
                                   names=names,
                                   formats=formats,
                                   )
        pchain.shape = n_walker, n_sample


        # which parameters to report
        #parms = ['psf_parms', 'offsy', 'offsx', 'r', 'r_out', 'gamma_in', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        # FIXME
        parms = ['offsy', 'offsx', 'r', 'r_in', 'r_out', 'gamma_in', 'gamma', 'beta', 'offset', 'a', 'a_out', 'e', 'I', 'omega', 'Omega']
        for p in copy.copy(parms):
            if p not in self.ringmodel.parmlist:
                parms.pop(parms.index(p))
                continue
            if self.ringmodel.is_fixed[p]:
                parms.pop(parms.index(p))
        n_parms = len(parms)
        angle_parms = ('I','omega','Omega')


        # get good samples in proper units
        samples = []
        for parm in parms:
            if parm in angle_parms:
                fact = 180./n.pi
            else: fact = 1.
            samples.append(pchain[parm][:,n_burn:].flatten()*fact)
        samples = n.array(samples)

        _log.info("mean, stdev of MCMC calculation results")
        for parm, vals in zip(parms, samples):
            _log.info("%s:\t%f\t+/- %f" % (parm, vals.mean(), vals.std()))
            print("%s:\t%f\t+/- %f" % (parm, vals.mean(), vals.std()))


        # projected center
        # FIXME  this depends on model
        ## import constants as c
        ## a = samples[parms.index('a'),:]
        ## e = samples[parms.index('e'),:]
        ## inc = samples[parms.index('I'),:]*c.dtor # [rad]
        ## omega = samples[parms.index('omega'),:]*c.dtor # [rad]
        ## Omega = samples[parms.index('Omega'),:]*c.dtor # [rad]

        ## # compute Thiele-Innes elements
        ## A = a*(n.cos(omega)*n.cos(Omega)-n.sin(omega)*n.sin(Omega)*n.cos(inc))
        ## B = a*(n.cos(omega)*n.sin(Omega)+n.sin(omega)*n.cos(Omega)*n.cos(inc))
        ## F = a*(-n.sin(omega)*n.cos(Omega)-n.cos(omega)*n.sin(Omega)*n.cos(inc))
        ## G = a*(-n.sin(omega)*n.sin(Omega)+n.cos(omega)*n.cos(Omega)*n.cos(inc))

        ## # compute coefficients for general ellipse
        ## a = (F**2+A**2/(1.-e**2))
        ## b = -(F*G+A*B/(1.-e**2))
        ## c = (G**2+B**2/(1.-e**2))
        ## d = e*F*(B*F-A*G)
        ## f = -e*G*(B*F-A*G)
        ## g = -(1.-e**2)*(B*F-A*G)**2

        ## # solve for ellipse parameters
        ## E0 = (c*d-b*f) / (b**2-a*c)
        ## N0 = (a*f-b*d) / (b**2-a*c)

        ## for parm, vals in zip(('E0', 'N0'), (E0, N0)):
        ##     print "%s:\t%f\t+/- %f" % (parm, vals.mean(), vals.std())
        
        #ipshell() # TEMP
        

class PolModelComparator(ModelComparator):
    
    def __init__(self, ringmodel, cens, w_ss, labels, ims_I, ims_P,
                 mask_rad=38., # [pix]
                 **kwargs):

        for im_I, im_P in zip(ims_I, ims_P):
            assert n.all(im_I.shape == im_P.shape)

        ## # high-pass filter
        ## self.fims_P = [self.filter_im_P(im_P) for im_P in ims_P]
        self.fims_P = ims_P

        # get stdev profile
        from image import radial_stdev_profile
        def get_info(fim_P, cen, w_s):
            rprof, sprof = radial_stdev_profile(fim_P, cen)
            y, x = n.mgrid[0:fim_P.shape[0],
                           0:fim_P.shape[1]]
            y -= cen[0]
            x -= cen[1]
            R = n.sqrt(x**2+y**2)
            stdmap = n.zeros_like(fim_P)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((R >= r-0.5) & (R < r+0.5))
                stdmap[wr] = s


            # extract relevant area
            tfim_P_orig = fim_P[w_s]
            stdim_P = stdmap[w_s]

            return tfim_P_orig, stdim_P

        self.tfims_P_orig, self.stdims_P = [], []
        for i in range(len(ims_I)):
            tfim_P_orig, stdim_P = get_info(self.fims_P[i], cens[i], w_ss[i])
            self.tfims_P_orig.append(tfim_P_orig)
            self.stdims_P.append(stdim_P)

        ModelComparator.__init__(self, ringmodel, cens, w_ss, labels, ims_I, mask_rad=mask_rad, **kwargs)

    @staticmethod
    def filter_im_P(im, i, fwhm=20.):
        im = ma.array(im)
        sig = fwhm/2.3548
        fim = im - gaussian_filter(im.filled(0.), sig)
        return fim


    def set_tfims(self, mask_rad=None, pol_mask_rad=None, **kwargs):
        ModelComparator.set_tfims(self, mask_rad=mask_rad, **kwargs)

        self.tfims_P = copy.deepcopy(self.tfims_P_orig)
        if (mask_rad is not None) or (pol_mask_rad is not None):
            if pol_mask_rad is None:
                pol_mask_rad = mask_rad
            for i in range(self.n_images):
                self.tfims_P[i][n.nonzero(self.Rs[i] < pol_mask_rad)] = ma.masked

    def get_model(self, shapes=None, cens=None):
        "compute a model filtered image"
        if shapes is None:
            shapes = [tfim.shape for tfim in self.tfims]
        if cens is None:
            cens = self.tcens
        # compute model image
        mims, mims_P = self.ringmodel.get_model_ring_image(shapes, cens, self.scale, n_step=self.n_step, drad0=self.drad0, fast=self.fast, fast_samp=self.fast_samp)
        # filter model image
        fmims = [self.filter_im_I(mim, i) for i, mim in enumerate(mims)]
        fmims_P = mims_P #[self.filter_im_P(mim_P, i) for i, mim_P in enumerate(mims_P)]
        return fmims, fmims_P

    def get_residuals(self, p):
        "1-d residual array"
        self.set_fit_parms(p)
        fmims, fmims_P = self.get_model()
        r = ((ma.array(self.tfims)-ma.array(fmims))/ma.array(self.stdims)).compressed()
        r_P = ((ma.array(self.tfims_P)-ma.array(fmims_P))/ma.array(self.stdims_P)).compressed()
        print 'chisq'
        print n.sum(r_P**2.) #+ n.sum(r**2.)
        return r
#        return n.concatenate((r, r_P))

    def update_stdmap(self):
        "use residuals from current model to recalculate stdev map"

        # get full image model
        shapes = [fim.shape for fim in self.fims]
        cens = self.cens
        mims, mims_P = self.get_model(shapes=shapes, cens=cens)
        resids = [fim-mim for fim, mim in zip(self.fims, mims)]
        resids_P = [fim_P-mim_P for fim_P, mim_P in zip(self.fims_P, mims_P)]
        from image import radial_stdev_profile
        for i in range(self.n_images):
            rprof, sprof = radial_stdev_profile(resids[i], self.cens[i])
            rprof_P, sprof_P = radial_stdev_profile(resids_P[i], self.cens[i])

            # compute thumbnail stdmap
            stdmap = n.zeros(self.tfims[i].shape, dtype=n.float)
            for r, s in zip(rprof, sprof):
                wr = n.nonzero((self.Rs[i] >= r-0.5) & (self.Rs[i] < r+0.5))
                stdmap[wr] = s
            self.stdims[i][:] = stdmap

            stdmap_P = n.zeros(self.tfims_P[i].shape, dtype=n.float)
            for r, s in zip(rprof_P, sprof_P):
                wr = n.nonzero((self.Rs[i] >= r-0.5) & (self.Rs[i] < r+0.5))
                stdmap_P[wr] = s
            self.stdims_P[i][:] = stdmap_P


    def show_model(self):
        fmims, fmims_P = self.get_model()
        # chi2 best fit
        impad_l = 0.6 # [in]
        impad_r = 0.1 # [in]
        impad_b = 0.6 # [in]
        impad_t = 0.4 # [in]
        imheight = 3. # [in]


        for k in range(self.n_images):

            ## extent = [-self.tcen[1]*self.scale,
            ##           (self.tfim.shape[1]-1-self.tcen[1])*self.scale,
            ##           -self.tcen[0]*self.scale,
            ##           (self.tfim.shape[0]-1-self.tcen[0])*self.scale]

            extent = [(self.tcens[k][1]+self.ringmodel.parms['offsx'])*self.scale,
                      -(self.tfims[k].shape[1]-1-(self.tcens[k][1]+self.ringmodel.parms['offsx']))*self.scale,
                      -(self.tcens[k][0]+self.ringmodel.parms['offsy'])*self.scale,
                      (self.tfims[k].shape[0]-1-(self.tcens[k][0]+self.ringmodel.parms['offsy']))*self.scale]

            imwidth = imheight * float(self.tfims[k].shape[1])/float(self.tfims[k].shape[0]) # [in]
            fig_width = impad_l + 3*imwidth + impad_r
            fig_height = impad_b + 2*imheight + impad_t
            figsize = (fig_width, fig_height)

            fig = pylab.figure(k, figsize=figsize)
            fig.clear()
            fig.hold(True)

            fig.suptitle(self.labels[k])

            axs = []
            for i in range(6):
                j = i / 3
                ax = fig.add_axes([(impad_l+(i%3)*imwidth)/fig_width,
                                   (impad_b+(1-j)*imheight)/fig_height,
                                   imwidth/fig_width,
                                   imheight/fig_height,
                                   ])
                axs.append(ax)
            vmax = n.nanmax(fmims[k])
            kw = {'vmin':-0.1*vmax,
                  'vmax':1.1*vmax,
                  'interpolation':'nearest',
                  'cmap':mpl.cm.gist_stern,
                  'extent':extent,
                  }
            axs[0].imshow(self.tfims[k], **kw)
            axs[1].imshow(fmims[k], **kw)
            axs[2].imshow(self.tfims[k]-fmims[k], **kw)
            vmax = n.nanmax(fmims_P[k])
            kw['vmin'] = -0.1*vmax
            kw['vmax'] = 1.1*vmax
            axs[3].imshow(self.tfims_P[k], **kw)
            axs[4].imshow(fmims_P[k], **kw)
            axs[5].imshow(self.tfims_P[k]-fmims_P[k], **kw)

            pyfits.writeto('model' + str(k) + '.fits', n.array(fmims[k]))
            # mark star
            for ax in axs:
                ax.set_autoscale_on(False)
                #ax.scatter((self.tcen[1],), (self.tcen[0],), c='k')
                ax.scatter((0.,), (0.,), c='k')
                ## ax.scatter((self.ringmodel.parms['offsx']*self.scale,),
                ##            (self.ringmodel.parms['offsy']*self.scale,),
                ##            c='b')

            # FIXME  update for offset ring
            ## # mark major axis
            ## #
            ## nu = n.array((0., n.pi))
            ## a = self.ringmodel.parms['a']
            ## e = self.ringmodel.parms['e']
            ## I = self.ringmodel.parms['I']
            ## omega = self.ringmodel.parms['omega']
            ## Omega = self.ringmodel.parms['Omega']

            ## # physical radius from star
            ## r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]

            ## so, co = n.sin(omega), n.cos(omega)
            ## sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
            ## sO, cO = n.sin(Omega), n.cos(Omega)
            ## si, ci = n.sin(I), n.cos(I)

            ## pN = r * (cO*conu-sO*sonu*ci) # [AU]
            ## pE = r * (sO*conu+cO*sonu*ci) # [AU]

            ## for ax in (axs[1], axs[4]):
            ##     ax.plot(pE, pN, c='g')
            ##     ax.scatter((pE[0],), (pN[0],), c='g')


            ## # mark line of nodes
            ## #
            ## nu = n.array((-omega, n.pi-omega))
            ## r = a * (1.-e**2) / (1.+e*n.cos(nu)) # [AU] assuming a is in [AU]
            ## so, co = n.sin(omega), n.cos(omega)
            ## sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
            ## sO, cO = n.sin(Omega), n.cos(Omega)
            ## si, ci = n.sin(I), n.cos(I)
            ## pN = r * (cO*conu-sO*sonu*ci) # [AU]
            ## pE = r * (sO*conu+cO*sonu*ci) # [AU]
            ## for ax in (axs[1], axs[4]):
            ##     ax.plot(pE, pN, c='m', ls='--')
            ##     ax.scatter((pE[0],), (pN[0],), c='m')


            # axis labels
            for ax in axs[1:3]+axs[4:6]:
                pylab.setp(ax.get_yticklabels(), visible=False)
            for ax in axs[0:3]:
                pylab.setp(ax.get_xticklabels(), visible=False)

            pylab.draw()
            pylab.show()


d = os.path.expanduser('~/work/gpi/')
if d not in sys.path: sys.path.append(d)
import gpiklip
import glob
import instruments.GPI as gpi
_KLIPPolModelComparatorBase = PolModelComparator 
class KLIPPolModelComparator(_KLIPPolModelComparatorBase):

    def __init__(self, im_save_fns, klip_save_fns, *args, **kwargs):
        _KLIPPolModelComparatorBase.__init__(self, *args, **kwargs)

        # load PA and KLIP mode information
        self.pas, self.cents, self.basisvals_all, self.coords_all, self.goodinds_all = [], [], [], [], []
        for im_save_fn, klip_save_fn in zip(im_save_fns, klip_save_fns):
            basisvals, coords, goodinds = gpiklip.load_klip_save_data(basispattern = klip_save_fn)
            self.basisvals_all.append(basisvals)
            self.coords_all.append(coords)
            self.goodinds_all.append(goodinds)
            image_arr = glob.glob(im_save_fn)
            image_arr = sorted(image_arr)
            ims_dataset = gpi.GPIData(image_arr)
            self.cents.append(ims_dataset.centers)
            self.pas.append(ims_dataset.PAs)

    def filter_im_I(self, im, i, **kwargs):
        fim = gpiklip.perform_klip_adi(im,
                                       self.pas[i],
                                       self.cents[i],
                                       basisvals_all = self.basisvals_all[i],
                                       coords_all = self.coords_all[i],
                                       goodinds_all = self.goodinds_all[i])
        #return fim
#       fim = im
        # NOTE  testing high-pass filter of klip images
        mfim = ma.masked_where(n.isnan(fim), fim)
        fim2 = _KLIPPolModelComparatorBase.filter_im_I(mfim, i, **kwargs)
        return fim2




def load_image_info():
    im_dir = '/home/parriaga/hr4796a_new/'
    labels = ['K1']
#    labels = ['K1', 'J']
#    im_I_fns = [im_dir+'S20140422K-KLmodes-KL5.fits',
#                im_dir+'S20140422J-KLmodes-KL5.fits',
#                ]
    im_P_fns = [im_dir+'HR4796A-K1-Qr-masked.fits',
#                im_dir+'HR4796A-J-Qr-masked.fits',
                ]
#    im_save_fns = [im_dir+'april_k/*podc*', im_dir + 'april_j/*podc*']
#    klip_save_fns = ['basis/basisK', 'basis/basisJ']
    #### TEST FIXME
    psf_save_fns = ['psf_k_short.fits']# , 'psf_j_short.fits']
#   return im_dir, labels, im_I_fns, im_P_fns, im_save_fns, klip_save_fns, psf_save_fns
    return im_dir, labels, im_P_fns, psf_save_fns



def fit_images(show=True,
               redo_lsq1=False,
               redo_lsq2=False,
               redo_mcmc=False,
               redo_all=False,
               lsq_only=False,
               ):
    import constants as c

    for name in ('image', 'gpiklip'):
        logging.getLogger(name).setLevel(logging.INFO)

    redo_lsq1 = redo_lsq1 or redo_all
    redo_lsq2 = redo_lsq2 or redo_lsq1
    redo_mcmc = redo_mcmc or redo_lsq2

    n_step = 200 # steps along ring for intensity
    drad0 = 1. # [AU] stepsize along radial direction #originally 2
    # MCMC parameters
    n_walker = 200
    n_sample = 400
    n_burn = 300


    # whether to fix the stellar position to the satspot location
    fix_star = True #False

    # fast mode for model computation
    fast = True
    fast_samp = 3

#    fit_pol = True

    # load GPI image data
    im_dir, labels, im_P_fns, psf_save_fns = load_image_info()
    psf_ims = []
    for psf_save_fn in psf_save_fns:
        psf_im = fits.getdata(psf_save_fn)
        psf_ims.append(psf_im)
    ims_P = []
    for im_P_fn in im_P_fns:
        # get polarization image
        _log.debug("loading %s" % im_P_fn)
        im, hdr = fits.getdata(im_P_fn, header=True)
        im_P = im #n.sqrt(im[1,:,:]**2 + im[2,:,:]**2)
        im_P = ma.masked_where(n.isnan(im_P), im_P)
        im_P = ma.masked_where(im_P > 300, im_P)
        im_P = ma.masked_where(im_P < 0, im_P)
        cen_P = n.array((140., 140.))
#        if ('BUNIT' in hdr) and (hdr['BUNIT'] == 'ADU per coadd'):
#            im_P /= hdr['ITIME'] # [ADU/coadd] -> [ADU/sec]


    lam_data = {'Y' :1.05e-6, # [m]
                'J' :1.22e-6,# FIXME  these are all eyeballed
                'H' :1.65e-6,
                'K1':2.09e-6,
                'K2':2.25e-6,
                }
    D = 7.7701 # [m] telescope diameter
    D *= 9.571/10. # reduction by lyot
    pix_scl = 0.01414 # [arcsec/pix]
    psf_sigs = n.array([lam_data[band]/D*c.as_per_rad/pix_scl for band in labels])

    Comparator = KLIPPolModelComparator



    n_int_parms = 10
    ringmodel = OffsetRingModel(n_int_parms=n_int_parms,
                                init_pol=fit_pol,
                                n_images=len(ims_I),
                                psf_method='inputim',
                                psf_ims = psf_ims
                                )
    args = (ringmodel, cens, w_ss, labels, ims_I)
    kwargs = {'fast':fast,
              'fast_samp':fast_samp,
              }
    args = (im_save_fns, klip_save_fns) + args + (ims_P,)
    mc = Comparator(*args, **kwargs)
    mc.n_step = n_step
    mc.drad0 = drad0
    mc.ringmodel.parms['psf_parms'][:] = psf_sigs
    mc.ringmodel.is_fixed['psf_parms'] = True
    mc.ringmodel.parms['int_parms'][:5] = 5e8
    mc.ringmodel.parms['int_parms'][:] = 7e8
    if fit_pol:
        mc.ringmodel.parms['pol_int_parms'][:5] = 2e8
        mc.ringmodel.parms['pol_int_parms'][5:] = 2e8

        mc.ringmodel.parms['pol_int_parms'][2:4] =5e5
        mc.ringmodel.parms['pol_int_parms'][7:9] = 5e5
        mc.ringmodel.is_fixed['pol_int_parms'] =False
        mc.ringmodel.is_fixed['int_parms'] = True #False
    if fix_star:
        mc.ringmodel.is_fixed['offsx'] = True
        mc.ringmodel.is_fixed['offsy'] = True
    else:
        # small offset for parameter ranges
        mc.ringmodel.parms['offsx'] = 0.01
        mc.ringmodel.parms['offsy'] = 0.01
    mc.ringmodel.parms['offsx'] = 0.004
    mc.ringmodel.parms['offsy'] = -0.014

    mc.ringmodel.is_fixed['r_out'] =False
    mc.ringmodel.parms['r_out'] = mc.ringmodel.parms['r']+5.
    mc.ringmodel.is_fixed['r_in'] =False
#    mc.ringmodel.is_fixed['r'] = True #False
    mc.ringmodel.parms['r_in'] = mc.ringmodel.parms['r']-5.
    mc.ringmodel.parms['gamma_in'] = -20.
    mc.ringmodel.parms['gamma'] = 5.
    mc.ringmodel.parms['beta'] = 0
#    mc.ringmodel.is_fixed['omega'] = False #True
#    mc.ringmodel.is_fixed['Omega'] = False #True
#    mc.ringmodel.is_fixed['offset'] = False #True
#    mc.ringmodel.is_fixed['I'] = False #True
    mc.ringmodel.is_fixed['gamma_in'] = False #True
    mc.ringmodel.is_fixed['gamma'] = False #True
#    mc.ringmodel.is_fixed['beta'] = True #False #True

    mask_rad = 20. # [pix]
    pol_mask_rad = 20. # [pix]
    mc.set_tfims(mask_rad=mask_rad, pol_mask_rad=pol_mask_rad)


    
    # run least-squares
    save_prefix = 'hr4796-fitter-'
    save_fn = save_prefix+'leastsq1.dat'
    if os.access(save_fn, os.R_OK) and not redo_lsq1:
        _log.info('loading first leastsq')
        mc.ringmodel.restore_parms(save_fn)
    else:
        _log.info('running first leastsq')
        mc.do_leastsq()
        mc.ringmodel.save_parms(save_fn)

    if hasattr(mc, 'leastsq_vals'):
        mc.report_leastsq()
    if show:
        
        mc.show_model()

    #assert False # TEMP

    # update stdmap, reduce pol mask size
    mc.update_stdmap()
    mask_rad = 15. # [pix]
    pol_mask_rad = 12. # [pix]
    mc.set_tfims(mask_rad=mask_rad, pol_mask_rad=pol_mask_rad)

    # scale the errors...
    p = mc.get_fit_parms()
    r = mc.get_residuals(p)
    n_data = len(r)
    # scale by pixels per resolution element
    #  dia = 1.22 * lambda / D * (arcsec/rad) / (arcsec/pix)
    # FIXME  this only valid for K1
    n_data /= n.pi * (1.22 * 2.05e-6 / 8. * 2.06e5 / mc.pix_scale / 2.)**2
    n_parm = len(p)
    n_dof = n_data-n_parm
    chi2nu = (r**2).sum()/n_dof
    fact = n.sqrt(chi2nu)
    #fact *= 10 # TEMP
    for i in range(mc.n_images):
        mc.stdims[i] *= fact
        if fit_pol:
            mc.stdims_P[i] *= fact


    # get new intensity parameters
    n_int_parm = 10
    mc.update_int_parms(n_int_parm)

    # fit for slopes
    mc.ringmodel.is_fixed['gamma_in'] = False
    mc.ringmodel.is_fixed['gamma'] = False
    mc.ringmodel.is_fixed['beta'] = False
    mc.ringmodel.parms['beta'] = 1e-3

    # run least squares with updated stdmap
    save_fn = save_prefix+'leastsq2.dat'
    if os.access(save_fn, os.R_OK) and not redo_lsq2:
        _log.info('loading second leastsq')
        mc.ringmodel.restore_parms(save_fn)
    else:
        _log.info('running second leastsq')
        mc.do_leastsq()
        mc.ringmodel.save_parms(save_fn)

    if hasattr(mc, 'leastsq_vals'):
        mc.report_leastsq()
    if show:
        mc.show_model()

    if lsq_only: return

    mc.ringmodel.is_fixed['pol_int_parms'] =False
    mc.ringmodel.is_fixed['int_parms'] = True #False

    mc.ringmodel.is_fixed['r_out'] =False
    mc.ringmodel.parms['r_out'] = mc.ringmodel.parms['r']+5.
    mc.ringmodel.is_fixed['r_in'] =False

    n_int_parm = 20
    mc.update_int_parms(n_int_parm)



    # run MCMC
    save_fn = save_prefix+'mcmc.dat'
    if os.access(save_fn, os.R_OK) and not redo_mcmc:
        _log.info('loading MCMC')
        with open(save_fn) as f:
            mc = pickle.load(f)
    else:
        _log.info('running MCMC')
        mc.run_MCMC(n_walker, n_sample)
        _log.debug('finished MCMC')

        with open(save_fn, 'w') as f:
            pickle.dump(mc, f, 2)
    chain = mc.chain[:,n_burn:,:]
    lnprobability = mc.lnprobability[:,n_burn:]
    n_walker, n_sample, n_parm = chain.shape

    # set model to max-likelihood parameters
    mc.set_fit_parms(chain.reshape(n_walker*n_sample,n_parm)[n.argmax(lnprobability)])
    mc.ringmodel.print_parms()
    if show:
        mc.show_model()

    # MCMC results
    mc.report_mcmc_results(n_burn)
    if show:
        mc.show_mcmc_results(n_burn, mask_rad, pol_mask_rad)

    #ipshell() # TEMP



if __name__=='__main__':
    #test_spline()
    #test_pspl_model()
    #test_pspl_model(test_180=True)
    #test_image_offset()
    #test_image_keplerian()
    #test_psf_model()
    #test_sbpl()

    ## fit_pol = True
    ## #fit_pol = False
    ## #redo = False
    ## redo = True
    ## fit_image(redo=redo, fit_pol=fit_pol)

    fit_images(redo_lsq1=False,
               redo_lsq2=False,
               redo_mcmc=False,
               lsq_only=False,
               show=True,
               )

#    leastsq_plot()
    #make_proposal_figure()


    ## x = 1.
    ## m = 1.+1e-8j
    ## phi_sca, sc_int = get_mie(x, m)
    ## show_mie(phi_sca, sc_int)

    #fit_mie()


    #make_figure_observed_images()
    #make_figure_observed_images(save_fn=None)
    #make_figure_profiles()
    #make_figure_profiles(save_fn=None)
    #make_figure_colors()
    #make_figure_colors(save_fn=None)
    #make_figure_cartoon()
    #make_figure_cartoon(save_fn=None)

    # cleanup
    if pool is not None:
        pool.close()

