import pickle
from hr4796a import KLIPPolModelComparator
from hr4796a import OffsetRingModel
import seaborn as sns
import numpy
import matplotlib.pyplot as plot

def maketrianglecov(cov, labels):
    #cov = covariance matrix
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    f, ax = plot.subplots(figsize=(11, 9))
    sns.heatmap(cov, cmap = cmap, vmin = -.3, vmax = .3, square = True, xticklabels = labels,
                yticklabels = labels, ax = ax)
    plot.show()



a = pickle.load(open('fitter-mc.dat'))
parm = a.get_freemodel_parminfo()
labels = parm[1]
cov = a.leastsq_vals[1]
labels = labels[0:-1]
cov = cov[0:len(labels), 0:len(labels)]
mask =  numpy.tri(cov.shape[0], k=-1)
cov[numpy.where(mask == 1)] = 0


maketrianglecov(cov, labels)


