import pyfits
import glob
import numpy
import constants as const
#import shift
import matplotlib.pyplot as plot
import scipy.ndimage as ndimage
import copy
import instruments.GPI as gpi
import os
import image
import pickle
import image
import multiprocessing as mp
from astropy import wcs

def perform_klip_adi(inputims, pas, cents, basisvals_all = None,coords_all = None, goodinds_all =None, basispattern = None, ind = None):
    """
    Load aligned images from file.  Load KLIP modes from file.
    Perform KLIP-based ADI on aligned images and save the output.
    ims - model images
    nrs - number of radii
    nphis - numpy of phi 
    ims - images aligned to center
    pas - pa of each image
    """
#    plot.imshow(inputims)
#    plot.show()
    if basispattern is None:
        if ind is not None:
            if ind == 0:
                basispattern = '/home/parriaga/hr4796a_red/basis/basisK'
            else:
                basispattern = '/home/parriaga/hr4796a_red/basis/basisJ'
#    ims = numpy.zeros(numpy.append(len(pas), numpy.shape(inputims)))
#    modelsize = numpy.shape(inputims)

    ims = rotate_imgs(numpy.tile(inputims, (len(pas), 1,1)), pas, numpy.tile([[140,140]], (len(pas),1)))
    
    imsh = numpy.shape(ims)
    flatims = numpy.reshape(ims, [imsh[0], imsh[1] * imsh[2]])
    ref_ims = numpy.zeros(flatims.shape, dtype = numpy.float) + numpy.nan    
    for imnum, im in enumerate(ims):
        if basispattern is not None:
            picklename = basispattern + str(imnum) + '.p'
            picklefile = open(picklename)
            basisvals_arr = pickle.load(picklefile)
            coords_arr = pickle.load(picklefile)
            goodinds = pickle.load(picklefile)
        else:
            basisvals_arr = basisvals_all[imnum]
            coords_arr = coords_all[imnum]
            goodinds = goodinds_all[imnum]
        #Looping through r
        for coords_smarr, basisvals_smarr in zip(coords_arr, basisvals_arr):
            #looping through phi
            for coords, basisvals in zip(coords_smarr, basisvals_smarr):
                imdata = flatims[imnum, coords]
                rims = flatims[goodinds[0]]
                rims = flatims[:, coords]
                #basisvals has dimension (N_basis x pixels), imdata has dimension pixels, 
                mim = rims.mean(axis = 0)
                msim = (imdata - mim)#) #.filled(0.)
                a = numpy.dot(msim, basisvals.T)
                rdata = numpy.dot(a, basisvals)
                ref_ims[imnum, coords] = rdata + mim #.filled(0.)
                # image subtraction
    sub_ims = flatims - ref_ims
    sub_ims = numpy.reshape(sub_ims, imsh)
    print numpy.max(sub_ims[0])

    sub_ims = rotate_imgs(sub_ims, -pas, numpy.tile([[140, 140]], (len(pas), 1)))
            # rotate images to sky orientation
    collapse = numpy.mean(sub_ims, axis = 0)
    #   plot.imshow(collapse)
 #   plot.show()
#
    
    return collapse





                        




def load_klip_save_data(ind = None, basispattern = None):
    if basispattern is None:
        assert ind is not None
        if ind == 0:
            basispattern = '/home/parriaga/hr4796a_red/basis/basisK'
        else:
            basispattern = '/home/parriaga/hr4796a_red/basis/basisJ'
    basisvals_all = []
    coords_all = []
    goodinds_all = []
    gl = glob.glob(basispattern + '*')
    nims = len(gl)
    for imnum in range(nims):
        picklename = basispattern + str(imnum) + '.p'
        picklefile = open(picklename)
        basisvals_arr = pickle.load(picklefile)
        coords_arr = pickle.load(picklefile)
        goodinds = pickle.load(picklefile)
        basisvals_all.append(basisvals_arr)
        coords_all.append(coords_arr)
        goodinds_all.append(goodinds)
    return basisvals_all, coords_all, goodinds_all





def getsnr(fname):
    #Read in injected data
#    fname = 'injecttest-KLmodes-all.fits'
    x = pyfits.open(fname)
    z = x[1]
    a = z.header
    w = wcs.WCS(header=a, naxis=[1,2])
    p0 = {'a': 100000.0, 'inc': 70.0, 'pa': 0, 'radius': 20.0}
    pa = -28
    mask = get_observed_model(p0,phi_lims = [0, 2* numpy.pi])
    mask[numpy.where(mask < .84 * numpy.max(mask))] = 0
    mask[numpy.where(mask > 0)] = 1
    modelsize = numpy.shape(mask)
    theta = -28
    #Create canvas
    canvas = numpy.zeros(numpy.shape(z.data[0]))
    canvas[0:modelsize[0], 0:modelsize[1]] = mask
    #Put centered mask. New image will be centered around 140 140
    orthomask = kliprotate(copy.deepcopy(canvas), -28, [modelsize[1] / 2., modelsize[0] / 2.], new_center = [140., 140.] )
    orthomask[numpy.where(numpy.isnan(orthomask))] = 0.
    immask = kliprotate(canvas, 62 , [modelsize[1] / 2., modelsize[0] / 2.], new_center = [140., 140.] )
    immask[numpy.where(numpy.isnan(immask))] = 0.
    



def inject_disk(frames, cents, params, headers, pa,head):
    """
    Bastardized from fakes.py in pyklip and Li-Wei's code yolo
    
    Input: 
        frames - set of images onto which disk will be injected
        cents - center of each image
        params - disk parameters
        headers - fits headers
    Outputs:
        Injected frames written to frames
    """
    model = get_observed_model(params)
    modelsize = numpy.shape(model)
    for frame, cent, hdr in zip(frames, cents, headers):
        # Change position angle into frame of image
        theta = -covert_pa_to_image_polar(pa + 90., hdr)
        #Create canvas
        canvas = numpy.zeros(numpy.shape(frame))
        canvas[0:modelsize[0], 0:modelsize[1]] = model
        #Put centered disk
        canvas = kliprotate(canvas, theta, [modelsize[1] / 2., modelsize[0] / 2.], new_center = [cent[0], cent[1]])
        canvas[numpy.where(numpy.isnan(canvas))] = 0.
        #Rotate model
        frame += canvas


def covert_pa_to_image_polar(pa, astr_hdr):
    """
    Given a parallactic angle (angle from N to Zenith rotating in the Eastward direction), calculate what
    polar angle theta (angle from +X CCW towards +Y) it corresponds to

    Input:
        pa: parallactic angle in degrees
        astr_hdr: wcs astrometry header (astropy.wcs)

    Output:
        theta: polar angle in degrees
    """
    rot_det = astr_hdr.wcs.cd[0,0] * astr_hdr.wcs.cd[1,1] - astr_hdr.wcs.cd[0,1] * astr_hdr.wcs.cd[1,0]
    if rot_det < 0:
        rot_sgn = -1.
    else:
        rot_sgn = 1.
    #calculate CCW rotation from +Y to North in radians
    rot_YN = numpy.arctan2(rot_sgn * astr_hdr.wcs.cd[0,1],rot_sgn * astr_hdr.wcs.cd[0,0])
    #now that we know where north it, find the CCW rotation from +Y to find location of planet
    rot_YPA = rot_YN - rot_sgn*pa*numpy.pi/180. #radians
    #rot_YPA = rot_YN + pa*numpy.pi/180. #radians

    theta = rot_YPA * 180./numpy.pi + 90.0 #degrees
    return theta



def covert_polar_to_image_pa(theta, astr_hdr):
    """
    Reversed engineer from covert_pa_to_image_polar by JB. Actually JB doesn't quite understand how it works...

    Input:
        theta: parallactic angle in degrees
        astr_hdr: wcs astrometry header (astropy.wcs)

    Output:
        theta: polar angle in degrees
    """
    rot_det = astr_hdr.wcs.cd[0,0] * astr_hdr.wcs.cd[1,1] - astr_hdr.wcs.cd[0,1] * astr_hdr.wcs.cd[1,0]
    if rot_det < 0:
        rot_sgn = -1.
    else:
        rot_sgn = 1.
    #calculate CCW rotation from +Y to North in radians
    rot_YN = numpy.arctan2(rot_sgn * astr_hdr.wcs.cd[0,1],rot_sgn * astr_hdr.wcs.cd[0,0])

    rot_YPA = (theta-90)*numpy.pi/180.

    pa = rot_sgn*(rot_YN-rot_YPA)* 180./numpy.pi

    return pa



def makeGaussian(size, fwhm=3, center=None):
    """ Make a square gaussian kernel. size is the length of a side of the 
    square fwhm is full-width-half-maximum, which can be thought of as an 
    effective radius."""
    x = numpy.arange(0, size, 1, float)
    y = x[:,numpy.newaxis]
    if center is None:
        x0 = y0 = size // 2
    else:
        x0 = center[0]
        y0 = center[1]
    return numpy.exp(-4*numpy.log(2) * ((x-x0)**2 + (y-y0)**2) / fwhm**2)



def get_observed_model(parameters, phi_lims = [0., 2. * numpy.pi]):
    
    fpr = 5  #hardcode yolo
    #------  Prepare for the finner sampled PSF  -------#
    normpsf = makeGaussian(141, fwhm=4) #more hardcode yolo
#    normpsf = makeGaussian(141, fwhm=3) #more hardcode yolo
    p0, p1 = normpsf.shape
    zppsf = numpy.zeros((2*p0+2,2*p1+2))                    # zero pad to 2N 
    y0, y1 = zppsf.shape 
    zppsf[(p0+1)/2:(3*p0+1)/2,(p1+1)/2:(3*p1+1)/2] = normpsf

    fftspsf = numpy.fft.fftshift(zppsf)                     # FFT shift 
    fftpsf = numpy.fft.fft2(fftspsf)                        # FFT
    fftspsf2 = numpy.fft.ifftshift(fftpsf)                  # FFT shift back 
    zppsf2 = numpy.zeros((fpr*y0,fpr*y1))+0.j               # zero pad to fpr*N 
    z0, z1 = zppsf2.shape
    f0, f1 = fftspsf2.shape
    zppsf2[(z0-f0)/2:(z0+f0)/2,(z1-f1)/2:(z1+f1)/2] = fftspsf2
    fftspsf3 = numpy.fft.fftshift(zppsf2)                   # FFT shift 

    #------  Prepare for the model  -------#
    im = get_model(parameters, phi_lims = phi_lims)
    i0, i1 = im.shape
    zpmodel = numpy.zeros_like(zppsf2)                      # zero pad to fpr*N
    zpmodel[(z0-1-i0)/2:(z0-1+i0)/2,(z1-1-i1)/2:(z1-1+i1)/2] = im
    fftsim = numpy.fft.fftshift(zpmodel)                    # FFT shift
    fftim = numpy.fft.fft2(fftsim)                          # FFT
        
    #------  Convolution  -------#
    fftprod = fftim*fftspsf3                            # multiply 
    ifft = numpy.fft.ifft2(fftprod)                         # inverse FFT
    ffts = numpy.fft.ifftshift(ifft)                        # FFT shift
    t0, t1 = ffts.shape                                 # extract
    c0, c1 = p0*fpr, p1*fpr                             # extract center
    eim = ffts[c0-1-i0/2:c0+i0/2,c1-1-i0/2:c1+i0/2]
    e0, e1 = eim.shape                                  # rebin
    X, Y = numpy.meshgrid(numpy.arange(e0),numpy.arange(e1))
    X, Y, E = X.flatten(), Y.flatten(), eim.real.flatten()
    rb = numpy.histogram2d(Y,X,bins=e0/fpr,range=[[-0.5,e0-0.5]]*2,weights=E)[0]
    rb_scaled = rb/numpy.max(rb)*parameters['a']
    return rb_scaled


def get_model(parameters, phi_lims = [0., 2. * numpy.pi]):
    fine_power = 5
    distance = 122.7 #pc
    pixscale = .01414 # pix
    r = parameters['radius'] / distance / pixscale  #[pix]
    inc = parameters['inc'] * const.dtor                      #[rad]
    pa = (parameters['pa']+90.) * const.dtor                  #[rad]
    
    s = 4*numpy.ceil(r) + 1                     # output image: s pix by s pix 
    r *= fine_power                    # oversampling
    n_pts = 10000                           # n points in the ring
    center = s*fine_power/2. - 0.5     # ring center
    
    phi = numpy.linspace(phi_lims[0], phi_lims[1], n_pts)   # [rad]
    x = numpy.cos(phi) * r                      # [pix]
    y = numpy.sin(phi) * numpy.cos(inc) * r         # [pix]
    xx = numpy.cos(pa) * x - numpy.sin(pa) * y + center
    yy = numpy.sin(pa) * x + numpy.cos(pa) * y + center
    #E = 1./((1./abs(numpy.cos(inc))-1.)*abs(numpy.cos(phi))+1)
    
    im = numpy.histogram2d(yy,xx,bins=s*fine_power,normed=True, #weights=E,
                       range=[[-0.5,s*fine_power-0.5]]*2)[0]
    im[numpy.where(im>0)] = 1.
    return im


def kliprotate(img, angle, center, new_center=None, flipx=False, astr_hdr=None):
    """
    Rotate an image by the given angle about the given center.
    Optional: can shift the image to a new image center after rotation. Also can reverse x axis for those left
              handed astronomy coordinate systems

    Inputs:
        img: a 2D image
        angle: angle CCW to rotate by (degrees)
        center: 2 element list [x,y] that defines the center to rotate the image to respect to
        new_center: 2 element list [x,y] that defines the new image center after rotation
        flipx: default is True, which reverses x axis.
        astr_hdr: wcs astrometry header for the image
    Outputs:
        resampled_img: new 2D image
    """
    #convert angle to radians
    angle_rad = numpy.radians(angle)

    #create the coordinate system of the image to manipulate for the transform
    dims = img.shape
    x, y = numpy.meshgrid(numpy.arange(dims[1], dtype=numpy.float32), numpy.arange(dims[0], dtype=numpy.float32))

    #if necessary, move coordinates to new center
    if new_center is not None:
        dx = new_center[0] - center[0]
        dy = new_center[1] - center[1]
        x -= dx
        y -= dy

    #flip x if needed to get East left of North
    if flipx is True:
        x = center[0] - (x - center[0])
    
    #do rotation. CW rotation formula to get a CCW of the image
    xp = (x-center[0])*numpy.cos(angle_rad) + (y-center[1])*numpy.sin(angle_rad) + center[0]
    yp = -(x-center[0])*numpy.sin(angle_rad) + (y-center[1])*numpy.cos(angle_rad) + center[1]


    #resample image based on new coordinates
    #scipy uses y,x convention when meshgrid uses x,y
    #stupid scipy functions can't work with masked arrays (NANs)
    #and trying to use interp2d with sparse arrays is way to slow
    #hack my way out of this by picking a really small value for NANs and try to detect them after the interpolation
    #then redo the transformation setting NaN to zero to reduce interpolation effects, but using the mask we derived
    minval = numpy.min([numpy.nanmin(img), 0.0])
    nanpix = numpy.where(numpy.isnan(img))
    medval = numpy.median(img[numpy.where(~numpy.isnan(img))])
    img_copy = numpy.copy(img)
    img_copy[nanpix] = minval * 5.0
    resampled_img_mask = ndimage.map_coordinates(img_copy, [yp, xp], cval=minval * 5.0)
    img_copy[nanpix] = medval
    resampled_img = ndimage.map_coordinates(img_copy, [yp, xp], cval=numpy.nan)
    resampled_img[numpy.where(resampled_img_mask < minval)] = numpy.nan

    #edit the astrometry header if given to compensate for orientation
    if astr_hdr is not None:
        _rotate_wcs_hdr(astr_hdr, angle, flipx=flipx)

    return resampled_img


def rotate_imgs(imgs, angles, centers, new_center=None, numthreads=None, flipx=False, hdrs=None,disable_wcs_rotation = False):
    """
    derotate a sequences of images by their respective angles

    Inputs:
        imgs: array of shape (N,y,x) containing N images
        angles: array of length N with the angle to rotate each frame. Each angle should be CW in degrees.
                (TODO: fix this angle convention)
        centers: array of shape N,2 with the [x,y] center of each frame
        new_centers: a 2-element array with the new center to register each frame. Default is middle of image
        numthreads: number of threads to be used
        flipx: flip the x axis to get a left handed coordinate system (oh astronomers...)
        hdrs: array of N wcs astrometry headers

    Output:
        derotated: array of shape (N,y,x) containing the derotated images
    """

    tpool = mp.Pool(processes=numthreads)

    #klip.rotate(img, -angle, oldcenter, [152,152]) for img, angle, oldcenter
    #multithreading the rotation for each image
    if hdrs is None:
        tasks = [tpool.apply_async(kliprotate, args=(img, angle, center, new_center, flipx, None))
                 for img, angle, center in zip(imgs, angles, centers)]
    else:
        tasks = [tpool.apply_async(kliprotate, args=(img, angle, center, new_center, flipx, None))
                 for img, angle, center in zip(imgs, angles, centers)]

    #reform back into a giant array
    derotated = numpy.array([task.get() for task in tasks])

    tpool.close()

    return derotated

