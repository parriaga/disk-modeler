from hr4796a import OffsetRingModel
from hr4796a import KLIPPolModelComparator
import numpy as n
import matplotlib.pyplot as plot
import pickle



def get_uncertanties(filename = '/home/parriaga/gpi_disks/hr4796a_new/dat_files/hr4796-fitter-jpol-mcmc.dat'):
    mc = pickle.load(open(filename))
    chain = mc.chain
    int_parms = chain[150:,:,-35:] 
    sh = n.shape(int_parms)
    resh_int_parms = n.reshape(int_parms, (sh[0] * sh[1], sh[2]))
    exp_int_parms = n.exp(resh_int_parms)
    uncertainty_parms = n.std(exp_int_parms, axis = 0)
    best_int_parms = mc.ringmodel.parms['pol_int_parms']
    return best_int_parms, uncertainty_parms

from scipy.interpolate import splev
eval_pspl = lambda phi, tck: splev(phi % (2.*n.pi), tck)

def get_tck(params, k=3):
    "get periodic spline parameters"
    n_param = len(params)
        
    x_min, x_max = 0., 2.*n.pi
    dt = (x_max-x_min)/n_param
        
    n_knot = n_param + 2*k + 1
    t = (n.arange(n_knot)-k)*dt
        
    c = n.zeros(n_knot, dtype=n.float)
    c[0:n_param] = params
    c[n_param:n_param+k] = params[0:k]
    return t, c, k


def plot_chains():
    filename = '/home/parriaga/gpi_disks/hr4796a_new/dat_files/hr4796-fitter-kpol-mcmc.dat'
    plot.subplot(311)
    plot.title('K band')
    plot.xlim([200, 350])
    plot.ylim([0, 1.5e9])
    nuarr, parmsarr_k, best_int_parms_k, nu = plot_chain(filename, [15, 20, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250])

    plot.subplot(312)
    plot.xlim([200, 350])
    plot.ylim([0, 1.5e9])

    plot.title('J band')
    filename = '/home/parriaga/gpi_disks/hr4796a_new/dat_files/hr4796-fitter-jpol-mcmc.dat'
    nuarr, parmsarr_j, best_int_parms_j, nu = plot_chain(filename,[15, 20, 30, 40, 50, 60, 70, 80, 90])

    plot.subplot(313)
    plot.title('Ratio')
    plot.xlim([200, 350])
    plot.ylim([.4, 1.5])

#    ratio_parmsarr = n.array(parmsarr_k) / n.array(parmsarr_j)
    ratio_best = n.array(best_int_parms_k) / n.array(best_int_parms_j)
#    plot.hexbin(nuarr, parmsarr, bins = [15, 20, 30, 40, 50, 60, 70, 80, 90], cmap = plot.cm.YlOrBr)

    doall()
    plot.plot(nu * 180. / n.pi, ratio_best, linewidth = 2, c = 'white')
    plot.xlabel('Phase Angle (degrees)')
    plot.show()




def plot_chain(filename, bins = [15, 20, 30, 40, 50, 60, 70, 80, 90] ):
    mc = pickle.load(open(filename))
    chain = mc.chain
    int_parms = chain[195:,:,-35:] 
    sh = n.shape(int_parms)
    resh_int_parms = n.reshape(int_parms, (sh[0] * sh[1], sh[2]))
    exp_int_parms = n.exp(resh_int_parms)
    nu = n.linspace(3., 2. * n.pi, 100.)
    nuarr = n.array([])
    parmsarr = n.array([])
    for parmset in exp_int_parms:
        parms = eval_pspl(nu, get_tck(parmset))
#        plot.plot(nu, parms, c = 'grey')
        nuarr = n.append(nuarr, nu * 180. / n.pi)
        parmsarr = n.append(parmsarr, parms)
        
    plot.hexbin(nuarr, parmsarr, bins = bins, cmap = plot.cm.YlOrBr)

    best_int_parms = mc.ringmodel.parms['pol_int_parms']
    parms = eval_pspl(nu, get_tck(best_int_parms))
    plot.plot(nu * 180. / n.pi, parms, linewidth = 2, c = 'blue')
#    plot.xlabel('Phase angle')
    plot.ylabel('Relative Flux')
    return nuarr, parmsarr, parms, nu
        




def doall():
    filename_j = '/home/parriaga/gpi_disks/hr4796a_new/dat_files/hr4796-fitter-jpol-mcmc.dat'
    filename_k = '/home/parriaga/gpi_disks/hr4796a_new/dat_files/hr4796-fitter-kpol-mcmc.dat'


    from scipy.interpolate import splev
    eval_pspl = lambda phi, tck: splev(phi % (2.*n.pi), tck)

    def get_tck(params, k=3):
        "get periodic spline parameters"
        n_param = len(params)
        
        x_min, x_max = 0., 2.*n.pi
        dt = (x_max-x_min)/n_param
        
        n_knot = n_param + 2*k + 1
        t = (n.arange(n_knot)-k)*dt
        
        c = n.zeros(n_knot, dtype=n.float)
        c[0:n_param] = params
        c[n_param:n_param+k] = params[0:k]
        return t, c, k

    kmc = pickle.load(open(filename_k))
    k_modparms = kmc.ringmodel.parms

    nu2 = n.linspace(3., 2. * n.pi, 100)
    jparms, juncert = get_uncertanties(filename_j)
    jparms = eval_pspl(nu2, get_tck(jparms))
    juncert = eval_pspl(nu2, get_tck(juncert))

    kparms, kuncert = get_uncertanties(filename_k)
    kparms = eval_pspl(nu2, get_tck(kparms))
    kuncert = eval_pspl(nu2, get_tck(kuncert))


    kmc = pickle.load(open(filename_k))
    k_modparms = kmc.ringmodel.parms
    kphi_sca = get_phi_sca(nu2, k_modparms['I'], k_modparms['Omega'], k_modparms['omega'], k_modparms['offset'], k_modparms['r'])
    ratio = kparms / jparms
    ratio_uncertainty = ratio * n.sqrt((kuncert / kparms)**2. + (juncert / jparms)**2.)
    
    tab = n.array([nu2, kphi_sca, jparms, juncert, kparms, kuncert, ratio, ratio_uncertainty]).T
    n.savetxt('4796_pol_intensities.txt', tab, delimiter = ',')


    print ratio


#    plot.subplot(311)
#    plot.title('J pol intensity')
#    plot.fill_between(nu2, jparms - juncert, jparms + juncert)
#    plot.plot(nu2, jparms, c = 'white', linewidth = 2)
#    plot.subplot(312)
#    plot.title('K pol intensity')
#    plot.fill_between(nu2, kparms - kuncert, kparms + kuncert)
#    plot.plot(nu2, kparms, c = 'white', linewidth = 2)
#    plot.subplot(313)
#    plot.title('J / K pol intensity')
    plot.fill_between(nu2 * 180. / n.pi, ratio - ratio_uncertainty, ratio + ratio_uncertainty)
#    print ratio
#    plot.plot(nu2, ratio, c = 'black', linewidth = 2)
#    plot.show()
    


def get_phi_sca(nu, I, Omega, omega, offset,r):
    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]                                             
    oE = offset * (sO*co+cO*so*ci) # [AU]                                             
    oz = offset * so*si # [AU]           
    sonu, conu = n.sin(omega+nu), n.cos(omega+nu)
    
    pN = r * (cO*conu-sO*sonu*ci) # [AU]                                          
    pE = r * (sO*conu+cO*sonu*ci) # [AU]                                          
    pz = r * sonu*si
    
    pN -= oN
    pE -= oE
    pz -= oz
    
    r_star = n.sqrt(pN**2 + pE**2 + pz**2)
    
    # scattering angle                                                            
    phi_sca = n.arccos(pz/r_star) # [rad]                                         
    
    return phi_sca
    
    
