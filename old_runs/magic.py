from diskmodeler import ModelComparator
import numpy as n
import numpy
import pickle

def make_file():
    mc = ModelComparator('hr4796_config_allpol.ini', mc_save = 'mcmc_all_pol_7.p')
    nu, phi = new_phisca_array(mc) 
    nu *= 180. / n.pi
    f = open('phi_tick_values.p', 'wb')

 
    pickle.dump(zip(phi, nu),f)
    f.close()


def new_phisca_array(mc, phi_scas = None):
    r = mc.parms['r']
    offset = mc.parms['offset']
    I = -mc.parms['I']
    omega = mc.parms['omega2']
    Omega = mc.parms['omega1']

    so, co = n.sin(omega), n.cos(omega)
    sO, cO = n.sin(Omega), n.cos(Omega)
    si, ci = n.sin(I), n.cos(I)
    oN = offset * (cO*co-sO*so*ci) # [AU]                                             
    oE = offset * (sO*co+cO*so*ci) # [AU]                                             
    oz = offset * so*si # [AU]                                                        
    def get_NE(nu):
        sonu, conu = n.sin(omega+nu), n.cos(omega+nu)

        pN = r * (cO*conu-sO*sonu*ci) # [AU]                                          
        pE = r * (sO*conu+cO*sonu*ci) # [AU]                                          
        pz = r * sonu*si

        pN -= oN
        pE -= oE
        pz -= oz

        r_star = n.sqrt(pN**2 + pE**2 + pz**2)

        # scattering angle                                                            
        phi_sca = n.arccos(pz/r_star) # [rad]                                         

        return pN, pE, phi_sca


    # calculate ellipse                                                               
    nu = n.linspace(0., 2.*n.pi, 30, endpoint=True)
    pN, pE, phi_sca = get_NE(nu)


    # nus for which theta_sca = min, max                                              
    from scipy.optimize import fmin, brentq
    nus = n.array([fmin(lambda nu: get_NE(nu)[2], 0.)[0], 
                    fmin(lambda nu: -get_NE(nu)[2], 0.)[0]]) % (2.*n.pi)
    nu_min = nus.min()
    nu_max = nus.max()
    print "min", nu_min*180./n.pi, get_NE(nu_min)[2]*180./n.pi                       
    print "max", nu_max*180./n.pi, get_NE(nu_max)[2]*180./n.pi                       

    if phi_scas is None:
        phi_scas = n.arange(13., 160., 7.)
    nuret = []
    phiret = []
    for phi in phi_scas:
        def comp(nu):
            d = (get_NE(nu)[2]-phi*n.pi/180.) % (2.*n.pi)
            if d > n.pi: d -= 2.*n.pi
            return d
        # which nus does this correspond to?
        nu1 = brentq(comp, nu_min, nu_max) #% (2.*n.pi)
        nu2 = brentq(comp, nu_max, nu_min+2.*n.pi) #%# (2.*n.pi)
        nuret.append(nu1)
        nuret.append(nu2)
        phiret.append(phi)
        phiret.append(phi)
        
    return numpy.array(nuret), numpy.array(phiret)
