from diskmodeler import ModelComparator
import diskmodeler
import numpy as n
import matplotlib.pyplot as plot
import matplotlib as mpl
from matplotlib import gridspec




def test():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    diskobj_ind = 0
    nu, curve_set = get_curves(mc, diskobj_ind)
    nu *= 180. / n.pi
    find_peak_stddev(nu, curve_set)



def plotall():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'

    mcj = ModelComparator('hr4796_config_jpol.ini', mc_save = 'mcmc_jpol.p')
    mc = ModelComparator(conf, mc_save = mc_save)

    f, ax = plot.subplots(1)

    curves(mc, 0, ax, color='blue', normalize = False, label = 'K1')
    curves(mcj, 0, ax, color='red', normalize = False, label = 'J')
    curves(mc, 2, ax, color='green', normalize = False, label = 'H')

    ax.set_title('Phase curve')
    nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[0].parametric_model_ring(n_step=2000)
    phi_max_nu = nu[n.where(phi_sca == n.max(phi_sca))]

    ax.vlines(phi_max_nu * 180. / n.pi, 0., 300000, linewidth = 1.2, label = 'Symmetry')
    ax.legend()
    ax.set_xlim([290, 315])
    ax.set_ylim([200000,270000])
    plot.xlabel('True anomaly (degrees)')
    plot.ylabel('Intensity (arbitrary units)')
    plot.show()






def curves(mc, diskobj_ind, ax, color = 'blue', lnprob_cutoff = 100, normalize = True, plotmax = True, label = 'K'):
    nu, best_curve = get_best_prob_curve(mc, diskobj_ind)
    nu, curve_set = get_curves(mc, diskobj_ind)

    nu *= 180. / n.pi 
    maxval = n.max(best_curve)
    wh = n.where(best_curve == maxval)
    numax = nu[wh]
    
    for curve in curve_set:
        if normalize is False:
            ax.plot(nu, curve, color = color, alpha = .01)
        else:
            ax.plot(nu, curve / n.max(curve), color = color, alpha = .01)
    if normalize is True:
        ax.plot(nu, best_curve / n.max(best_curve), linewidth = 1.5, color = color, label = label)
        vmax = 1.3
    else:
        ax.plot(nu, best_curve, linewidth = 2, color = color, label = label)
        vmax = n.max(best_curve) * 1.3
    if plotmax ==True:
        stdpeaks = find_peak_stddev(nu, curve_set)
        rect = mpl.patches.Rectangle((numax - stdpeaks / 2, 0), stdpeaks, vmax, color = color, alpha = .3)
        ax.vlines([numax], 0, vmax, color = color)
        ax.add_patch(rect)

        

def find_peak_stddev(nu, curve_set):
    peaks = []
    for curve in curve_set:
        peaks.append(nu[n.where(curve == n.max(curve))])
    return n.std(peaks)


def test_curvemaps():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    curve_maps(mc, 0)
    


def curve_maps(mc, diskobj_ind, color = 'blue', lnprob_cutoff = 100, label = 'K'):
    numin = 280
    numax = 330
    imin = 160000
    imax = 300000
    nbins = 300

    nu, curve_set = get_curves(mc, diskobj_ind)
    nu *= 180. / n.pi
    wh = n.where((nu > numin) & (nu < numax))
    nu = nu[wh]
    
    im = n.zeros((nbins, nbins))
    nu_binsize = (numax - numin) / nbins
    i_binsize = (imin - imax) / nbins


    nupoints = []
    curvepoints = []
    for curve in curve_set:
        nupoints.append(nu)
        curvepoints.append(curve[wh])
    
    nupoints = n.array(nupoints).flatten()
    curvepoints = n.array(curvepoints).flatten()

    H, xedges, yedges = n.histogram2d(nupoints, curvepoints, bins = [300,70], range=[[numin,numax], [imin, imax]])
    plot.imshow(H.T,cmap = 'Greys')
    plot.show()
    


def fit_gaussian():
    conf = 'hr4796_config_allpol.ini'
    mc_save = 'mcmc_allpol_intparms_test.p'
    mc = ModelComparator(conf, mc_save = mc_save)
    nu, intensity_k = get_best_prob_curve(mc,0)
    nu, intensity_j = get_best_prob_curve(mc,1)
    nu, intensity_h = get_best_prob_curve(mc,2)

    nu *= 180. / n.pi
    wh = n.where((nu > 290) & (nu < 320))
    nu = nu[wh]

    intensity_k = intensity_k[wh] 
    intensity_j = intensity_j[wh]
    intensity_h = intensity_h[wh] 

    from astropy.modeling import models, fitting
    g_init = models.Gaussian1D(amplitude = 300000, mean = 300, stddev = 30)
    fit_g = fitting.LevMarLSQFitter()
    
    g_k = fit_g(g_init, nu, intensity_k)
    g_j = fit_g(g_init, nu, intensity_j)
    g_h = fit_g(g_init, nu, intensity_h)

    print(g_k)
    print(g_h)
    print(g_j)

    plot.plot(nu, intensity_k)
    plot.plot(nu, g_k(nu))

    plot.plot(nu, intensity_j)
    plot.plot(nu, g_j(nu))

    plot.plot(nu, intensity_h)
    plot.plot(nu, g_h(nu))

    plot.show()
    def slope_func(x, slope):
        y = slope * x / 90  + 1
        return y
        

    g_k.amplitude = 250000
    g_j.amplitude = 250000
    g_h.amplitude = 250000

    g_k.amplitude = 300
    g_j.amplitude = 300
    g_h.amplitude = 300


    plot.plot(nu, g_k(nu) * slope_func(nu, 1), label = 'K')
    plot.plot(nu, g_j(nu) * slope_func(nu, 1), label = 'J')
    plot.plot(nu, g_h(nu) * slope_func(nu, 1), label = 'H')
    plot.vlines([300], 0, 1400)
    plot.legend()
    plot.show()



def get_best_prob_curve(mc, diskobj_ind):
    chain = mc.chain
    sh = n.shape(chain)
    best_lnprob = n.where(mc.lnprobability == n.max(mc.lnprobability))
    param_set = chain[best_lnprob[0][1], best_lnprob[1][1], :]
    mc.set_fit_parms(param_set, fit_parmlist_wtf = mc.fit_parmlist)
    nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=4321)
    return nu, intensity
    


def get_curves(mc, diskobj_ind, lnprob_cutoff = 100):
    chain = mc.chain
    sh = n.shape(chain)
    n_burn = sh[1] - 5
    n_walkers = sh[0]
    best_lnprob = n.max(mc.lnprobability)

    phase_curves = []
    for step in range(n_burn, sh[1]):
        for walker in range(n_walkers):
            param_set = chain[walker, step,:]
            if mc.lnprobability[walker, step] > best_lnprob - lnprob_cutoff:
                mc.set_fit_parms(param_set, fit_parmlist_wtf = mc.fit_parmlist)
                nu, r, phi_sca, r_proj, PA_proj,intensity = mc.diskobjs[diskobj_ind].parametric_model_ring(n_step=4321)
                
                phase_curves.append(intensity)
    return nu, phase_curves
